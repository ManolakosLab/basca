function [x,y] = BOGraphVertexToImageCoor(v,m,n)
    [x,y] = ind2sub([m,n],v);
end