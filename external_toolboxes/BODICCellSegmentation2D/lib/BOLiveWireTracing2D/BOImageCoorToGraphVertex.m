function idx = BOImageCoorToGraphVertex(x,y,m,n)
    idx = sub2ind([m,n],x,y);
end