function [DG,Graph,Edges,Vertices,Weights] = ...
    BOImageToGraph8(imE,imSE,imS,imSW,imW,imNW,imN,imNE,x,y)
% rows -> Y; cols -> X;
% o--------- 
% |        Y
% |
% |
%  X

%% Image indexes
[m,n] = size(imE);
if isempty(x) && isempty(y)
    idx = find(ones([m,n]));
else
    idx = sub2ind([m,n],x',y')';
end
%% Index offsets
% East         M
% Southeast    M + 1
% South        1
% Southwest   -M + 1
% West        -M
% Northwest   -M - 1
% North       -1
% Northeast    M - 1
E   =  m;
SE  =  m + 1;
S   =  1;
SW  = -m + 1;
W   = -m;
NW  = -m - 1;
N   = -1;
NE  =  m - 1;
%% Indexes
idxE  = idx + E;
idxSE = idx + SE;
idxS  = idx + S;
idxSW = idx + SW;
idxW  = idx + W;
idxNW = idx + NW;
idxN  = idx + N;
idxNE = idx + NE;
%% Compute the values of all the north, south, ..., neighbors
pE  = [idx, idxE ]; % pairs
pSE = [idx, idxSE];
pS  = [idx, idxS ];
pSW = [idx, idxSW];
pW  = [idx, idxW ];
pNW = [idx, idxNW];
pN  = [idx, idxN ];
pNE = [idx, idxNE];
%% Remove outside of image points
pE  = pE( ismember(idxE, idx),:);
pSE = pSE(ismember(idxSE,idx),:);
pS  = pS( ismember(idxS, idx),:);
pSW = pSW(ismember(idxSW,idx),:);
pW  = pW( ismember(idxW, idx),:);
pNW = pNW(ismember(idxNW,idx),:);
pN  = pN( ismember(idxN, idx),:);
pNE = pNE(ismember(idxNE,idx),:);
%% Create Weights for each of directions
wE  = imE( pE( :,1));
wSE = imSE(pSE(:,1));
wS  = imS( pS( :,1));
wSW = imSW(pSW(:,1));
wW  = imW( pW( :,1));
wNW = imNW(pNW(:,1));
wN  = imN( pN( :,1)); 
wNE = imNE(pNE(:,1));
%% Construct the graph
Weights  = [wE; wSE; wS; wSW; wW; wNW; wN; wNE]';
Edges    = [pE; pSE; pS; pSW; pW; pNW; pN; pNE]';
Graph    = [Edges; double(Weights)];
Vertices = unique([Graph(1,:) Graph(2,:)]);
%% Correction for zeros
Weights(Weights==0) = eps;
%%
DG       = sparse(Edges(1,:),Edges(2,:),double(Weights));
%% End
end