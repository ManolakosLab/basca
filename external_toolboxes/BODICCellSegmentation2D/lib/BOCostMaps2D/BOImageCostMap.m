function C = BOImageCostMap(im)
% rows -> Y/j; cols -> X/i;
% o--------- 
% |        j
% |
% |
%  i
%                   N
% NW |i-1,j-1| - |i-1,j| - |i-1,j+1| NE
% W  |i,  j-1| - |i  ,j| - |i  ,j+1| E
% SW |i+1,j-1| - |i+1,j| - |i+1,j+1| SE
%                   S
%%
[m,n] = size(im);
C = inf*ones(m,n,8);
for i=2:m-1
    for j=2:n-1
        C(i,j,1) = im(i  ,j+1); %E
        C(i,j,2) = im(i+1,j+1); %SE
        C(i,j,3) = im(i+1,j  ); %S
        C(i,j,4) = im(i+1,j-1); %SW
        C(i,j,5) = im(i  ,j-1); %W
        C(i,j,6) = im(i-1,j-1); %NW
        C(i,j,7) = im(i-1,j  ); %N
        C(i,j,8) = im(i-1,j+1); %NE 
    end
end
end