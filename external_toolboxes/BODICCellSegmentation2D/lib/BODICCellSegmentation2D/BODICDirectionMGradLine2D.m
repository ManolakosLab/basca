function [amu,a,v,p] = BODICDirectionMGradLine2D(im,s,nr)
%%  BODICDirectionMGradLine2D - direction of phase in DIC images
%   
%   REFERENCE:
%       SOON :)
%
%   INPUT:
%       im  - image
%       s   - line length
%       nr  - number of rotations
%
%   OUTPUT:
%       amu - DIC direction angle [deg] 
%       a   - angles [deg]
%       v   - sum of gradients for each direction
%       p   - Von Mises function parameters
%
%   USAGE:
%
%   AUTHOR:
%       Boguslaw Obara, http://boguslawobara.net/
%
%   VERSION:
%       0.1 - 14/10/2010 First implementation
%% Find direction of the phase 
a = (0:180/nr:180-180/nr)';
v = zeros(length(a),1);
for i=1:length(a)
    se = strel('line',s,a(i));
    imgrad = imdilate(im,se) - imerode(im,se);
    %v(i) = sum(imgrad(:));
    v(i) = max(imgrad(:));
    %v(i) = mean(imgrad(:));
end
%% Scale data
v = v/max(v(:)); % [0,1]
x = (-pi + 2*pi*(0:180/nr:180-180/nr)/180)'; % [0,180] -> [-pi,pi]
y = v;
%% Fit Von Mises function
ps = rand(1,3);
ps(3) = x(find(y==max(y),1));
options = optimset('Display','off');
p = fminsearch(@BOVonMisesFit,ps,options,x,y);
amu = 180*(p(3) + pi)/(2*pi); % [-pi,pi] -> [0,180]
%% End
end