function [xcl,ycl,dcl,xc,yc,xn,yn,xt,yt,dtef,dtea,dtm,ca,ci,na,ni,xea,yea,nsa,nla] ...
    = BODICCellAnalysis(imc,imn,imthc,imthn)
%%  BODICCellAnalysis - analysis of spatial properties
%   
%   REFERENCE:
%       SOON :)
%
%   INPUT:
%       imc     - cell image
%       imn     - nucleus/cluster image
%       imthc   - segmented cell image
%       imthn   - segmented nucleus/cluster image
%
%   OUTPUT:
%       xcl,ycl - centerline coordinates (ccccccccc)
%       dcl     - centerline distance map from (E) to (e) 
%                  which is the cumulative sum: = + == + === + ==== ...
%       xc,yc   - cell centroid coordinates (#)
%       xn,yn   - nucleus/cluster centroid coordinates (*)
%       xt,yt   - point on the centerline coordinates (t)
%       dtef    - distance form (t) to the furthest (E) or (e)
%       dtea    - distance form (t) to (E) - the arbitary end 
%       dtm     - distance form (t) to (m) - (m) is the middle of the (ccc)
%       ca      - cell area
%       ci      - sum of cell intensity
%       na      - nucleus/cluster area
%       ni      - sum of nucleus/cluster intensity
%       xea,yea - arbitary end point coordinates (E)
%       nsa,nla - nucleus/cluster minor and major axis length
%
%
%      -------------------------------------------------
%    /             _                _                    \
%   |            /   \            /   \                   | 
%   |           |  n  |          |  n  |                  |
%   |            \ : /            \ : /                   |  
%   |              :                :                     |
%   |              :                :                     |
%   e  ccccccccccc t cccccccc m ccc t ccccccccccccccccccc E
%   |                                                     |
%   |                        #                            |
%   |                                                     |
%   |                                                     |
%    \                                                   /
%      -------------------------------------------------
%
%   USAGE:
%
%   AUTHOR:
%       Boguslaw Obara, http://boguslawobara.net/
%
%   VERSION:
%       0.1 - 14/10/2010 First implementation
%
%% Setup
xcl = []; ycl = []; dcl = []; xc = []; yc = []; xn = []; yn = []; 
xt = []; yt = []; dtef = []; dtea = []; dtm = []; ci = []; ni = []; 
ca = []; na = []; xea = []; yea = []; nsa = []; nla = [];
%% Cell intensity
ci = sum(sum(imc(imthc)));
%% Cell Area
ca = sum(imthc(:));
%% Cell centroid
region = regionprops(double(imthc),'Centroid');
cc = uint32(cat(1,region.Centroid));
xc = cc(:,2); yc = cc(:,1);
%% Cell centerline
[xcl,ycl] = BOObjectCenterline2D(imthc);
idxcl = sub2ind(size(imc),xcl,ycl);
%% Cell centerline length
imcl = zeros(size(imthc))==1;
imcl(xcl(1),ycl(1)) = 1;
imdist = bwdist(imcl);
dcl = imdist(idxcl);
dcl = cumsum(abs(diff(dcl))); % distance map for cell centerline 
dcl = [0 dcl]; 
%% Distance: nuclei <-> cell centerline -> nuclei <-> cell centerline end
if ~isempty(find(imthn==1,1))
    %region = regionprops(imthn,'Centroid','PixelIdxList');
    region = regionprops(bwlabel(imthn),'Centroid','PixelIdxList',...
        'MinorAxisLength','MajorAxisLength');
    nla = uint32(cat(1,region.MinorAxisLength));
    nsa = uint32(cat(1,region.MajorAxisLength));
    nc = uint32(cat(1,region.Centroid));
    xn = nc(:,2); yn = nc(:,1);
    xt = zeros(length(xn),1);
    yt = zeros(length(xn),1);
    dtef = zeros(length(xn),1);
    dtea = zeros(length(xn),1);
    dtm = zeros(length(xn),1);
    ni = zeros(length(xn),1);
    idxclt = zeros(length(xc),1);
    for i=1:length(xn);
        imnc = zeros(size(imthc))==1;
        imnc(xn(i),yn(i)) = 1;
        imdist = bwdist(imnc);
        d = imdist(idxcl);
        [mclt,idxclt(i)] = find(d==min(d),1);
        [xt(i),yt(i)] = ind2sub(size(imc),idxcl(idxclt(i)));
        %% Intensity measurements
        ni(i) = sum(imn(region(i).PixelIdxList));
        %% Area
        na(i) = length(region(i).PixelIdxList);
    end
    %% Furthest end
    idxf = zeros(length(xn),1);
    cll = dcl(length(dcl));
    for i=1:length(xn);
        d1 = dcl(idxclt(i));
        d2 = cll - d1;
        [dtef(i),idxf(i)] = max([d1 d2]);
        dtea(i) = d1;
    end
    [m,idx] = max(dtef);
    xea = xcl(1);
    yea = ycl(1);
    if idxf(idx)==2
        dtea = cll - dtea;
        xea = xcl(length(xcl));
        yea = ycl(length(ycl));
    end
    
    dtm = abs(dtea- cll/2);
end
%% End
end