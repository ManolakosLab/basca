function sse = BOVonMisesFit(p,x,y)
%% FIT
yf = p(1)*exp(p(2)*cos(x-p(3)));
%% Error
error = yf - y;
%% Error^2
% When curvefitting, a typical quantity to
% minimize is the sum of squares error
sse = sum(error.^2);
%% End
end