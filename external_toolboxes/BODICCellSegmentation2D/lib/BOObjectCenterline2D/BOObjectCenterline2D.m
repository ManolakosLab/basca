function [xp,yp] = BOObjectCenterline2D(imth)
%% Contour
[x,y] = find(imth==1,1);
c = bwtraceboundary(imth,[x,y],'N',8);
c(length(c),:) = []; % remove last
cn = length(c);
%% Dist
imdist = bwdist(~imth);
imdist = abs(imdist-max(imdist(:)));
% imdist = zeros(size(imth));
% for i=1:cn
%     imdist2 = BODistanceMap(imth,c(i,1),c(i,2));
%     imdist2 = abs(imdist2-max(imdist2(:)));
%     imdist = max(imdist,imdist2);
% end
% imagesc(imdist);
% axis equal; axis tight; axis image; set(gca,'xtick',[]);set(gca,'ytick',[]);
%% Image cost map
C = BOImageCostMap(imdist);
%% Image To Graph
[xsk,ysk] = find(imth); %% Use only skeleton pixels in the graph
[DG,G,E,V,W] = BOImageToGraph8(C(:,:,1),C(:,:,2),C(:,:,3),C(:,:,4),...
                            C(:,:,5),C(:,:,6),C(:,:,7),C(:,:,8),xsk,ysk);
%% Find Shortest Path
[xn,yn] = size(imth); xp = []; yp = []; d0 = 0; D = []; C = [];
%dd0 = 0; xpp = []; ypp = [];
%cn
for i=1:cn
    %i
    %dd0 = 0;
    %imdist2 = BODistanceMap(imth,c(i,1),c(i,2));
    %imagesc(imdist); hold on; hold on; plot(ypp,xpp,'wo'); pause(0.02)
    
    cj = round((i + 3*cn/8):(i + 5*cn/8));
    cj(cj>cn) = cj(cj>cn) - cn;
    %m = 0.9*max(imdist2(:));
    %idx = sub2ind([xn,yn],c(:,1),c(:,2));
    %v = imdist2(idx);
    %cj = find(v>m)';
    %cj = 1:cn;
    for j=cj
        if i~=j
            xs = c(i,1); ys = c(i,2);
            xt = c(j,1); yt = c(j,2);
            %% Construct Graph 
            vs = BOImageCoorToGraphVertex(xs,ys,xn,yn);
            vt = BOImageCoorToGraphVertex(xt,yt,xn,yn);
            %% Run Dijkstra Algorithm
            [d,p,pred] = graphshortestpath(DG,vs,vt);
            %% Convert vertex to x,y
            [x,y] = BOGraphVertexToImageCoor(p,xn,yn);
            d1 = sum(sqrt(diff(x).^2 + diff(y).^2));
            %curv = BOCurv(x,y);
            %d1 = d1*curv;
            %idx = sub2ind([xn,yn],x,y);
            %d2 = sum(diff(imdist2(idx)));
            %d1 = d1 + d2;
            %d1 = d1;
            if d0<=d1
                xp = x; yp = y;
                d0 = d1;
                %D = [D;d1];
                %C = [C;curv];
            end
%             if dd0<d1
%                 xpp = x; ypp = y;
%                 dd0 = d1;
%             end
        end
    end
end  
%% End
end

function curv = BOCurv(x,y)
    curv = 0;
    for i=2:length(x)-1
        curv = curv + (x(i-1)-2*x(i)+x(i+1))^2 + (y(i-1)-2*y(i)+y(i+1))^2;
    end
end
function imdist = BODistanceMap(imth,x,y)
    imdist = zeros(size(imth));
    ims0 = zeros(size(imth))==1;
    ims0(x,y) = 1; d = 1; i = 1;
    while d>0
        imd = bwdist(ims0);
        ims1 = immultiply((imd<=1)&(imd>0),imth);
        d = abs(sum(sum(ims1)));
        ims0 = max(ims0,ims1);
        imdist = max(imdist,i*ims1);
        i = i+1;
    end    
end