function BODICCellSegmentationGUI()
%% Clear
clc; close all; clear all;
%% Path - Libs
addpath('./lib/BOHitOrMissE2D');
addpath('./lib/BOLocalThreshold2D');
addpath('./lib/BOObjectCenterline2D');
addpath('./lib/BOLiveWireTracing2D');
addpath('./lib/BOCostMaps2D');
addpath('./lib/BODICCellSegmentation2D');
%% GUI elements
%% Figure
gui.scxy=get(0,'ScreenSize');
gui.xw=205; gui.yw=950; %720;
%gui.xs=(gui.scxy(1,3)-gui.xw)/2;
%gui.ys=(gui.scxy(1,4)-gui.yw)/2;
gui.xs=10; gui.ys=gui.scxy(1,4)-gui.yw-50;
gui.units='pixels';
gui.figure=figure('Name','DIC','MenuBar','none','Toolbar','none',...
    'Units',gui.units,'NumberTitle','off','Position',[gui.xs gui.ys gui.xw gui.yw],...
    'Resize','off','Color', get(0, 'defaultuicontrolbackgroundcolor'));
%% Progress
gui.tprog=uicontrol('Parent',gui.figure,'Style','text','Units',gui.units,...
    'Position',[10 2 gui.xw-10 20],'String','University of Durham',...
    'ForegroundColor',[0 0 1]);
%% Panel - Segmentation
gui.ps=uipanel('Parent',gui.figure,'Units',gui.units,...
    'Position',[10 25 188 710],'Title','Segmentation');
gui.bload=uicontrol('Parent',gui.ps,'Style','pushbutton','Units',gui.units,...
    'Position',[5 5 175 30],'String','Load Image','Callback',@gui_load);
%% Panel - Select Cluster
gui.pc=uipanel('Parent',gui.ps,'Units',gui.units,...
    'Position',[5 40 175 40],'Title','Select cluster channel');
gui.chcluster1=uicontrol('Parent',gui.pc,'Style','checkbox','Units',gui.units,...
    'Position',[25 2 50 20],'Min',0,'Max',1,'String','1','Value',1,'Callback',@gui_cluster);
gui.chcluster2=uicontrol('Parent',gui.pc,'Style','checkbox','Units',gui.units,...
    'Position',[100 2 50 20],'Min',0,'Max',1,'String','2','Value',0,'Callback',@gui_cluster);
%% Panel - Hilbert 
gui.ph=uipanel('Parent',gui.ps,'Units',gui.units,...
    'Position',[5 85 175 110],'Title','DIC reconstruction');
gui.brunh=uicontrol('Parent',gui.ph,'Style','pushbutton','Units',gui.units,...
    'Position',[5 35 162 30],'String','Hilbert Transform','Callback',@gui_hilbert);
gui.ttheta=uicontrol('Parent',gui.ph,'Style','text','Units',gui.units,...
    'Position',[5 68 40 17],'String','Angle:');
gui.etheta=uicontrol('Parent',gui.ph,'Style','edit','Units',gui.units,...
    'Position',[50 68 50 20],'String','52.5');
gui.chtheta=uicontrol('Parent',gui.ph,'Style','checkbox','Units',gui.units,...
    'Position',[100 68 60 20],'Min',0,'Max',1,'Value',0,'String','Invert','Callback',@gui_angle);

gui.tsea=uicontrol('Parent',gui.ph,'Style','text','Units',gui.units,...
    'Position',[5 8 20 15],'String','SE:');
gui.esea=uicontrol('Parent',gui.ph,'Style','edit','Units',gui.units,...
    'Position',[30 8 40 20],'String','20');
gui.bautoangle=uicontrol('Parent',gui.ph,'Style','pushbutton','Units',gui.units,...
    'Position',[75 5 92 30],'String','Phase Angle','Callback',@gui_autoangle);
%% Panel - TopHat
gui.pf=uipanel('Parent',gui.ps,'Units',gui.units,...
    'Position',[5 200 175 55],'Title','Enhancement - Bacteria');
gui.brunf=uicontrol('Parent',gui.pf,'Style','pushbutton','Units',gui.units,...
    'Position',[75 5 92 30],'String','Top Hat','Callback',@gui_filter);
gui.tse=uicontrol('Parent',gui.pf,'Style','text','Units',gui.units,...
    'Position',[5 8 20 15],'String','SE:');
gui.ese=uicontrol('Parent',gui.pf,'Style','edit','Units',gui.units,...
    'Position',[30 8 40 20],'String','10');
%% Panel - Threshold Bacteria
gui.pth1=uipanel('Parent',gui.ps,'Units',gui.units,...
    'Position',[5 260 175 80],'Title','Threshold - Bacteria');
gui.bthresh1=uicontrol('Parent',gui.pth1,'Style','pushbutton','Units',gui.units,...
    'Position',[5 5 162 30],'String','Auto Threshold','Callback',@gui_threshold1);
gui.ethreshold1=uicontrol('Parent',gui.pth1,'Style','edit','Units',gui.units,...
    'Position',[135 40 35 20],'String','0.0');
gui.sthreshold1=uicontrol('Parent',gui.pth1,'Style','slider','Units',gui.units,...
    'Position',[4 40 130 20],'Min',0,'Max',1,'Value',0,'Callback',@gui_slider1);
%% Panel - Filter
gui.pf2=uipanel('Parent',gui.ps,'Units',gui.units,...
    'Position',[5 345 175 77],'Title','Filter - Bacteria');
gui.brunf2=uicontrol('Parent',gui.pf2,'Style','pushbutton','Units',gui.units,...
    'Position',[5 5 162 30],'String','Filter','Callback',@gui_filter2);
gui.tos2=uicontrol('Parent',gui.pf2,'Style','text','Units',gui.units,...
    'Position',[5 38 40 15],'String','OS:');
gui.eos2=uicontrol('Parent',gui.pf2,'Style','edit','Units',gui.units,...
    'Position',[40 38 40 20],'String','2');
gui.chborder2=uicontrol('Parent',gui.pf2,'Style','checkbox','Units',gui.units,...
    'Position',[95 38 60 20],'Min',0,'Max',1,'String','Border','Value',1);
%% Panel - Threshold Clusters
gui.pth2=uipanel('Parent',gui.ps,'Units',gui.units,...
    'Position',[5 425 175 150],'Title','Threshold - Cluster');
gui.bthresh2=uicontrol('Parent',gui.pth2,'Style','pushbutton','Units',gui.units,...
    'Position',[5 5 162 30],'String','Global Threshold','Callback',@gui_threshold2);
gui.ethreshold2=uicontrol('Parent',gui.pth2,'Style','edit','Units',gui.units,...
    'Position',[135 40 35 20],'String','0.0');
gui.sthreshold2=uicontrol('Parent',gui.pth2,'Style','slider','Units',gui.units,...
    'Position',[4 40 130 20],'Min',0,'Max',1,'Value',0,'Callback',@gui_slider2);
gui.plocalth2=uicontrol('Parent',gui.pth2,'Style','popup','Units',gui.units,...
    'Position',[5 115 80 15],'Min',0,'Max',1,...
    'String', 'mean|median|midgrey|niblack|bernsen|sauvola',...
    'Value',1,'Callback',@gui_localthreshold2);
gui.elocalth2=uicontrol('Parent',gui.pth2,'Style','edit','Units',gui.units,...
    'Position',[90 105 80 20],'Min',0,'Max',1,...
    'horizontalalignment','left','String', '15,-40');
gui.bthresh2=uicontrol('Parent',gui.pth2,'Style','pushbutton','Units',gui.units,...
    'Position',[5 70 162 30],'String','Local Threshold','Callback',@gui_threshold3);
%%  Panel - Filter
gui.pf3=uipanel('Parent',gui.ps,'Units',gui.units,...
    'Position',[5 580 175 77],'Title','Filter - Cluster');
gui.brunf3=uicontrol('Parent',gui.pf3,'Style','pushbutton','Units',gui.units,...
    'Position',[5 5 162 30],'String','Filter','Callback',@gui_filter3);
gui.tos3=uicontrol('Parent',gui.pf3,'Style','text','Units',gui.units,...
    'Position',[5 38 40 15],'String','OS:');
gui.eos3=uicontrol('Parent',gui.pf3,'Style','edit','Units',gui.units,...
    'Position',[40 38 40 20],'String','2');
gui.chborder3=uicontrol('Parent',gui.pf3,'Style','checkbox','Units',gui.units,...
    'Position',[95 38 60 20],'Min',0,'Max',1,'String','Border','Value',1);
%% Save
gui.bsave=uicontrol('Parent',gui.ps,'Style','pushbutton','Units',gui.units,...
    'Position',[5 660 175 30],'String','Save Image','Callback',@gui_save);
%% Panel - Review
gui.pr=uipanel('Parent',gui.figure,'Units',gui.units,...
    'Position',[10 740 188 85],'Title','Postprocessing');
% %% Load Review
% gui.bload=uicontrol('Parent',gui.pr,'Style','pushbutton','Units',gui.units,...
%    'Position',[5 5 175 30],'String','Load Image','Callback',@gui_loadr);
%% Review
gui.breview=uicontrol('Parent',gui.pr,'Style','pushbutton','Units',gui.units,...
   'Position',[5 5 175 30],'String','Review','Callback',@gui_review);
%% Save
gui.bsaver=uicontrol('Parent',gui.pr,'Style','pushbutton','Units',gui.units,...
   'Position',[5 35 175 30],'String','Save Image','Callback',@gui_saver);
%% Panel - Analysis
gui.pa=uipanel('Parent',gui.figure,'Units',gui.units,...
    'Position',[10 835 188 115],'Title','Analysis');
%% Load Analysis
gui.bloada1=uicontrol('Parent',gui.pa,'Style','pushbutton','Units',gui.units,...
   'Position',[5 5 85 30],'String','Load Image','Callback',@gui_load);
gui.bloada2=uicontrol('Parent',gui.pa,'Style','pushbutton','Units',gui.units,...
   'Position',[95 5 85 30],'String','Load Segm','Callback',@gui_loads);
%% Review
gui.banalysis=uicontrol('Parent',gui.pa,'Style','pushbutton','Units',gui.units,...
   'Position',[5 35 175 30],'String','Analysis','Callback',@gui_analysis);
%% Save
gui.bsavea=uicontrol('Parent',gui.pa,'Style','pushbutton','Units',gui.units,...
   'Position',[5 65 175 30],'String','Save Analysis','Callback',@gui_savea);
%% ------------------------------------------------------------------------
%% ------------------------------------------------------------------------
%% ------------------------------------------------------------------------
gui.lastpath    = './images/';
gui.hfigim      = [];
gui.hfigth      = [];
gui.hfigh       = [];
gui.hfigf       = [];
gui.hfigreview  = [];
gui.hfigana     = [];
gui.hfiga       = [];
gui.exb         = 60;
gui.index1      = [];
gui.D           = []; 
gui.ch          = 1;
gui.im1         = [];
gui.im2         = [];
gui.im1th       = [];
gui.im2th       = [];
gui.im1h        = [];
gui.im1f        = [];
gui.im2th       = logical(gui.im2th);
%% Get Parameters
function gui_getparam()
    gui.theta   = str2double(get(gui.etheta,'String'));     
    gui.se      = str2double(get(gui.ese,'String'));
    gui.sea     = str2double(get(gui.esea,'String'));
    gui.os2     = str2double(get(gui.eos2,'String'));    
    gui.os3     = str2double(get(gui.eos3,'String'));    
    gui.theta   = pi*gui.theta/180;
    gui.border2 = get(gui.chborder2,'Value');
    gui.border3 = get(gui.chborder3,'Value');
end
%% Angle Auto
function gui_autoangle(obj,event)
    if isempty(gui.im1); warndlg('Please load the image!','!! Warning !!'); return; end        
    set(gui.tprog,'String','Hilbert Transform ...'); pause(.000001);      
    gui_getparam();
    
    nr = 180;
    [amu,a,v,p] = BODICDirectionMGradLine2D(gui.im1,gui.sea,nr);
    set(gui.etheta,'String',num2str(amu,3));

    %Von Mises PDF
    x = (-pi + 2*pi*(0:180/nr:180-180/nr)/180)'; % [0,180] -> [-pi,pi]
    yf = p(1)*exp(p(2)*cos(x-p(3)));
    x = (0:180/nr:180-180/nr)'; % [0,180] -> [-pi,pi]
    y = v;
    v(find(v==max(v))) = yf(find(v==max(v)));    
    
    gui.hfiga = find_figure(gui.hfiga,'Auto Angle');
    clf(gui.hfiga,'reset');
    plot(x,y,'ko'); hold on; plot(x,yf,'k-','LineWidth',2);
    %xlim([-pi,pi]); 
    %xlim([0,180]); 
    %ylim([0.5,1.02]); 
    l = legend('gradient data','von Mises distribution');
    xlabel('Angle [deg]'); ylabel('Intensity');   
    
    set(gui.tprog,'String',''); pause(.000001);   
end
%% Hilbert angle + 180deg
function gui_angle(obj,event)
    a = str2double(get(gui.etheta,'String')); 
    a = mod(a+180,360);
    set(gui.etheta,'String',num2str(a));
end
%% Local Threshold
function gui_localthreshold2(obj,event) 
    v = get(obj,'Value');
    if v==1
        set(gui.elocalth2,'String','15,0');
    elseif v==2
        set(gui.elocalth2,'String','15,0');
    elseif v==3
        set(gui.elocalth2,'String','15,0');
    elseif v==4
        set(gui.elocalth2,'String','15,-0.2');
    elseif v==5
        set(gui.elocalth2,'String','15,15');
    elseif v==6
        set(gui.elocalth2,'String','15,0.5,128');
    end        
end
%% Channel
function gui_cluster(obj,event)
    set(gui.chcluster1,'Value',0);    
    set(gui.chcluster2,'Value',0);    
    gui.ch = str2double(get(obj,'String'));
    set(obj,'Value',1)
         
    if isempty(gui.im2); warndlg('Please load the image!','!! Warning !!'); return; end  
end
%% Save
function gui_savea(obj,event)
    if isempty(gui.D); warndlg('Please run analysis!','!! Warning !!'); return; end        
    f = [];
        
    [pathstr,name,ext] = fileparts(fullfile(gui.pathname,gui.filename));
    filename = [name '_analysis_' num2str(gui.ch) '.txt'];
    [filename, pathname] = uiputfile('*.txt','Save Classification',fullfile(pathstr, filename));
    if isequal(filename,0) || isequal(pathname,0)
        disp('User selected Cancel')
    else
        f = fullfile(pathname, filename);
    end
    
    if ~isempty(f)
        pause(0.1);
        set(gui.tprog,'String','Saving ...'); pause(.000001); 
      
        fid = fopen(f,'w');
        fprintf(fid,'Cx\t Cy\t Cl\t Ca\t Ci\t xt\t yt\t Nx\t Ny\t Dfe\t Dae\t Dm\t Na\t Ni\t Nsa\t Nla\n');

        D = gui.D;
        for i=1:size(D,2) 
            fprintf(fid,'%6d\t %6d\t %6.2f\t %6d\t %6.2f\n',D(i).xc,D(i).yc,max(D(i).dcl),D(i).ca,D(i).ci);
            for j=1:size(D(i).xn,1) %2
               fprintf(fid,'\t \t \t \t \t %6d\t %6d\t %6d\t %6d\t %6.2f\t %6.2f\t %6.2f\t %6d\t %6.2f\t %6.2f\t %6.2f\n',...
               D(i).xt(j),D(i).yt(j),D(i).xn(j),D(i).yn(j),...
               D(i).dtef(j),D(i).dtea(j),D(i).dtm(j),D(i).na(j),D(i).ni(j),D(i).nsa(j),D(i).nla(j));
            end
            %%fprintf(fid,'\n');
        end
        fclose(fid);
        set(gui.tprog,'String',''); pause(.000001); 
        
        filename1 = [name '_cl.tif'];
        filename2 = [name '_c.tif'];
        imcl = zeros(gui.xs,gui.ys)==1;
        imc = zeros(ui.xs,gui.ys)==1;
        for i=1:size(D,2) 
            for j=1:length(D(i).ycl) 
                imcl(D(i).xcl(j),D(i).ycl(j)) = 1;
            end
        end
        for i=1:size(D,2) 
            imc(D(i).xc,D(i).yc) = 1;
        end
        imwrite(imcl,fullfile(pathstr, filename1),'Compression','none');
        imwrite(imc,fullfile(pathstr, filename2),'Compression','none');
    end
end
%% Analysis
function gui_analysis(obj,event)
    set(gui.tprog,'String','Analysis ...'); pause(.000001); 
   
    label = bwlabel(gui.im1ths);
    imthn = gui.im2th(:,:,gui.ch);
    nr = max(label(:));
    D = struct([]);
    for i=1:max(label(:))    
        set(gui.tprog,'String',['Analysis: ' num2str(i) ' of ' num2str(nr)]); pause(.000001); 
        imthc = label==i;
        imthnm = immultiply(imthn,imthc);
        imthnm = imreconstruct(imthnm,imthn);

        stats = regionprops(imthc|imthnm,'BoundingBox');
        r = cat(1,stats.BoundingBox);
        x1 = ceil(r(2))-1; x2 = ceil(r(2)) + floor(r(4)) + 1;
        y1 = ceil(r(1))-1; y2 = ceil(r(1)) + floor(r(3)) + 1;
        if x1<1; x1 = 1; end
        if y1<1; y1 = 1; end
        if x2>size(gui.im1,1); x2 = size(gui.im1,1); end
        if y2>size(gui.im1,2); y2 = size(gui.im1,2); end
        [D(i).xcl,D(i).ycl,D(i).dcl,D(i).xc,D(i).yc,D(i).xn,D(i).yn,...
            D(i).xt,D(i).yt,D(i).dtef,D(i).dtea,D(i).dtm,D(i).ca,D(i).ci,D(i).na,D(i).ni,...
            D(i).xea,D(i).yea,D(i).nsa,D(i).nla] =...
            BODICCellAnalysis(gui.im1(x1:x2,y1:y2),...
            gui.im2(x1:x2,y1:y2,gui.ch),imthc(x1:x2,y1:y2),imthnm(x1:x2,y1:y2));

        D(i).xcl = D(i).xcl + x1 - 1;
        D(i).ycl = D(i).ycl + y1 - 1;
        D(i).xc = D(i).xc + x1 - 1;
        D(i).yc = D(i).yc + y1 - 1;
        D(i).xn = D(i).xn + x1 - 1;
        D(i).yn = D(i).yn + y1 - 1;
        D(i).xt = D(i).xt + x1 - 1;
        D(i).yt = D(i).yt + y1 - 1;
        D(i).xea = D(i).xea + x1 - 1;
        D(i).yea = D(i).yea + y1 - 1;
    end    
    
    imrgb = rgb_image(gui.im1,gui.im1ths,gui.im2th(:,:,gui.ch),[]);

    gui.hfigana = find_figure(gui.hfigana,'Analysis');
    imagesc(imrgb); 
    set(gca,'xtick',[]); set(gca,'ytick',[]); axis equal; axis tight;    
    for i=1:size(D,2)    
        hold on; plot(D(i).ycl,D(i).xcl,'-b');
        hold on; plot(D(i).yn,D(i).xn,'*y');
        hold on; plot(D(i).yt,D(i).xt,'*m');
        hold on; plot(D(i).yc,D(i).xc,'oy');
        hold on; plot(D(i).yea,D(i).xea,'xg');           
    end
    gui.D = D;
    set(gui.tprog,'String',''); pause(.000001);     
end
%% Mouse events
function gui_mouse(obj,event)
    pt = get(obj, 'CurrentPoint');
    x = pt(1,1);
    y = pt(1,2);
    x = int32(x);
    y = int32(y);
    x = x + int32(gui.r(1));
    y = y + int32(gui.r(2));
    if gui.label(y,x) ~= 0
        gui.index1 = gui.label(y,x);
        if ~isempty(gui.index1)|| gui.index1~=gui.index          
            
            if ~isempty(gui.im1f)
                imrgb1 = rgb_image(gui.im1f,gui.label>0,gui.label==gui.index,gui.label==gui.index1);
            else
                imrgb1 = rgb_image(gui.im1f,gui.label>0,gui.label==gui.index,gui.label==gui.index1);
            end
            imrgb2 = rgb_image(gui.im1,gui.label>0,gui.label==gui.index,gui.label==gui.index1);            
            
            r = uint32(gui.region(gui.index).BoundingBox);
            r(1) = max(r(1)-gui.exb,1);
            r(2) = max(r(2)-gui.exb,1);
            r(3) = min(r(1)+r(3)+2*gui.exb,gui.ys) - r(1); %% x->y; y->x
            r(4) = min(r(2)+r(4)+2*gui.exb,gui.xs) - r(2);     
            gui.r = r;
            
            gui.hfigreview = find_figure(gui.hfigreview,'Review');    
            subplot(1,2,2);  
            imagesc(imrgb2(r(2):r(2)+r(4)-1,r(1):r(1)+r(3)-1,:)); 
            axis equal; axis tight; axis image; 
            set(gca,'xtick',[]);set(gca,'ytick',[]);
            title(['Number of cells: ' num2str(gui.index) ' of ' num2str(gui.maxindex)])            
            
            subplot(1,2,1);  
            imagesc(imrgb1(r(2):r(2)+r(4)-1,r(1):r(1)+r(3)-1,:)); 
            axis equal; axis tight; axis image;
            set(gca,'xtick',[]);set(gca,'ytick',[]);
            title(['Number of cells: ' num2str(gui.index) ' of ' num2str(gui.maxindex)])
                        
            set(findobj(gca,'type','image'),'hittest','off');
            set(gca,'ButtonDownFcn',@gui_mouse);
            set(gcf,'KeyPressFcn',@gui_key);   
        end
    end
    set(findobj(gca,'type','image'),'hittest','off')
    set(gca,'ButtonDownFcn',@gui_mouse)
end
%% Key
function gui_key(obj,event)
    if strcmp(event.Key,'space') || ...
       strcmp(event.Key,'rightarrow') || ...
       strcmp(event.Key,'leftarrow') || ...
       strcmp(event.Key,'uparrow')
        if strcmp(event.Key,'rightarrow')
           gui.index = gui.index + 1;
            if gui.index  > gui.maxindex
                gui.index = gui.maxindex;
            end
        end
        if strcmp(event.Key,'leftarrow')
           gui.index = gui.index - 1;
            if gui.index < 1
                gui.index = 1;
            end
        end
        if strcmp(event.Key,'space')
            gui.index = gui.index + 1;
            if gui.index > gui.maxindex
                gui.index = gui.maxindex;
            end
        end
        if strcmp(event.Key,'uparrow')
            if gui.index < 1
                gui.index = 1;
            end
            if gui.index > gui.maxindex
                gui.index = gui.maxindex;
            end
        end
           
        %imrgb1 = rgb_image(gui.im1f,gui.label>0,gui.label==gui.index,[]);
        if ~isempty(gui.im1f)
            imrgb1 = rgb_image(gui.im1f,gui.label>0,gui.label==gui.index,[]);
        else
            imrgb1 = rgb_image(gui.im1,gui.label>0,gui.label==gui.index,[]);
        end        
        imrgb2 = rgb_image(gui.im1,gui.label>0,gui.label==gui.index,[]);        
        
        r = uint32(gui.region(gui.index).BoundingBox);
        r(1) = max(r(1)-gui.exb,1);
        r(2) = max(r(2)-gui.exb,1);
        r(3) = min(r(1)+r(3)+2*gui.exb,gui.ys) - r(1); %% x->y; y->x
        r(4) = min(r(2)+r(4)+2*gui.exb,gui.xs) - r(2);        
        gui.r = r;
        
        gui.hfigreview = find_figure(gui.hfigreview,'Review');  
        subplot(1,2,2);
        imagesc(imrgb2(r(2):r(2)+r(4)-1,r(1):r(1)+r(3)-1,:)); 
        axis equal; axis tight; axis image; 
        set(gca,'xtick',[]);set(gca,'ytick',[]);
        title(['Number of cells: ' num2str(gui.index) ' of ' num2str(gui.maxindex)])
        
        subplot(1,2,1);        
        imagesc(imrgb1(r(2):r(2)+r(4)-1,r(1):r(1)+r(3)-1,:)); 
        axis equal; axis tight; axis image;
        set(gca,'xtick',[]);set(gca,'ytick',[]);
        title(['Number of cells: ' num2str(gui.index) ' of ' num2str(gui.maxindex)])
        
        set(findobj(gca,'type','image'),'hittest','off');
        set(gca,'ButtonDownFcn',@gui_mouse);
        set(gcf,'KeyPressFcn',@gui_key);  
    else
        if strcmp(event.Character,'.') || strcmp(event.Key,'delete')
            gui.label(gui.label==gui.index) = 0;
            gui.label(gui.label>gui.index) = gui.label(gui.label>gui.index) - 1;
            gui.region  = regionprops(gui.label, 'BoundingBox');
            gui.maxindex = max(gui.label(:));
            event.Key = 'uparrow';
            gui_key(obj,event);
        elseif strcmp(event.Character,'m')
            if ~isempty(gui.index1) && gui.index~=gui.index1
                imt = max(gui.label==gui.index,gui.label==gui.index1);
                se = strel('disk',1);
                imd = imdilate(imt,se);
                ime = imerode(imd,se);
                indexmin = min(gui.index,gui.index1);
                indexmax = max(gui.index,gui.index1);
                gui.label(ime) = indexmin;
                gui.label(gui.label>indexmax) = gui.label(gui.label>indexmax) - 1;
                gui.region  = regionprops(gui.label, 'BoundingBox');
                gui.maxindex = max(gui.label(:));
                gui.index = indexmin;
                event.Key = 'uparrow';
                gui_key(obj,event);
            end
            gui.index1 = [];
        elseif strcmp(event.Character,'s')
            imt = gui.label==gui.index;
            D = bwdist(~imt);
            D = -D;
            D(~imt) = -Inf;
            L = watershed(D);
            %% OLD
            %imts = immultiply(imt,imcomplement(L==0));
            %se = strel('disk',1);
            %imc = imopen(imts,se);
            %imts = imreconstruct(imc,imts);
            %% NEW
            stats  = regionprops(bwlabel(imt),'Area');
            idx = find([stats.Area] == max([stats.Area]));
            L(L==idx) = -1;
            imts = L>0;
            %imts = immultiply(imt,imcomplement(imp));          
            
            gui.label(imt) = 0;
            gui.label(imts) = 1;            
            gui.label = bwlabel(gui.label); 
            gui.region  = regionprops(gui.label, 'BoundingBox');
            gui.maxindex = max(gui.label(:));            
            s = gui.label(imt);
            gui.index = min(s(s>0)) ;
            event.Key = 'uparrow';
            gui_key(obj,event);
        elseif strcmp(event.Character,'f')
            imt = gui.label==gui.index;
            imt = imfill(imt,'holes');

            gui.label(imt) = 1;            
            gui.label = bwlabel(gui.label); 
            gui.region  = regionprops(gui.label, 'BoundingBox');
            gui.maxindex = max(gui.label(:));            
            s = gui.label(imt);
            gui.index = min(s(s>0)) ;
            event.Key = 'uparrow';
            gui_key(obj,event);
        elseif strcmp(event.Character,'d')
            imt = gui.label==gui.index;
            se = strel('disk',1);
            imt = imdilate(imt,se);

            gui.label(imt) = 1;            
            gui.label = bwlabel(gui.label); 
            gui.region  = regionprops(gui.label, 'BoundingBox');
            gui.maxindex = max(gui.label(:));            
            s = gui.label(imt);
            gui.index = min(s(s>0)) ;
            event.Key = 'uparrow';
            gui_key(obj,event);
        elseif strcmp(event.Character,'e')
            imt = gui.label==gui.index;
            gui.label(imt) = 0;            
            
            se = strel('disk',1);
            imt = imerode(imt,se);

            gui.label(imt) = 1;            
            gui.label = bwlabel(gui.label); 
            gui.region  = regionprops(gui.label, 'BoundingBox');
            gui.maxindex = max(gui.label(:));            
            s = gui.label(imt);
            gui.index = min(s(s>0)) ;
            event.Key = 'uparrow';
            gui_key(obj,event);            
        end          
    end
end
%% Split
function gui_review(obj,event)
    if isempty(gui.im1th)
        warndlg('Please segment the image!','!! Warning !!'); return; 
    end     
    if strcmp(get(obj,'String'),'Review');
        set(obj,'String','Done');
        set(gui.tprog,'String','Review ...'); pause(.000001); 
   
        gui.index = 1;
        gui.label = bwlabel(gui.im1ths);
        gui.region  = regionprops(gui.label, 'BoundingBox');
        gui.maxindex = max(gui.label(:));
        
        %imrgb1 = rgb_image(gui.im1f,gui.label>0,gui.label==gui.index,[]);
        if ~isempty(gui.im1f)
            imrgb1 = rgb_image(gui.im1f,gui.label>0,gui.label==gui.index,[]);
        else
            imrgb1 = rgb_image(gui.im1,gui.label>0,gui.label==gui.index,[]);
        end
        imrgb2 = rgb_image(gui.im1,gui.label>0,gui.label==gui.index,[]);

        r = uint32(gui.region(gui.index).BoundingBox);
        r(1) = max(r(1)-gui.exb,1);
        r(2) = max(r(2)-gui.exb,1);
        r(3) = min(r(1)+r(3)+2*gui.exb,gui.ys) - r(1); %% x->y; y->x
        r(4) = min(r(2)+r(4)+2*gui.exb,gui.xs) - r(2);             
        gui.r = r;
        
        gui.hfigreview = find_figure(gui.hfigreview,'Review');    
        subplot(1,2,2); 
        imagesc(imrgb2(r(2):r(2)+r(4)-1,r(1):r(1)+r(3)-1,:)); 
        axis equal; axis tight; axis image; 
        set(gca,'xtick',[]);set(gca,'ytick',[]);
        title(['Number of cells: ' num2str(gui.index) ' of ' num2str(gui.maxindex)])
        
        subplot(1,2,1);
        imagesc(imrgb1(r(2):r(2)+r(4)-1,r(1):r(1)+r(3)-1,:)); 
        axis equal; axis tight; axis image; 
        set(gca,'xtick',[]);set(gca,'ytick',[]);
        title(['Number of cells: ' num2str(gui.index) ' of ' num2str(gui.maxindex)])

        set(findobj(gca,'type','image'),'hittest','off');
        set(gca,'ButtonDownFcn',@gui_mouse);
        set(gcf,'KeyPressFcn',@gui_key);
    else    
        set(obj,'String','Review');

        gui.im1ths = gui.label>0;

        %imrgb1 = rgb_image(gui.im1f,gui.im1ths,[],[]);
        if ~isempty(gui.im1f)
            imrgb1 = rgb_image(gui.im1f,gui.im1ths,[],[]);
        else
            imrgb1 = rgb_image(gui.im1,gui.im1ths,[],[]);
        end
        imrgb2 = rgb_image(gui.im1,gui.im1ths,[],[]);

        
        gui.hfigreview = find_figure(gui.hfigreview,'Review');    
        subplot(1,2,2); imagesc(imrgb2); 
        axis equal; axis tight; axis image; 
        set(gca,'xtick',[]);set(gca,'ytick',[]);
        
        subplot(1,2,1); imagesc(imrgb1); 
        axis equal; axis tight; axis image; 
        set(gca,'xtick',[]);set(gca,'ytick',[]);        
        set(gui.tprog,'String',''); pause(.000001);    
    end
end
%% Plot
function plot_b(im,imth,c)
    [B,L] = bwboundaries(imth);
    imagesc(im), hold on;
    set(gca,'xtick',[]);set(gca,'ytick',[]); axis equal; axis tight;
    for k = 1:length(B)
        boundary = B{k};
        plot(boundary(:,2), boundary(:,1), c, 'LineWidth', 2)
    end
    hold off;
end
%% Run slider2
function gui_slider2(obj,event)
    if isempty(gui.im2); warndlg('Please load the image!','!! Warning !!'); return; end           
    set(gui.tprog,'String','Manual threshold ...'); pause(.000001);      
    gui.level2 = get(obj,'Value');
    set(gui.ethreshold2,'String',gui.level2);    
    
    gui.im2th(:,:,gui.ch) = gui.im2(:,:,gui.ch) > gui.level2;
    gui.hfigth = find_figure(gui.hfigth,'Segmentation');
    if gui.ch==1
        subplot(1,3,2); imagesc(gui.im2th(:,:,gui.ch)); colormap gray;
        set(gca,'xtick',[]);set(gca,'ytick',[]); axis equal; axis tight;
         title('Cluster1');
    else
        subplot(1,3,3); imagesc(gui.im2th(:,:,gui.ch)); colormap gray;
        set(gca,'xtick',[]);set(gca,'ytick',[]); axis equal; axis tight;
        title('Cluster2');
    end 
    
    set(gui.tprog,'String',''); pause(.000001);    
end
%% Run slider1
function gui_slider1(obj,event)
    if isempty(gui.im1f); warndlg('Please enhance the image!','!! Warning !!'); return; end        
    set(gui.tprog,'String','Manual threshold ...'); pause(.000001);      
    gui.level1 = get(obj,'Value');
    set(gui.ethreshold1,'String',gui.level1);    
    
    gui.im1th = gui.im1f > gui.level1;
    gui.hfigth = find_figure(gui.hfigth,'Segmentation');
    subplot(1,3,1); imagesc(gui.im1th); colormap gray;
    set(gca,'xtick',[]);set(gca,'ytick',[]); axis equal; axis tight;
    title('Bacteria');        

    set(gui.tprog,'String',''); pause(.000001);    
    
    gui.im1ths = gui.im1th;
end
%% Run auto-threshold - Bacteria
function gui_threshold1(obj,event)
    if isempty(gui.im1f); warndlg('Please enhance the image!','!! Warning !!'); return; end        
    set(gui.tprog,'String','Auto threshold ...'); pause(.000001);      
    
    gui.level1 = graythresh(gui.im1f);
    gui.im1th = gui.im1f> gui.level1;
    
    gui.hfigth = find_figure(gui.hfigth,'Segmentation');
    subplot(1,3,1); imagesc(gui.im1th); colormap gray;
    set(gca,'xtick',[]);set(gca,'ytick',[]); axis equal; axis tight;
    title('Bacteria');

    set(gui.sthreshold1,'Value',gui.level1);
    set(gui.ethreshold1,'String',gui.level1);
    
    set(gui.tprog,'String',''); pause(.000001); 
    gui.im1ths = gui.im1th;
end
%% Run auto-threshold - Cluster
function gui_threshold2(obj,event)
    if isempty(gui.im2); warndlg('Please load the image!','!! Warning !!'); return; end        
    set(gui.tprog,'String','Auto threshold ...'); pause(.000001);      
    
    gui.level2 = graythresh(gui.im2(:,:,gui.ch));
    gui.im2th(:,:,gui.ch) = gui.im2(:,:,gui.ch)> gui.level2;
    
    gui.hfigth = find_figure(gui.hfigth,'Segmentation');
    if gui.ch==1
        subplot(1,3,2); imagesc(gui.im2th(:,:,gui.ch)); colormap gray;
        set(gca,'xtick',[]);set(gca,'ytick',[]); axis equal; axis tight;
         title('Cluster1');
    else
        subplot(1,3,3); imagesc(gui.im2th(:,:,gui.ch)); colormap gray;
        set(gca,'xtick',[]);set(gca,'ytick',[]); axis equal; axis tight;
        title('Cluster2');
    end 

    set(gui.sthreshold2,'Value',gui.level2);
    set(gui.ethreshold2,'String',gui.level2);

    set(gui.tprog,'String',''); pause(.000001); 
end
%% Run local-threshold - Cluster
function gui_threshold3(obj,event)
    if isempty(gui.im2); warndlg('Please load the image!','!! Warning !!'); return; end        
    set(gui.tprog,'String','Auto threshold ...'); pause(.000001);      
    
    
    m = get(gui.plocalth2,'Value');
    
    im = im2uint8(gui.im2(:,:,gui.ch));
    s = get(gui.elocalth2,'String');
    s = regexp(s,',','split');
    if m==1
        n = str2double(s(1)); c = str2double(s(2));
        gui.im2th(:,:,gui.ch) = BOMeanThreshold2D(im,n,c);
    elseif m==2
        n = str2double(s(1)); c = str2double(s(2));        
        gui.im2th(:,:,gui.ch) = BOMedianThreshold2D(im,n,c);
    elseif m==3
        n = str2double(s(1)); c = str2double(s(2));        
        gui.im2th(:,:,gui.ch) = BOMidGreyThreshold2D(im,n,c);
    elseif m==4
        n = str2double(s(1)); k = str2double(s(2));        
        gui.im2th(:,:,gui.ch) = BONiblackThreshold2D(im,n,k);
    elseif m==5
        n = str2double(s(1)); c = str2double(s(2));        
        gui.im2th(:,:,gui.ch) = BOBernsenThreshold2D(im,n,c);
    elseif m==6
        n = str2double(s(1)); k = str2double(s(2)); R = str2double(s(3));          
        gui.im2th(:,:,gui.ch) = BOSauvolaThreshold2D(im,n,k,R);
    end

    gui.hfigth = find_figure(gui.hfigth,'Segmentation');
    if gui.ch==1
        subplot(1,3,2); imagesc(gui.im2th(:,:,gui.ch)); colormap gray;
        set(gca,'xtick',[]);set(gca,'ytick',[]); axis equal; axis tight;
         title('Cluster1');
    else
        subplot(1,3,3); imagesc(gui.im2th(:,:,gui.ch)); colormap gray;
        set(gca,'xtick',[]);set(gca,'ytick',[]); axis equal; axis tight;
        title('Cluster2');
    end 

    set(gui.tprog,'String',''); pause(.000001); 
end
%% Filter3
function gui_filter3(obj,event)
    if isempty(gui.im1h); warndlg('Please segment the DIC image!','!! Warning !!'); return; end        
    set(gui.tprog,'String','Filter ...'); pause(.000001);      
    gui_getparam();
    
    imlabel = bwlabel(gui.im2th(:,:,gui.ch)); 
    stats  = regionprops(imlabel,'Area');
    idx = find([stats.Area] > gui.os3);
    gui.im2th(:,:,gui.ch) = ismember(imlabel,idx);
    
    if gui.border3==1
       gui.im2th(:,:,gui.ch) = imclearborder(gui.im2th(:,:,gui.ch),8);
    end
    
    gui.hfigth = find_figure(gui.hfigth,'Segmentation');
    if gui.ch==1
        subplot(1,3,2); imagesc(gui.im2th(:,:,gui.ch)); colormap gray;
        set(gca,'xtick',[]);set(gca,'ytick',[]); axis equal; axis tight;
         title('Cluster1');
    else
        subplot(1,3,3); imagesc(gui.im2th(:,:,gui.ch)); colormap gray;
        set(gca,'xtick',[]);set(gca,'ytick',[]); axis equal; axis tight;
        title('Cluster2');
    end 
    
    set(gui.tprog,'String',''); pause(.000001);
    gui.im1ths = gui.im1th;
end
%% Filter2
function gui_filter2(obj,event)
    if isempty(gui.im1h); warndlg('Please segment the DIC image!','!! Warning !!'); return; end        
    set(gui.tprog,'String','Filter ...'); pause(.000001);      
    gui_getparam();
    
    imlabel = bwlabel(gui.im1th); 
    stats  = regionprops(imlabel,'Area');
    idx = find([stats.Area] > gui.os2);
    gui.im1th = ismember(imlabel,idx);
    
    if gui.border2==1
       gui.im1th = imclearborder(gui.im1th,8);
    end
    
    gui.hfigth = find_figure(gui.hfigth,'Segmentation');
    subplot(1,3,1); imagesc(gui.im1th); colormap gray;
    set(gca,'xtick',[]);set(gca,'ytick',[]); axis equal; axis tight;
    title('Bacteria');
    
    set(gui.tprog,'String',''); pause(.000001);
    gui.im1ths = gui.im1th;
end
%% Filter
function gui_filter(obj,event)
    if isempty(gui.im1h); warndlg('Please reconstruct the DIC image!','!! Warning !!'); return; end        
    set(gui.tprog,'String','Top Hat ...'); pause(.000001);      
    gui_getparam();
    
    se = strel('disk',gui.se);
    gui.im1f = imtophat(gui.im1h,se);
    gui.im1f = double(gui.im1f); 
    gui.im1f = (gui.im1f - min(gui.im1f(:))) / (max(gui.im1f(:)) - min(gui.im1f(:)));
    
    gui.hfigf = find_figure(gui.hfigf,'Filter');
    imagesc(gui.im1f); colormap gray;
    set(gca,'xtick',[]);set(gca,'ytick',[]); axis equal; axis tight;

    set(gui.tprog,'String',''); pause(.000001);
end
%% Hilbert Transform
function gui_hilbert(obj,event)
    if isempty(gui.im1); warndlg('Please load the image!','!! Warning !!'); return; end        
    set(gui.tprog,'String','Hilbert Transform ...'); pause(.000001);      
    gui_getparam();
    
    %theta = pi/2 + pi*(8-1)/24;    
    gui.theta = gui.theta + pi/2;
    [gui.im1h] = BOHilbertTransform2D(gui.im1,gui.theta);
    gui.im1h = real(gui.im1h);
    
    gui.hfigh = find_figure(gui.hfigh,'Hilbert');
    imagesc(gui.im1h); colormap jet;
    set(gca,'xtick',[]);set(gca,'ytick',[]); axis equal; axis tight;

    set(gui.tprog,'String',''); pause(.000001);
end
%% Save
function gui_saver(obj,event)
    if isempty(gui.im1th); warndlg('Segment the image!','!! Warning !!'); return; end        
    if isempty(gui.im2th); warndlg('Segment the image!','!! Warning !!'); return; end        
    if ~isempty(gui.im2th); 
        if size(gui.im2th,3)<2
            warndlg('Segment the second cluster image!','!! Warning !!'); 
            return;
        end
    end
    set(gui.tprog,'String','Save image ...'); pause(.000001);  
    
    [pathstr,name,ext] = fileparts(fullfile(gui.pathname,gui.filename));
    filename = [name '_segm_review.tif'];
     
    [filename, pathname] = uiputfile({'*.tif','All Image Files';...
          '*.*','All Files' },'Save Image',fullfile(pathstr,filename));
    if isequal(filename,0)
        disp('User selected Cancel')
    else
        imwrite(gui.im1ths,fullfile(pathname,filename),'tif','Compression','none');
        imwrite(gui.im2th(:,:,1),fullfile(pathname,filename),'tif','Compression','none','WriteMode','append');
        imwrite(gui.im2th(:,:,2),fullfile(pathname,filename),'tif','Compression','none','WriteMode','append');
    end
    set(gui.tprog,'String',''); pause(.000001);   
end
%% Save
function gui_save(obj,event)
    if isempty(gui.im1th); warndlg('Segment the image!','!! Warning !!'); return; end        
    if isempty(gui.im2th); warndlg('Segment the image!','!! Warning !!'); return; end        
    if ~isempty(gui.im2th); 
        if size(gui.im2th,3)<2
            warndlg('Segment the second cluster image!','!! Warning !!'); 
            return;
        end
    end
    set(gui.tprog,'String','Save image ...'); pause(.000001);  
    
    [pathstr,name,ext] = fileparts(fullfile(gui.pathname,gui.filename));
    filename = [name '_segm.tif'];

     
    [filename, pathname] = uiputfile({'*.tif','All Image Files';...
          '*.*','All Files' },'Save Image',fullfile(pathstr,filename));
    if isequal(filename,0)
        disp('User selected Cancel')
    else
        imwrite(gui.im1th,fullfile(pathname,filename),'tif','Compression','none');
        imwrite(gui.im2th(:,:,1),fullfile(pathname,filename),'tif','Compression','none','WriteMode','append');
        imwrite(gui.im2th(:,:,2),fullfile(pathname,filename),'tif','Compression','none','WriteMode','append');
    end
    set(gui.tprog,'String',''); pause(.000001);   
end
%% Load Review
function gui_loads(obj,event)
    set(gui.tprog,'String','Load image ...'); pause(.000001);  
    [gui.filename,gui.pathname] =...
        uigetfile({'*.tif'},'Select the image',gui.lastpath);
    if isequal(gui.filename,0)
        disp('User selected Cancel')
    else
        if ~isequal(fullfile(gui.pathname,gui.filename),0)
            pause(0.1);
            gui.im2th = [];
            gui.im2th = logical(gui.im2th);
            gui.im1th = imread(fullfile(gui.pathname,gui.filename),1);
            gui.im2th(:,:,1) = imread(fullfile(gui.pathname,gui.filename),2);
            gui.im2th(:,:,2) = imread(fullfile(gui.pathname,gui.filename),3);            
            gui.im1ths = gui.im1th;
            clear im;
            [gui.xs,gui.ys] = size(gui.im1th);
        end
        if ~isempty(gui.im1th) && ~isempty(gui.im2th)
            gui.hfigth = find_figure(gui.hfigth,'Segmentation');
            subplot(1,3,1); imagesc(gui.im1th); colormap gray;
            set(gca,'xtick',[]);set(gca,'ytick',[]); axis equal; axis tight;
            title('Bacteria');
            
            subplot(1,3,2); imagesc(gui.im2th(:,:,1)); colormap gray;
            set(gca,'xtick',[]);set(gca,'ytick',[]); axis equal; axis tight;
            title('Cluster1');
            
            subplot(1,3,3); imagesc(gui.im2th(:,:,2)); colormap gray;
            set(gca,'xtick',[]);set(gca,'ytick',[]); axis equal; axis tight;            
            title('Cluster2');
        end        
    end
    set(gui.tprog,'String',''); pause(.000001);   
end
%% Load
function gui_load(obj,event)
    set(gui.tprog,'String','Load image ...'); pause(.000001);  
    [gui.filename,gui.pathname] =...
        uigetfile({'*.tif'},'Select the image',gui.lastpath);
    if isequal(gui.filename,0)
        disp('User selected Cancel')
    else
        if ~isequal(fullfile(gui.pathname,gui.filename),0)
            del_figs('DIC');
            gui.lastpath = gui.pathname;
            
            gui.im1th       = [];
            gui.im2th       = [];
            gui.im1h        = [];
            gui.im1f        = [];
            gui.im1th       = logical(gui.im1th);
            gui.im2th       = logical(gui.im2th);
            
            pause(0.1);
            info = imfinfo(fullfile(gui.pathname,gui.filename));
            im = imread(fullfile(gui.pathname,gui.filename));
            im1 = double(im(:,:,1));
            im2 = double(im(:,:,2));
            im3 = double(im(:,:,3));
            
            im1 = (im1 - min(im1(:))) / (max(im1(:)) - min(im1(:)));
            im2 = (im2 - min(im2(:))) / (max(im2(:)) - min(im2(:)));
            im3 = (im3 - min(im3(:))) / (max(im3(:)) - min(im3(:)));
            gui.im1 = im1;
            gui.im2 = [];
            gui.im2(:,:,1) = im2;
            gui.im2(:,:,2) = im3;           

            clear im im1 im2 im3;            
            [gui.xs,gui.ys] = size(gui.im1);
        end
        if ~isempty(gui.im1) && ~isempty(gui.im2)
            gui.hfigim = find_figure(gui.hfigim,'Image');
            subplot(1,3,1); imagesc(gui.im1); colormap gray;
            set(gca,'xtick',[]);set(gca,'ytick',[]); axis equal; axis tight;
            title('Bacteria');

            subplot(1,3,2); imagesc(gui.im2(:,:,1)); colormap gray;
            set(gca,'xtick',[]);set(gca,'ytick',[]); axis equal; axis tight;
            title('Claster1');

            subplot(1,3,3); imagesc(gui.im2(:,:,2)); colormap gray;
            set(gca,'xtick',[]);set(gca,'ytick',[]); axis equal; axis tight;
            title('Claster2');
        end        
    end
    set(gui.tprog,'String',''); pause(.000001);   
end
%% Find Figure
function h = find_figure(h,name)
    if isempty(findobj('type','figure','name',name))
        h = figure('Name',name,'NumberTitle','off'); 
    else
        h = figure(h);
        set(h ,'Name',name,'NumberTitle','off'); 
    end
end
%% Make RGB Image
function imrgb = rgb_image(im,imth1,imth2,imth3)
    imr = im;
    img = im;
    imb = im;
    imr(bwperim(imth1)) = 0;
    img(bwperim(imth1)) = 158/255;
    imb(bwperim(imth1)) = 115/255;
    if ~isempty(imth2)
        imr(bwperim(imth2)) = 240/255;
        img(bwperim(imth2)) = 228/255;
        imb(bwperim(imth2)) = 66/255;
    end
    if ~isempty(imth3)
        imr(bwperim(imth3)) = 0;
        img(bwperim(imth3)) = 1;
        imb(bwperim(imth3)) = 0;
    end
    imrgb(:,:,1) = imr;
    imrgb(:,:,2) = img;
    imrgb(:,:,3) = imb;  
end
%% Delete all figures
function del_figs(name)
    all_figs = findobj(0,'type','figure');
    figs2keep = findobj('type','figure','name',name);
    delete(setdiff(all_figs,figs2keep));
end
end