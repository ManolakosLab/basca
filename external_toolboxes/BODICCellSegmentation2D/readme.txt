***************************************************************************
*  AUTHOR: Boguslaw Obara, http://boguslawobara.net/
***************************************************************************
*
*  README for MATLAB package for DIC cell analysis
*
***************************************************************************
*
* Common files
*   BODICCellSegmentationGUI  - main file
*   ./lib                     - libraries
*   readme                    - this file!
*
***************************************************************************
*
*  Analysis
*       Cx,Cy   - cell centroid coordinates (#)
*       Cl      - cell length
*       Ca      - cell area
*       Ci      - sum of cell intensity
*       Nx,Ny   - nucleus/cluster centroid coordinates (*)
*       xt,yt   - point on the centerline coordinates (t)
*       Dfe     - distance form (t) to the furthest (E) or (e)
*       Dae     - distance form (t) to (E) - the arbitary end 
*       Dm      - distance form (t) to (m) - (m) is the middle of the (ccc)
*       Ni      - nucleus/cluster area
*       Ni      - sum of nucleus/cluster intensity
*       Nsa,Nla - nucleus/cluster minor and major axis length
*
*
*      -------------------------------------------------
*    /             _                _                    \
*   |            /   \            /   \                   |
*   |           |  n  |          |  n  |                  |
*   |            \ : /            \ : /                   |
*   |              :                :                     |
*   |              :                :                     |
*   e  ccccccccccc t cccccccc m ccc t ccccccccccccccccccc E
*   |                                                     |
*   |                        #                            |
*   |                                                     |
*   |                                                     |
*    \                                                   /
*      -------------------------------------------------
*
***************************************************************************
*
* Local threshold functions
*   Mean
*       n       - window size 
*       c       - constant value
*   MidGrey
*       n       - window size 
*       c       - constant value
*   Median
*       n       - window size 
*       c       - constant value
*   Niblack
*       n       - window size 
*       k       - weight value
*   Bernsen
*       n       - window size 
*       c       - constant value
*   Sauvola:
*       n       - window size
*       k       - weight value
*       R       - dynamic range of the standard deviation
*
***************************************************************************
*
* Review procedure
*   Space, <--, -->     - move from cell to cell
*   S                   - split the cell
*   M                   - merge the cell part with the next cell part
*                         (use mouse to select closest part)
*   D                   - dilate the cell by 1 pixel
*   E                   - erode the cell by 1 pixel
*   F                   - fill holes inside the cell
*   Delete              - remove cell/noise
*
***************************************************************************
