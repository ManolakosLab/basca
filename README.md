These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

## Prerequisites

In order to use BaSCA you need to have the MATLAB software with the following toolboxes: 

```
Signal Processing Toolbox, Image Processing Toolbox, Statistics and Machine Learning Toolbox, Curve Fitting Toolbox, Wavelet Toolbox, Bioinformatics Toolbox, Parallel Computing Toolbox (not necessary).
```

## Download BaSCA

There are two options for downloading the code:

* Simple option: zipped archive Navigate to GitLab/Code Click the green button on the right 'Clone or Download' and then select 'Download Zip'.
* Advanced option: git clone The benefit of cloning from git instead of downloading the zip file is that you can easily obtain the new version of the code if there are improvements or bug fixes later on. In order to clone the git repository:
* Open a terminal window
* Navigate to the directory where you would like the code to reside
* Run the following command in terminal: git clone https://gitlab.com/ManolakosLab/basca.git
* Every time you want to update the software you need to navigate to the BaSCA directory and run the following command in terminal: git pull

## Installing

In order for MATLAB to be able to find the different pieces of the code, the BaSCA folder needs to be in your path.
In the Home tab, in the Environment section, click Set Path. The Set Path dialog box appears.
Click Add with subfolders and add the BaSCA folder.

## BaSCA Wiki

For more details on BaSCA architecture, running and examples see:

1. [Home](https://gitlab.com/ManolakosLab/basca/-/wikis/1.-Home)
2. [Getting Started with BaSCA](https://gitlab.com/ManolakosLab/basca/-/wikis/2.-Getting-Started-with-BaSCA)
3. [BaSCA Functionality Overview](https://gitlab.com/ManolakosLab/basca/-/wikis/3.-BaSCA-Functionality-Overview)
4. [Cell Instances and Cells](https://gitlab.com/ManolakosLab/basca/-/wikis/4.-Cell-Instances-and-Cells)
5. [Running Segmentation and Tracking in BaSCA](https://gitlab.com/ManolakosLab/basca/-/wikis/5.--Running-Segmentation-and-Tracking-in-BaSCA)
6. [Automatic and Manual Correction of BaSCA Output](https://gitlab.com/ManolakosLab/basca/-/wikis/6.-Automatic-and-Manual-Correction-of-BaSCA-Output)
7. [BaSCA Output](https://gitlab.com/ManolakosLab/basca/-/wikis/7.-BaSCA-Output)
8. [Case study](https://gitlab.com/ManolakosLab/basca/-/wikis/8.-Case-study)

## Online documentation

For segmentaion and tracking functions see [BaSCA documentation](https://manolakoslab.gitlab.io/basca/BaSCA/BaSCA.html).

For analytics functions see [BaSCA analytics documentation](https://manolakoslab.gitlab.io/basca/BaSCA/analytics/index.html).

## Contributed Packages

BaSCA has dependencies on a number of other matlab packages available online:

* Local Adaptive Thresholding: https://www.mathworks.com/matlabcentral/fileexchange/8647-local-adaptive-thresholding 
* Line Simplification: https://www.mathworks.com/matlabcentral/fileexchange/21132-line-simplification
* cbrewer : colorbrewer schemes for Matlab: https://www.mathworks.com/matlabcentral/fileexchange/34087-cbrewer-colorbrewer-schemes-for-matlab
* export_fig: https://www.mathworks.com/matlabcentral/fileexchange/23629-export_fig
* Multivariate Copula Analysis Toolbox (MvCAT): https://www.mathworks.com/matlabcentral/fileexchange/69217-multivariate-copula-analysis-toolbox-mvcat
* Βetter Skeletonization: https://www.mathworks.com/matlabcentral/fileexchange/11123-better-skeletonization
* XTICKLABEL_ROTATE: https://www.mathworks.com/matlabcentral/fileexchange/3486-xticklabel_rotate
* Contourlet toolbox: https://www.mathworks.com/matlabcentral/fileexchange/8837-contourlet-toolbox
* Tree data structure as a MATLAB class: https://www.mathworks.com/matlabcentral/fileexchange/35623-tree-data-structure-as-a-matlab-class
* extrema.m, extrema2.m: https://www.mathworks.com/matlabcentral/fileexchange/12275-extrema-m-extrema2-m
* BODICCellSegmentation2D – Toolbox to Automatically Extract Bacterial Cells from DIC Microscopy Images: 
  https://community.dur.ac.uk/boguslaw.obara/software/BODICCellSegmentation2D.zip 
* arclength: https://www.mathworks.com/matlabcentral/fileexchange/34871-arclength
* Find angles: https://www.mathworks.com/matlabcentral/fileexchange/37389-find-angles
* Unsupervised learning of finite mixture models: http://www.lx.it.pt/~mtf/mixturecode2.zip


## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

**Athanasios D. Balomenos** - *Initial work* - [abalomenos](https://gitlab.com/abalomenos)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Publications

* Aspridou Z, Balomenos AD, Tsakanikas P, Manolakos E, Koutsoumanis K. Heterogeneity of single cell inactivation: Assessment 
  of the individual cell time to death and implications in population behavior. Food Microbiology. 2019;80:85-92. doi: 
  https://doi.org/10.1016/j.fm.2018.12.011
* Balomenos AD, Stefanou V, Manolakos ES. Bacterial Image Analysis and Single-Cell Analytics to Decipher the Behavior of Large 
  Microbial Communities. In Proceedings of the 25th IEEE International Conference on Image Processing (ICIP); Athens; Oct. 7- 
  10 2018;2436-2440. doi: https://doi.org/10.1109/ICIP.2018.8451137
* Balomenos AD and Manolakos ES, Reconstructing the forest of lineage trees of diverse bacterial communities using bio- 
  inspired image analysis. In Proceedings of the 25th European Signal Processing Conference (EUSIPCO); Kos; 28 Aug.-2 Sep. 
  2017;1887-1891. doi: https://doi.org/10.23919/EUSIPCO.2017.8081537
* Balomenos AD, Tsakanikas P, Aspridou Z, Tampakaki AP, Koutsoumanis KP, Manolakos ES. Image analysis driven single-cell 
  analytics for systems microbiology. BMC Syst Biol. 2017;11(1):43. doi: https://doi.org/10.1186/s12918-017-0399-z
* Balomenos AD, Tsakanikas P, Manolakos ES. Tracking single-cells in overcrowded bacterial colonies. In Proceedings of the 
  37th Annual International Conference of the IEEE Engineering in Medicine and Biology Society (EMBC); Milan; 25-29 Aug. 
  2015:6473-6476. doi: https://doi.org/10.1109/EMBC.2015.7319875

## Youtube Channel
See more on [BaSCA youtube channel](https://www.youtube.com/channel/UCynFJOFj3kKYBFtsVrDTKPg/videos).

## Contact

If you have questions / comments on BaSCA please contact us:

Prof. Elias S. Manolakos

Information Technologies in Medicine and Biology \
Dept. of Informatics and Telecommunications \
University of Athens

eliasm@di.uoa.gr


## Acknowledgments

* Dr. Tsakanikas, Laboratory of Food Microbiology and Biotechnology \
  Dept. of Food Science and Human Nutrition \
  Agricultural University of Athens
* Prof. Koutsoumanis, Head of Laboratory of Food Microbiology and Hygiene \
  Dept. of Food Science and Technology \
  Faculty of Agriculture, Aristotle University of Thessaloniki
* [Onassis Foundation](https://www.onassis.org/)

