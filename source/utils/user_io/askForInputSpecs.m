function [configuration, error_message] = askForInputSpecs(configuration)
% Asks user input for time-lapse movie specifications.
%
% [params, error_message] = askForInputSpecs(configuration)
%
% INPUT :
%  configuration : a struct containing the user's input. 
%
% OUTPUT :
%  configuration : configuration struct with the new fields concerning the
%  specifications of the time-lapse movie. The new fields are:
%  -microscopyType : type of microscopy for the input dataset. 
%
%  -pixelSize: spatial calibration factor of the experiment�s microscope 
%   used to convert the size of an image object from pixels to physical
%   units (�m), i.e. the "size" of pixels.
%
%  -timeSlice (in time units) : time interval between frames of the input 
%   time-lapse movie.
% 
%  -timeUnits : time units of the timeSlice.
% 
%  -boundaryWidth (in pixels) : the width of the colony's boundary. 
%   BaSCA uses this parameter to distinct cell instants that lie on the 
%   boundary of a colony from the other cell instants lying within colony
%   boundary.
%
%  error_message : a char-array containing the error message if the user's 
%  input is incorrect, otherwise it is empty.
%
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
% 
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.
error_message = [];
dlg_title = 'User''s input for the input dataset specifications';
prompt = {    
    'microscopyType: '
    'pixelSize (or calFactor)(in microns): '
    'timeSlice (in time units): '
    'timeUnits: '
    'boundaryWidth (in pixels): '
    };

default_answers = {'phase','0.125', '5', 'min.', '15'};
dims = [1 60];
answers = inputdlg(prompt, dlg_title, dims, default_answers);
%checking user's input
if size(answers) == 0
    error_message = sprintf('The program execution was canceled by the user!\n');
    return;    
else
    input = str2double(answers);

    if sum(strcmp(answers{1}, {'phase', 'bright', 'fluo', 'DIC'})) == 0
        error_message = 'Error in input: <microscopyType> must be in one of {''phase'', ''bright'', ''fluo'', ''DIC''}.\n';
        return;          
    end
    if sum((input(2) < 0)) ~= 0 || sum(input(2) > 1) ~= 0 || sum(isnan(input(2))) ~= 0
        error_message = 'Error in input: <pixelSize> must be in [0, 1].\n';
        return;        
    end
    if sum(input(3) < 0) ~= 0 || sum(isnan(input(3))) ~= 0
        error_message = 'Error in input: <timeSlice> must be positive numbers.\n';
        return;
    end
    if ~isnan(input(4))
        error_message = 'Error in input: <timeUnits> must be char array.\n';
        return;        
    end     
    if sum((input(5) < 0)) ~= 0 || sum(ceil(input(5)) ~= input(5)) ~= 0 || sum(isnan(input(5))) ~= 0
        error_message = 'Error in input: <boundaryWidth> must be in a positive integer.\n';
        return;        
    end 
    configuration(1).microscopyType = answers{1};   
    configuration(1).pixelSize = input(2);
    configuration(1).timeSlice = input(3);
    configuration(1).timeUnits = answers{4};
    configuration(1).boundaryWidth = input(5);    
end