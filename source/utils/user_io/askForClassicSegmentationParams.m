function [configuration, error_message] = askForClassicSegmentationParams(configuration)
% Asks user for the segmentation parameters. 
% 
% [configuration, error_message] = askForSegmentationParams(configuration)
%
% INPUT :
%  configuration : a struct containing the user's input. 
%
% OUTPUT :
%  configuration : configuration struct with the new fields concerning the
%  segmentation parameters. The new fields are:
%  -minAngle [0, 180]: the minimum value allowed for the angle formed
%   between the main axes of two objects so as to be considered as a single
%   object (i.e. cell instant). Choose values between 135 and 180 degrees
%   for regular-shaped bacteria (i.e. not filaments).
% 
%  -maxEccentricity [0, 1] : the maximum value allowed for an object's 
%   eccentricity so as to be considered as a cell instant. Circular objects
%   have eccentricity close to 1, i.e. not bacteria. Usually, values 0.5 to
%   0.65 work well.
% 
%  -minArea (pixels) : the minimum value allowed for an object's area so as
%   to be considered as a cell instant. This parameter depends on the width
%   (in microns) of the examined species and the pixel size (calibration
%   factor) of the time-lapse movie.
% 
%  -minDivisionRatio [0, 1] : the minimum value allowed for the 
%   divisionRatio of two objects so as to be considered as a single object
%   (i.e. cell instant). The division ratio is the ratio of the minimum to 
%   maximum width (i.e. the distance of diametric pixels with respect to
%   the object's centerline from the center of the one pole semi-circle to 
%   the center the other pole semi-circle). Choose values in the range 
%   [0.65, 0.75].
% 
%  -filteringRadius : the radius of the filter for preprocessing operations.
%
%  -clearBorder (yes or no) : a flag indicating whether to remove
%   micro-colonies from image's borders in the preprocessing step. This can
%   accelarate segmentation and tracking of the input dataset, because the 
%   input's dataset micro-colonies number decreases. However when a
%   micro-colony, that was previously in the field of view, touches the
%   image's border or merges with a micro-colony that touches the image's 
%   border, is going to removed too.
%
%  -clearMergingColonies (all, none, colonyTrajectoryThresh) : 
%   a flag indicating whether to remove all micro-colonies when they merge
%   with each other (all or none). If yes is given merging micro-colonies
%   are removed only after colonyTrajectoryThresh frames. This can
%   accelarate segmentation and tracking of the input dataset, because the 
%   input's dataset micro-colonies number decreases.
%
%  error_message : a char-array containing the error message if the user's 
%  input is incorrect, otherwise it is empty.
%
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
% 
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.

error_message = [];
dlg_title = 'User''s input for segmentation';
prompt = { 'minAngle [0, 180]: '
    'maxEccentricity [0, 1]: '
    'minDivisionRatio [0, 1]: '
    'minArea (in pixels): '
    'filteringRadius (in pixels): '
    'clearBorders (yes or no): '
    'clearMergingColonies (all, none, colonyTrajectoryThresh): '};

default_answers = {'140', '0.55', '0.65', '40', '9', 'no', 'colonyTrajectoryThresh'};
dims = [1 60];
answers = inputdlg(prompt, dlg_title, dims, default_answers);
%checking user's input
if size(answers) == 0
    error_message = sprintf('The program execution was canceled by the user!\n');
    return;    
else
    input = str2double(answers);
    if sum(input(1:5) < 0) ~= 0 || sum(isnan(input(1:5))) ~= 0
        error_message = 'Error in input: All paramaters must be positive numbers.\n';
        return;
    end    
    if sum((input(1) < 0)) ~= 0 || sum((input(1) > 180)) ~= 0 ~= 0
        error_message = 'Error in input: <minAngle> must be in [0, 180].\n';
        return;        
    end    
    if sum((input(2:3) < 0)) ~= 0 || sum(input(2:3) > 1) ~= 0
        error_message = 'Error in input: <maxEccentricity> and <minDivisionRatio> must be in [0, 1].\n';
        return;        
    end
    if sum((input(4:5) < 0)) ~= 0 || sum(ceil(input(4:5)) ~= input(4:5)) ~= 0
        error_message = 'Error in input: <minArea> and <filteringRadius> must be positive integers.\n';
        return;        
    end  
    if sum(strcmpi(answers(6), {'yes', 'no'})) == 0
        error_message = 'Error in input: <clearBorders> must be yes or no.\n';
        return;
    end    
    if sum(strcmpi(answers(7), {'all', 'none', 'colonyTrajectoryThresh'})) == 0
        error_message = 'Error in input: <clearMergingColonies> must be all or none or colonyTrajectoryThresh.\n';
        return;
    end       
    configuration(1).minAngle = input(1);
    configuration(1).minEccentricity = input(2);
    configuration(1).minDivisionRatio = input(3);
    configuration(1).minArea = input(4);
    configuration(1).filteringRadius = input(5);
    if strcmpi('yes', answers{6})
        configuration(1).clearBorders = true;
    else
        configuration(1).clearBorders = false;        
    end
    configuration(1).clearMergingColonies = answers{7};
end