function [configuration, error_message] = askForFluoImages(configuration)
% Asks user to specify the fluorescence images corresponding to the input dataset.
% This function is called when the input dataset contains grayscale images.
%
% [configuration, error_message] = askForFluoChannelsImages(imagesNum, configuration)
%
% INPUT :
%
%  configuration : a struct containing the user's input.
%
% OUTPUT :
%  configuration : configuration struct with the new fields concerning the
%  input dataset for fluorescence measurement.
%  The new fields are:
%  -fluo_channels_filenames : a cell array containing the filename(s) of the
%   corrsponding fluorescence channel image(s) organized per channel. The
%   user can specify up to 3 fluorescence channels.
%
%  -fluo_channels_input_path : a cell array containing up to 3 input
%   directories of the corrsponding fluorescence channel image(s).
%
%  -fluorescence_channels : a cell-array of chars containing up to 3
%   channels for fluorescence measurement. Each value can be one of 'r',
%   'g','b'.
%
%  error_message : a char-array containing the error message if the user's
%  input is incorrect, otherwise it is empty.
%
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
%
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.
imagesNum = length(configuration.filenames);
configuration.fluo_channels_filenames = [];
configuration.fluo_channels_input_path = [];
configuration.fluorescence_channels = [];
error_message = [];
for i = 1:3
    if i==1
        choice = questdlg(sprintf('Would you like to process the corresponding fluorescent channel (channel-%d) from another image(s)?', i), 'Fluorescence', 'yes', 'no', 'cancel', 'no');
    else
        choice = questdlg(sprintf('Would you like to process another fluorescent channel (channel-%d) from another image(s)?', i), 'Fluorescence', 'yes', 'no', 'cancel', 'no');
    end
    if(strcmp('yes', choice) == 1)
        if(imagesNum == 1)
            [fluo_channels_filename, fluo_channel_input_path] = uigetfile( '*', 'Select an image to segment' );
            if(isnumeric(fluo_channels_filename))
                return;
            else
                configuration.fluo_channels_filenames{i}{1} = fluo_channels_filename;
                configuration.fluo_channels_input_path{i} = fluo_channel_input_path;
            end
        else
            fluo_channel_images_num = 0;
            fluo_channel_input_path = uigetdir( './', 'Select a directory of the fluorescent images' );
            if(isnumeric(fluo_channel_input_path))
                return;
            else
                configuration.fluo_channels_input_path{i} = fluo_channel_input_path;
            end
            dlg_title = 'Input for image type';
            prompt = { 'Enter the image-type:'};
            def = {'.tif'};
            answer = inputdlg(prompt, dlg_title,[1, 50], def);
            %checking user's input
            if size(answer) == 0
                return;
            end
            img_type = answer{1};
            listing = dir(configuration.fluo_channels_input_path{i});
            expression = sprintf('\\w*%s', img_type);
            for ii = 1:length(listing)
                ind = regexpi(listing(ii).name, expression);
                if(~isempty(ind))
                    fluo_channel_images_num = fluo_channel_images_num + 1;
                    configuration.fluo_channels_filenames{i}{fluo_channel_images_num} = listing(ii).name;
                end
            end
        end
        dlg_title = sprintf('Input for flourescent channel-%d', i);
        prompt = { 'Enter the channel label (R, G, B):'};
        num_lines = 1;
        def = {'R'};
        options.Resize = 'on';
        answer = inputdlg(prompt, dlg_title, num_lines, def, options);
        %checking user's input
        if size(answer) == 0
            return;
        elseif(sum(strcmpi(answer, {'R', 'G', 'B', 'RGB'})) == 0)
            error_message = sprintf('Error in answer: possible options (R, G, B)\n');
            return;
        end
        configuration.fluorescence_channels = [configuration.fluorescence_channels;answer];
    elseif(strcmp('no', choice) == 1)
        return;
    else
        error_message = sprintf('The program execution was canceled by the user!\n');
        return;
    end
end