function [configuration, error_message] = askForCellTrackingParams(configuration)
% Asks user for the cell tracking parameters. 
% 
% [configuration, error_message] = askForCellTrackingParams(configuration)
%
% INPUT :
%  configuration : a struct containing the user's input. 
%
% OUTPUT :
%  configuration : configuration struct with the new fields concerning the
%  cell tracking parameters. The new fields are:
%  -neighborhoodSize : the algorithm tries to match neighborhoods that have
%   at least (neighborhoodSize^2)/2 members. This parameter depends on how 
%   similar are  neighborhood patterns in the movie (usually depending on
%   the size of the microcolonies), i.e. increase this number if you think
%   that a bigger neighborhood is needed in order to identify uniquely how
%   a specific cell of the current frame matches to its ancestor in the
%   previous frame.
% 
%  -neighborDistanceFactor : the algorithm considers as adjacent cells, the
%   cells that the distance between their boundaries is less than
%   neighborDistanceFactor*cellwidth of the cells. If cells are not close
%   with each other, i.e. the microcolony is sparse then you should increase
%   this number.
% 
%  -overlapThresh : the minimum overlap score between two cells that matched
%   so as to be considered valid. Values closer to 1 make tracking alorithm
%   more strict, usually values between 0.75 to 0.85 work well depending on
%   the image resolution.
% 
%  -translationStep : is the translation (in pixels) that cell tracking
%   does to a neighborhood of the current frame so as to match it to a
%   neighborhood of the previous frame.
% 
%  -enableCellRotation : enable cell rotation when trying to match cells 
%   between consecutive frames.
% 
%  -correctErrors : Enable correction of segmentation/tracking errors. To 
%   correct an error, first it has to determine whether an error has occured.
%   If so the error type (which defines how the error will be corrected) 
%   must be determined. Finally it has to be determnined which cell(s) must
%   be modified according to the  correction strategy. To determine that an
%   error had occurred, the function exploits the prior knowledge of the 
%   lineage tree structure that corresponds to a time-lapse  movie with
%   growing cells (without cell death and with binary fission), given that 
%   the tree nodes must have up to two children and every leaf node must be
%   in the last level of the tree. Every other tree structure is incoherent. 
%   The user can specify the correction mode. See the available modes below:
%    1. auto : determine automatically the type of segmentation error based
%       on <trajectoryThresh> and specify automatically the cells that must
%       be modified so as to have a coherent lineage tree.
%    2. specifyError : the user must specify the error type and then the
%       function determines automatically the cells that must be modified so
%       as to have a coherent lineage tree.
%    3. specifyCells : the function determines automatically the error type
%       and then the user will have to specify the cell instants to be 
%       modified according to the error type so as to get a coherent
%       lineage tree.
%    4. manual : fisrt the user must specify the error type and then they
%       must specify the cell instant(s) to be modified according to the 
%       error type.
%    5. no : the correction mode is disabled.
%   Segmentation errors leading to incoherent tree structures:
%    a. oversegmentation in previous frame(s) (type II tree pattern) : the 
%       algorithm has oversegment the same cell multiple times (in previous
%       frames).
%    b. undersegmentation in current frame (type II tree pattern): the 
%       algorithm has undersegment a cell in current frame.
%    c. oversegmentation in current frame (type I tree pattern): when a
%       cell instant has more than 2 daughter cells then oversegmentation 
%      (in current frame) occured, because a cell instant can have only two
%       daughter cells.
%   Correction strategy for each error:
%    a. mergeCells : the function merges oversegmented cells with neighing
%       cells when oversegmentaion in previous frame(s) occured.
%    b. splitCells : the function splits undersegmented cell when
%       undersegmentation in current frame occurred.
%    c. mergeMultipleCells : the function merges oversegmented cells when
%       oversegmentation in current frame occurred.
%    
%   If the function fail to correct the errors (in 'auto' mode), or the user
%   determines that the incoherent tree pattern does not match to any of the
%   segmentation errors, then it is determined that existing trajectory is
%   wrong, i.e. tracking algorithm failed to match the cell instants. In
%   this case the function removes the trajectory from the existing tree.
%   Note that if cell death occurs in the time-lapse movie, thw uaer MUST 
%   specify only modes 2, 4 and 5.
%
%  -trajectoryThresh : when the function identifies type II incoherent  
%   lineage tree pattern, i.e. a cell of the previous frame is not matched 
%   to a cell of the current frame, the autocorrection approach has to 
%   decide whether this pattern occured due to under segmentation (at 
%   current frame) or over-segmentation (at previous frames), so if the 
%   existing trajectory of a cell (frame of birth - previous frame) is
%   higher than trajectoryThresh then the functions determines that under 
%   segmentation happened in current frame, and thus it cannot track its 
%   successor, so is segments the object and continues tracking. Otherwise, 
%   the algorithm considers it as over-segmentation errors that happened  
%   trajectoryThresh-1 consecutive times, so it merges these objects with
%   their closest neighbors per frame. <trajectoryThresh> is ommited when 
%   <correctErrors> value is ['no' | 'manual' | 'specifyError'].
%
%  error_message : a char-array containing the error message if the user's 
%  input is incorrect, otherwise it is empty.
%
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
% 
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.

error_message = [];
dlg_title = 'User''s input for tracking';
prompt = { 
    'neighborhoodSize: '
    'neighborDistanceFactor (in av. cell width): '
    'overlapThreshold [0, 1]: '
    'translationStep (in pixels): '
    'enableCellRotation (yes or no): '
    'correctErrors (auto, specifyError, specifyCells, manual, no): '
    'trajectoryThreshold: '
    };

default_answers = {'4', '1.5', '0.75', '2', 'yes', 'auto', '3', 'no'};
dims = [1 70];
answers = inputdlg(prompt, dlg_title, dims, default_answers);
%checking user's input
if size(answers) == 0
    error_message = sprintf('The program execution was canceled by the user!\n');
    return;    
else
    input = str2double(answers);
    if sum(input(1:4) < 0) ~= 0 || sum(isnan(input(1:4))) ~= 0
        error_message = 'Error in input: Paramaters 1-4 must be positive numbers.\n';
        return;
    end
    if input(1) ~= ceil(input(1)) || input(1) < 0
        error_message = 'Error in input: <neighborhoodSize> must be a positive integer.\n';
        return;        
    end        
    if sum((input(3) < 0)) ~= 0 || sum(input(3) > 1) ~= 0
        error_message = 'Error in input: <overlapThreshold> must be in [0, 1].\n';
        return;        
    end
    if input(4) ~= ceil(input(4)) || input(4) < 0
        error_message = 'Error in input: <translationStep> must be a positive integer.\n';
        return;        
    end            
    if sum(strcmpi(answers(5), {'yes', 'no'})) == 0         
        error_message = 'Error in input: <enableCellRotation> must be yes or no.\n';
        return;
    end
    if sum(strcmpi(answers(6), {'auto', 'specifyError', 'specifyCells', 'manual', 'no'})) == 0         
        error_message = 'Error in input: <correctErrors> must be {''auto'', ''specifyError'', ''specifyCells'', ''manual'', ''no''}.\n';
        return;
    end    
    if input(7) ~= ceil(input(7)) || input(7) < 2
        error_message = 'Error in input: <trajectoryThreshold> must be a integer > 1.\n';
        return;        
    end         
    configuration.neighborhoodSize = input(1);
    configuration.neighborDistanceFactor = input(2);
    configuration.overlapThresh = input(3);
    configuration.translationStep =input(4);
    if(strcmpi(answers{5}, 'yes'))
        configuration.enableCellRotation = true;   
    else
        configuration.enableCellRotation = false;       
    end
    
    configuration.correctErrors = answers{6};
    configuration.trajectoryThresh =input(7);    
end

end