function [configuration, error_message] = askForSegmAndFluoChannels(configuration)
% Asks user to specify the channels that will be extracted for segmentation and fluorescence measurement from the input dataset. 
% This function is called when the input dataset contains RGB images. 
% 
%
% [configuration, error_message] = askForGrayAndFluorescenceChannels(configuration)
%
% INPUT :
%  configuration : a struct containing the user's input. 
%
% OUTPUT :
%  configuration : configuration struct with the new fields concerning the
%  selection of channels for segmentation and fluorescence measurement.
%  The new fields are:
%  -grayscale_channel : a char-array containing the channel for segmentation
%   image extraction. It can be one of 'r', 'g', 'b', 'rgb'. 'rgb' is
%   returned when the user chooses to extract the grayscale image from the 3
%   channels' combination.
%
%  -fluorescence_channels : a cell-array of chars containing up to 3 
%   channels for fluorescence measurement. Each value can be one of 'r',
%   'g','b'. 
%
%  error_message : a char-array containing the error message if the user's 
%  input is incorrect, otherwise it is empty.
%
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
% 
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.

error_message = [];
dlg_title = 'Input for image processing';
prompt = { 'Generate the grayscale image from (R, G, B or RGB) channel:'};
num_lines = 1;
def = {'G'};
options.Resize = 'on';
answer = inputdlg(prompt, dlg_title, num_lines, def, options);
%checking user's input
if size(answer) == 0
    error_message = sprintf('The program execution was canceled by the user!\n');
    return;
elseif(sum(strcmpi(answer, {'R', 'G', 'B', 'RGB'})) == 0)
    error_message = sprintf('Error in answer: possible options (R, G, B or RGB)\n');
    return;
end
configuration.segmentation_channel = answer{1};
if isfield(configuration, 'fluorescence_channels')
    configuration = rmfield( configuration , 'fluorescence_channels');
end
for i = 1:3
    if i == 1
        choice = questdlg(sprintf('Would you like to include fluorescent channels into the processing?'), 'Fluorescence', 'yes', 'no', 'quit', 'yes');
    else
        choice = questdlg(sprintf('Would you like to include another fluorescent channel into the processing?'), 'Fluorescence', 'yes', 'no', 'quit', 'no');
    end
    if(strcmp('yes', choice) == 1)
        dlg_title = sprintf('Input for flourescent channel-%d', i);
        prompt = { 'Enter a channel (R, G, B):'};
        num_lines = 1;
        def = {'R'};
        options.Resize = 'on';
        answer = inputdlg(prompt, dlg_title, num_lines, def, options);
        %checking user's input
        if size(answer) == 0
            return;
        elseif(sum(strcmpi(answer, {'R', 'G', 'B', 'RGB'})) == 0)
            error_message = sprintf('Error in answer: possible options (R, G, B)\n');
            return;
        end
        configuration.fluorescence_channels = [configuration.fluorescence_channels;answer];
    elseif(strcmp('no', choice) == 1)
        return
    else
        return;
    end
end

end