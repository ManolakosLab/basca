function [configuration, error_message] = askForExecSettings(configuration)
% Asks user for the execution settings. 
% 
% [configuration, error_message] = askForExecSettings(configuration)
%
% INPUT :
%  configuration : a struct containing the user's input. 
%
% OUTPUT :
%  configuration : configuration struct with the new fields concerning the
%  execution settings. The new fields are:
%  -saveAnalysis (yes or no): a flag indicating whether to save the 
%   analysis or not throughout pipeline steps. If yes is chosen, BaSCA
%   creates a directory to save the analysis. The name of the directory
%   is the same as the input dataset directory (time-series) or file (still
%   image) extended with the time-stamp of the analysis beginning.
% 
%  -segmentationPlotType (contours or surfaces): a string indicating how to
%   plot segmentation results. If "contours" is chosen then BaSCA renders
%   segmentation cell contours by overlaying them on the input frame(s). If
%   "surfaces" is chosen then BaSCA renders segmentation cell surfaces with
%   random colors. The plot is saved in .fig format for further inspection 
%   and modification.
%   
%  -segmentationMethod (classic or cnn): a string indicating how to
%   segment the input dataset. If "classic" is chosen then BaSCA runs the
%   default segmentation algorithm. If "cnn" is chosen then BaSCA runs
%   python based script to run segmentation with pretrained convolutional 
%   neural networks.
%
%  -verb (yes or no): a flag indicating whether to display more information
%   about the current pipeline step in the console or not.
%
%  -output_dir_path : the full path to the directory created by BaSCA to 
%   save the results (.mat and .fig files).
%
%  -output_math_path : the full path for the .mat file that will
%  contain BaSCA's output.
%
%  error_message : a char-array containing the error message if the user's 
%  input is incorrect, otherwise it is empty.
%
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
% 
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.

filenames =  configuration.filenames;
input_dir_path =  configuration.input_dir_path;
error_message = [];
configuration.output_dir_path = [];
configuration.output_mat_path = [];
dlg_title = 'User''s input for the analysis';
prompt = {    'saveAnalysis (yes or no): '
    'segmentationPlotType (contours or surfaces): '
    'verb (yes or no): '
    'startFrame: '
    'endFrame:'
    'colonyTrajectoryThresh:'    
    };

default_answers = {'yes', 'contours', 'no', '1', num2str(length(filenames)), num2str(ceil(length(filenames)/2))};
dims = [1 60];
answers = inputdlg(prompt, dlg_title, dims, default_answers);
%checking user's input
if size(answers) == 0
    error_message = sprintf('The program execution was canceled by the user!\n');
    return;
else
    if sum(strcmpi(answers(1), {'yes', 'no'})) == 0
        error_message = 'Error in input: <saveAnalyis> must be yes or no.\n';
        return;
    end
    
    if sum(strcmpi(answers(2), {'contours', 'surfaces'})) == 0
        error_message = 'Error in input: <segmentationPlotType> must be contours or surfaces.\n';
        return;
    end
    
    if sum(strcmpi(answers(3), {'yes', 'no'})) == 0
        error_message = 'Error in input: <verb> must be yes or no.\n';
        return;
    end
    
    if (str2double(answers(4))< 0)  || (ceil(str2double(answers(4))) ~= str2double(answers(4)))  || (str2double(answers(4)) > length(filenames))
        error_message = 'Error in input: <startFrame> must be a positive integer less than num of frames.\n';
        return;        
    end 
    
    if (str2double(answers(5)) < str2double(answers(5)))  || (ceil(str2double(answers(5))) ~= str2double(answers(5)))  || (str2double(answers(5)) > length(filenames))
        error_message = 'Error in input: <endFrame> must be an integer less or equal to the number of frames and greater than <startFrame>.\n';
        return;        
    end
    
    if (str2double(answers(6)) > (str2double(answers(5))-str2double(answers(4))+1) || (ceil(str2double(answers(6))) ~= str2double(answers(6))) || (str2double(answers(6)) < 1))
        error_message = 'Error in input: <colonyTrajectoryThresh> must be a positive integer less or equal to the number of frames.\n';
        return;        
    end
    
    format shortg
    c = clock;
    c = fix(c);
    path_dirs = regexp(input_dir_path, filesep, 'split');
    if(length(filenames) > 1)
        output_dir_name = path_dirs{end};
    else
        indices = strfind(filenames{1}, '.');
        output_dir_name = filenames{1}(1:indices(length(indices))-1);
    end
    if strcmp(path_dirs{1}, '')
        path_dirs{1} = '/';
    end
    partial_output_dir_path = fullfile(path_dirs{1:end-1});

    configuration.output_dir_path = fullfile(partial_output_dir_path, sprintf('%s %d-%d-%d %d.%d.%d', output_dir_name, c(3), c(2), c(1), c(4), c(5), c(6)));
    configuration.output_mat_path = fullfile(configuration.output_dir_path, sprintf('%s.mat',  output_dir_name));


    if(strcmpi(answers(1), 'yes'))
        mkdir(configuration.output_dir_path);    
        configuration.saveAnalysis = true;        
    else
        configuration.saveAnalysis = false;
    end
    
    configuration.segmentationPlotType = answers{2};
    
    if(strcmpi(answers(3), 'yes'))
        configuration.verb = true;
    else
        configuration.verb = false;
    end    
    configuration.startFrame = str2double(answers(4));
    configuration.endFrame = str2double(answers(5));
    configuration.colonyTrajectoryThresh = str2double(answers(6));  
       
end

end