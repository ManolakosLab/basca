function [configuration, error_message] = askForInputDataset(configuration)
% Asks user for the input dataset.
%
% [configuration, error_message] = askForInputDataset()
%
% OUTPUT :
%  configuration : a struct with the fields below:
%  -filenames : a cell-array containing the filename(s) of the
%  nput dataset. filenames contains only one element if the user specifies
%  as input dataset a still-frame.
%
%  -input_dir_path : a char-array containing the dataset's input directory.
%
%  -imageFormat : a char-array indicating the dataset's image format
%   ['grayscale' | 'rgb'].
%
%  error_message : a char-array containing the error message if the user's 
%  input is incorrect, otherwise it is empty.
%
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
% 
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.
%configuration = struct([]);
configuration(1).filenames = {};
configuration(1).input_dir_path = [];
error_message=[];
choice = questdlg('Would you like to process a still image or time series?', 'Processing type', 'still image', 'time series', 'cancel', 'time series');
if(strcmp('still image', choice) == 1)
    
    [temp_filename, temp_input_dir_path] = uigetfile( '*', 'Select an image to process' );
    if(isnumeric(temp_filename))
        error_message = sprintf('Error in input: You must specify a file for processing!\n');        
        return;
    end
    configuration(1).filenames{1} = temp_filename;
    configuration(1).input_dir_path = temp_input_dir_path;
elseif(strcmp('time series', choice) == 1)
    temp_input_dir_path = uigetdir( './', 'Select a directory (time-series) to segment' );
    if(isnumeric(temp_input_dir_path))
        error_message = sprintf('Error in input: You must specify a directory for processing!\n');        
        return;
    end
    configuration(1).input_dir_path = temp_input_dir_path;

    dlg_title = 'Input for image type';
    prompt = { 'Enter the image-type:'};
    def = {'.tif'};
    answer = inputdlg(prompt, dlg_title, [1 50], def);
    %checking user's input
    if size(answer) == 0
        error_message = sprintf('Error in answer: You must specify image type for input dataset!\n');
        return;
    end
    imagesNum = 0;
    img_type = answer{1};
    listing = dir(configuration(1).input_dir_path);
    expression = sprintf('\\w*%s', img_type);
    for i = 1:length(listing)
        ind = regexpi(listing(i).name, expression);
        if(~isempty(ind))
            imagesNum = imagesNum + 1;
            configuration(1).filenames{imagesNum, 1} = listing(i).name;
        end
    end
else
    error_message = sprintf('The program execution was canceled by the user!\n');
    return;
end
imageFilename = sprintf('%s/%s', configuration.input_dir_path, configuration.filenames{1});
input_im = imread(imageFilename);
if(size(input_im, 3) == 3)
    configuration.segmentation_channel = '';
elseif(size(input_im, 3) == 1)
    configuration.segmentation_channel = 'gray';    
else
    error_message = sprintf('Error in input: You must specify images in grayscale or RGB format!\n');
    return;
end
end