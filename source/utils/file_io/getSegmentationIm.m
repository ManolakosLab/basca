function [segmentation_im, frame_id] = getSegmentationIm(configuration, i)
% Gets the image that will be used for segmentation either by reading a raw grayscale image or by extracting it from a raw RGB image.  
% 
% [gray_im, frame_id] = getSegmentationIm(configuration, i)
%
% INPUT :
%  configuration : the struct with the user's input, containing the
%  necessary info to get the segmentation image from the input dataset.
%
% OUTPUT :
%  segmantaion_im : the image that will be used for segmentation.
% 
%  frame_id : the filename of the frame without its extention, used as 
%  frame id.
%
%
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
% 
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.

input_path = configuration.input_dir_path;
filename = configuration.filenames{i};
segmentation_channel = configuration.segmentation_channel;
imageFilename = sprintf('%s/%s', input_path, filename);
input_im = imread(imageFilename);

if(~strcmp(segmentation_channel, 'gray'))
    if(strcmpi('R', segmentation_channel) == 1)
        segmentation_im = input_im(:, :, 1);
    elseif(strcmpi('G', segmentation_channel) == 1)
        segmentation_im = input_im(:, :, 2);
    elseif(strcmpi('B', segmentation_channel) == 1)
        segmentation_im = input_im(:, :, 3);
    elseif(strcmpi('RGB',segmentation_channel) == 1)
        segmentation_im = rgb2gray(input_im);
    end
else
    segmentation_im = input_im;
end

indices = regexpi(filename, '\.');
frame_id = filename(1:indices(end)-1);
end