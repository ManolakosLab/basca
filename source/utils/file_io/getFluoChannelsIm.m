function fluo_channels_im = getFluoChannelsIm(configuration, i)
% Gets the images that will be used for fluorescence measurement either by reading a raw grayscale image or by extracting it from a raw RGB image.  
% 
% fluo_channels_im = getFluoChannelsIm(configuration, i)
%
% INPUT :
%  configuration : the struct with the user's input, containing the
%  necessary info to get fluorescence channels from the input dataset.
%
%  i : the value indicating the frame's index.
%
%  segmentation_channel: the channel from which the image from segmentation is
%  going to be extracted from the input RGB image. It applies only when the
%  input is RGB image.
%
% OUTPUT :
%  fluo_channels_im : a cell array with fluorescence images that are going
%  to be used for fluorescence measurent. It contains 
%  length(configuration.fluorescence_channels) images.
%
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
% 
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.

if ~isfield(configuration, 'fluo_channels_filenames')
    imageFilename = sprintf('%s/%s', configuration.input_path, configuration.filename);
    input_im = imread(imageFilename);
    fluo_channels_im = cell(length(configuration.fluorescence_channels), 1);
    for j = 1:length(configuration.fluorescence_channels)
        if(strcmpi('R', configuration.fluorescence_channels{j}) == 1)
            fluo_input_im = input_im(:, :, 1);
        elseif(strcmpi('G', configuration.fluorescence_channels{j}) == 1)
            fluo_input_im = input_im(:, :, 2);
        elseif(strcmpi('B', configuration.fluorescence_channels{j}) == 1)
            fluo_input_im = input_im(:, :, 3);
        end
        fluo_channels_im{j} = fluo_input_im;
    end
    
else
    fluo_channels_im = cell(length(configuration.fluo_channels_filenames), 1);
    for j = 1:length(configuration.fluo_channels_filenames)
        imageFilename = sprintf('%s/%s', configuration.fluo_channels_input_path{j}, configuration.fluo_channels_filenames{j}{i});
        fluo_input_im = imread(imageFilename);
        if(length(size(fluo_input_im)) == 3)
            fluo_channels_im{j} = rgb2gray(fluo_input_im);
        else
            fluo_channels_im{j} = fluo_input_im;
        end
    end
end