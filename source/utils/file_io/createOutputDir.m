function configuration = createOutputDir(configuration)
% Creates a another output directory and updates configuration struct with 
% the new paths.
%
% configuration = createOutputDir(configuration)
%
% INPUT :
%  configuration : a struct containing the user's input. 
%
% OUTPUT :
%  configuration : configuration struct with the updated output paths
%
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
% 
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.

    filenames = configuration.filenames;
    input_dir_path = configuration.input_dir_path;
    format shortg
    c = clock;
    c = fix(c);
    path_dirs = regexp(input_dir_path, filesep, 'split');
    if(length(filenames) > 1)
        output_dir_name = path_dirs{end};
    else
        indices = strfind(filenames{1}, '.');
        output_dir_name = filenames{1}(1:indices(length(indices))-1);
    end
    if strcmp(path_dirs{1}, '')
        path_dirs{1} = '/';
    end
    partial_output_dir_path = fullfile(path_dirs{1:end-1});

    configuration.output_dir_path = fullfile(partial_output_dir_path, sprintf('%s %d-%d-%d %d.%d.%d', output_dir_name, c(3), c(2), c(1), c(4), c(5), c(6)));
    configuration.output_mat_path = fullfile(configuration.output_dir_path, sprintf('%s.mat',  output_dir_name));
    mkdir(configuration.output_dir_path);    
end
