function [bwColoniesMask, bwInitSegmentation] = preProcessingPhase(I, filteringRadius, clearBorders)
% Preprocess Phase contrast images and extract the bw images for colony mask and initial segmentaiton.  
% 
% [bwColoniesMask, bwInitSegmentation] = preProcessingPhase(I, filteringRadius, clearBorders)
%
% INPUT :
%  segmentaion_im : the grayscale image that is going to be preprocessed.
%  
%  filteringRadius : the radius of the filter for preprocessing operations.
%
%  clearBorder : a flag indicating whether to remove micro-colonies from 
%  image's borders in the preprocessing step.
%
% OUTPUT :
%  bwColoniesMask : a bw image containing white pixels for micro-colonies
%  surfaces and black for background.
%
%  bwInitSegmentation : a bw image containing white pixels for
%  micro-colonies' objects' surfaces and black for background.
%
%
%  See also preProcessingFluo, preProcessingDIC, preProcessingBright
%
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
% 
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.
%I = main_CT_Bayes(im2double(I));
I = im2double(I);
I_equalized = adapthisteq(I);

I_equalized_compl = imcomplement(I);
%%%%%%adaptive threshold%%%%%%%
bw_adaptive = adaptivethreshold(I_equalized_compl, filteringRadius, 0.01, 0);
%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%Canny ED%%%%%%%
bw_edges = edge(I_equalized,'canny', [0.5*graythresh(I_equalized), graythresh(I_equalized)], 3);
%%%%%%%%%%%%%%%%%%

se = strel('disk', ceil(1.5*filteringRadius));
bw_edges = imdilate(bw_edges, se);

bwColoniesMask = imfill(bw_edges, 'holes');

[bwColoniesMask] = imfillborder(bwColoniesMask);

se = strel('disk', ceil(1.5*filteringRadius));
bwColoniesMask = imerode(bwColoniesMask, se);

if clearBorders
    bwColoniesMask = imclearborder(bwColoniesMask);
end
bwInitSegmentation = bwColoniesMask(:, :).*bw_adaptive(:, :);
bwInitSegmentation = medfilt2(bwInitSegmentation);
end
