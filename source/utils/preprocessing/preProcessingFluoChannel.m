function [fluo_channels_thresh, fluo_channels_im ] = preProcessingFluoChannel(fluo_channels_im, bwInitSegmentation, filteringRadius)
% Preprocess fluorescence images that will be used for fluorescence measurement and compute the corresponding fluorescence thresholds. 
% 
% [fluo_channels_thresh, fluo_channels_im ] = preProcessingFluoChannel(fluo_channels_im, bwInitSegmentation, se_r)
%
% INPUT :
%  fluo_channels_im : a cell-array containing up to 3 images, one for each
%  fluorescence channel.
%
%  bwInitSegmentation : a bw image containing white pixels for
%  micro-colonies' objects' surfaces and black for background.
%  
%  filteringRadius : the radius of the filter for preprocessing operations.
%
% OUTPUT :
%  fluo_channels_thresh : a vector containing up to 3 values, one for each
%  fluorescence channel threshold. The threshold is computed as the value
%  of the 95% percentile of each channel's fluorescence intensities
%  extracted from the backround, i.e. region that does not contain colonies.
%
%  fluo_channels_im : a cell-array containing up to 3 images, one for each
%  preprocessed fluorescence channel.
%
%
%  See also preProcessingPhase, preProcessingDIC, preProcessingBright, preProcessingFluo
%
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
% 
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.

fluo_channels_thresh = zeros(length(fluo_channels_im), 1);

for i = 1:length(fluo_channels_im)
    I = double(fluo_channels_im{i});    
    I = main_CT_Bayes(I);
        
    I_tophat = imtophat(I, strel('disk', filteringRadius));
    I_adjusted = minMaxTransform(I_tophat, 0, 1);
    
    mask_complement = imcomplement(double(bwInitSegmentation));
    fluo_background = mask_complement.*I_adjusted;
    
    [row, column] = find(fluo_background > 0);
    intensities = zeros(length(row), 1);
    for ii = 1:length(row)
        intensities(ii) = fluo_background(row(ii), column(ii));
    end
    
    %mu = mean(intensities);
    %sigma = std(intensities);
    %fluo_channels_thresh{i} = mu+5*sigma;
    fluo_channels_thresh(i) = prctile(intensities, 95);
    fluo_channels_im{i} = I_adjusted;
end
end