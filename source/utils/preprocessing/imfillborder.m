function [bw_filled] = imfillborder(bw)
% bw_a = padarray(bw,[1 1],1,'pre');
% bw_a_filled = imfill(bw_a,'holes');
% bw_a_filled = bw_a_filled(2:end,2:end);
% 
% 
% %Now fill against the top and the right border.
% bw_b = padarray(padarray(bw,[1 0],1,'pre'),[0 1],1,'post');
% bw_b_filled = imfill(bw_b,'holes');
% bw_b_filled = bw_b_filled(2:end,1:end-1);
% 
% %Next, fill against the right and bottom borders.
% bw_c = padarray(bw,[1 1],1,'post');
% bw_c_filled = imfill(bw_c,'holes');
% bw_c_filled = bw_c_filled(1:end-1,1:end-1);
% 
% %Finally, fill against the bottom and left borders.
% bw_d = padarray(padarray(bw,[1 0],1,'post'),[0 1],1,'pre');
% bw_d_filled = imfill(bw_d,'holes');
% bw_d_filled = bw_d_filled(1:end-1,2:end);

%fill against the top border
bw_a = padarray(bw,[1 0], 1, 'pre');
bw_a_filled = imfill(bw_a,'holes');
bw_a_filled = bw_a_filled(2:end,1:end);

%fill against the left border
bw_b = padarray(bw,[0 1], 1, 'pre');
bw_b_filled = imfill(bw_b,'holes');
bw_b_filled = bw_b_filled(1:end,2:end);

%fill against the right border
bw_c = padarray(bw,[0 1], 1, 'post');
bw_c_filled = imfill(bw_c,'holes');
bw_c_filled = bw_c_filled(1:end,1:end-1);

%fill against the bottom border
bw_d = padarray(bw, [1 0], 1, 'post');
bw_d_filled = imfill(bw_d,'holes');
bw_d_filled = bw_d_filled(1:end-1,1:end);
%The last step is then to "logical OR" all these images together.
bw_filled = bw_a_filled | bw_b_filled | bw_c_filled | bw_d_filled;
end