function [bwColoniesMask, bwInitSegmentation] = preProcessingDIC(segmentation_im, filteringRadius, clearBorders)
% Preprocess DIC images and extract the bw images for colony mask and initial segmentaiton.  
% 
% [bwColoniesMask, bwInitSegmentation] = preProcessingDIC(I, filteringRadius, clearBorders)
%
% INPUT :
%  segmentaion_im : the grayscale image that is going to be preprocessed.
%  
%  filteringRadius : the radius of the filter for preprocessing operations.
%
%  clearBorder : a flag indicating whether to remove micro-colonies from 
%  image's borders in the preprocessing step.
%
% OUTPUT :
%  bwColoniesMask : a bw image containing white pixels for micro-colonies
%  surfaces and black for background.
%
%  bwInitSegmentation : a bw image containing white pixels for
%  micro-colonies' objects' surfaces and black for background.
%
%
%  See also preProcessingFluo, preProcessingPhase, preProcessingBright
%
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
% 
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.

theta = zeros(9, 1);
for i = 1:9
    [theta(i),~,~,~] = BODICDirectionMGradLine2D(minMaxTransform(segmentation_im, 0, 1), filteringRadius, 180);
end
theta = mode(round(theta, 1));

theta = mod(theta+180,360);
theta = pi*theta/180;
theta = theta + pi/2;

[imH, ~] = BOHilbertTransform2D(minMaxTransform(segmentation_im, 0, 1), theta);
segmentation_im = minMaxTransform(real(imH), 0, 1);

segmentation_im = main_CT_Bayes(segmentation_im);
I_equalized = adapthisteq(segmentation_im);
h = fspecial('average', [filteringRadius filteringRadius]);
I_smoothed = imfilter(minMaxTransform(I_equalized, 0, 1), h);

bw_adaptive = adaptivethreshold(I_smoothed, 2*filteringRadius, 0.01, 0);

h = fspecial('average', [filteringRadius filteringRadius]);
I_smoothed_2 = imfilter(minMaxTransform(imcomplement(I_smoothed), 0, 1), h);

%%%%%%Canny ED%%%%%%%
bwColoniesMask = edge(I_smoothed_2, 'canny', [0.5*graythresh(I_smoothed_2), graythresh(I_smoothed_2)], 3);
%%%%%%%%%%%%%%%%%%

se = strel('disk', floor(1.5*filteringRadius));
bwColoniesMask = imdilate(bwColoniesMask, se);
[bwColoniesMask] = imfillborder(bwColoniesMask);
se = strel('disk', floor(filteringRadius));
bwColoniesMask = imerode(bwColoniesMask, se);
if clearBorders
    bwColoniesMask = imclearborder(bwColoniesMask);
end
bwInitSegmentation = bwColoniesMask.*bw_adaptive;
bwInitSegmentation = medfilt2(bwInitSegmentation, [filteringRadius filteringRadius]);
end
