%

function [nvar] = est_noise(scale)

% for each subband

[m,n] = size(scale);

for i = 1:n
    tmp_scale = scale{i};
    nvar_array(i) = median(abs(tmp_scale(:)))/0.6745;
    clear tmp_scale;
end

nvar = max(nvar_array);
