% Main function -> Contourlet denoising with Bivariate shrinkage
function [est_im] = main_CT_Bayes(im)

[x, y] = size(im);

l = pow2(floor(log2(y)));
h = pow2(ceil(log2(y)));
if abs(l-y) < abs(h-y)
    y_power2 = l;
else
    y_power2 = h;
end
l = pow2(floor(log2(x)));
h = pow2(ceil(log2(x)));
if abs(l-x) < abs(h-x)
    x_power2 = l;
else
    x_power2 = h;
end

im = imresize(im, [x_power2 y_power2]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              Image decomposition by contourlets using the               %                 
%               pyramidal directional filter bank (PDFB).                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Parameteters:                         
nlevels = [2,3,3];        % Decomposition level
pfilter = 'coif5';        % Pyramidal filter
dfilter = 'cd';           % Directional filter

% Contourlet decomposition
coeffs = pdfbdec(im, pfilter, dfilter, nlevels);
est_coeffs = coeffs;
%-------------------------------------------------------------------------%

% Noise Estimmation
finest_scale = coeffs{end};
nvar = est_noise(finest_scale);
%-------------------------------------------------------------------------%

% Bayes Shrinkage
%-------------------------------------------------------------------------%
for scale = length(coeffs):-1:2
        
        for dir = 1:length(coeffs{scale})
        
            % noisy coefficients
            Y_coefficient = coeffs{scale}{dir};
            [k,l] = size(Y_coefficient);
            Y = reshape(Y_coefficient,1,k*l);    
            % Threshold value estimation 
            T = bayes(Y,nvar);
        
            % Bayes Shrinkage
            est_coeffs{scale}{dir} = sthresh(Y_coefficient,T);
        end
        
end
%-------------------------------------------------------------------------%

% Recostruction
est_im = pdfbrec(est_coeffs, pfilter, dfilter);
%-------------------------------------------------------------------------%

est_im = imresize(est_im, [x y]);
