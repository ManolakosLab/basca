function op=sthresh(X,T);

%A function to perform soft thresholding on a 
%given an input vector X with a given threshold T
% S=sthresh(X,T);

    ind=find(abs(X)<3*T);
    ind1=find(abs(X)>=3*T);
    X(ind)=0;
    X(ind1)=(X(ind1)-T);
    op=X;
    