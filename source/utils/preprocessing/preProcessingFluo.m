function [bwColoniesMask, bwInitSegmentation] = preProcessingFluo(segmentation_im, filteringRadius, clearBorders)
% Preprocess Fluo images and extract the bw images for colony mask and initial segmentaiton. 
% Fluorescence must be constantly expressed to every cell.
% 
% [bwColoniesMask, bwInitSegmentation] = preProcessingFluo(I, filteringRadius, clearBorders)
%
% INPUT :
%  segmentaion_im : the grayscale image that is going to be preprocessed.
%  
%  filteringRadius : the radius of the filter for preprocessing operations.
%
%  clearBorder : a flag indicating whether to remove micro-colonies from 
%  image's borders in the preprocessing step.
%
% OUTPUT :
%  bwColoniesMask : a bw image containing white pixels for micro-colonies
%  surfaces and black for background.
%
%  bwInitSegmentation : a bw image containing white pixels for
%  micro-colonies' objects' surfaces and black for background.
%
%
%  See also preProcessingPhase, preProcessingDIC, preProcessingBright
%
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
% 
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.

segmentation_im = main_CT_Bayes(im2double(segmentation_im));
I_equalized = adapthisteq(segmentation_im);

%%%%%%adaptive threshold%%%%%%%
bw_adaptive = adaptivethreshold(I_equalized, filteringRadius, 0.01, 0);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% find the background of the image
se = strel('disk', filteringRadius);
segmentation_im_background = imopen(segmentation_im, se);

%%%%%%global threshold%%%%%%%
bwColoniesMask = im2bw(segmentation_im_background, graythresh(segmentation_im_background));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

bwColoniesMask = imfill(bwColoniesMask, 'holes');
if clearBorders
    bwColoniesMask = imclearborder(bwColoniesMask);
end
bwInitSegmentation = bwColoniesMask.*bw_adaptive;
bwInitSegmentation = medfilt2(bwInitSegmentation, [filteringRadius filteringRadius]);
end