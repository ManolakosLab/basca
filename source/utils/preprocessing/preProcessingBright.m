function [bwColoniesMask, bwInitSegmentation] = preProcessingBright(I, filteringRadius, clearBorders)
% Preprocess bright field images and extract the bw images for colony mask and initial segmentaiton.  
% 
% [bwColoniesMask, bwInitSegmentation] = preProcessingBright(I, filteringRadius, clearBorders)
%
% INPUT :
%  segmentaion_im : the grayscale image that is going to be preprocessed.
%  
%  filteringRadius : the radius of the filter for preprocessing operations.
%
%  clearBorder : a flag indicating whether to remove micro-colonies from 
%  image's borders in the preprocessing step.
%
% OUTPUT :
%  bwColoniesMask : a bw image containing white pixels for micro-colonies
%  surfaces and black for background.
%
%  bwInitSegmentation : a bw image containing white pixels for
%  micro-colonies' objects' surfaces and black for background.
%
%
%  See also preProcessingFluo, preProcessingDIC, preProcessingPhase
%
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
% 
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.
I = main_CT_Bayes(im2double(I));

I_equalized = adapthisteq(I);

I_equalized_compl = imcomplement(I);
h = fspecial('average', [filteringRadius filteringRadius]);
I_equalized_compl = imfilter(minMaxTransform(I_equalized_compl, 0, 1), h);

%%%%%%global threshold%%%%%%%
bw_global = im2bw(I_equalized_compl, graythresh(I_equalized_compl));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%Canny ED%%%%%%%
bwColoniesMask = edge(I_equalized, 'canny', [0.5*graythresh(I_equalized), graythresh(I_equalized)], 3);
%%%%%%%%%%%%%%%%%%

se = strel('disk', filteringRadius);
bwColoniesMask = imdilate(bwColoniesMask, se);
[bwColoniesMask] = imfillborder(bwColoniesMask);
if clearBorders
    bwColoniesMask = imclearborder(bwColoniesMask);
end
bwInitSegmentation = bwColoniesMask.*bw_global;
bwInitSegmentation = medfilt2(bwInitSegmentation, [filteringRadius filteringRadius]);

end
