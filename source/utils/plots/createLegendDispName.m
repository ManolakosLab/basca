function myTitle = createLegendDispName(header, groupBy_label, groupBy_index, timeUnits, timeSlice)
if strcmp(groupBy_label, 'Frame')
    if isempty(timeUnits)
        myTitle = sprintf('%s%s %d', header, groupBy_label, groupBy_index);
    else
        myTitle = sprintf('%s%s(%s) %d(%4.2f)', header, groupBy_label, timeUnits, groupBy_index, timeSlice*groupBy_index);
    end
elseif strcmp(groupBy_label, 'Generation')
    myTitle = sprintf('%sGen. %d', header, groupBy_index);   
else
    myTitle = sprintf('%sCol. %d', header, groupBy_index);
end
end