function plotTrajectory(figname, frame, frame_indices, colony_indices, cellIndicesForAnnotation, annotationContourColor, contourColor, textColor)

figure('Name', figname);


if length(frame_indices) == 1
    [p, ~] = numSubplots(2);
    ii = 1;

    subplot(p(1), p(2), 1);
    imagesc(frame(frame_indices(ii)).colonyProps(colony_indices(ii)).prevColonyProps.IColony); colormap gray;
    hold('all');
    numOfCells = size(frame(frame_indices(ii)).colonyProps(colony_indices(ii)).prevColonyProps.cellProps, 1);
    
    for i = 1:numOfCells;
            plot(frame(frame_indices(ii)).colonyProps(colony_indices(ii)).prevColonyProps.cellProps{i, 'boundaryPixelList'}{1}(:, 1),...
                frame(frame_indices(ii)).colonyProps(colony_indices(ii)).prevColonyProps.cellProps{i, 'boundaryPixelList'}{1}(:, 2), ...
                'color',  contourColor,...
                'LineStyle','-', 'lineWidth', 1.25);
        
        text(frame(frame_indices(ii)).colonyProps(colony_indices(ii)).prevColonyProps.cellProps{i, 'centroid'}{1}(:, 1), ...
            frame(frame_indices(ii)).colonyProps(colony_indices(ii)).prevColonyProps.cellProps{i, 'centroid'}{1}(:, 2), ...
            sprintf('%d', i), 'FontWeight', 'bold', 'FontSize', 12, 'Color', textColor)
    end
    title(sprintf('Frame %s', frame(frame_indices(ii)-1).id(end-3:end)));
    
    subplot(p(1), p(2), 2);
    imagesc(frame(frame_indices(ii)).colonyProps(colony_indices(ii)).IColony); colormap gray;
    hold('all');
    numOfCells = size(frame(frame_indices(ii)).colonyProps(colony_indices(ii)).cellProps, 1);
    
    for i = 1:numOfCells;
        if ~ismember(i, cellIndicesForAnnotation{ii})
            plot(frame(frame_indices(ii)).colonyProps(colony_indices(ii)).cellProps{i, 'boundaryPixelList'}{1}(:, 1),...
                frame(frame_indices(ii)).colonyProps(colony_indices(ii)).cellProps{i, 'boundaryPixelList'}{1}(:, 2), ...
                'color',  contourColor,...
                'LineStyle','-', 'lineWidth', 1.25);
        else
            plot(frame(frame_indices(ii)).colonyProps(colony_indices(ii)).cellProps{i, 'boundaryPixelList'}{1}(:, 1),...
                frame(frame_indices(ii)).colonyProps(colony_indices(ii)).cellProps{i, 'boundaryPixelList'}{1}(:, 2), ...
                'color',  annotationContourColor,...
                'LineStyle','-', 'lineWidth', 1.25);
        end
        
        text(frame(frame_indices(ii)).colonyProps(colony_indices(ii)).cellProps{i, 'centroid'}{1}(:, 1), ...
            frame(frame_indices(ii)).colonyProps(colony_indices(ii)).cellProps{i, 'centroid'}{1}(:, 2), ...
            sprintf('%d', i), 'FontWeight', 'bold', 'FontSize', 12, 'Color', textColor)
    end
    title(sprintf('Frame %s', frame(frame_indices(ii)).id(end-3:end)));
else
    [p, ~] = numSubplots(length(frame_indices));
    
    for ii = 1:length(frame_indices)
        subplot(p(1), p(2), ii);
        % Plot samples of different labels with different colors.
        %     axes('YDir','reverse');
        %     box('on');
        %     grid('off');
        imagesc(frame(frame_indices(ii)).colonyProps(colony_indices(ii)).IColony); colormap gray;
        hold('all');
        numOfCells = size(frame(frame_indices(ii)).colonyProps(colony_indices(ii)).cellProps, 1);
        
        for i = 1:numOfCells;
            if ~ismember(i, cellIndicesForAnnotation{ii})
                plot(frame(frame_indices(ii)).colonyProps(colony_indices(ii)).cellProps{i, 'boundaryPixelList'}{1}(:, 1),...
                    frame(frame_indices(ii)).colonyProps(colony_indices(ii)).cellProps{i, 'boundaryPixelList'}{1}(:, 2), ...
                    'color',  contourColor,...
                    'LineStyle','-', 'lineWidth', 1.25);
            else
                plot(frame(frame_indices(ii)).colonyProps(colony_indices(ii)).cellProps{i, 'boundaryPixelList'}{1}(:, 1),...
                    frame(frame_indices(ii)).colonyProps(colony_indices(ii)).cellProps{i, 'boundaryPixelList'}{1}(:, 2), ...
                    'color',  annotationContourColor,...
                    'LineStyle','-', 'lineWidth', 1.25);
            end
            
            text(frame(frame_indices(ii)).colonyProps(colony_indices(ii)).cellProps{i, 'centroid'}{1}(:, 1), ...
                frame(frame_indices(ii)).colonyProps(colony_indices(ii)).cellProps{i, 'centroid'}{1}(:, 2), ...
                sprintf('%d', i), 'FontWeight', 'bold', 'FontSize', 12, 'Color', textColor)
        end
        title(sprintf('Frame %s', frame(frame_indices(ii)).id(end-3:end)));
    end
end