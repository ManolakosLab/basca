function [xaxisTitle] = createXaxisTitle(timeUnits)
if isempty(timeUnits)
    xaxisTitle = 'Frame';
else
    xaxisTitle = sprintf('Frame (%s)', timeUnits);
end
end