function [XTicklabels, XTicks] = createXticks(numOfFrames, frame_indices, step, timeSlice, timeUnits)
xAxis=1:step:numOfFrames;
XTicklabels = cell(length(xAxis), 1);
XTicks = zeros(length(xAxis), 1);
j = 1;
for i = xAxis
    if ~isempty(timeUnits)
        XTicklabels{j} = sprintf('%d (%3.1f)', frame_indices(i), frame_indices(i)*timeSlice);
    else
        XTicklabels{j} = sprintf('%d', frame_indices(i));
    end
    XTicks(j) = i;
    j = j + 1;
end
end