function cm = getColormap(attributeType, binsNum)
    if binsNum <=3
        binsNum = 3;
    end
    if strcmp('numerical', attributeType) 
        cm = cbrewer('div', 'RdBu', binsNum);
    elseif strcmp('categorical', attributeType) 
        cm = cbrewer('div', 'Spectral', binsNum);
    else
        cm = cbrewer('div', 'Spectral', binsNum);
    end
end