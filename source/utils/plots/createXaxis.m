function timeAxisValues=createXaxis(timeSlice,cellTrajectoriesArray)
    %fix axis problem
    if ~isempty(timeSlice)
        timeAxisValues = 0:timeSlice:timeSlice*(size(cellTrajectoriesArray, 2)-1);
    else
        timeAxisValues = 0:(size(cellTrajectoriesArray, 2)-1);
    end
end