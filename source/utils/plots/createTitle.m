function myTitle = createTitle(headerTitle, groupBy_label1, groupBy_indices1, groupBy_label2, groupBy_indices2, timeSlice, timeUnits, attribute1, attributeStat1, attribute2, attributeStat2, attribute3, attributeStat3)

isInterval1 = 1-range(diff(groupBy_indices1));
isInterval2 = 1-range(diff(groupBy_indices2));

myTitle = sprintf('%s', headerTitle);
if strcmp(groupBy_label1, 'Frame')
    if ~isempty(timeUnits)
        if isInterval1 == 1
            myTitle = sprintf('%s%s(%s) %d(%4.2f)-%d(%4.2f)',  myTitle, groupBy_label1, timeUnits, groupBy_indices1(1),groupBy_indices1(1)*timeSlice, groupBy_indices1(length(groupBy_indices1)), groupBy_indices1(length(groupBy_indices1))*timeSlice);
        else
            myTitle = sprintf('%s%s(%s) %d(%4.2f)',  myTitle, groupBy_label1, timeUnits, groupBy_indices1(1), groupBy_indices1(1)*timeSlice);
            for ii = 2:length(groupBy_indices1)
                myTitle = sprintf( '%s, %d(%4.2f)', myTitle, groupBy_indices1(ii), groupBy_indices1(ii)*timeSlice);
            end
        end
    else
        if isInterval1 == 1
            myTitle = sprintf('%s%s %d-%d',  myTitle, groupBy_label1, groupBy_indices1(1), groupBy_indices1(length(groupBy_indices1)));
        else
            myTitle = sprintf('%s%s %d',  myTitle, groupBy_label1, groupBy_indices1(1));
            for ii = 2:length(groupBy_indices1)
                myTitle = sprintf( '%s, %d', myTitle, groupBy_indices1(ii));
            end
        end
    end
    if ~isempty(groupBy_label2)
        
        if isInterval2 == 1
            myTitle = sprintf('%s, %s %d-%d',  myTitle, groupBy_label2, groupBy_indices2(1), groupBy_indices2(length(groupBy_indices2)));
        else
            myTitle = sprintf('%s, %s %s', myTitle, groupBy_label2, num2str(groupBy_indices2(1)));
            for ii = 2:length(groupBy_indices2)
                myTitle = sprintf( '%s, %d', myTitle, groupBy_indices2(ii));
            end
        end
    end
else
    if isInterval1 == 1
        myTitle = sprintf('%s%s %d-%d',  myTitle, groupBy_label1, groupBy_indices1(1), groupBy_indices1(length(groupBy_indices1)));
    else
        myTitle = sprintf('%s%s %d',  myTitle, groupBy_label1, groupBy_indices1(1));
        for ii = 2:length(groupBy_indices1)
            myTitle = sprintf( '%s, %d', myTitle, groupBy_indices1(ii));
        end
    end
    if ~isempty(groupBy_label2)
        if strcmp(groupBy_label2, 'Frame')
            if ~isempty(timeUnits)
                if isInterval2 == 1
                    myTitle = sprintf('%s, %s(%s) %d(%4.2f)-%d(%4.2f)',  myTitle, groupBy_label2, timeUnits, groupBy_indices2(1),groupBy_indices2(1)*timeSlice, groupBy_indices2(length(groupBy_indices2)), groupBy_indices2(length(groupBy_indices2))*timeSlice);
                else
                    myTitle = sprintf('%s, %s(%s) %d(%4.2f)',  myTitle, groupBy_label2, timeUnits, groupBy_indices2(1), groupBy_indices2(1)*timeSlice);
                    for ii = 2:length(groupBy_indices1)
                        myTitle = sprintf( '%s, %d(%4.2f)', myTitle, groupBy_indices2(ii), groupBy_indices2(ii)*timeSlice);
                    end
                end
            else
                if isInterval2 == 1
                    myTitle = sprintf('%s, %s %d-%d',  myTitle, groupBy_label2, groupBy_indices2(1), groupBy_indices2(length(groupBy_indices2)));
                else
                    myTitle = sprintf('%s, %s %d',  myTitle, groupBy_label2, groupBy_indices2(1));
                    for ii = 2:length(groupBy_indices2)
                        myTitle = sprintf( '%s, %d', myTitle, groupBy_indices2(ii));
                    end
                end
            end
        else
            if isInterval2 == 1
                myTitle = sprintf('%s, %s %d-%d',  myTitle, groupBy_label2, groupBy_indices2(1), groupBy_indices2(length(groupBy_indices2)));
            else
                myTitle = sprintf('%s, %s %s', myTitle, groupBy_label2, num2str(groupBy_indices2(1)));
                for ii = 2:length(groupBy_indices2)
                    myTitle = sprintf( '%s, %d', myTitle, groupBy_indices2(ii));
                end
            end
        end
    end
end

if (~isempty(attribute1))
    if(~isempty(attributeStat1))
        attributeStat1=sprintf('%s ',attributeStat1);
    end
    myTitle = sprintf( '%s_%s%s', myTitle, attributeStat1, attribute1);
end
if (~isempty(attribute2))
    if(~isempty(attributeStat2))
        attributeStat2=sprintf('%s ',attributeStat2);
    end
    myTitle = sprintf('%s vs %s%s', myTitle, attributeStat2, attribute2);
end
if (~isempty(attribute3))
    if(~isempty(attributeStat3))
        attributeStat3=sprintf('%s ',attributeStat3);
    end
    myTitle = sprintf('%s vs %s%s', myTitle, attributeStat3, attribute3);
end
end