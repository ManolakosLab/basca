function [figure1] = plotCellInstantContours(background_im, colonyProps, figureName, contourColor, textColor, cellInstantEnumeration)
% Plot samples of different labels with different colors.
numOfColonies = length(colonyProps);

figure1 = figure('Name', figureName);
axes1 = axes('Parent', figure1,'YDir','reverse');
box(axes1,'on');
grid(axes1,'off');
imagesc(background_im); colormap gray;
hold(axes1,'all');
cellInstantCounter = 1;
for j = 1:numOfColonies
    numOfCells = size(colonyProps(j).cellProps, 1);
    
    text(colonyProps(j).bBoxULCorner(1, 1), colonyProps(j).bBoxULCorner(1, 2), sprintf('%d',j), 'FontWeight', 'bold', 'FontSize', 12, 'Color', textColor);
    
    for i = 1:numOfCells;
        plot(colonyProps(j).cellProps{i, 'boundaryPixelList'}{1}(:, 1) + colonyProps(j).bBoxULCorner(1, 1) - 1,...
            colonyProps(j).cellProps{i, 'boundaryPixelList'}{1}(:, 2) + colonyProps(j).bBoxULCorner(1, 2) - 1, ...
            'color',  contourColor,...
            'LineStyle','-', 'lineWidth', 1.25);
        
        if(strcmp('no', cellInstantEnumeration))
            plot(colonyProps(j).cellProps{i, 'centroid'}{1}(:, 1) + colonyProps(j).bBoxULCorner(1, 1), ...
                colonyProps(j).cellProps{i, 'centroid'}{1}(:, 2) + colonyProps(j).bBoxULCorner(1, 2), ...
                'MarkerSize',5,...
                'Marker','+',...
                'LineWidth',2,...
                'LineStyle','none',...
                'Color', textColor);
        elseif(strcmp('frame', cellInstantEnumeration))
            text(colonyProps(j).cellProps{i, 'centroid'}{1}(:, 1) + colonyProps(j).bBoxULCorner(1, 1), ...
                colonyProps(j).cellProps{i, 'centroid'}{1}(:, 2) + colonyProps(j).bBoxULCorner(1, 2), ...
                sprintf('%d', cellInstantCounter), 'FontWeight', 'bold', 'FontSize', 12, 'Color', textColor)
        elseif(strcmp('col', cellInstantEnumeration))
            text(colonyProps(j).cellProps{i, 'centroid'}{1}(:, 1) + colonyProps(j).bBoxULCorner(1, 1), ...
                colonyProps(j).cellProps{i, 'centroid'}{1}(:, 2) + colonyProps(j).bBoxULCorner(1, 2), ...
                sprintf('%d', i), 'FontWeight', 'bold', 'FontSize', 12, 'Color', textColor)
        end
        cellInstantCounter = cellInstantCounter + 1;
    end
end