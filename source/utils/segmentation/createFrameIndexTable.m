function frame = createFrameIndexTable(frame)

if(isfield(frame, 'indexOfCells'))
    frame = rmfield( frame, 'indexOfCells' );
end

for i = 1:length(frame)
    frame(i).indexOfCells = table(0, 'VariableNames', {'colonyInd'});
    for j = 1:length(frame(i).colonyProps)
        if(~isempty(frame(i).colonyProps(j).cellProps))
            for k = 1:height(frame(i).colonyProps(j).cellProps)
                cellId = sprintf('x%07d_y%07d', ...
                    round((frame(i).colonyProps(j).cellProps{k, 'centroid'}{1}(1, 1)+frame(i).colonyProps(j).bBoxULCorner(1, 1))*1000) , ...
                    round((frame(i).colonyProps(j).cellProps{k, 'centroid'}{1}(1, 2)+frame(i).colonyProps(j).bBoxULCorner(1, 2))*1000));
                
                frame(i).indexOfCells{cellId, 1} = j;
            end
        end
    end
    frame(i).indexOfCells(1, :) = [];
end