function [cellProps, objectSegmentationTime] = objectsSegmentation(frame, ijk, configuration)
% Extracts the cell-instants from bw objects, i.e. from clusters of cell instants.
%
% [cellProps, objectSegmentationTime] = objectsSegmentation(frame, ijk, configuration)
%
%  INPUT :
%    frame : a struct array containing the objects generated  from 
%    colonyPartition step.
%
%    ijk : a 3-valued vector indexing an object in the frame struct
%    [frameInd, colonyInd, objectInd].
%   
%    configuration : a struct with the segmentaion paramaters.
%
%  OUTPUT :
%    cellProps : a cell array with structs containing the extracted cell
%    instant properties.
%
%    objectSegmentationTime: segmentation time for each object.
%   
%
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
% 
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.

frameInd = ijk(1);
colonyInd = ijk(2);
objectInd = ijk(3);
[x, y] = size(frame(frameInd).colonyProps(colonyInd).bwColony);

fprintf('Processing object(%d, %d, %d)...\n', frameInd, colonyInd, objectInd);
tic;
%create the binary image with the object
[bwObject] = createBW(x, y, frame(frameInd).colonyProps(colonyInd).objectProps(objectInd).pixelList);
%figure; imagesc(bwObject);

%split recursively the object
[segmentProps] = recursiveSegmentation(bwObject, bwObject, [],  configuration.minDivisionRatio, configuration.minAngle, configuration.minArea, configuration.verb);

%merge heuristically the object's segments to form cell-objects
[mergedSegmentProps] = mergeSegments(x, y, segmentProps, configuration.minAngle, configuration.minDivisionRatio, Inf, configuration.verb);

%merge the remaining segments (with high eccentricity and/or small size)
%without taking into consideration bowties constraint
[cellProps] = mergeObjectSegments(x, y, mergedSegmentProps,  configuration.minDivisionRatio, configuration.minEccentricity, configuration.minAngle, Inf, configuration.verb);
objectSegmentationTime = toc;

end

