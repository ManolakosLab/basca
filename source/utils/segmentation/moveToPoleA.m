function [coords2Pole, segLen2Pole] = moveToPoleA(objectCLVerticesWithoutPoles, i, dir2Pole, boundaryCoords, segLenCenterLine, dd)
coords2Pole = [];
segLen2Pole = [];

ycoord = objectCLVerticesWithoutPoles(i, 1);
xcoord = objectCLVerticesWithoutPoles(i, 2);
while(1)
    if(isequal(dir2Pole, 'N'))
        ycoord = ycoord-1;
        segLen2Pole = [segLen2Pole; 1]; %#ok<*AGROW>
        coords2Pole = [coords2Pole; ycoord xcoord];
    elseif(isequal(dir2Pole, 'NE'))
        xcoord = xcoord+1;
        ycoord = ycoord-1;
        segLen2Pole = [segLen2Pole ;dd ];
        coords2Pole = [coords2Pole; ycoord xcoord];
    elseif(isequal(dir2Pole, 'E'))
        xcoord = xcoord+1;
        segLen2Pole = [segLen2Pole; 1];
        coords2Pole = [coords2Pole; ycoord xcoord];
    elseif(isequal(dir2Pole, 'SE'))
        xcoord = xcoord+1;
        ycoord = ycoord+1;
        segLen2Pole = [segLen2Pole; dd ];
        coords2Pole = [ coords2Pole; ycoord xcoord];
    elseif(isequal(dir2Pole, 'S'))
        ycoord = ycoord+1;
        segLen2Pole = [segLen2Pole; 1];
        coords2Pole = [coords2Pole; ycoord xcoord];
    elseif(isequal(dir2Pole, 'SW'))
        ycoord = ycoord+1;
        xcoord = xcoord-1;
        segLen2Pole = [segLen2Pole; dd ];
        coords2Pole = [coords2Pole; ycoord xcoord];
    elseif(isequal(dir2Pole, 'W'))
        xcoord = xcoord-1;
        segLen2Pole = [segLen2Pole; 1];
        coords2Pole = [coords2Pole; ycoord xcoord];
    elseif(isequal(dir2Pole, 'NW'))
        ycoord = ycoord-1;
        xcoord = xcoord-1;
        segLen2Pole = [segLen2Pole; dd ];
        coords2Pole = [coords2Pole; ycoord xcoord];
    else
    end
    
    [isInside, isOnBoundary] = inpolygon(xcoord, -ycoord, boundaryCoords(:, 2), -boundaryCoords(:, 1));
    if(isOnBoundary == 1)
        segLen2Pole(end) = segLen2Pole(end)+segLen2Pole(end)/2;
        break;
    end
    
    if(~isInside && ~isOnBoundary)
        segLen2Pole(end) = [];
        coords2Pole(end, :) = [];
        if(isempty(segLen2Pole))
            segLenCenterLine(i) =  segLenCenterLine(i)+ segLenCenterLine(i)/2;
        else
            segLen2Pole(end) = segLen2Pole(end)+segLen2Pole(end)/2;
        end
        break;
    end
end