function [uLocalMinima] = findLocalMinima(distanceToBoundary, thresh, windowSize, verb)

graphLength = size(distanceToBoundary, 1);
if(windowSize >= graphLength)
    halfWindowSize = floor((graphLength-1)/2);
else
    halfWindowSize = floor(windowSize/2); 
end

centerlinePixelsNum = size(distanceToBoundary, 1);
x = 1:graphLength;
y = distanceToBoundary;
[ymax, xmax, ymin, xmin] = extrema(y);

if(verb == 1)
    figure;
    plot(x, y, '-b');
    hold on; plot(x(xmax),ymax,'r*',x(xmin),ymin,'g*'); hold off
end


c_i = 1;
localMinimaInd = []; localMinima= [];
for k = 1:length(xmin)
    if(y(xmin(k)) >= 0 && xmin(k) > 1 && xmin(k) < graphLength )
        
        localMinimumIndex = xmin(k);
        localMinimum = y(xmin(k));
        
        if(localMinimumIndex > halfWindowSize && localMinimumIndex < centerlinePixelsNum-halfWindowSize)
            windowIndices = -halfWindowSize:1:halfWindowSize;
        elseif(localMinimumIndex <= halfWindowSize)
            windowIndices = -localMinimumIndex+1:2*halfWindowSize-localMinimumIndex+1;
        else
            windowIndices = -2*halfWindowSize+(centerlinePixelsNum-localMinimumIndex):(centerlinePixelsNum-localMinimumIndex);
        end
        ind = find(windowIndices == 0);
        windowIndices = windowIndices+localMinimumIndex;
        windowDistances = y(windowIndices);
        leftLocalMaximum = max(windowDistances(1:ind-1));
        rightLocalMaximum = max(windowDistances(ind+1:end));
        rratio = localMinimum/rightLocalMaximum; rratio = round(rratio*100)/100;
        lratio = localMinimum/leftLocalMaximum; lratio = round(lratio*100)/100;
        if(~isempty(rratio) && ~isempty(lratio))
            if(rratio < thresh && lratio < thresh)
                localMinimaInd(c_i) = localMinimumIndex;
                localMinima(c_i) = localMinimum;
                if(verb == 1)
                    fprintf('\nWindow size: %4.2f\n', windowSize);
                    fprintf('Right ratio: %4.2f\nLeft ratio: %4.2f\nSplit point: %d\n\n', rratio, lratio, localMinimaInd(c_i));
                end
                c_i = c_i + 1;
            end
        end
    end
end
nearLocalMin = zeros(length(localMinimaInd), length(localMinimaInd));
for i = 1:length(localMinimaInd)
    for j = i+1:length(localMinimaInd)
        if(abs(localMinimaInd(i) - localMinimaInd(j)) <= halfWindowSize)
            nearLocalMin(i, j) = 1;
            nearLocalMin(j, i) = 1;
        end
    end
end
uLocalMinima = [];
for i = 1:size(nearLocalMin)
    if(sum(nearLocalMin(i, :)) > 0)
        ind = find(nearLocalMin(i, :));
        ind = [i ind];
        [~, minimumInd] = min(localMinima(ind));
        uLocalMinima = [uLocalMinima; localMinimaInd(ind(minimumInd))];
    else
        uLocalMinima = [uLocalMinima; localMinimaInd(i)];
    end
end
uLocalMinima = unique(uLocalMinima);

end