function [frame] = objectExtraction(frame, minArea)
% Extracts the objects of each micro-colony and stores their attributes.
% It removes objects with area lower than minArea, and objects that have
% median pixel intensity with the micro-colonies local background.
% 
% [frame] = objectExtraction(frame, minArea)
%
% INPUT :
%  frame : a struct containing the colonies of the movie organized
%  per frame. The colonies attributes of a frame are stored in colonyProps 
%  struct array.
%
%  minArea : the minimum value allowed for an object's area so as
%  to be considered as a valid object. 
%
%  
% OUTPUT :
%  frame : the frame struct updated with the objects of each colony, stored
%  in objectProps struct array. For example, in the i-th frame, the objects
%  of the j-th colony are stored in frame(i).colonyProps(j).objectProps.
%
%
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
% 
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.

frame.segmentationTime = 0;
numOfColonies = length(frame.colonyProps);
for j = 1:numOfColonies
    L = bwlabel(frame.colonyProps(j).bwColony, 4);
    props  = regionprops(L, 'PixelList','Centroid');
    frame.colonyProps(j).objectProps = struct([]);
    counter = 1;
    for i = 1:length(props)
        if(length(props(i).PixelList) > minArea)
            intensity = zeros(size(props(i).PixelList, 1), 1);
            for ii = 1:length(intensity)
                intensity(ii) = frame.colonyProps(j).IColony(props(i).PixelList(ii, 2), props(i).PixelList(ii, 1));
            end
            medianIntensity = median(intensity);
            %%%%%% adjusted py panos
            if medianIntensity < max(median(frame.colonyProps(j).IColony)) %%%%%
                frame.colonyProps(j).objectProps(counter, 1).pixelList = props(i).PixelList;
                frame.colonyProps(j).objectProps(counter, 1).centroid = props(i).Centroid;
                counter = counter + 1;
            end
            
        end
    end
end
end