function [segmentsProps] = recursiveSegmentation(bwObject, prevBwObject, segmentsProps, minDivisionRatio, minAngle, minArea, verb)
bwObject = bwmorph(bwObject, 'fill');
x = size(bwObject, 1);
y = size(bwObject, 2);
objectProps = regionprops(bwObject, 'PixelList', 'Solidity', 'MajorAxisLength', 'MinorAxisLength', 'Orientation', 'Area');
[~, ind] = max([objectProps(:).Area]);
objectProps = objectProps(ind);

%Use chessboard distance transform to get the object radius.
bwObjectWithPad = padarray(bwObject,[1 1]);
chessBoardDT = bwdist(~bwObjectWithPad, 'chessboard');
chessBoardDT = chessBoardDT(2:end-1,2:end-1);

%assuming that the radius of the object is the maximum distance from the boundary.
objectRadiusInPixels = max(max(chessBoardDT));

%Use chessboard distance transform to get the object radius.
bwObjectWithPad = padarray(bwObject,[1 1]);
euclideanDT = bwdist(~bwObjectWithPad, 'euclidean');
euclideanDT = euclideanDT(2:end-1,2:end-1);

%assuming that the radius of the object is the maximum distance from the boundary.
objectRadius= max(max(euclideanDT));
%get the skeleton of the initial object by manual thresholding
[skel,  ~] = skeleton(bwObject);
bwObjectSkeleton = bwmorph(skel > pi*objectRadius, 'skel', inf);

skeletonJunctions = [];
[r, c] = find(bwmorph(bwObjectSkeleton, 'branchpoints'));
if(~isempty(r))
    skeletonJunctions(:, 1) = c;
    skeletonJunctions(:, 2) = r;
end

skeletonEndPoints = [];
[r, c] = find(bwmorph(bwObjectSkeleton, 'endpoints'));
if(~isempty(r))
    skeletonEndPoints(:, 1) = c;
    skeletonEndPoints(:, 2) = r;
end

%Junction based watershed transform (WT) of the object
%first try WT with chessboard distance
%which is more tolerant to noisy boundaries and thus
%oversegmenting the object less than WT with euclidean distance

[L, numberOfSegments, junctionsWeight] = getJunctionSegments2(bwObject, bwObjectSkeleton, skeletonJunctions, minAngle+0.2*minAngle, 'chessboard');

if(~isempty(junctionsWeight))
    if(isequal(bwObject,L)  && (sum(junctionsWeight) > 0))
        
        [L, numberOfSegments, junctionsWeight] = getJunctionSegments2(bwObject, bwObjectSkeleton, skeletonJunctions, minAngle+0.2*minAngle, 'euclidean');
        
        if(isequal(bwObject,L)  && (sum(junctionsWeight) > 0))
            
            bwObjectWithPad = padarray(bwObject,[1 1]);
            euclideanDT = bwdist(~bwObjectWithPad, 'euclidean');
            euclideanDT = euclideanDT(2:end-1,2:end-1);
            
            %centroids = [skeletonEndPoints; skeletonJunctions];
            centroids = skeletonEndPoints;
            centroids = [centroids(:, 1) -centroids(:, 2)];
            distances = zeros(size(objectProps.PixelList, 1), 1);
            for ii = 1:size(objectProps.PixelList, 1)
                distances(ii, 1) = euclideanDT(objectProps.PixelList(ii, 2), objectProps.PixelList(ii, 1));
            end
            
            [generatedDataset] = generateData(distances, [objectProps.PixelList(:, 1) -objectProps.PixelList(:, 2)], centroids, minArea);
            kmax = size(centroids, 1);
            kmin = sum(junctionsWeight > 1)+1;
            try
                [~, pp, mu, sigma, ~, ~] = mixtures2D(generatedDataset', kmin, kmax, centroids', [], [], 1e-3, 1e-4, 0, 0);
            catch
                [~, pp, mu, sigma, ~, ~] = mixtures2D(generatedDataset', kmin, kmax, centroids', [], [], 1e-3, 1e-4, 0, 0);
            end
            [newSegments, L] = classifyPixels(x, y, [objectProps.PixelList(:, 1) -objectProps.PixelList(:, 2)], mu',sigma, pp);
            numberOfSegments = length(newSegments);
        end
    end
end

if(numberOfSegments > 1) %if the object is segmented, junctions have been found (i.e. complex object)
    splitObjectsProps  = regionprops(L, 'PixelList');
    newSegments = cell(length(splitObjectsProps), 1);
    for jj = 1:length(splitObjectsProps)
        newSegments{jj} = splitObjectsProps(jj).PixelList;
    end
    
    
    if(verb)
        figure('name', 'complex object', 'Position', [256, 256, 1024, 512]);
        subplot(1,2,1);
        imagesc(bwObject); colormap jet;
        subplot(1,2,2);
        imagesc(L); colormap jet;
    end
else
    %the object has no junctions (i.e. object with circular skeleton,
    %object with bowties, object with non-convex shape, cell obect)
    
    %getting the centerline of the object
    [bwObjectCL, endpoints] = objectCenterline(bwObject);
    objectCLlen = length(find(bwObjectCL));
    %check for object with circular skeleton (without junctions)...
    %They must be treated as complex objects but with classic watershed
    %because they have no junctions!!!
    if(max(max(skel)) == Inf)
        %[W, newSegments] = watershedSegmentation(bwObject, x, y, 'euclidean');
        
        bwObjectWithPad = padarray(bwObject,[1 1]);
        euclideanDT = bwdist(~bwObjectWithPad, 'euclidean');
        euclideanDT = euclideanDT(2:end-1,2:end-1);
        [row, column] = find(bwObjectSkeleton);
        
        centroids = [skeletonEndPoints; skeletonJunctions];
        step = floor(length(centroids)/4);
        if(step < 1)
            step = 4;
        end
        centroids = [column(1:step:end), -row(1:step:end)];
        distances = zeros(size(objectProps.PixelList, 1), 1);
        for ii = 1:size(objectProps.PixelList, 1)
            distances(ii, 1) = euclideanDT(objectProps.PixelList(ii, 2), objectProps.PixelList(ii, 1));
        end
        
        [generatedDataset] = generateData(distances, [objectProps.PixelList(:, 1) -objectProps.PixelList(:, 2)], centroids, minArea);
        kmax = size(centroids, 1);
        kmin = sum(junctionsWeight > 1)+1;
        [~, pp, mu, sigma, ~, ~] = mixtures2D(generatedDataset', kmin, kmax, centroids', [], [], 1e-3, 1e-4, 0, 0);
        [newSegments, L] = classifyPixels(x, y, [objectProps.PixelList(:, 1) -objectProps.PixelList(:, 2)], mu',sigma, pp);
        if(verb)
            figure('name', 'objects in circle', 'Position', [256, 256, 1024, 512]);
            subplot(1,2,1);
            imagesc(bwObject); colormap jet;
            subplot(1,2,2);
            imagesc(L); colormap jet;
        end
    elseif(objectCLlen <= objectRadiusInPixels || objectRadiusInPixels < 2) %when the segment is too small, no need to segmented more
        if(verb)
            figure('name', 'small object', 'Position', [256, 256, 1024, 512]);
            subplot(1,2,1);
            imagesc(bwObject); colormap jet;
            subplot(1,2,2);
            imagesc(bwObjectCL); colormap jet;
        end
        
        segmentsNum = length(segmentsProps)+1;
        segmentsProps(segmentsNum).pixelList = uint16(objectProps(1, 1).PixelList);
        %segmentsProps(segmentsNum).bwObject = bwObject;
        segmentsProps(segmentsNum).centroid(1, :) =  mean(objectProps(1, 1).PixelList);
        segmentsProps(segmentsNum).widthInPixels = 2*objectRadiusInPixels+1;
        segmentsProps(segmentsNum).width = objectProps.MinorAxisLength;%2*max(max(bwdist(~bwObject, 'euclidean')))+1;
        
        segmentsProps(segmentsNum).lengthInPixels = round(objectProps.MajorAxisLength);
        segmentsProps(segmentsNum).length = objectProps.MajorAxisLength;
        
        bwObjectPerim = bwperim(bwObject);
        
        segmentsProps(segmentsNum).minorAxis = objectProps.MinorAxisLength;
        segmentsProps(segmentsNum).majorAxis = objectProps.MajorAxisLength;
        segmentsProps(segmentsNum).eccentricity = objectProps.MinorAxisLength/objectProps.MajorAxisLength;
        segmentsProps(segmentsNum).orientation = objectProps.Orientation;
        segmentsProps(segmentsNum).solidity = objectProps.Solidity;
        if(segmentsProps(segmentsNum).orientation < 0)
            segmentsProps(segmentsNum).orientation = 360+segmentsProps(segmentsNum).orientation;
        end
        segmentsProps(segmentsNum).covariance = cov(objectProps(1, 1).PixelList);
        b = bwboundaries(bwObjectPerim);
        segmentsProps(segmentsNum).boundaryPixelList = uint16([b{1}(:, 2) b{1}(:, 1)]);
        segmentsProps(segmentsNum).centralPixel = ceil(segmentsProps(segmentsNum).centroid);
        if bwObject(ceil(segmentsProps(segmentsNum).centroid(2)), ceil(segmentsProps(segmentsNum).centroid(1))) == 0
            segmentsProps(segmentsNum).hasIrregularShape = true;
        else
            segmentsProps(segmentsNum).hasIrregularShape = false;
        end
        return;
    else
        %if the object is non-complex, get the object's centerline to
        %check for bowties
        objectCLVerticesWithoutPoles = bwtraceboundary(bwObjectCL, [endpoints(2, 1) endpoints(1, 1)], 'N', 8, objectCLlen);
        [verticesFromPoleA, ~, verticesToPoleB, ~] = getObjectPoles(bwObject, objectCLVerticesWithoutPoles, 'euclidean', verb);
        objectCLVertices = [verticesFromPoleA; verticesToPoleB; objectCLVerticesWithoutPoles];
        objectCLVertices = [objectCLVertices(:, 2) objectCLVertices(:, 1)];
        
        %a trick to order the vertices of the extended centerline.
        bwObjectCL = createBW(x, y, objectCLVertices);
        [~, endpoints, ~] = anaskel(bwObjectCL);
        orderedObjectCLVertices = bwtraceboundary(bwObjectCL, [endpoints(2, 1) endpoints(1, 1)], 'N', 8, length(objectCLVertices));
        
        [distanceToBoundaryInPixels, ~, ~, ~] = meausureDistToBoundary2(bwObject, orderedObjectCLVertices, 'chessboard');
        
        windowSize = 2*max(medfilt1(distanceToBoundaryInPixels, double(objectRadiusInPixels)))+3;
        
        [distanceToBoundary, orientationAngles, ~, ~] = meausureDistToBoundary2(bwObject, orderedObjectCLVertices, 'euclidean');
        
        localMinima = findLocalMinima(distanceToBoundary, minDivisionRatio, windowSize, verb-1);
        
        if(~isempty(localMinima))  %if current object has at least one local minimum, i.e. bowtie, it must be divided.
            splitCoordinates = zeros(length(localMinima), 2);
            splitMask = cell(length(localMinima), 1);
            for j = 1:length(localMinima)
                se = strel('line', distanceToBoundary(localMinima(j))+1.4142, orientationAngles(localMinima(j)));
                nhood = getnhood(se);
                splitMask{j} = nhood;
                splitCoordinates(j, :) = orderedObjectCLVertices(localMinima(j), :);
            end
            [W, newSegments] = divideObject(bwObject, splitCoordinates, splitMask);
            if(verb)
                figure('name', 'object with bowties', 'Position', [256, 256, 1024, 512]);
                subplot(1,2,1);
                imagesc(bwObject); colormap jet;
                subplot(1,2,2);
                imagesc(W); colormap jet;
            end
        else
            %simplify Polygon line (i.e. centerline vertices) if needed.
            [objectSimplifiedCLVertices, ~] = dpsimplify(orderedObjectCLVertices , sqrt(2));
            
            [objectAngles, isCollinear] = computeObjectAngles(y, objectSimplifiedCLVertices, minAngle);
            %the skeleton belongs either to a single-cell or to a non convex object
            if(sum(isCollinear) == length(objectAngles))
                segmentsNum = length(segmentsProps)+1;
                %segmentsProps(segmentsNum).bwObject = bwObject;
                segmentsProps(segmentsNum).pixelList = uint16(objectProps(1, 1).PixelList);
                
                segmentsProps(segmentsNum).widthInPixels = max(medfilt1(distanceToBoundaryInPixels(2:end-1), double(objectRadiusInPixels)));
                segmentsProps(segmentsNum).width = max(medfilt1(distanceToBoundary(2:end-1), ceil(segmentsProps(segmentsNum).widthInPixels/2)));
                
                segmentsProps(segmentsNum).lengthInPixels = length(orderedObjectCLVertices);
                [segmentsProps(segmentsNum).length, ~] = arclength(objectSimplifiedCLVertices(:, 2), objectSimplifiedCLVertices(:, 1), 'linear');
                
                segmentsProps(segmentsNum).centroid(1, :) = mean(objectProps(1, 1).PixelList);
                bwObjectPerim = bwperim(bwObject);
                
                segmentsProps(segmentsNum).minorAxis = objectProps.MinorAxisLength;
                segmentsProps(segmentsNum).majorAxis = objectProps.MajorAxisLength;
                segmentsProps(segmentsNum).eccentricity = objectProps.MinorAxisLength/objectProps.MajorAxisLength;
                segmentsProps(segmentsNum).orientation = objectProps.Orientation;
                segmentsProps(segmentsNum).solidity = objectProps.Solidity;
                if(segmentsProps(segmentsNum).orientation < 0)
                    segmentsProps(segmentsNum).orientation = 360+segmentsProps(segmentsNum).orientation;
                end
                segmentsProps(segmentsNum).covariance = cov(objectProps(1, 1).PixelList);
                b = bwboundaries(bwObjectPerim);
                segmentsProps(segmentsNum).boundaryPixelList = uint16([b{1}(:, 2) b{1}(:, 1)]);
                
                segmentsProps(segmentsNum).centralPixel = orderedObjectCLVertices(ceil(size(orderedObjectCLVertices, 1)/2), :);
                segmentsProps(segmentsNum).centralPixel = [segmentsProps(segmentsNum).centralPixel(2) segmentsProps(segmentsNum).centralPixel(1)];
                
                if bwObject(ceil(segmentsProps(segmentsNum).centroid(2)), ceil(segmentsProps(segmentsNum).centroid(1))) == 0
                    segmentsProps(segmentsNum).hasIrregularShape = true;
                else
                    segmentsProps(segmentsNum).hasIrregularShape = false;
                end
                return;
            else
                splitCoordinates = zeros(sum(~isCollinear), 2);
                splitMask = cell(sum(~isCollinear), 1);
                jj = 1;
                for j = 1:length(isCollinear)
                    if(~isCollinear(j))
                        logicalInd = ismember(objectCLVertices, [objectSimplifiedCLVertices(j+1, 2) objectSimplifiedCLVertices(j+1, 1)], 'rows');
                        se = strel('line', distanceToBoundary(logicalInd)+1.4142, orientationAngles(logicalInd));
                        nhood = getnhood(se);
                        splitMask{jj} = nhood;
                        splitCoordinates(jj, :) = orderedObjectCLVertices(logicalInd, :);
                        jj = jj + 1;
                    end
                end
                [W, newSegments] = divideObject(bwObject, splitCoordinates, splitMask);
                if(verb)
                    figure('name', 'non-convex object', 'Position', [256, 256, 1024, 512]);
                    subplot(1,2,1);
                    imagesc(bwObject); colormap jet;
                    subplot(1,2,2);
                    imagesc(W); colormap jet;
                end
            end
        end
    end
end

for ii = 1:length(newSegments);
    if(length(newSegments{ii}) > 5)
        [bwObject] = createBW(x, y, newSegments{ii});
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%maybe useless code%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if (isequal(bwObject, prevBwObject))
            segmentsNum = length(segmentsProps)+1;
            %segmentsProps(segmentsNum).bwObject = bwObject;
            
            segmentsProps(segmentsNum).widthInPixels(1, 1) = 2*objectRadiusInPixels+1;
            segmentsProps(segmentsNum).width = objectProps.MinorAxisLength; %2*max(max(bwdist(~bwObject, 'euclidean')))+1;
            
            segmentsProps(segmentsNum).centroid(1, :) =  mean(objectProps(1, 1).PixelList);
            segmentsProps(segmentsNum).pixelList = uint16(objectProps(1, 1).PixelList);
            segmentsProps(segmentsNum).lengthInPixels = round(objectProps.MajorAxisLength);
            segmentsProps(segmentsNum).length = objectProps.MajorAxisLength;
            
            bwObjectPerim = bwperim(bwObject);
            
            segmentsProps(segmentsNum).minorAxis = objectProps.MinorAxisLength;
            segmentsProps(segmentsNum).majorAxis = objectProps.MajorAxisLength;
            segmentsProps(segmentsNum).eccentricity = objectProps.MinorAxisLength/objectProps.MajorAxisLength;
            segmentsProps(segmentsNum).orientation = objectProps.Orientation;
            segmentsProps(segmentsNum).solidity = objectProps.Solidity;
            if(segmentsProps(segmentsNum).orientation < 0)
                segmentsProps(segmentsNum).orientation = 360+segmentsProps(segmentsNum).orientation;
            end
            segmentsProps(segmentsNum).covariance = cov(objectProps(1, 1).PixelList);
            b = bwboundaries(bwObjectPerim);
            segmentsProps(segmentsNum).boundaryPixelList = uint16([b{1}(:, 2) b{1}(:, 1)]);

            segmentsProps(segmentsNum).centralPixel = ceil(segmentsProps(segmentsNum).centroid);          
            if bwObject(ceil(segmentsProps(segmentsNum).centroid(2)), ceil(segmentsProps(segmentsNum).centroid(1))) == 0
                segmentsProps(segmentsNum).hasIrregularShape = true;
            else
                segmentsProps(segmentsNum).hasIrregularShape = false;
            end
            return;
        end
        prevBwObject = bwObject;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        [segmentsProps] = recursiveSegmentation(bwObject, prevBwObject, segmentsProps, minDivisionRatio, minAngle,minArea,  verb);
    end
end

