function [bw] = createBW(x, y, pixelList)
bw = false(x, y);
pixelsNum = size(pixelList, 1);
for i = 1:pixelsNum
    if(pixelList(i, 2) > 0 && pixelList(i, 1) > 0 && pixelList(i, 2) <= x && pixelList(i, 1) <= y)
        bw(pixelList(i, 2), pixelList(i, 1)) = true;
    end
end