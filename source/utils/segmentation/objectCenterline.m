function [bwObjectSkeleton, endpoints] = objectCenterline(bwObject)
d = [  0 -1;
    1 -1;
    1 0;
    1 1;
    0 1;
    -1 1;
    -1 0;
    -1 -1;
    ];
bwObject = bwmorph(bwObject, 'spur');
[skel,  ~] = skeleton(bwObject);
[column, row] = find(skel == Inf);

for ii = 1:length(column)
    skel(column(ii), row(ii)) = 0;
end
distanceMax = max(max(skel));
for ii = 1:length(column)
    skel(column(ii), row(ii)) = distanceMax+1;
end
objectRadius = max(max(bwdist(~bwObject, 'quasi-euclidean')));
r = 1;
while(1)
    %test each threshold in order to get the optimum centerline
    bwObjectSkeleton = bwmorph(skel > r*objectRadius*pi, 'thin', inf);
    junctions = [];
    [row, column] = find(bwmorph(bwObjectSkeleton, 'branchpoints'));
    if(~isempty(row))
        junctions(:, 1) = column;
        junctions(:, 2) = row;
    end
    if(size(junctions, 1) == 0)
        break;
    end
    r = r + 0.1;
end
[~, endpoints, ~] = anaskel(bwObjectSkeleton);
% skelPoints = [endpoints' ;  junctions];
% skelPointsBool = [zeros(size(endpoints', 1), 1); ones(size(junctions, 1), 1)];
% % endpoints = endpoints';
% % if(size(junctions, 1) == 2)
% %     D1 = bwdistgeodesic(bwObjectSkeleton, junctions(1, 1), junctions(1, 2), 'quasi-euclidean');
% %     D2 = bwdistgeodesic(bwObjectSkeleton, junctions(2, 1), junctions(2, 2), 'quasi-euclidean');
% %     D = D1 + D2;
% %     D = round(D * 8) / 8;
% %     D(isnan(D)) = inf;
% %     bwObjectCL = imregionalmin(D);
% %     figure; imagesc(bwObjectCL);
% % elseif(size(junctions, 1) == 1)
% %     bwObjectCL = bwObjectSkeleton;
% %     for i = 1:size(endpoints, 1)
% %         D1 = bwdistgeodesic(bwObjectSkeleton, junctions(1, 1), junctions(1, 2), 'quasi-euclidean');
% %         D2 = bwdistgeodesic(bwObjectSkeleton, endpoint(i, 1), junctions(i, 2), 'quasi-euclidean');
% %         D = D1 + D2;
% %         D = round(D * 8) / 8;
% %         D(isnan(D)) = inf;
% %         bwObjectCL = imregionalmin(D);
% %     end
% % else
% %     bwObjectCL = bwObjectSkeleton;
% % end
% %bwObjectCL = cell(size(skelPoints, 1), size(skelPoints, 1));
% bwObjectCLLength = zeros(size(skelPoints, 1), size(skelPoints, 1));
% bwObjectCL = bwObjectSkeleton;
% lolo = bwObjectSkeleton;
% for i = 1:size(skelPoints, 1)
%     for j = i+1:size(skelPoints, 1)
%         D1 = bwdistgeodesic(bwObjectSkeleton, skelPoints(i, 1), skelPoints(i, 2), 'quasi-euclidean');
%         D2 = bwdistgeodesic(bwObjectSkeleton, skelPoints(j, 1), skelPoints(j, 2), 'quasi-euclidean');
%         D = D1 + D2;
%         D = round(D * 8) / 8;
%         D(isnan(D)) = inf;
%         if(length(find(imregionalmin(D))) < objectRadius+2 && (skelPointsBool(i) || skelPointsBool(j)))
%             bwObjectCL = bwObjectCL - imregionalmin(D);
%         end
%         if(skelPointsBool(i) && skelPointsBool(j))
%             bwObjectCL = bwObjectCL + imregionalmin(D);
%         end        
%         lolo = lolo + imregionalmin(D);
% %         bwObjectCLLength(i, j) = sum(sum(bwObjectCL{i, j}));
% %         figure; imagesc(bwObjectCL{i, j});
%     end
% end
% %[ii, jj] = find(bwObjectCLLength == max(max(bwObjectCLLength)));
% for i = 1:size(junctions, 1)
%     if(bwObjectCL(junctions(i, 2), junctions(i, 1)) == 0)
%         bwObjectCL(junctions(i, 2), junctions(i, 1)) = 1; 
%     end
% %     else
% %         bwObjectCL(junctions(i, 2), junctions(i, 1)) = 0;         
% %     end
% end
% bwObjectCL = bwObjectCL > 0;
% % for i = 1:size(junctions, 1) 
% %     junctionNeighborhoodCoords = d+repmat(junctions(i, :),[8 1]);
% %     for j = 1:8
% %         if(junctionNeighborhoodCoords(j, 1) > 0 && junctionNeighborhoodCoords(j, 2) > 0 && junctionNeighborhoodCoords(j, 1) <= size(bwObjectSkeleton, 2) && junctionNeighborhoodCoords(j, 2) <= size(bwObjectSkeleton, 1))
% %             bwObjectCL{ii, jj}(junctionNeighborhoodCoords(j, 2), junctionNeighborhoodCoords(j,1)) = 0;         
% %         end
% %     end
% % end
% bwObjectCL = bwmorph(bwObjectCL, 'thin', 1);
% [~, endpoints, ~] = anaskel(bwObjectCL);

end
