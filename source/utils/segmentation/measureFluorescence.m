function [fluo_channel_int_pixelList] = measureFluorescence(fluo_channel_im, pixelList)
[n_rows, n_cols] = size(fluo_channel_im);
pixelsNum = size(pixelList, 1);
fluo_channel_int_pixelList = zeros(pixelsNum, 1);
for i = 1:pixelsNum
    if(pixelList(i, 2) > 0 && pixelList(i, 1) > 0 && pixelList(i, 2) <= n_rows && pixelList(i, 1) <= n_cols)
        fluo_channel_int_pixelList(i, 1) = fluo_channel_im(pixelList(i, 2), pixelList(i, 1));
    end
end