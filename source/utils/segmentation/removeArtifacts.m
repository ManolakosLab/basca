function newframe = removeArtifacts(frame, colonyTrajectoryThresh, clearMergingColonies)
% Removes incoming artifacts from the field of view.
% Incoming artifacts may be considered:
% 1. Colonies that come into after the 1-st frame.
% 2. Colonies that grow on the time-lapse movie's border.
% 3. Debrees that appear in one or more frame(s) (not previously removed by
%    preprocessing step).
% A valid colony must be tracked for more than <colonyTrajectoryThresh> frames
% started from the 1-st frame.
% newframe = removeArtifacts(frame, colonyTrajectoryThresh)
%
% INPUT :
%  frame : a struct containing the colonies of the movie organized
%  per frame. The colonies attributes of a frame are stored in colonyProps
%  struct array.
%
%  colonyTrajectoryThresh : an integer indicating the minimum
%  colony trajectory length.
%
%  clearMergingColonies : a boolean indicating whether to remove colonies
%  when they merge.
%
%
% OUTPUT :
%  newframe : the updated frame struct. The colonies considered as
%  artifacts are removed from the colonyProps struct array of each frame.
%
%
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
%
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.

%% Cleaning step
newframe = frame;
remove_ind = [];

%begin from 1-st frame
i = 1;
for j = 1:length(frame(i).colonyProps)
    correspondingColNextFrameInd = frame(i).colonyProps(j).correspondingColNextFrameInd;
    [nextColIndices] = getNextColonyIndices(frame, i, j, []);
    
    %cases
    %1. colonies (i.e. single-cells) start growing in the 1st frame on border
    %2. colonies (i.e. single-cells) start growing in the 1st frame and
    %   continue growing for less than <colonyTrajectoryThresh> frames
    if (frame(i).colonyProps(j).isOnBorder || isnan(correspondingColNextFrameInd) || size(nextColIndices, 1) <= colonyTrajectoryThresh)
        if ~isnan(correspondingColNextFrameInd)
            
            %if the colony did not merged with another colony in the next
            %frame, i.e. the colony corresponds only to one colony.
            if length(frame(i+1).colonyProps(correspondingColNextFrameInd).correspondingColPrevFrameInd) == 1
                remove_ind = [remove_ind; j];
                frame(i+1).colonyProps(correspondingColNextFrameInd).correspondingColPrevFrameInd = NaN;
                %probably useless case because a colony pointing to a colony in the next frame
                %will always be pointed back by the next frame's colony
            elseif isnan(frame(i+1).colonyProps(correspondingColNextFrameInd).correspondingColPrevFrameInd)
                remove_ind = [remove_ind; j];
            else
                %if the colony merged with one or more colony in the next
                %frame, it must be removed if and only if ALL the merging colonies
                %point back to nowhere. Otherwise, we need to keep the merging colonies
                %(only in the frame before merging), because they facilitate the
                %tracking of the merging colonies that have to be tracked
                merging_colonies_pointing_to_nan_count = 0;
                for k = 1:length(frame(i+1).colonyProps(correspondingColNextFrameInd).correspondingColPrevFrameInd)
                    prevFrameColInd = frame(i+1).colonyProps(correspondingColNextFrameInd).correspondingColPrevFrameInd(k);
                    
                    if sum(isnan(frame(i).colonyProps(prevFrameColInd).correspondingColPrevFrameInd)) ~=0
                        merging_colonies_pointing_to_nan_count = merging_colonies_pointing_to_nan_count + 1;
                    end
                end
                if merging_colonies_pointing_to_nan_count == length(frame(i+1).colonyProps(correspondingColNextFrameInd).correspondingColPrevFrameInd)
                    remove_ind = [remove_ind; j];
                    frame(i+1).colonyProps(correspondingColNextFrameInd).correspondingColPrevFrameInd = NaN;
                end
            end
        else
            remove_ind = [remove_ind; j];
        end
    end
end
if (~isempty(remove_ind))
    newframe(i).colonyProps(remove_ind) = [];
end
remove_ind_frame = [];
for i = 2:length(frame)
    remove_ind = [];
    for j = 1:length(frame(i).colonyProps)
        correspondingColNextFrameInd = frame(i).colonyProps(j).correspondingColNextFrameInd;
        if (isnan(frame(i).colonyProps(j).correspondingColPrevFrameInd))
            if ~isnan( correspondingColNextFrameInd )
                %i, j, correspondingColNextFrameInd
                if length(frame(i+1).colonyProps(correspondingColNextFrameInd).correspondingColPrevFrameInd) == 1
                    remove_ind = [remove_ind; j];
                    frame(i+1).colonyProps(correspondingColNextFrameInd).correspondingColPrevFrameInd = NaN;
                elseif isnan(frame(i+1).colonyProps(correspondingColNextFrameInd).correspondingColPrevFrameInd)
                    remove_ind = [remove_ind; j];
                else
                    merging_colonies_pointing_to_nan_count = 0;
                    for k = 1:length(frame(i+1).colonyProps(correspondingColNextFrameInd).correspondingColPrevFrameInd)
                        prevFrameColInd = frame(i+1).colonyProps(correspondingColNextFrameInd).correspondingColPrevFrameInd(k);
                        if sum(isnan(frame(i).colonyProps(prevFrameColInd).correspondingColPrevFrameInd)) ~=0
                            merging_colonies_pointing_to_nan_count = merging_colonies_pointing_to_nan_count + 1;
                        end
                    end
                    if merging_colonies_pointing_to_nan_count == length(frame(i+1).colonyProps(correspondingColNextFrameInd).correspondingColPrevFrameInd)
                        remove_ind = [remove_ind; j];
                        frame(i+1).colonyProps(correspondingColNextFrameInd).correspondingColPrevFrameInd = NaN;
                    end
                end
            else
                remove_ind = [remove_ind; j];
            end
        end
        if (length(frame(i).colonyProps(j).correspondingColPrevFrameInd) > 1)            
            if strcmp(clearMergingColonies, 'all')
                remove_ind = [remove_ind; j];
                for k = 1:length(frame(i).colonyProps(j).correspondingColPrevFrameInd)
                    prevFrameColInd = frame(i).colonyProps(j).correspondingColPrevFrameInd(k);
                    frame(i-1).colonyProps(prevFrameColInd).correspondingColNextFrameInd = NaN;
                end
                frame(i+1).colonyProps(correspondingColNextFrameInd).correspondingColPrevFrameInd = NaN;
            elseif strcmp(clearMergingColonies, 'colonyTrajectoryThresh')
                if (i > colonyTrajectoryThresh)
                    remove_ind = [remove_ind; j];
                    for k = 1:length(frame(i).colonyProps(j).correspondingColPrevFrameInd)
                        prevFrameColInd = frame(i).colonyProps(j).correspondingColPrevFrameInd(k);
                        frame(i-1).colonyProps(prevFrameColInd).correspondingColNextFrameInd = NaN;
                    end
                    if ~isnan(correspondingColNextFrameInd)
                        frame(i+1).colonyProps(correspondingColNextFrameInd).correspondingColPrevFrameInd = NaN;
                    end
                end
            end
        end
    end
    if (~isempty(remove_ind))
        newframe(i).colonyProps(remove_ind) = [];
    end
    if (isempty(newframe(i).colonyProps))
        remove_ind_frame = [remove_ind_frame; i];
    end
end
newframe(remove_ind_frame) = [];
%% Reassign each colony indices to the matching colony in the next and the previous frame.

for i = 2:length(newframe)
    numOfColoniesInPrevFrame = length(newframe(i-1).colonyProps);
    numOfColoniesInCurrFrame = length(newframe(i).colonyProps);
    newframe(i).colonyProps = rmfield(newframe(i).colonyProps, 'correspondingColPrevFrameInd');
    newframe(i).colonyProps = rmfield(newframe(i).colonyProps, 'correspondingColNextFrameInd');
    correspondanceMatrix = zeros(numOfColoniesInPrevFrame, numOfColoniesInCurrFrame);
    for ii = 1:numOfColoniesInPrevFrame
        for jj = 1:numOfColoniesInCurrFrame
            [centroid_x, centroid_y] = transformPointsForward(newframe(i).tform,newframe(i-1).colonyProps(ii).centroid(1, 1),newframe(i-1).colonyProps(ii).centroid(1, 2));
            
            if((newframe(i).colonyProps(jj).bBoxULCorner(1, 1)  <= centroid_x && ...
                    centroid_x <= newframe(i).colonyProps(jj).bBoxULCorner(1, 1) + newframe(i).colonyProps(jj).bBoxOffsets(1)) && ...
                    (newframe(i).colonyProps(jj).bBoxULCorner(1, 2)  <= centroid_y && ...
                    centroid_y <= newframe(i).colonyProps(jj).bBoxULCorner(1, 2) + newframe(i).colonyProps(jj).bBoxOffsets(2)))
                
                correspondanceMatrix(ii, jj) = newframe(i-1).colonyProps(ii).area/newframe(i).colonyProps(jj).area;
            end
        end
    end
    
    for ii = 1:numOfColoniesInPrevFrame
        [maxOverlap, maxOverlapInd] = max(correspondanceMatrix(ii, :));
        correspondanceMatrix(ii, :) = 0;
        correspondanceMatrix(ii, maxOverlapInd) = maxOverlap;
    end
    %assign each colony of the previous frame to correspond in one colony of
    %current frame.
    for ii = 1:numOfColoniesInPrevFrame
        newframe(i-1).colonyProps(ii).correspondingColNextFrameInd = NaN;
        index = find(correspondanceMatrix(ii, :) > 0);
        if(~isempty(index))
            if(length(index) > 1)
                [~, minIndex] = min(correspondanceMatrix(ii, :));
                newframe(i-1).colonyProps(ii).correspondingColNextFrameInd = minIndex;
            else
                newframe(i-1).colonyProps(ii).correspondingColNextFrameInd = index;
            end
        end
    end
    
    %assign each colony of current frame to correspond in one or more colonies of
    %previous frame.
    for ii = 1:size(correspondanceMatrix, 2)
        indices = find(correspondanceMatrix(:, ii) > 0);
        if ~isempty(indices)
            newframe(i).colonyProps(ii).correspondingColPrevFrameInd = indices;
        else
            newframe(i).colonyProps(ii).correspondingColPrevFrameInd = NaN;
        end
        newframe(i).colonyProps(ii).correspondingColNextFrameInd = NaN;
    end
    newframe(i).colonyCorrespondanceMatrix = correspondanceMatrix;
end
end