function [currFrameColoniesProps, prevFrameColoniesProps, correspondanceMatrix] = colonyExtractionAndMathcing(segmentation_im, bwColoniesMask, bwInitSegmentation, prevFrameColoniesProps, tform, minArea)
% Extract colonies and their attributes from the bw masks and match colonies between consecutive frames.  
% 
% [currFrameColoniesProps, prevFrameColoniesProps, correspondanceMatrix] = colonyExtractionAndMathcing(I, bwColoniesMask, bwColoniesObjects, prevFrameColoniesProps, areaThresh, clearMergingColonies)
%
% INPUT :
%  segmentaion_im : the grayscale image that is going to be preprocessed.
%  
%  bwColoniesMask : a bw image containing white pixels for micro-colonies
%  surfaces and black for background.
%
%  bwInitSegmentation : a bw image containing white pixels for
%  micro-colonies' objects' surfaces and black for background.
% 
%  prevFrameColoniesProps : a struct array containing the attributes of the
%  previous frame's colonies. The function uses this struct to match the
%  colonies of the current frame with the corresponding colonies of the
%  previous frame.
%
%  minArea : the minimum value allowed for an object's area so as
%  to be considered as a micro-colony. 
%
%  clearMergingColonies : a flag indicating whether to continue track
%  micro-colonies that merge with each other.
%  
% OUTPUT :
%  currFrameColoniesProps : a struct array containing the attributes of the
%  current frame's colonies.
%
%  prevFrameColoniesProps : a struct array containing the attributes of the
%  previous frame's colonies, with updated fields
%
%  correspondanceMatrix : a matrix containing the overlap scores of the
%  matched colonies
% 
%
%
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
% 
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.

%% colony extraction step
[x, y] = size(bwColoniesMask);
L = logical(bwColoniesMask);
objectsProps = regionprops(L, 'PixelList', 'Image', 'BoundingBox', 'Area', 'Centroid', 'MinorAxisLength', 'MajorAxisLength');
numOfColoniesInCurrFrame = 1;
for i = 1:size(objectsProps, 1)
    if(objectsProps(i, 1).Area > minArea)
        
        currFrameColoniesProps(numOfColoniesInCurrFrame).bBoxULCorner(1, :) = ceil(objectsProps(i, 1).BoundingBox(1:2));
        currFrameColoniesProps(numOfColoniesInCurrFrame).bBoxOffsets = objectsProps(i, 1).BoundingBox(3:4)-1;
                
        rows = currFrameColoniesProps(numOfColoniesInCurrFrame).bBoxULCorner(2):(currFrameColoniesProps(numOfColoniesInCurrFrame).bBoxULCorner(2)+currFrameColoniesProps(numOfColoniesInCurrFrame).bBoxOffsets(2));
        columns = currFrameColoniesProps(numOfColoniesInCurrFrame).bBoxULCorner(1):(currFrameColoniesProps(numOfColoniesInCurrFrame).bBoxULCorner(1)+currFrameColoniesProps(numOfColoniesInCurrFrame).bBoxOffsets(1));
        currFrameColoniesProps(numOfColoniesInCurrFrame).bwColony = bwInitSegmentation(rows, columns);
        currFrameColoniesProps(numOfColoniesInCurrFrame).bwColonyMask = logical(bwColoniesMask(rows, columns));
        currFrameColoniesProps(numOfColoniesInCurrFrame).IColony = segmentation_im(rows, columns);
        
        currFrameColoniesProps(numOfColoniesInCurrFrame).centroid(1, :) = objectsProps(i, 1).Centroid;
        currFrameColoniesProps(numOfColoniesInCurrFrame).pixelList = uint16(objectsProps(i, 1).PixelList);
        currFrameColoniesProps(numOfColoniesInCurrFrame).area = objectsProps(i, 1).Area;
        currFrameColoniesProps(numOfColoniesInCurrFrame).majorAxis = objectsProps(i, 1).MajorAxisLength;
        currFrameColoniesProps(numOfColoniesInCurrFrame).minorAxis = objectsProps(i, 1).MinorAxisLength;
        
        thisColonyProps = regionprops(logical(currFrameColoniesProps(numOfColoniesInCurrFrame).bwColonyMask), 'PixelList', 'Area');
        if(length(thisColonyProps) > 1)
            [~, index] = max([thisColonyProps(:).Area]);
            [cropped_x, cropped_y] = size(currFrameColoniesProps(numOfColoniesInCurrFrame).bwColony);
            
            bwColonyMask = createBW(cropped_x, cropped_y, thisColonyProps(index).PixelList);
            currFrameColoniesProps(numOfColoniesInCurrFrame).bwColony = zeros(cropped_x, cropped_y); %#ok<*AGROW>
            currFrameColoniesProps(numOfColoniesInCurrFrame).bwColonyMask = false(cropped_x, cropped_y);
            
            [row, column] = find(bwColonyMask);
            for ii = 1:length(row)
                currFrameColoniesProps(numOfColoniesInCurrFrame).bwColony(row(ii), column(ii)) =  bwInitSegmentation(row(ii) + currFrameColoniesProps(numOfColoniesInCurrFrame).bBoxULCorner(1, 2)-1, column(ii) + currFrameColoniesProps(numOfColoniesInCurrFrame).bBoxULCorner(1, 1)-1);
                currFrameColoniesProps(numOfColoniesInCurrFrame).bwColonyMask(row(ii), column(ii)) =  bwColoniesMask(row(ii) + currFrameColoniesProps(numOfColoniesInCurrFrame).bBoxULCorner(1, 2)-1, column(ii) + currFrameColoniesProps(numOfColoniesInCurrFrame).bBoxULCorner(1, 1)-1);
            end            
        end
        
        currFrameColoniesProps(numOfColoniesInCurrFrame).correspondingColNextFrameInd = NaN;
        currFrameColoniesProps(numOfColoniesInCurrFrame).isOnBorder = sum(sum(imclearborder(createBW(x, y, currFrameColoniesProps(numOfColoniesInCurrFrame).pixelList), 8))) == 0;
        
        if sum(sum(currFrameColoniesProps(numOfColoniesInCurrFrame).bwColony)) == 0 
            currFrameColoniesProps(numOfColoniesInCurrFrame) = [];
            continue;
        end
        
        L = bwlabel(currFrameColoniesProps(numOfColoniesInCurrFrame).bwColony, 4);
        props  = regionprops(L, 'PixelList','Centroid');
        currFrameColoniesProps(numOfColoniesInCurrFrame).objectProps = struct([]);
        counter = 1;
        for props_i = 1:length(props)
            if(length(props(props_i).PixelList) > minArea)
                intensity = zeros(size(props(props_i).PixelList, 1), 1);
                for ii = 1:length(intensity)
                    intensity(ii) = currFrameColoniesProps(numOfColoniesInCurrFrame).IColony(props(props_i).PixelList(ii, 2), props(props_i).PixelList(ii, 1));
                end
                medianIntensity = median(intensity);
                %%%%%% adjusted py panos
                if medianIntensity < max(median(currFrameColoniesProps(numOfColoniesInCurrFrame).IColony)) %%%%%
                    currFrameColoniesProps(numOfColoniesInCurrFrame).objectProps(counter, 1).pixelList = props(props_i).PixelList;
                    currFrameColoniesProps(numOfColoniesInCurrFrame).objectProps(counter, 1).centroid = props(props_i).Centroid;
                    counter = counter + 1;
                end
            end
        end  
        if isempty(currFrameColoniesProps(numOfColoniesInCurrFrame).objectProps)
            currFrameColoniesProps(numOfColoniesInCurrFrame) = [];
            continue;
        end        
        numOfColoniesInCurrFrame = numOfColoniesInCurrFrame + 1;
    end
end

%% Colonies Tracking step
% Create the correspondance matrix between the colonies of the current and
% previous frame.
if(isempty(prevFrameColoniesProps))
    numOfColoniesInPrevFrame = 0;
else
    numOfColoniesInPrevFrame = length(prevFrameColoniesProps); 
end
numOfColoniesInCurrFrame = length(currFrameColoniesProps);
overlapThresh = 0.85;
correspondanceMatrix = NaN(numOfColoniesInPrevFrame, numOfColoniesInCurrFrame);
for i = 1:numOfColoniesInPrevFrame
    for j = 1:numOfColoniesInCurrFrame
        [centroid_x, centroid_y] = transformPointsForward(tform,prevFrameColoniesProps(i).centroid(1, 1),prevFrameColoniesProps(i).centroid(1, 2));

        if((currFrameColoniesProps(j).bBoxULCorner(1, 1)  <= centroid_x && ...
                centroid_x <= currFrameColoniesProps(j).bBoxULCorner(1, 1) + currFrameColoniesProps(j).bBoxOffsets(1)) && ...
                (currFrameColoniesProps(j).bBoxULCorner(1, 2)  <= centroid_y && ...
                centroid_y <= currFrameColoniesProps(j).bBoxULCorner(1, 2) + currFrameColoniesProps(j).bBoxOffsets(2)))
            
            correspondanceMatrix(i, j) = prevFrameColoniesProps(i).area/currFrameColoniesProps(j).area;
        end
    end
end

for i = 1:numOfColoniesInPrevFrame
    [maxOverlap, maxOverlapInd] = max(correspondanceMatrix(i, :));
    correspondanceMatrix(i, :) = NaN;
    correspondanceMatrix(i, maxOverlapInd) = maxOverlap;
end

% removedColoniesIndices = [];
% if(numOfColoniesInPrevFrame)
%     %if merging is not allowed
%     if(clearMergingColonies)
%         for i = 1:numOfColoniesInCurrFrame
%             % remove a colony of the current frame if it corresponds to
%             % more than one colonies in the previous frame, i.e. merged
%             % colonies to one colony.
%             if(sum(correspondanceMatrix(:, i) > 0) > 1)
%                 removedColoniesIndices = [removedColoniesIndices i];
%                 
%             % remove a colony of the current frame if it corresponds to no
%             % colony in the previous frame, i.e. it is a colony that came into
%             % the field of view, or it is an artifact (e.g. debreee) that
%             % preprocessing step failed to remove.
%             elseif(sum(correspondanceMatrix(:, i) > 0) == 0 )
%                 removedColoniesIndices = [removedColoniesIndices i];
%                 
%             %remove a colony of the current frame if it corresponds to a colony in the previous frame,
%             %having very low overlap score. low overlap score implies that colony
%             %merged with at least one colony that left field of view in previous frame
%             %and just returned in it in this frame or a colony that had previously been merged
%             %and rejected and now merged again with this colony.
%             elseif(sum(correspondanceMatrix(:, i) > 0) == 1)
%                 index = correspondanceMatrix(:, i) > 0;
%                 if(correspondanceMatrix(index, i) < overlapThresh)
%                     removedColoniesIndices = [removedColoniesIndices i];
%                 end
%             end
%         end
%     end
% end
% correspondanceMatrix(:, removedColoniesIndices) = [];
% currFrameColoniesProps(removedColoniesIndices) = [];

%assign each colony of the previous frame to correspond in one colony of
%current frame.
for i = 1:numOfColoniesInPrevFrame
    index = find(correspondanceMatrix(i, :) > 0);
    if(isempty(index))
        prevFrameColoniesProps(i).correspondingColNextFrameInd = NaN;
    else
        prevFrameColoniesProps(i).correspondingColNextFrameInd = index;
    end
end

%assign each colony of current frame to correspond in one or more colonies of
%previous frame.
for i = 1:size(correspondanceMatrix, 2)
    indices = find(correspondanceMatrix(:, i) > 0);
    if isempty(indices)
        currFrameColoniesProps(i).correspondingColPrevFrameInd = NaN;
    else
        currFrameColoniesProps(i).correspondingColPrevFrameInd = indices;
    end
end

end