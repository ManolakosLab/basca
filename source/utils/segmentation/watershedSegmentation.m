function [W, watersheds] = watershedSegmentation(bwObject, x, y, d)
BW = logical(bwObject);
BWWithPad = padarray(BW,[1 1]);
D = -bwdist(~BWWithPad, d);
D = D(2:end-1,2:end-1);
D(~BW) = -Inf;
L = watershed(D);
bwObjPerimeter = bwperim(bwObject, 8);
for i = 1:x
    for j = 1:y
        if(bwObjPerimeter(i,j) == 1)
            L(i,j) = 0;
        end
    end
end
L = double(L);
L(L == 0) = -1;

temp = zeros(x, y);
for i = 1:x
    for j = 1:y
        if(bwObject(i,j) == 1)
            temp(i,j) = L(i, j);
        end
    end
end

L = temp;

[c, r] = find(L == -1);
boundaryPixels = [c r];

[c, r] = find(L > 0);
internalPixels = [c r];
L2 = L;
nn = knnsearch(internalPixels, boundaryPixels);
for i = 1:length(nn)
    L2(boundaryPixels(i, 1), boundaryPixels(i, 2)) = L(internalPixels(nn(i), 1), internalPixels(nn(i), 2));
end
L = L2;
W = zeros(x, y);
index = unique(L);
n = length(index);
watersheds = cell(n-1, 1);
for i = 2:n
    [c, r] = find(L == index(i));
    watersheds{i-1, 1} = [r, c];
    for ii = 1:size([r, c],1)
        W(c(ii), r(ii)) = i-1;
    end
end
end