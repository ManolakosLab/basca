function [distances, orientationAnglesMin, boundaryCoordsA2D, boundaryCoordsB2D] = meausureDistToBoundary2(bwObject, objCenterlineVertices, distance)

bwObjectPerim = bwperim(bwObject);
[xcoords, ycoords] = find(bwObjectPerim == true);
boundaryCoords  = [xcoords, ycoords];

[xcoords, ycoords] = find(bwObject == false);
backgroundCoords  = [xcoords, ycoords];

if(strcmp(distance, 'euclidean'))
    dd = 1.4142;
elseif(strcmp(distance, 'chessboard'))
    dd = 1;
elseif(strcmp(distance, 'manhattan'))
    dd = 2;
else
    dd = 1.4142;
end

centerlinePixelsNum = size(objCenterlineVertices, 1);
orientationAngles = zeros(centerlinePixelsNum, 4);
distances = zeros(centerlinePixelsNum, 4);
coordsToBoundaryA = zeros(centerlinePixelsNum, 4, 2);
coordsToBoundaryB = zeros(centerlinePixelsNum, 4, 2);
for i = 2:centerlinePixelsNum-1
    for j = 1:4
        if(j == 1)
            %distances(i, j) = -1;
            perpendicularDirectionA = 'N';
            perpendicularDirectionB = 'S';
            orientationAngles(i, j) = 90;
        elseif(j == 2)
            %distances(i, j) = -dd;
            perpendicularDirectionA = 'SE';
            perpendicularDirectionB = 'NW';
            orientationAngles(i, j) = 135;
        elseif(j == 3 )
            %distances(i, j) = -1;
            perpendicularDirectionA = 'E';
            perpendicularDirectionB = 'W';
            orientationAngles(i, j) = 0;
        else
            %distances(i, j) = -dd;
            perpendicularDirectionA = 'NE';
            perpendicularDirectionB = 'SW';
            orientationAngles(i, j) = 45;
        end
        coordsToBoundaryA(i, j, 1) = objCenterlineVertices(i, 1);
        coordsToBoundaryA(i, j, 2) = objCenterlineVertices(i, 2);
        if(coordsToBoundaryA(i, j, 2) < 1 || coordsToBoundaryA(i, j, 2) > size(bwObject, 2)); continue; end;
        if(coordsToBoundaryA(i, j, 1) < 1 || coordsToBoundaryA(i, j, 1) > size(bwObject, 1)); continue; end;
        
        while(1)
            if(isequal(perpendicularDirectionA, 'N'))
                coordsToBoundaryA(i, j, 1) = coordsToBoundaryA(i, j, 1)-1;
                distances(i, j) = distances(i, j) + 1;
            elseif(isequal(perpendicularDirectionA, 'NE'))
                coordsToBoundaryA(i, j, 2) = coordsToBoundaryA(i, j, 2)+1;
                coordsToBoundaryA(i, j, 1) = coordsToBoundaryA(i, j, 1)-1;
                distances(i, j) = distances(i, j) + dd ;
            elseif(isequal(perpendicularDirectionA, 'E'))
                coordsToBoundaryA(i, j, 2) = coordsToBoundaryA(i, j, 2)+1;
                distances(i, j) = distances(i, j) + 1;
            elseif(isequal(perpendicularDirectionA, 'SE'))
                coordsToBoundaryA(i, j, 2) = coordsToBoundaryA(i, j, 2)+1;
                coordsToBoundaryA(i, j, 1) = coordsToBoundaryA(i, j, 1)+1;
                distances(i, j) = distances(i, j) + dd;
            elseif(isequal(perpendicularDirectionA, 'S'))
                coordsToBoundaryA(i, j, 1) = coordsToBoundaryA(i, j, 1)+1;
                distances(i, j) = distances(i, j) + 1;
            elseif(isequal(perpendicularDirectionA, 'SW'))
                coordsToBoundaryA(i, j, 1) = coordsToBoundaryA(i, j, 1)+1;
                coordsToBoundaryA(i, j, 2) = coordsToBoundaryA(i, j, 2)-1;
                distances(i, j) = distances(i, j) + dd;
            elseif(isequal(perpendicularDirectionA, 'W'))
                coordsToBoundaryA(i, j, 2) = coordsToBoundaryA(i, j, 2)-1;
                distances(i, j) = distances(i, j) + 1;
            elseif(isequal(perpendicularDirectionA, 'NW'))
                coordsToBoundaryA(i, j, 1) = coordsToBoundaryA(i, j, 1)-1;
                coordsToBoundaryA(i, j, 2) = coordsToBoundaryA(i, j, 2)-1;
                distances(i, j) = distances(i, j) + dd;
            end
                        isOnBoundary = ismember([coordsToBoundaryA(i, j, 1) coordsToBoundaryA(i, j, 2)], boundaryCoords, 'rows');
            isOutside = ismember([coordsToBoundaryA(i, j, 1) coordsToBoundaryA(i, j, 2)], backgroundCoords, 'rows');
            if(isOnBoundary)
                if(j == 1)
                    distances(i, j) = distances(i, j) + 0.5;
                elseif(j == 2)
                    distances(i, j) = distances(i, j) + dd/2;
                elseif(j == 3 )
                    distances(i, j) = distances(i, j) + 0.5;
                else
                    distances(i, j) = distances(i, j) + dd/2;
                end
                break;
            end   
            if(isOutside && ~isOnBoundary)
                if(j == 1)
                    distances(i, j) = distances(i, j) - 0.5;
                elseif(j == 2)
                    distances(i, j) = distances(i, j) - dd/2;
                elseif(j == 3 )
                    distances(i, j) = distances(i, j) - 0.5;
                else
                    distances(i, j) = distances(i, j) - dd/2;
                end
                break;
            end
            if(coordsToBoundaryA(i, j, 2) < 1 || coordsToBoundaryA(i, j, 2) > size(bwObject, 2)); break; end;
            if(coordsToBoundaryA(i, j, 1) < 1 || coordsToBoundaryA(i, j, 1) > size(bwObject, 1)); break; end;            
        end
        
        coordsToBoundaryB(i, j, 1) = objCenterlineVertices(i, 1);
        coordsToBoundaryB(i, j, 2) = objCenterlineVertices(i, 2);
        if(coordsToBoundaryB(i, j, 2) < 1 || coordsToBoundaryB(i, j, 2) > size(bwObject, 2)); continue; end;
        if(coordsToBoundaryB(i, j, 1) < 1 || coordsToBoundaryB(i, j, 1) > size(bwObject, 1)); continue; end;
        
        while(1)

            if(isequal(perpendicularDirectionB, 'N'))
                coordsToBoundaryB(i, j, 1) = coordsToBoundaryB(i, j, 1)-1;
                distances(i, j) = distances(i, j) + 1;
            elseif(isequal(perpendicularDirectionB, 'NE'))
                coordsToBoundaryB(i, j, 2) = coordsToBoundaryB(i, j, 2) + 1;
                coordsToBoundaryB(i, j, 1) = coordsToBoundaryB(i, j, 1) - 1;
                distances(i, j) = distances(i, j) + dd;
            elseif(isequal(perpendicularDirectionB, 'E'))
                coordsToBoundaryB(i, j, 2) = coordsToBoundaryB(i, j, 2)+1;
                distances(i, j) = distances(i, j) + 1;
            elseif(isequal(perpendicularDirectionB, 'SE'))
                coordsToBoundaryB(i, j, 2) = coordsToBoundaryB(i, j, 2) + 1;
                coordsToBoundaryB(i, j, 1) = coordsToBoundaryB(i, j, 1) + 1;
                distances(i, j) = distances(i, j) + dd;
            elseif(isequal(perpendicularDirectionB, 'S'))
                coordsToBoundaryB(i, j, 1) = coordsToBoundaryB(i, j, 1)+1;
                distances(i, j) = distances(i, j) + 1;
            elseif(isequal(perpendicularDirectionB, 'SW'))
                coordsToBoundaryB(i, j, 1) = coordsToBoundaryB(i, j, 1) + 1;
                coordsToBoundaryB(i, j, 2) = coordsToBoundaryB(i, j, 2) - 1;
                distances(i, j) = distances(i, j) + dd;
            elseif(isequal(perpendicularDirectionB, 'W'))
                coordsToBoundaryB(i, j, 2) = coordsToBoundaryB(i, j, 2) - 1;
                distances(i, j) = distances(i, j) + 1;
            elseif(isequal(perpendicularDirectionB, 'NW'))
                coordsToBoundaryB(i, j, 1) = coordsToBoundaryB(i, j, 1) - 1;
                coordsToBoundaryB(i, j, 2) = coordsToBoundaryB(i, j, 2) - 1;
                distances(i, j) = distances(i, j) + dd;
            end
            isOnBoundary = ismember([coordsToBoundaryB(i, j, 1) coordsToBoundaryB(i, j, 2)], boundaryCoords, 'rows');
            isOutside = ismember([coordsToBoundaryB(i, j, 1) coordsToBoundaryB(i, j, 2)], backgroundCoords, 'rows');
            if(isOnBoundary == 1)
                if(j == 1)
                    distances(i, j) = distances(i, j) + 0.5;
                elseif(j == 2)
                    distances(i, j) = distances(i, j) + dd/2;
                elseif(j == 3 )
                    distances(i, j) = distances(i, j) + 0.5;
                else
                    distances(i, j) = distances(i, j) + dd/2;
                end
                break;
            end   
            if(isOutside && ~isOnBoundary)
                if(j == 1)
                    distances(i, j) = distances(i, j) - 0.5;
                elseif(j == 2)
                    distances(i, j) = distances(i, j) - dd/2;
                elseif(j == 3 )
                    distances(i, j) = distances(i, j) - 0.5;
                else
                    distances(i, j) = distances(i, j) - dd/2;
                end
                break;
            end
            if(coordsToBoundaryB(i, j, 2) < 1 || coordsToBoundaryB(i, j, 2) > size(bwObject, 2)); break; end;
            if(coordsToBoundaryB(i, j, 1) < 1 || coordsToBoundaryB(i, j, 1) > size(bwObject, 1)); break; end;
            
        end
    end
end
[distances, minIndices] = min(distances, [], 2);
orientationAnglesMin = zeros(centerlinePixelsNum, 1);
for i = 1:centerlinePixelsNum
    orientationAnglesMin(i) = orientationAngles(i, minIndices(i));
end
boundaryCoordsA2D = zeros(size(coordsToBoundaryA, 1), 2);
for i = 1:size(coordsToBoundaryA, 1)
    boundaryCoordsA2D(i, 1) = coordsToBoundaryA(i, minIndices(i), 1);
    boundaryCoordsA2D(i, 2) = coordsToBoundaryA(i, minIndices(i), 2);
end
boundaryCoordsB2D = zeros(size(coordsToBoundaryB, 1), 2);
for i = 1:size(coordsToBoundaryB, 1)
    boundaryCoordsB2D(i, 1) = coordsToBoundaryB(i, minIndices(i), 1);
    boundaryCoordsB2D(i, 2) = coordsToBoundaryB(i, minIndices(i), 2);
end