function [objectAngles, isCollinearBool] = computeObjectAngles(y, objectSimplifiedCLVertices, angleThresh)
if(size(objectSimplifiedCLVertices, 1) == 2)
    objectAngles = 180;
    isCollinearBool = 1;
    return;
end
objectAngles = zeros(size(objectSimplifiedCLVertices, 1)-2, 1);
isCollinearBool = zeros(size(objectSimplifiedCLVertices, 1)-2, 1);

for i = 2:size(objectSimplifiedCLVertices,1)-1
    M = [ones(3, 1) objectSimplifiedCLVertices([i-1 i i+1], 2) y-objectSimplifiedCLVertices([i-1 i i+1], 1)];
%    figure;
%    plot(objectSimplifiedCLVertices([i-1 i i+1], 2), y-objectSimplifiedCLVertices([i-1 i i+1], 1), '-b');
%    
    if(det(M) >= 0)
        angles = TRIangles([objectSimplifiedCLVertices([i-1 i i+1], 2) y-objectSimplifiedCLVertices([i-1 i i+1], 1)]);
    else
        angles = TRIangles([objectSimplifiedCLVertices([i+1 i i-1], 2) y-objectSimplifiedCLVertices([i+1 i i-1], 1)]);
    end
    objectAngles(i-1) = angles(2);
    if(objectAngles(i-1) <= angleThresh)
        isCollinearBool(i-1) = 0;
    else
        isCollinearBool(i-1) = 1;
    end
end


end