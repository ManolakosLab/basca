function [localMinimumInd, localMinimumRatio] = findLocalMinimaSides2(distanceToBoundary, thresh, leftWindowSize, rightWindowSize,  leftCentroidInd, rightCentroidInd, verb)


graphLength = size(distanceToBoundary, 1);
if((leftWindowSize + rightWindowSize) >= graphLength)
    leftWindowSize = floor(graphLength/2)-1;
    rightWindowSize = floor(graphLength/2)-1;    
end

if(mod(leftWindowSize+rightWindowSize, 2) == 0)
    windowSize = leftWindowSize+rightWindowSize;
else
    windowSize = leftWindowSize+rightWindowSize-1;
end

centerlinePixelsNum = size(distanceToBoundary, 1);
x = 1:graphLength;
y = distanceToBoundary;
[ymax, xmax, ymin, xmin] = extrema(y);

if(verb == 1)
    figure;
    plot(x, y, '-b');
    hold on; plot(x(xmax),ymax,'r*',x(xmin),ymin,'g*'); hold off
end


c_i = 1;
localMinimumInd = [];
localMinimumRatio = [];
for k = 1:length(xmin)
    if((xmin(k) ~= 1 && xmin(k) ~= graphLength) && (xmin(k) >= leftCentroidInd && xmin(k) <=  rightCentroidInd))
        
        localMinimumIndex = xmin(k);
        localMinimum = y(xmin(k));
        
        if(localMinimumIndex > leftWindowSize && localMinimumIndex < centerlinePixelsNum-rightWindowSize)
            windowIndices = -leftWindowSize:1:rightWindowSize;
        elseif(localMinimumIndex <= leftWindowSize)
            windowIndices = -localMinimumIndex+1:windowSize-localMinimumIndex+1;
        else
            windowIndices = -windowSize+(centerlinePixelsNum-localMinimumIndex):(centerlinePixelsNum-localMinimumIndex);
        end
        ind = find(windowIndices == 0);
        windowIndices = windowIndices+localMinimumIndex;
        windowDistances = y(windowIndices);
        leftLocalMaximum = max(windowDistances(1:ind-1));
        rightLocalMaximum = max(windowDistances(ind+1:end));
        if(isempty(localMinimum) || isempty(rightLocalMaximum))
        end
        rratio = localMinimum/rightLocalMaximum; rratio = round(rratio*100)/100;
        lratio = localMinimum/leftLocalMaximum; lratio = round(lratio*100)/100;
        if(~isempty(rratio) && ~isempty(lratio))
            if(rratio < thresh && lratio < thresh)
                localMinimumInd(c_i) = localMinimumIndex;
                localMinimumRatio(c_i) = (rratio+lratio)/2;
                if(verb == 1)
                    fprintf('\nWindow size: %4.2f\n', leftWindowSize + rightWindowSize);
                    fprintf('Right ratio: %4.2f\nLeft ratio: %4.2f\nSplit point: %d\n\n', rratio, lratio, localMinimumInd(c_i));
                end
                c_i = c_i + 1;
            end
        end
    end
end
nearLocalMin = zeros(length(localMinimumInd), length(localMinimumInd));
for i = 1:length(localMinimumInd)
    for j = i+1:length(localMinimumInd)
        if(abs(localMinimumInd(i) - localMinimumInd(j)) <= (leftWindowSize+rightWindowSize))
            nearLocalMin(i, j) = 1;
            nearLocalMin(j, i) = 1;
        end
    end
end
uLocalMinima = [];
for i = 1:size(nearLocalMin)
    if(sum(nearLocalMin(i, :)) > 0)
        ind = nearLocalMin(i, :) == 1;
        ind(i) = true;
        uLocalMinima = [uLocalMinima; floor(median(localMinimumInd(ind)))];
    else
        uLocalMinima = [uLocalMinima; localMinimumInd(i)];
    end
end
uLocalMinima = unique(uLocalMinima);

end