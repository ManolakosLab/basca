function [y] = minMaxTransform(im, min_, max_)
y = double(im);

if length(size(y)) == 2
    l = min(min(y));
    h = max(max(y));
    y = ((y - l)/(h-l))*max_+min_;
    return;
end

if length(size(y)) == 3
    
    for i = 1:3
        l = min(min(y(:, :, i)));
        h = max(max(y(:, :, i)));
        y(:, :, i) = ((y(:, :, i) - l)/(h-l))*max_(i)+min_(i);
    end
end

