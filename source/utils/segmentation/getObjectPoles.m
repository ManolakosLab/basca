function [coords2PoleA, segLen2PoleA, coords2PoleB, segLen2PoleB] = getObjectPoles(bwObject, objectCLVerticesWithoutPoles, distance, verb)
[x, y] = size(bwObject);
bwObjectPerim = bwperim(bwObject);
boundaryCoords = bwboundaries(bwObjectPerim);
boundaryCoords  = boundaryCoords{1};

coords2PoleB = [];
segLen2PoleB = [];

if(strcmp(distance, 'euclidean'))
    dd = 1.4142;
elseif(strcmp(distance, 'chessboard'))
    dd = 1;
elseif(strcmp(distance, 'manhattan'))
    dd = 2;
else
    dd = 1.4142;
end

d = [ -1 0; -1 1; 0 1; 1 1; 1 0;  1 -1;  0 -1; -1 -1];
dpos = [1 2; 1 3; 2 3; 3 3; 3 2; 3 1; 2 1; 1 1];


[objectSimplifiedCLVertices, ~] = dpsimplify(objectCLVerticesWithoutPoles, 1.4142);
if(size(objectSimplifiedCLVertices, 1) == 1)
    objectSimplifiedCLVertices = objectCLVerticesWithoutPoles;
end

numOfPixels = size(objectCLVerticesWithoutPoles, 1);
segLenCenterLine = zeros(numOfPixels-1, 1);
for i = 1:numOfPixels
    if(i ~= 1 && i ~= length(objectCLVerticesWithoutPoles))
        if(strcmp(distance, 'euclidean'))
            segLenCenterLine(i) = norm(objectCLVerticesWithoutPoles(i, :)-objectCLVerticesWithoutPoles(i+1, :), 2);
        elseif(strcmp(distance, 'chessboard'))
            segLenCenterLine(i) = norm(objectCLVerticesWithoutPoles(i, :)-objectCLVerticesWithoutPoles(i+1, :), Inf);
        elseif(strcmp(distance, 'manhattan'))
            segLenCenterLine(i) = norm(objectCLVerticesWithoutPoles(i, :)-objectCLVerticesWithoutPoles(i+1, :), 1);
        else
            segLenCenterLine(i) = norm(objectCLVerticesWithoutPoles(i, :)-objectCLVerticesWithoutPoles(i+1, :));
        end
    elseif(i == 1)
        
        windowSize = norm(objectSimplifiedCLVertices(1, :)-objectSimplifiedCLVertices(2, :), Inf);
        windowIndices = 1:windowSize+1;
        
        segmentCoords = objectCLVerticesWithoutPoles(windowIndices, :);
        
        %find the orientation of the segments centerline
        bwSegmentCL = createBW(x, y, [segmentCoords(:, 2), segmentCoords(:, 1)]);
        segmentProps = regionprops(bwSegmentCL, 'Orientation');
        segmentOrientation = segmentProps.Orientation;
        
        neighbors = d+repmat(objectCLVerticesWithoutPoles(i, :),[8 1]);
        nhoodDist = zeros(3, 3);
        for ii = 1:8
            if(ismember(neighbors(ii, :), objectCLVerticesWithoutPoles, 'rows'))
                nhoodDist(dpos(ii, 1), dpos(ii, 2)) = 1;
            end
        end
        if(segmentOrientation < 0)
            angle = 360 + segmentOrientation;
            angle = mod(angle, 180);
        else
            angle = segmentOrientation;
        end
        if(strcmp(distance, 'euclidean'))
            segLenCenterLine(i) = norm(objectCLVerticesWithoutPoles(i, :)-objectCLVerticesWithoutPoles(i+1, :), 2);
        elseif(strcmp(distance, 'chessboard'))
            segLenCenterLine(i) = norm(objectCLVerticesWithoutPoles(i, :)-objectCLVerticesWithoutPoles(i+1, :), Inf);
        elseif(strcmp(distance, 'manhattan'))
            segLenCenterLine(i) = norm(objectCLVerticesWithoutPoles(i, :)-objectCLVerticesWithoutPoles(i+1, :), 1);
        else
            segLenCenterLine(i) = norm(objectCLVerticesWithoutPoles(i, :)-objectCLVerticesWithoutPoles(i+1, :), 2);
        end
        coords2PoleA = [];
        segLen2PoleA = [];
        dir2PoleA = 'NaN';
        if(0 <= angle && angle <= 20)
            [coords2PoleDir1, segLen2PoleDir1] = moveToPoleA(objectCLVerticesWithoutPoles, i, 'W', boundaryCoords, segLenCenterLine, dd);
            [coords2PoleDir2, segLen2PoleDir2] = moveToPoleA(objectCLVerticesWithoutPoles, i, 'E', boundaryCoords, segLenCenterLine, dd);
            if(size(coords2PoleDir1, 1) < size(coords2PoleDir2, 1))
                if(~isempty(coords2PoleDir1))
                    if(sum(ismember(objectCLVerticesWithoutPoles, coords2PoleDir1, 'rows')) == 0)
                        coords2PoleA = coords2PoleDir1;
                        segLen2PoleA = segLen2PoleDir1;
                        dir2PoleA = 'W';
                    end
                end
            else
                if(~isempty(coords2PoleDir2))
                    if(sum(ismember(objectCLVerticesWithoutPoles, coords2PoleDir2, 'rows')) == 0)
                        coords2PoleA = coords2PoleDir2;
                        segLen2PoleA = segLen2PoleDir2;
                        dir2PoleA = 'E';
                    end
                end
            end
        elseif(20 < angle && angle <= 70)
            [coords2PoleDir1, segLen2PoleDir1] = moveToPoleA(objectCLVerticesWithoutPoles, i, 'SW', boundaryCoords, segLenCenterLine, dd);
            [coords2PoleDir2, segLen2PoleDir2] = moveToPoleA(objectCLVerticesWithoutPoles, i, 'NE', boundaryCoords, segLenCenterLine, dd);
            if(size(coords2PoleDir1, 1) < size(coords2PoleDir2, 1))
                if(~isempty(coords2PoleDir1))
                    if(sum(ismember(objectCLVerticesWithoutPoles, coords2PoleDir1, 'rows')) == 0)
                        coords2PoleA = coords2PoleDir1;
                        segLen2PoleA = segLen2PoleDir1;
                        dir2PoleA = 'SW';
                    end
                end
            else
                if(~isempty(coords2PoleDir2))
                    if(sum(ismember(objectCLVerticesWithoutPoles, coords2PoleDir2, 'rows')) == 0)
                        coords2PoleA = coords2PoleDir2;
                        segLen2PoleA = segLen2PoleDir2;
                        dir2PoleA = 'NE';
                    end
                end
            end
        elseif( 70 < angle && angle <= 110)
            [coords2PoleDir1, segLen2PoleDir1] = moveToPoleA(objectCLVerticesWithoutPoles, i, 'S', boundaryCoords, segLenCenterLine, dd);
            [coords2PoleDir2, segLen2PoleDir2] = moveToPoleA(objectCLVerticesWithoutPoles, i, 'N', boundaryCoords, segLenCenterLine, dd);
            if(size(coords2PoleDir1, 1) < size(coords2PoleDir2, 1))
                if(~isempty(coords2PoleDir1))
                    if(sum(ismember(objectCLVerticesWithoutPoles, coords2PoleDir1, 'rows')) == 0)
                        coords2PoleA = coords2PoleDir1;
                        segLen2PoleA = segLen2PoleDir1;
                        dir2PoleA = 'S';
                    end
                end
            else
                if(~isempty(coords2PoleDir2))
                    if(sum(ismember(objectCLVerticesWithoutPoles, coords2PoleDir2, 'rows')) == 0)
                        coords2PoleA = coords2PoleDir2;
                        segLen2PoleA = segLen2PoleDir2;
                        dir2PoleA = 'N';
                    end
                end
            end
        elseif(110 < angle && angle <= 160)
            [coords2PoleDir1, segLen2PoleDir1] = moveToPoleA(objectCLVerticesWithoutPoles, i, 'NW', boundaryCoords, segLenCenterLine, dd);
            [coords2PoleDir2, segLen2PoleDir2] = moveToPoleA(objectCLVerticesWithoutPoles, i, 'SE', boundaryCoords, segLenCenterLine, dd);
            if(size(coords2PoleDir1, 1) < size(coords2PoleDir2, 1))
                if(~isempty(coords2PoleDir1))
                    if(sum(ismember(objectCLVerticesWithoutPoles, coords2PoleDir1, 'rows')) == 0)
                        coords2PoleA = coords2PoleDir1;
                        segLen2PoleA = segLen2PoleDir1;
                        dir2PoleA = 'NW';
                    end
                end
            else
                if(~isempty(coords2PoleDir2))
                    if(sum(ismember(objectCLVerticesWithoutPoles, coords2PoleDir2, 'rows')) == 0)
                        coords2PoleA = coords2PoleDir2;
                        segLen2PoleA = segLen2PoleDir2;
                        dir2PoleA = 'SE';
                    end
                end
            end
        elseif(160 < angle && angle <= 180)
            [coords2PoleDir1, segLen2PoleDir1] = moveToPoleA(objectCLVerticesWithoutPoles, i, 'W', boundaryCoords, segLenCenterLine, dd);
            [coords2PoleDir2, segLen2PoleDir2] = moveToPoleA(objectCLVerticesWithoutPoles, i, 'E', boundaryCoords, segLenCenterLine, dd);
            if(size(coords2PoleDir1, 1) < size(coords2PoleDir2, 1))
                if(~isempty(coords2PoleDir1))
                    if(sum(ismember(objectCLVerticesWithoutPoles, coords2PoleDir1, 'rows')) == 0)
                        coords2PoleA = coords2PoleDir1;
                        segLen2PoleA = segLen2PoleDir1;
                        dir2PoleA = 'W';
                    end
                end
            else
                if(~isempty(coords2PoleDir2))
                    if(sum(ismember(objectCLVerticesWithoutPoles, coords2PoleDir2, 'rows')) == 0)
                        coords2PoleA = coords2PoleDir2;
                        segLen2PoleA = segLen2PoleDir2;
                        dir2PoleA = 'E';
                    end
                end
            end
        end
        if(verb)
            fprintf('From (%d, %d) to Pole A -> %s (phi = %5.2f)\n', segmentCoords(1, 2), segmentCoords(1, 1), dir2PoleA, angle);
        end
        
        
        
    elseif(i == length(objectCLVerticesWithoutPoles))
        windowSize = norm(objectSimplifiedCLVertices(end-1, :)-objectSimplifiedCLVertices(end, :), Inf);
        windowIndices = size(objectCLVerticesWithoutPoles, 1)-windowSize:size(objectCLVerticesWithoutPoles, 1);
        
        segmentCoords = objectCLVerticesWithoutPoles(windowIndices, :);
        bwSegmentCL = createBW(x, y, [segmentCoords(:, 2), segmentCoords(:, 1)]);
        segmentProps = regionprops(bwSegmentCL, 'Orientation');
        segmentOrientation = segmentProps.Orientation;
        neighbors = d+repmat(objectCLVerticesWithoutPoles(i, :),[8 1]);
        nhoodDist = zeros(3, 3);
        for ii = 1:8
            if(ismember(neighbors(ii, :), objectCLVerticesWithoutPoles, 'rows'))
                nhoodDist(dpos(ii, 1), dpos(ii, 2)) = 1;
            end
        end
        
        if(segmentOrientation < 0)
            angle = 360 + segmentOrientation;
            angle = mod(angle, 180);
        else
            angle = segmentOrientation;
        end

        coords2PoleB = [];
        segLen2PoleB = [];
        dir2PoleB = 'NaN';
        if(0 <= angle && angle <= 20)
            [coords2PoleDir1, segLen2PoleDir1] = moveToPoleB(objectCLVerticesWithoutPoles, i, 'W', boundaryCoords, segLenCenterLine, dd);
            [coords2PoleDir2, segLen2PoleDir2] = moveToPoleB(objectCLVerticesWithoutPoles, i, 'E', boundaryCoords, segLenCenterLine, dd);
            if(size(coords2PoleDir1, 1) < size(coords2PoleDir2, 1))
                if(~isempty(coords2PoleDir1))
                    if(sum(ismember(objectCLVerticesWithoutPoles, coords2PoleDir1, 'rows')) == 0)
                        coords2PoleB = coords2PoleDir1;
                        segLen2PoleB = segLen2PoleDir1;
                        dir2PoleB = 'W';
                    end
                end
            else
                if(~isempty(coords2PoleDir2))
                    if(sum(ismember(objectCLVerticesWithoutPoles, coords2PoleDir2, 'rows')) == 0)
                        coords2PoleB = coords2PoleDir2;
                        segLen2PoleB = segLen2PoleDir2;
                        dir2PoleB = 'E';
                    end
                end
            end
        elseif(20 < angle && angle <= 70)
            [coords2PoleDir1, segLen2PoleDir1] = moveToPoleB(objectCLVerticesWithoutPoles, i, 'SW', boundaryCoords, segLenCenterLine, dd);
            [coords2PoleDir2, segLen2PoleDir2] = moveToPoleB(objectCLVerticesWithoutPoles, i, 'NE', boundaryCoords, segLenCenterLine, dd);
            if(size(coords2PoleDir1, 1) < size(coords2PoleDir2, 1))
                if(~isempty(coords2PoleDir1))
                    if(sum(ismember(objectCLVerticesWithoutPoles, coords2PoleDir1, 'rows')) == 0)
                        coords2PoleB = coords2PoleDir1;
                        segLen2PoleB = segLen2PoleDir1;
                        dir2PoleB = 'SW';
                    end
                end
            else
                if(~isempty(coords2PoleDir2))
                    if(sum(ismember(objectCLVerticesWithoutPoles, coords2PoleDir2, 'rows')) == 0)
                        coords2PoleB = coords2PoleDir2;
                        segLen2PoleB = segLen2PoleDir2;
                        dir2PoleB = 'NE';
                    end
                end
            end
        elseif( 70 < angle && angle <= 110)
            [coords2PoleDir1, segLen2PoleDir1] = moveToPoleB(objectCLVerticesWithoutPoles, i, 'S', boundaryCoords, segLenCenterLine, dd);
            [coords2PoleDir2, segLen2PoleDir2] = moveToPoleB(objectCLVerticesWithoutPoles, i, 'N', boundaryCoords, segLenCenterLine, dd);
            if(size(coords2PoleDir1, 1) < size(coords2PoleDir2, 1))
                if(~isempty(coords2PoleDir1))
                    if(sum(ismember(objectCLVerticesWithoutPoles, coords2PoleDir1, 'rows')) == 0)
                        coords2PoleB = coords2PoleDir1;
                        segLen2PoleB = segLen2PoleDir1;
                        dir2PoleB = 'S';
                    end
                end
            else
                if(~isempty(coords2PoleDir2))
                    if(sum(ismember(objectCLVerticesWithoutPoles, coords2PoleDir2, 'rows')) == 0)
                        coords2PoleB = coords2PoleDir2;
                        segLen2PoleB = segLen2PoleDir2;
                        dir2PoleB = 'N';
                    end
                end
            end
        elseif(110 < angle && angle <= 160)
            [coords2PoleDir1, segLen2PoleDir1] = moveToPoleB(objectCLVerticesWithoutPoles, i, 'NW', boundaryCoords, segLenCenterLine, dd);
            [coords2PoleDir2, segLen2PoleDir2] = moveToPoleB(objectCLVerticesWithoutPoles, i, 'SE', boundaryCoords, segLenCenterLine, dd);
            if(size(coords2PoleDir1, 1) < size(coords2PoleDir2, 1))
                if(~isempty(coords2PoleDir1))
                    if(sum(ismember(objectCLVerticesWithoutPoles, coords2PoleDir1, 'rows')) == 0)
                        coords2PoleB = coords2PoleDir1;
                        segLen2PoleB = segLen2PoleDir1;
                        dir2PoleB = 'NW';
                    end
                end
            else
                if(~isempty(coords2PoleDir2))
                    if(sum(ismember(objectCLVerticesWithoutPoles, coords2PoleDir2, 'rows')) == 0)
                        coords2PoleB = coords2PoleDir2;
                        segLen2PoleB = segLen2PoleDir2;
                        dir2PoleB = 'SE';
                    end
                end
            end
        elseif(160 < angle && angle <= 180)
            [coords2PoleDir1, segLen2PoleDir1] = moveToPoleB(objectCLVerticesWithoutPoles, i, 'W', boundaryCoords, segLenCenterLine, dd);
            [coords2PoleDir2, segLen2PoleDir2] = moveToPoleB(objectCLVerticesWithoutPoles, i, 'E', boundaryCoords, segLenCenterLine, dd);
            if(size(coords2PoleDir1, 1) < size(coords2PoleDir2, 1))
                if(~isempty(coords2PoleDir1))
                    if(sum(ismember(objectCLVerticesWithoutPoles, coords2PoleDir1, 'rows')) == 0)
                        coords2PoleB = coords2PoleDir1;
                        segLen2PoleB = segLen2PoleDir1;
                        dir2PoleB = 'W';
                    end
                end
            else
                if(~isempty(coords2PoleDir2))
                    if(sum(ismember(objectCLVerticesWithoutPoles, coords2PoleDir2, 'rows')) == 0)
                        coords2PoleB = coords2PoleDir2;
                        segLen2PoleB = segLen2PoleDir2;
                        dir2PoleB = 'E';
                    end
                end
            end
        end
        if(verb)
            fprintf('From (%d, %d) to Pole B -> %s (phi = %5.2f)\n', segmentCoords(end, 2), segmentCoords(end, 1), dir2PoleB, angle);
        end
    end
end

