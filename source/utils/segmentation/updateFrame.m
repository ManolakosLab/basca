function [frame] = updateFrame(frame, cellProps, objectSegmentationTime, ijk)
% Updates the frame struct with cellProps extracted from object Segmentation step.
%
%
%  INPUT :
%    frame : a struct array containing the objects generated from 
%    colonyPartition step.
%
%    cellProps : a cell array with structs containing the extracted cell
%    instant properties.
%   
%    objectSegmentationTime: segmentation time for each object.
%
%    ijk : a 3-valued vector indexing an object in the frame struct
%    [frameInd, colonyInd, objectInd].
%
%  OUTPUT :
%    frame : the updated struct array containing the cellProps grouped by
%    frame and colony.
%   
%  Examples:
%     >> frame = updateFrame(frame, cellProps, objectSegmentationTime, ijk);
%
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
% 
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.
warning('off', 'MATLAB:table:RowsAddedExistingVars');

for i = 1:length(frame)
    frame(i).segmentationTime = 0;
    for j = 1:length(frame(i).colonyProps)
        
        frame(i).colonyProps(j).cellProps = table([]);
        
        frame(i).colonyProps(j).segmentationTime = 0;
        for k = 1:length(frame(i).colonyProps(j).objectProps)
            
            
            ind = ismember(ijk(:, 1:3), [i, j, k],'rows');
            if(~isempty(ind))
                frame(i).colonyProps(j).objectProps(k).segmentationTime = objectSegmentationTime(ind);
                
                for l = 1:length(cellProps{ind})
                    if(size(cellProps{ind}(l).pixelList, 1) > 1)
                        cellId = sprintf('x%07d_y%07d', ...
                            round((cellProps{ind}(l).centroid(1, 1)+frame(i).colonyProps(j).bBoxULCorner(1, 1))*1000) , ...
                            round((cellProps{ind}(l).centroid(1, 2)+frame(i).colonyProps(j).bBoxULCorner(1, 2))*1000));
                        
                        frame(i).colonyProps(j).cellProps(cellId, 'centroid') = {{cellProps{ind}(l).centroid}};
                        frame(i).colonyProps(j).cellProps(cellId, 'centralPixel') = {{cellProps{ind}(l).centralPixel}};
                        frame(i).colonyProps(j).cellProps(cellId, 'pixelList') = {{cellProps{ind}(l).pixelList}};
                        frame(i).colonyProps(j).cellProps(cellId, 'boundaryPixelList') = {{cellProps{ind}(l).boundaryPixelList}};
                        
                        frame(i).colonyProps(j).cellProps(cellId, 'has_irregular_shape') = {cellProps{ind}(l).hasIrregularShape};                       
                        frame(i).colonyProps(j).cellProps(cellId, 'width_in_pixels') = {cellProps{ind}(l).width};
                        frame(i).colonyProps(j).cellProps(cellId, 'width_in_pixels_chessboard') = {cellProps{ind}(l).widthInPixels};
                        frame(i).colonyProps(j).cellProps(cellId, 'length_in_pixels') = {cellProps{ind}(l).length};
                        frame(i).colonyProps(j).cellProps(cellId, 'length_in_pixels_chessboard') = {cellProps{ind}(l).lengthInPixels};
                        frame(i).colonyProps(j).cellProps(cellId, 'minorAxis') = {cellProps{ind}(l).minorAxis};
                        frame(i).colonyProps(j).cellProps(cellId, 'majorAxis') = {cellProps{ind}(l).majorAxis};
                        frame(i).colonyProps(j).cellProps(cellId, 'eccentricity') = {cellProps{ind}(l).eccentricity};
                        frame(i).colonyProps(j).cellProps(cellId, 'orientation') = {cellProps{ind}(l).orientation};
                        frame(i).colonyProps(j).cellProps(cellId, 'solidity') = {cellProps{ind}(l).solidity};
                        frame(i).colonyProps(j).cellProps(cellId, 'covariance') = {{cellProps{ind}(l).covariance}};
                        
                        for channel_i = 1:length(frame(i).fluo_channels_thresh)
                            [fluo_channel_int_pixelList] = measureFluorescence(frame(i).fluo_channels_im{channel_i}, cellProps{ind}(l).pixelList+uint16([ones(size(cellProps{ind}(l).pixelList, 1), 1)*frame(i).colonyProps(j).bBoxULCorner(1) ones(size(cellProps{ind}(l).pixelList, 1), 1)*frame(i).colonyProps(j).bBoxULCorner(2)]));
                            frame(i).colonyProps(j).cellProps(cellId, sprintf('fluo_pixelList_channel%d', channel_i )) = {{fluo_channel_int_pixelList}};
                            frame(i).colonyProps(j).cellProps(cellId, sprintf('fluo_int_channel%d', channel_i)) = {mean(fluo_channel_int_pixelList(fluo_channel_int_pixelList>frame(i).fluo_channels_thresh(channel_i)))};
                            frame(i).colonyProps(j).cellProps(cellId, sprintf('fluo_cov_channel%d', channel_i)) = {sum(fluo_channel_int_pixelList>frame(i).fluo_channels_thresh(channel_i))/length(fluo_channel_int_pixelList)};
                        end
                    end
                end
                
                frame(i).colonyProps(j).segmentationTime = frame(i).colonyProps(j).segmentationTime +  frame(i).colonyProps(j).objectProps(k).segmentationTime;
            end
        end
        frame(i).colonyProps(j).cellProps.Var1 = [];

        frame(i).segmentationTime = frame(i).segmentationTime + frame(i).colonyProps(j).segmentationTime;
        
    end
end
