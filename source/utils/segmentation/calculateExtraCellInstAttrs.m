function frame = calculateExtraCellInstAttrs(frame, configuration)
for i = 1:length(frame)
    for j = 1:length(frame(i).colonyProps)
        colonyCentroid = frame(i).colonyProps(j).centroid-frame(i).colonyProps(j).bBoxULCorner;
        distanceMatrix = bwdist(~frame(i).colonyProps(j).bwColonyMask, 'euclidean');
        outterRing = distanceMatrix < configuration.boundaryWidth;
        [col, row] = find(outterRing);
        frame(i).colonyProps(j).cellProps{:, 'LW'} = frame(i).colonyProps(j).cellProps{:, 'length_in_pixels'}./frame(i).colonyProps(j).cellProps{:, 'width_in_pixels'};
        distance_from_centroid_in_pixels = zeros(height(frame(i).colonyProps(j).cellProps), 1);
        perimeter_in_pixels = zeros(height(frame(i).colonyProps(j).cellProps), 1);
        isInsideColony = zeros(height(frame(i).colonyProps(j).cellProps), 1);
        area_in_pixels = zeros(height(frame(i).colonyProps(j).cellProps), 1);
        for k = 1:height(frame(i).colonyProps(j).cellProps)
            distance_from_centroid_in_pixels(k) = norm(frame(i).colonyProps(j).cellProps{k, 'centroid'}{1} - colonyCentroid);
            overlapVector = ismember(frame(i).colonyProps(j).cellProps{k, 'pixelList'}{1}, [row col], 'rows');
            overlapPercent = sum(overlapVector)/length(frame(i).colonyProps(j).cellProps{k, 'pixelList'}{1});
            if(overlapPercent > 0.66)
                isInsideColony(k) = 0;
            else
                isInsideColony(k) = 1;
            end
            area_in_pixels(k) = size(frame(i).colonyProps(j).cellProps{k, 'pixelList'}{1}, 1);
            perimeter_in_pixels(k) = size(frame(i).colonyProps(j).cellProps{k, 'boundaryPixelList'}{1}, 1);
        end
        frame(i).colonyProps(j).cellProps{:, 'isInsideColony'} = isInsideColony;
        frame(i).colonyProps(j).cellProps{:, 'distance_from_centroid_in_pixels'} = distance_from_centroid_in_pixels;
        frame(i).colonyProps(j).cellProps{:, 'area_in_pixels'} = area_in_pixels;
        frame(i).colonyProps(j).cellProps{:, 'perimeter_in_pixels'} = perimeter_in_pixels;
        
        %just convert pixels in real units, e.g. microns
        frame(i).colonyProps(j).cellProps{:, 'length'} = frame(i).colonyProps(j).cellProps{:, 'length_in_pixels'}*configuration.pixelSize;
        frame(i).colonyProps(j).cellProps{:, 'width'} = frame(i).colonyProps(j).cellProps{:, 'width_in_pixels'}*configuration.pixelSize;
        frame(i).colonyProps(j).cellProps{:, 'area'} = area_in_pixels*configuration.pixelSize*configuration.pixelSize;
        frame(i).colonyProps(j).cellProps{:, 'perimeter'} = perimeter_in_pixels*configuration.pixelSize;
        frame(i).colonyProps(j).cellProps{:, 'distance_from_centroid'} = distance_from_centroid_in_pixels*configuration.pixelSize;               
    end
end
end
