function [data] = generateData(pixelIntensities, pixelCoords, centroidCoords, minArea)

%rescale the intensities so as to have meaningful gaussian clusters!
xmin = min(pixelIntensities);
if xmin == 1
    xmin = 0;
end
xmax = max(pixelIntensities);
normalized_pixelIntensities = (pixelIntensities - xmin)/(xmax-xmin);

%keep pixels with intensity higher than average of
%normalized_pixelIntensities.
normalized_pixelIntensities_kept = [];
normalized_pixelIntensities_kept_ind = [];
kept_pixel_counter = 1;
for i = 1:length(normalized_pixelIntensities)
    if normalized_pixelIntensities(i) >= mean(normalized_pixelIntensities)
        normalized_pixelIntensities_kept(kept_pixel_counter) = normalized_pixelIntensities(i);
        normalized_pixelIntensities_kept_ind(kept_pixel_counter) = i;
        kept_pixel_counter = kept_pixel_counter+1;
    end
end

%estimate the probability of each pixel
mu = zeros(length(normalized_pixelIntensities_kept), 2);
prob = zeros(length(normalized_pixelIntensities_kept), 1);
for i = 1:length(normalized_pixelIntensities_kept)
    mu(i, :) = pixelCoords(normalized_pixelIntensities_kept_ind(i),:);
    prob(i) =  normalized_pixelIntensities_kept(i)/sum(normalized_pixelIntensities_kept);
end

mu = mu';
cov_matrix = zeros(2, 2, length(normalized_pixelIntensities_kept));
for i = 1:length(normalized_pixelIntensities_kept)
    cov_matrix(:,:,i) = 0.3*eye(2);
end
npoints = 10*minArea*length(centroidCoords(:,1));

[dim, modes] = size(mu);
data = []; 
for i=1:modes
    
   if dim~=1
      filter = sqrtm(cov_matrix(:,:,i));
   else
      filter = sqrt(cov_matrix(i));
   end
   len = round(npoints*prob(i));
   noi = randn(dim,len);
   data = [data, kron(mu(:,i),ones(1,len))+filter*noi];
end
data = data';