function [clusters, L] = classifyPixels(x, y, pixelList, mu, sigma, prior)

numOfPixels = size(pixelList, 1);
numOfClusters = size(mu, 1);
mixture = zeros(numOfPixels, numOfClusters);
posterior = zeros(numOfPixels, numOfClusters);

for i = 1:numOfPixels
    for j = 1:numOfClusters
        temp = squeeze(sigma(:, :, j));
        if(abs(temp(1, 1)-temp(2, 2)) < 1e-4)
            temp(1, 1) = temp(2, 2);
        end
        if(abs(temp(2, 1)-temp(1, 2)) < 1e-4)
            temp(2, 1) = temp(1, 2);
        end
        
        mixture(i, j) = mvnpdf(pixelList(i, :), mu(j, :), temp)*prior(j);
    end
    for j = 1:numOfClusters
        temp = squeeze(sigma(:, :, j));
        if(abs(temp(1, 1)-temp(2, 2)) < 1e-4)
            temp(1, 1) = temp(2, 2);
        end
        if(abs(temp(2, 1)-temp(1, 2)) < 1e-4)
            temp(2, 1) = temp(1, 2);
        end
        
        posterior(i, j) = (prior(j)*mvnpdf(pixelList(i, :), mu(j, :), temp))/sum(mixture(i, :));
    end
end
[~, labels] = max(posterior, [], 2);
% [sortedPosteriors, ~] = sort(posterior, 2);
% labels(abs(sortedPosteriors(:, 9)-sortedPosteriors(:, 10)) < 0.15) = 0;
L = zeros(x, y);
clusters = cell(numOfClusters, 1);
for i = 1:numOfClusters
    clusters{i} = [pixelList(labels == i, 1), -pixelList(labels == i, 2)];
    for j = 1:size(clusters{i}, 1)
        L(clusters{i}(j, 2), clusters{i}(j, 1)) = i;
    end
end

end