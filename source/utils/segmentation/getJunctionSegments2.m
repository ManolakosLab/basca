function [A, n, junctionsWeight] = getJunctionSegments2(bwObject, bwObjectSkeleton, junctions, convexityThresh, distanceMetric)

d = [  0 -1;
    1 -1;
    1 0;
    1 1;
    0 1;
    -1 1;
    -1 0;
    -1 -1;
    ];
[x, y] = size(bwObject);
bwObjectWithPad = padarray(bwObject,[1 1]);

%Use euclidean distance matrix to get the object radius.
%euclideanDM = bwdist(~bwObjectWithPad, 'euclidean');
%euclideanDM = euclideanDM(2:end-1,2:end-1);

[L, wRegions]  = watershedSegmentation(bwObject, size(bwObject, 1), size(bwObject, 2), distanceMetric);

wRegionsWithJunctionsIndices = [];

%Use manhattan  distance matrix to get the object radius.
chessDT = bwdist(~bwObjectWithPad, 'chessBoard');
chessDT = chessDT(2:end-1,2:end-1);
junctionsWeight = zeros(size(junctions, 1), 1);
temp = bwObject;
for i = 1:size(junctions, 1)

    segmentsPerJunction = 0;
    local_radius = double(chessDT(junctions(i, 2), junctions(i, 1)));
    junctionSegmentEndpoints = [];
    junctionNeighborhoodCoords = d+repmat(junctions(i, :),[8 1]);
    for j = 1:8
        if(junctionNeighborhoodCoords(j, 1) > 0 && junctionNeighborhoodCoords(j, 2) > 0 && junctionNeighborhoodCoords(j, 1) <= size(bwObjectSkeleton, 2) && junctionNeighborhoodCoords(j, 2) <= size(bwObjectSkeleton, 1))
            if(bwObjectSkeleton(junctionNeighborhoodCoords(j, 2), junctionNeighborhoodCoords(j,1)) == 1)
                if(j == 1)
                    orientation = 'N';
                elseif(j == 2)
                    orientation = 'NE';
                elseif(j == 3)
                    orientation = 'E';
                elseif(j == 4)
                    orientation = 'SE';
                elseif(j == 5)
                    orientation = 'S';
                elseif(j == 6)
                    orientation = 'SW';
                elseif(j == 7)
                    orientation = 'W';
                elseif(j == 8)
                    orientation = 'NW';
                end
                
%                 if(local_radius < 2)
%                     continue;
%                 end
                
                bwObjectSegmentedSkeleton = bwObjectSkeleton;
                for jj = 1:8
                    if(j ~= jj)
                        if(junctionNeighborhoodCoords(jj, 1) > 0 && junctionNeighborhoodCoords(jj, 2) > 0 && junctionNeighborhoodCoords(jj, 1) <= size(bwObjectSkeleton, 2) && junctionNeighborhoodCoords(jj, 2) <= size(bwObjectSkeleton, 1))                          
                            bwObjectSegmentedSkeleton(junctionNeighborhoodCoords(jj, 2), junctionNeighborhoodCoords(jj,1)) = 0;
                        end
                    end
                end
                %sometimes junction point is not a boundary pixel 
                %(when having four branches and 3x3 pixel neighborhood)
                %vertices = bwtraceboundary( bwObjectSegmentedSkeleton, [junctions(i, 2), junctions(i, 1)], orientation, 8, double(local_radius)+1);                

                %sometimes junction point is not a boundary pixel
                %(when having four branches and 3x3 pixel neighborhood)
                vertices = bwtraceboundary( bwObjectSegmentedSkeleton, [junctions(i, 2), junctions(i, 1)], orientation, 8);
                reversed_vertices = vertices(end:-1:1, :);
                logical_indices = all(vertices == reversed_vertices, 2);
                if(sum(logical_indices) == length(logical_indices))
                    vertices = vertices(1:ceil(length(vertices)/2), :);
                else
                    indices = find(logical_indices == 0);
                    vertices = vertices(1:indices(1)-1, :);
                end
                
%                 D = bwdistgeodesic(bwObjectSkeleton, junctions(i, 1), junctions(i,2), 'chessboard');
%                 distances = impixel(D, vertices(:, 2), vertices(:, 1));
                if(size(vertices, 1)-1 >= local_radius)
                    segmentsPerJunction = segmentsPerJunction + 1;
                    junctionsWeight(i) = junctionsWeight(i) + 1;
                    junctionSegmentEndpoints(segmentsPerJunction, :) = mean(vertices);
                end
            end
        end
    end
    if(segmentsPerJunction > 2)
        for k = 1:length(wRegions)
            if(ismember(junctions(i, :), wRegions{k}, 'rows'))
                wRegionsWithJunctionsIndices = [wRegionsWithJunctionsIndices; k];
                break;
            end
        end
        
%         angles = zeros(length(junctionSegments), length(junctionSegments));
% 
%         for ii = 1:length(junctionSegments)
%             bwSegment1CL = createBW(x, y, [junctionSegments{ii}(:, 2) junctionSegments{ii}(:, 1)]);
%             segment1Props = regionprops(bwSegment1CL, 'Orientation');
%             segment1Orientation = segment1Props.Orientation;
%             
%             for jj = ii+1:length(junctionSegments)
%                 vertices = [junctionSegments{ii}(end:-1:1, :); junctionSegments{jj}(2:end, :)];
%                 vertices = [vertices(:, 2) vertices(:, 1)];
%                 
%                 
%                 bwSegment2CL = createBW(x, y, [junctionSegments{jj}(:, 2) junctionSegments{jj}(:, 1)]);
%                 segment2Props = regionprops(bwSegment2CL, 'Orientation');
%                 segment2Orientation = segment2Props.Orientation;
%                 %if(abs(segment1Orientation-segment2Orientation) < 10)
%                     angles(ii, jj) = abs(segment1Orientation-segment2Orientation);
%                 %end
% %                 [~, endpoints, ~] = anaskel(bwSegment12CL);
% %                 vertices = bwtraceboundary(bwSegment12CL, [endpoints(2, 1) endpoints(1, 1)], 'N', 8,length(vertices));
% %                 [distanceToBoundary, orientationAngles, ~, ~] = meausurePerpDistToBoundary(bwObject, vertices, local_radius, 'euclidean');
% %                 windowSize = 2*double(local_radius)+3;
% %                 localMinima = findLocalMinima(distanceToBoundary, 0.7, windowSize, verb);
% %                 if(~isempty(localMinima))  %if current object has at least one local minimum, i.e. bowtie, it must be divided.
% %                     
% %                     for kk = 1:length(localMinima)
% %                         se = strel('line', distanceToBoundary(localMinima(kk))+1.4142, orientationAngles(localMinima(kk)));
% %                         nhood = getnhood(se);
% %                         splitMask{offset+kk} = nhood;
% %                         splitCoordinates(offset+kk, :) = vertices(localMinima(kk), :);
% %                     end
% %                     offset = offset + length(localMinima);
% %                 end
%             end
%         end
    elseif(segmentsPerJunction == 2)
        triangleVerts = [junctionSegmentEndpoints(1, :); junctions(i, 2) junctions(i, 1); junctionSegmentEndpoints(2, :)];
        M = [ones(3, 1) triangleVerts(:, 2) y-triangleVerts(:, 1)];        
        if(det(M) >= 0)
            angles = TRIangles([triangleVerts(:, 2) y-triangleVerts(:, 1)]);
        else
            angles = TRIangles([triangleVerts(end:-1:1, 2) y-triangleVerts(end:-1:1, 1)]);
        end
        if(angles(2) < convexityThresh)
            for k = 1:length(wRegions)
                if(ismember(junctions(i, :), wRegions{k}, 'rows'))
                    wRegionsWithJunctionsIndices = [wRegionsWithJunctionsIndices; k];
                    break;
                end
            end
        else
            junctionsWeight(i) = 0;
        end
    else
        junctionsWeight(i) = 0;    
    end
end
if(~isempty(wRegionsWithJunctionsIndices))
    wRegionsWithJunctionsIndices = Unique(wRegionsWithJunctionsIndices);
    for i = 1:length(wRegionsWithJunctionsIndices)
        for k = 1:size(wRegions{wRegionsWithJunctionsIndices(i)},1)
            temp(wRegions{wRegionsWithJunctionsIndices(i)}(k, 2), wRegions{wRegionsWithJunctionsIndices(i)}(k, 1)) = 0;
        end
    end
    [A, n] = bwlabel(temp, 4);
    for i = 1:length(wRegionsWithJunctionsIndices)
        for k = 1:size(wRegions{wRegionsWithJunctionsIndices(i)},1)
            A(wRegions{wRegionsWithJunctionsIndices(i)}(k, 2), wRegions{wRegionsWithJunctionsIndices(i)}(k, 1)) = n+i;
        end
    end
    n = n+length(wRegionsWithJunctionsIndices);
else
    A = bwObject;
    n = 1;
end

