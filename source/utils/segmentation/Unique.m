function [C] = Unique(A)
n = size(A, 1);
B = cell(1, n);

for i = 1:n
    B{i} = A(i, :);
end

for i = 1:n
    for j = i+1:n
        if((size(B{1, j}, 1)~=0) && isequal(B{i}, B{j}))
            B{j} = [];
        end
    end
end
c_i = 1;
for i = 1:n
    if(size(B{1, i}, 1)~=0)
        C(c_i, :) = B{i}(1, :);
        c_i = c_i + 1;
    end
end