function [segmentsProps] = mergeSegments(x, y, segmentsProps, minAngle, minDivisionRatio, numOfSegmentsThresh, verb)
numOfSegments = length(segmentsProps);
if(numOfSegments == numOfSegmentsThresh)
    return;
end
segmentsSize = zeros(numOfSegments, 1);
for i = 1:numOfSegments
    segmentsSize(i) = size(segmentsProps(i).pixelList, 1);
end
[~, indices] = sort(segmentsSize, 'descend');

sortedSegmentsProps = segmentsProps;
for i = 1:length(indices)
    sortedSegmentsProps(i) = segmentsProps(i);
end

segmentsProps = sortedSegmentsProps;
%adjacency matrix with weights(convexity)
convexityMatrix = -Inf*ones(length(segmentsProps), length(segmentsProps));
while(1)
    for i = 1:size(convexityMatrix, 1)
        %get the segment1 skeleton
        %bwSegment1 = segmentsProps(i).bwObject;
        bwSegment1 = createBW(x, y, segmentsProps(i).pixelList);

        for j = i+1:size(convexityMatrix, 2)
            if(convexityMatrix(i, j) ~= -Inf)
                continue;
            end
            
            bwSegment12 = createBW(x, y, [segmentsProps(i).pixelList; segmentsProps(j).pixelList]);
            %bwSegment12 = logical(segmentsProps(i).bwObject+segmentsProps(j).bwObject);
            [~, n] = bwlabel(bwSegment12);
            if(n ~= 1)
                continue;
            end
            
            segment12Props = regionprops(bwSegment12, 'MajorAxisLength', 'MinorAxisLength', 'orientation', 'solidity');
            
%             if(segment12Props.MinorAxisLength < 20);
%                 bwSegment12 = bwmorph(bwSegment12, 'fill');
%             end
            
            %get skel array
            [skel12, ~] = skeleton(bwSegment12);
            if(max(max(skel12)) == Inf)
                %if skel(max) == Inf, then the segment has circes
                continue;
            end
%             minSegment12Radius = ceil(min([segmentsProps(i).widthInPixels segmentsProps(j).widthInPixels])/2);
            if(segmentsProps(i).widthInPixels > 3 && segmentsProps(j).widthInPixels > 3)
                minSegment12Radius = min([segmentsProps(i).widthInPixels segmentsProps(j).widthInPixels])/2;
            elseif(segmentsProps(i).widthInPixels > 3)
                minSegment12Radius = floor(segmentsProps(i).widthInPixels/2);
            elseif(segmentsProps(j).widthInPixels > 3)
                minSegment12Radius = floor(segmentsProps(j).widthInPixels/2);
            else
                minSegment12Radius = min([segmentsProps(i).widthInPixels segmentsProps(j).widthInPixels]);
            end
            
            %get the segment12 skeleton
            bwSegment12Skeleton = bwmorph(skel12 > pi*minSegment12Radius, 'skel', inf);
            skeleton12Junctions = [];
            [r, c] = find(bwmorph(bwSegment12Skeleton, 'branchpoints'));
            if(~isempty(r))
                skeleton12Junctions(:, 1) = c;
                skeleton12Junctions(:, 2) = r;
            end
            [~, ~, junctions12Weight] = getJunctionSegments2(bwSegment12, bwSegment12Skeleton, skeleton12Junctions, minAngle, 'euclidean');
            
            %get the segment1 skeleton
            [skel1, ~] = skeleton(bwSegment1);
            %segment1Props = regionprops(bwSegment1, 'orientation');
            bwSegment1Skeleton = bwmorph(skel1 > pi*minSegment12Radius, 'skel', inf);
            skeleton1Junctions = [];
            [r, c] = find(bwmorph(bwSegment1Skeleton, 'branchpoints'));
            if(~isempty(r))
                skeleton1Junctions(:, 1) = c;
                skeleton1Junctions(:, 2) = r;
            end
            [~, ~, junctions1Weight] = getJunctionSegments2(bwSegment1, bwSegment1Skeleton, skeleton1Junctions, minAngle, 'euclidean');

            for ii = 1:size(skeleton1Junctions, 1)
                for jj = 1:size(skeleton12Junctions, 1)
                    if(skeleton1Junctions(ii, :) == skeleton12Junctions(jj, :))
                        junctions12Weight(jj) = junctions12Weight(jj) - junctions1Weight(ii);
                    end
                end
            end
            
            %get the segment2 skeleton
            bwSegment2 = createBW(x, y, segmentsProps(j).pixelList);
            %segment2Props = regionprops(bwSegment2, 'orientation');
            [skel2, ~] = skeleton(bwSegment2);
            bwSegment2Skeleton = bwmorph(skel2 > pi*minSegment12Radius, 'skel', inf);
            skeleton2Junctions = [];
            [r, c] = find(bwmorph(bwSegment2Skeleton, 'branchpoints'));
            if(~isempty(r))
                skeleton2Junctions(:, 1) = c;
                skeleton2Junctions(:, 2) = r;
            end
            [~, ~, junctions2Weight] = getJunctionSegments2(bwSegment2, bwSegment2Skeleton, skeleton2Junctions, minAngle, 'euclidean');
            for ii = 1:size(skeleton2Junctions, 1)
                for jj = 1:size(skeleton12Junctions, 1)
                    if(skeleton2Junctions(ii, :) == skeleton12Junctions(jj, :))
                        junctions12Weight(jj) = junctions12Weight(jj) - junctions2Weight(ii);
                    end
                end
            end
            %if the segment is collinear, get the segment's centerline to
            %check for bowties
            if(sum(junctions12Weight) <= 0)
                [bwSegment12CL, endpoints] = objectCenterline(bwSegment12);
                segment12CLlen = length(find(bwSegment12CL));
                
                %Use chessboard distance transform to get the segment12 radius.
                bwSegment12WithPad = padarray(bwSegment12,[1 1]);
                chessBoardDT = bwdist(~bwSegment12WithPad, 'chessboard');
                chessBoardDT = chessBoardDT(2:end-1,2:end-1);
                
                %assuming that the radius of the segment is the maximum distance from the boundary.
                segment12Radius = max(max(chessBoardDT));
                if(segment12CLlen <= segment12Radius)
                    convexityMatrix(i, j) = 1+segment12Props.Solidity;
                    convexityMatrix(j, i) = 1+segment12Props.Solidity;              
                else
                    
                    segment12CLVerticesWithoutPoles = bwtraceboundary(bwSegment12CL, [endpoints(2, 1) endpoints(1, 1)], 'N', 8, segment12CLlen);
                    %[~, verticesFromPoleA, ~, verticesToPoleB, ~] = measureBacLength(bwSegment12, segment12CLVerticesWithoutPoles, double(segmentRadius), 'euclidean', 1);
                    [verticesFromPoleA, ~, verticesToPoleB, ~] = getObjectPoles(bwSegment12, segment12CLVerticesWithoutPoles, 'euclidean', verb);
                    vertices = [verticesFromPoleA; verticesToPoleB; segment12CLVerticesWithoutPoles];
                    %if the segment is non-complex, get the segment's centerline to
                    %check for bowties
                    vertices = [vertices(:, 2) vertices(:, 1)];
                    bwSegment12CL = createBW(x, y, vertices);
                    [~, endpoints, ~] = anaskel(bwSegment12CL);
                    if(isempty(endpoints))
                       figure; imagesc(bwSegment12CL);
                       endpoints
                    end
                    segment12CLVertices = bwtraceboundary(bwSegment12CL, [endpoints(2, 1) endpoints(1, 1)], 'N', 8,length(vertices));
                    [distanceToBoundary, ~, ~, ~] = meausureDistToBoundary2(bwSegment12, segment12CLVertices, 'euclidean');
                    
                    IDX = knnsearch([segment12CLVertices(:, 2) segment12CLVertices(:, 1)], [segmentsProps(i).centroid ; segmentsProps(j).centroid]);
                    if(IDX(1) < IDX(2))
                        [localMinimumInd, ~] = findLocalMinimaSides2(distanceToBoundary, minDivisionRatio, ceil(segmentsProps(i).widthInPixels)+1, ceil(segmentsProps(j).widthInPixels)+1, IDX(1),  IDX(2), verb);
                    else
                        [localMinimumInd, ~] = findLocalMinimaSides2(distanceToBoundary, minDivisionRatio, ceil(segmentsProps(j).widthInPixels)+1, ceil(segmentsProps(i).widthInPixels)+1, IDX(2),  IDX(1), verb) ;                       
                    end
                    if(isempty(localMinimumInd))
                        %if current segment has no local minimum, i.e. bowtie,
                        %segment1 and segment2 must be merged.
                        
                        %simplify Polygon line (i.e. centerline vertices) if needed.
                        [segment12SimplifiedCLVertices, ~] = dpsimplify(segment12CLVertices , max([segmentsProps(i).width, segmentsProps(j).width])/2);
                        [segment12Angles, isConvexSegment12] = computeObjectAngles(y,segment12SimplifiedCLVertices, minAngle);
%                         [segment12Angles, isConvexSegment12]  = angleBetween2Objects(y, segmentsProps(i).orientation, segmentsProps(i).centroid, segmentsProps(j).orientation, segmentsProps(j).centroid, convexityThresh);
                        %the skeleton belongs either to a single-cell or to a non convex segment
                        if(length(isConvexSegment12) == sum(isConvexSegment12))
                            convexityMatrix(i, j) = mean(segment12Angles./180) + segment12Props.Solidity;%1-(abs(abs(segment12Props.Orientation)-abs(segment1Props.Orientation))+abs(abs(segment12Props.Orientation)-abs(segment2Props.Orientation)))/180;
                            convexityMatrix(j, i) = mean(segment12Angles./180) + segment12Props.Solidity;
                        else
                            if(verb)
                                figure('name', 'non-convex segment');
                                imagesc(bwSegment12);
                            end
                        end
                        
                    else
                        if(verb)
                            figure('name', 'bowties segment');
                            imagesc(bwSegment12);
                        end
                    end
                end
            else
                if(verb)
                    figure('name', 'complex segment');
                    imagesc(bwSegment12);
                end
            end
        end
    end
    
    numOfSegments = length(segmentsProps);
    mergingEventsCounter = 0;
    mergedSegmentsIndices = [];
    for i = 1:size(convexityMatrix, 1)
        [maxColumnConvexity, maxColumnConvexityInd] = max(convexityMatrix(i, :));
        if(maxColumnConvexity ~= -Inf && ~ismember(i, mergedSegmentsIndices)) %If merging happend to i-th segment
            [~, maxRowConvexityInd] = max(convexityMatrix(:, maxColumnConvexityInd));
            if(maxRowConvexityInd == i && ~ismember(maxColumnConvexityInd, mergedSegmentsIndices))
                mergingEventsCounter = mergingEventsCounter + 1;
                
                %bwSegment12 = logical(segmentsProps(i).bwObject+segmentsProps(maxColumnConvexityInd).bwObject);
                bwSegment12 = createBW(x, y, [segmentsProps(i).pixelList; segmentsProps(maxColumnConvexityInd).pixelList]);
                
                segment12Props = regionprops(bwSegment12, 'MinorAxisLength', 'MajorAxisLength', 'orientation', 'solidity');
                
                [bwSegment12CL, endpoints] = objectCenterline(bwSegment12);
                segment12CLlen = length(find(bwSegment12CL));
                
                %Use chessboard distance transform to get the segment12 radius.
                bwSegment12WithPad = padarray(bwSegment12,[1 1]);
                chessBoardDT = bwdist(~bwSegment12WithPad, 'chessboard');
                chessBoardDT = chessBoardDT(2:end-1,2:end-1);
                
                %assuming that the radius of the segment is the maximum distance from the boundary.
                segment12Radius = max(max(chessBoardDT));
                if(segment12CLlen <= segment12Radius)
                    segment12WidthInPixels = 2*double(segment12Radius)+1;
                    segment12Width = segment12Props.MinorAxisLength;
                    segment12LengthInPixels = round(segment12Props.MajorAxisLength);
                    segment12Length = segment12Props.MajorAxisLength;
                    segment12CentralPixel = round(mean(double([segmentsProps(i).pixelList; segmentsProps(maxColumnConvexityInd).pixelList])));
                    
                else                   
                    segment12CLVerticesWithoutPoles = bwtraceboundary(bwSegment12CL, [endpoints(2, 1) endpoints(1, 1)], 'N', 8, segment12CLlen);
                    [verticesFromPoleA, ~, verticesToPoleB, ~] = getObjectPoles(bwSegment12, segment12CLVerticesWithoutPoles, 'euclidean', verb);
                    vertices = [verticesFromPoleA; verticesToPoleB; segment12CLVerticesWithoutPoles];
                    %if the segment is non-complex, get the segment's centerline to
                    %check for bowties
                    vertices = [vertices(:, 2) vertices(:, 1)];
                    bwSegment12CL = createBW(x, y, vertices);
                    [~, endpoints, ~] = anaskel(bwSegment12CL);
                    segment12CLVertices = bwtraceboundary(bwSegment12CL, [endpoints(2, 1) endpoints(1, 1)], 'N', 8,length(vertices));
                    [segment12SimplifiedCLVertices, ~] = dpsimplify(segment12CLVertices , 1.4142);
                 
                    [distanceToBoundaryInPixels, ~, ~, ~] = meausureDistToBoundary2(bwSegment12, segment12CLVertices, 'chessboard');
                    [distanceToBoundary, ~, ~, ~] = meausureDistToBoundary2(bwSegment12, segment12CLVertices, 'euclidean');
                    segment12LengthInPixels = length(distanceToBoundary);
                    
                    [segment12Length, ~] = arclength(segment12SimplifiedCLVertices(:, 2), segment12SimplifiedCLVertices(:, 1), 'linear');
                    
                    segment12WidthInPixels = max(medfilt1(distanceToBoundaryInPixels(2:end-1), double(segment12Radius)));
                    segment12Width = max(medfilt1(distanceToBoundary(2:end-1), ceil(segment12WidthInPixels/2)));
                    segment12CentralPixel = segment12CLVertices(ceil(size(segment12CLVertices, 1)/2), :);
                    segment12CentralPixel = [segment12CentralPixel(2) segment12CentralPixel(1)];
                end
                segmentsProps(numOfSegments+mergingEventsCounter).pixelList = [segmentsProps(i).pixelList; segmentsProps(maxColumnConvexityInd).pixelList];
                %segmentsProps(numOfSegments+mergingEventsCounter).bwObject = bwSegment12;
                segmentsProps(numOfSegments+mergingEventsCounter).centroid = mean(double([segmentsProps(i).pixelList; segmentsProps(maxColumnConvexityInd).pixelList]));
                segmentsProps(numOfSegments+mergingEventsCounter).centralPixel = segment12CentralPixel;
                
                if bwSegment12(ceil(segmentsProps(numOfSegments+mergingEventsCounter).centroid(2)), ceil(segmentsProps(numOfSegments+mergingEventsCounter).centroid(1))) == 0
                    segmentsProps(numOfSegments+mergingEventsCounter).hasIrregularShape = true;
                else
                    segmentsProps(numOfSegments+mergingEventsCounter).hasIrregularShape = false;
                end
                segmentsProps(numOfSegments+mergingEventsCounter).width = segment12Width;
                segmentsProps(numOfSegments+mergingEventsCounter).widthInPixels = segment12WidthInPixels;
                segmentsProps(numOfSegments+mergingEventsCounter).lengthInPixels = segment12LengthInPixels;
                segmentsProps(numOfSegments+mergingEventsCounter).length = segment12Length;
                bwSegment12Perim = bwperim(bwSegment12);
                
                segmentsProps(numOfSegments+mergingEventsCounter).minorAxis = segment12Props.MinorAxisLength;
                segmentsProps(numOfSegments+mergingEventsCounter).majorAxis = segment12Props.MajorAxisLength;
                segmentsProps(numOfSegments+mergingEventsCounter).eccentricity = segment12Props.MinorAxisLength/segment12Props.MajorAxisLength;
                segmentsProps(numOfSegments+mergingEventsCounter).orientation = segment12Props.Orientation;
                segmentsProps(numOfSegments+mergingEventsCounter).solidity = segment12Props.Solidity;
                if(segmentsProps(numOfSegments+mergingEventsCounter).orientation < 0)
                    segmentsProps(numOfSegments+mergingEventsCounter).orientation = 360+segmentsProps(numOfSegments+mergingEventsCounter).orientation;
                end
                segmentsProps(numOfSegments+mergingEventsCounter).covariance = cov(double(segmentsProps(numOfSegments+mergingEventsCounter).pixelList));
                b = bwboundaries(bwSegment12Perim);
                segmentsProps(numOfSegments+mergingEventsCounter).boundaryPixelList = uint16([b{1}(:, 2) b{1}(:, 1)]);
                
                
                convexityMatrix(numOfSegments+mergingEventsCounter, :) = -Inf; %#ok<*NBRAK>
                convexityMatrix(:, numOfSegments+mergingEventsCounter) = -Inf;
                
                mergedSegmentsIndices = [mergedSegmentsIndices; i; maxColumnConvexityInd];
            end
        end
    end
    
    if(isempty(mergedSegmentsIndices))
        break;
    end
    
    segmentsProps(mergedSegmentsIndices) = [];
    if(length(segmentsProps) == numOfSegmentsThresh)
        break;
    end
    convexityMatrix(mergedSegmentsIndices, :) = [];
    convexityMatrix(:, mergedSegmentsIndices) = [];
end

end
