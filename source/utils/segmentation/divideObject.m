function [L, objects] = divideObject(bwObject, splitCoordinates, splitMask)


bwObjectSegmented = (bwObject);

for k = 1:size(splitCoordinates, 1)
    currMask = splitMask{k};
    currMask = double(currMask);
    icount = 0;
    c = floor(size(currMask, 1)/2);
    d = floor(size(currMask, 2)/2);
    for ii = splitCoordinates(k, 1)-c:splitCoordinates(k, 1)+c,
        icount = icount+1;
        jcount = 0;
        for jj = splitCoordinates(k, 2)-d:splitCoordinates(k, 2)+d,
            jcount = jcount+1;
            if(currMask(icount, jcount) == 1 && ii > 0 && jj > 0 && ii < size(bwObject, 1)+1 && jj < size(bwObject, 2)+1)
                bwObjectSegmented(ii, jj) = 0;
            end
        end
    end
end

[L, n] = bwlabel(bwObjectSegmented, 4);
[c, r] = find(L >= 2);
acceptedPixels = [c r];

temp = bwObject-bwObjectSegmented;
[c, r] = find(temp == 1);
rejectedPixels = [c r];

L2 = L;
nn = knnsearch(acceptedPixels, rejectedPixels);
for i = 1:length(nn)
    L2(rejectedPixels(i, 1), rejectedPixels(i, 2)) = L(acceptedPixels(nn(i), 1), acceptedPixels(nn(i), 2));
end
L = L2;


if(n >= 2)
    splitObjectsProps  = regionprops(L, 'PixelList');
    objects = cell(length(splitObjectsProps), 1);
    for jj = 1:length(splitObjectsProps)
        objects{jj} = splitObjectsProps(jj).PixelList;
    end
else
    splitObjectsProps  = regionprops(logical(bwObject), 'PixelList');
    objects = {splitObjectsProps(1,1).PixelList};
end


end