function [colonyProps] = insertToAdjacencyMatrix(colonyProps, index, neighborhoodDistanceFactor, neighborhoodSize)

k = 2*neighborhoodSize*neighborhoodSize-1;

centroids = cell2mat(colonyProps.cellProps{:, 'centralPixel'});
Mdl = KDTreeSearcher(centroids, 'distance', 'chebychev','bucketSize', k);

if(neighborhoodDistanceFactor == 0)
    neighborDistanceFactor = 1.0;
else
    cellWidth = mean(colonyProps.cellProps{:, 'width_in_pixels'});
    neighborDistanceFactor = neighborhoodDistanceFactor*cellWidth;
end

IdxNN = knnsearch(Mdl, centroids(index, :), 'K', k);
currCellPixelList = double(colonyProps.cellProps{index, 'boundaryPixelList'}{1});
for j = 1:length(IdxNN)
    dist = min(min(pdist2(currCellPixelList, double(colonyProps.cellProps{IdxNN(j), 'boundaryPixelList'}{1}), 'chebychev')));
    if(dist <= neighborDistanceFactor)
        colonyProps.adjacencyMatrix(index, IdxNN(j)) = dist;
        colonyProps.adjacencyMatrix(IdxNN(j), index) = dist;
    end
    
end

end
