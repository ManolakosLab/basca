function [neighborhoodIndices] = getNeighbors(currColonyProps, cellInd, adjacencyMatrix, neighborhoodSize, neighborDistanceFactor)

sparseAdjacencyMatrix = sparse(double(adjacencyMatrix));
cellWidth = max(currColonyProps.cellProps{:, 'width_in_pixels'});

if(mod(neighborhoodSize, 2))
    neighborsThresh = (neighborhoodSize^2-1)/2+1;
else
    neighborsThresh = (neighborhoodSize^2)/2+1;
end
neighborhoodIndices = [];

numOfNeighbors = 0;
depthCounter = 1;
while(1)
    %get the i-th level neighbors of a cell instant.
    if(depthCounter > 1)
        currNeighborhoodIndices = graphtraverse(sparseAdjacencyMatrix, cellInd, 'Depth', depthCounter, 'Method', 'BFS');
        
        neighborMaxDistance = depthCounter*cellWidth*neighborDistanceFactor;
        
        candidateNeighborsIndices = currNeighborhoodIndices(~ismember(currNeighborhoodIndices, neighborhoodIndices));
        
        %just compute the distance of central cell instant with the
        %candidate neighbors
        distanceFromCentralCell = zeros(length(candidateNeighborsIndices) , 1);
        for i = 1:length(candidateNeighborsIndices)
            distanceFromCentralCell(i) = min(min(pdist2(double(cell2mat(currColonyProps.cellProps{cellInd, 'boundaryPixelList'})), double(currColonyProps.cellProps{candidateNeighborsIndices(i), 'boundaryPixelList'}{1}),  'chebychev')));
        end
        
        %keep the cell instants woth distance less than neighborMaxDistance
        neighborhoodIndices = [neighborhoodIndices candidateNeighborsIndices(distanceFromCentralCell <= neighborMaxDistance)]; %#ok<AGROW>
    else
        currNeighborhoodIndices = graphtraverse(sparseAdjacencyMatrix, cellInd, 'Depth', depthCounter, 'Method', 'BFS');

        neighborhoodIndices = currNeighborhoodIndices;     
    end
    currNeighborsNum = length(neighborhoodIndices);
    if(currNeighborsNum  >= neighborsThresh || numOfNeighbors == currNeighborsNum)
        break;
    end
    
    %recenter the neighborhood, find the cell instant that lies in the
    %center of the neighborhood. Non-balanced neighbrhood occur in
    %colony's boundaries.
    distanceFromNeighborhoodCentroid = zeros(length(neighborhoodIndices) , 1);
    for i = 1:length(neighborhoodIndices)
        distanceFromNeighborhoodCentroid(i) = min(min(pdist2(mean(double(cell2mat(currColonyProps.cellProps{neighborhoodIndices, 'pixelList'}))), double(currColonyProps.cellProps{neighborhoodIndices(i), 'boundaryPixelList'}{1}),  'chebychev')));
    end
    %now cellInd is the cell instant closest to the neighborhood centroid.
    [~, tempInd] = min(distanceFromNeighborhoodCentroid);    
    cellInd = neighborhoodIndices(tempInd);
    numOfNeighbors = currNeighborsNum;
    depthCounter = depthCounter + 1;
end

end
