function [parentIndAtMax, prevCellOverlapAtMax, currCellOverlapAtMax] = matchNeighborhood(currColonyProps, prevColonyProps, tform, candidateParentsIndices, rotatedCoords, neighborhoodInd, translationStep, verb)

% [col, row] = find(currColonyProps.bwColonyMask);
% currColonyMaskPixels = [row+currColonyProps.bBoxULCorner(1, 1) col+currColonyProps.bBoxULCorner(1, 2)];

neighborhoodCoords = [];
candidateParentIndicesSet = [];
for k = 1:length(neighborhoodInd)
    candidateParentIndicesSet = [candidateParentIndicesSet candidateParentsIndices{neighborhoodInd(k)}];
    neighborhoodCoords = [neighborhoodCoords; currColonyProps.cellProps{neighborhoodInd(k), 'pixelList'}{1}]; %#ok<*AGROW>
end
neighborhoodCoords = double(neighborhoodCoords);
neighborhoodCoords = [neighborhoodCoords(:, 1)+currColonyProps.bBoxULCorner(1, 1)-1 -(neighborhoodCoords(:, 2)+currColonyProps.bBoxULCorner(1, 2)-1)];

%neighborhoodCentroid = double(currColonyMaskPixels(knnsearch(currColonyMaskPixels, mean(neighborhoodCoords), 'K', 1), :));
neighborhoodCentroid = mean(neighborhoodCoords);
candidateParentIndicesSet = unique(candidateParentIndicesSet);
if(~isempty(candidateParentIndicesSet))
    
    count = 1;
    boundingBoxLeftCorner = min(neighborhoodCoords);
    boundingBoxOffset = max(neighborhoodCoords)-boundingBoxLeftCorner+1;
    
    myGrid = zeros(boundingBoxOffset(1)*boundingBoxOffset(2), 2);
    
    for ii = boundingBoxLeftCorner(1):boundingBoxLeftCorner(1)+boundingBoxOffset(1)
        for jj = boundingBoxLeftCorner(2):boundingBoxLeftCorner(2)+boundingBoxOffset(2)
            myGrid(count, :)=[ii, jj];
            count = count + 1;
        end
    end
    %subsampling grid to reduce complexity
    myGrid = myGrid(1:translationStep:end, :);
    
    if(length(neighborhoodInd) <= 3)
        coeff = 1;
    else
        coeff = ((5-sqrt(length(neighborhoodInd)-3))/5)/translationStep;
        if(coeff < 0.01)
            coeff = 0.01;
        end
    end
    candidateAlignmentPositions = getNNMahalanobis(myGrid, neighborhoodCentroid, cov(neighborhoodCoords), ceil(coeff*length(neighborhoodCoords)));
    
    currCellOverlap = cell(size(candidateAlignmentPositions, 1), 1);
    prevCellOverlap = cell(size(candidateAlignmentPositions, 1), 1);
    
    neighborhoodOverlap = zeros(size(candidateAlignmentPositions, 1), 1);
    parentIndices = cell(size(candidateAlignmentPositions, 1), 1);
    parfor j = 1:length(candidateAlignmentPositions)
        prevCellOverlapMatrix = zeros(length(neighborhoodInd), length(candidateParentIndicesSet));
        currCellOverlapMatrix = zeros(length(neighborhoodInd), length(candidateParentIndicesSet));
        overlapMatrix = zeros(length(neighborhoodInd), length(candidateParentIndicesSet));
        
        for i = 1:length(neighborhoodInd)
            for k = 1:length(candidateParentIndicesSet)
                alignedCellCoords = [];
                if(~isempty(rotatedCoords{neighborhoodInd(i), candidateParentIndicesSet(k)}))
                    alignedCellCoords(:, 1) = rotatedCoords{neighborhoodInd(i), candidateParentIndicesSet(k)}(:, 1) + currColonyProps.bBoxULCorner(1, 1) -1 - neighborhoodCentroid(1, 1) + candidateAlignmentPositions(j, 1);
                    alignedCellCoords(:, 2) = -(rotatedCoords{neighborhoodInd(i), candidateParentIndicesSet(k)}(:, 2) + currColonyProps.bBoxULCorner(1, 2)) -1 - neighborhoodCentroid(1, 2) + candidateAlignmentPositions(j, 2);
                    
                    fatherCoords = double(prevColonyProps.cellProps{candidateParentIndicesSet(k), 'boundaryPixelList'}{1});
                    fatherCoords = [fatherCoords(:,1)+prevColonyProps.bBoxULCorner(1, 1)-1 (fatherCoords(:, 2)+prevColonyProps.bBoxULCorner(1, 2)-1)];
                    
                    [fatherCoords(:, 1), fatherCoords(:, 2)] = transformPointsForward(tform,fatherCoords(:, 1),fatherCoords(:, 2));
                    fatherCoords = [fatherCoords(:,1) -(fatherCoords(:, 2))];

                    %bw = createBW(x, y, fatherCoords);
                    [in, ~] = inpolygon(alignedCellCoords(:, 1), alignedCellCoords(:, 2), fatherCoords(:, 1), fatherCoords(:, 2));
                    overlapBitmap = sum(in);
                    
                    %overlapMatrix(i, k) = sum(overlapBitmap)/(length(prevColonyProps.cellProps{candidateParentIndicesSet(k), 'pixelList'}{1})+length(currColonyProps.cellProps{neighborhoodInd(i), 'pixelList'}{1})-sum(overlapBitmap));
                    
                    currCellOverlapMatrix(i, k) = sum(overlapBitmap)/length(currColonyProps.cellProps{neighborhoodInd(i), 'pixelList'}{1});
                    prevCellOverlapMatrix(i, k) = sum(overlapBitmap)/length(prevColonyProps.cellProps{candidateParentIndicesSet(k), 'pixelList'}{1});
                    overlapMatrix(i, k) = currCellOverlapMatrix(i, k) + prevCellOverlapMatrix(i, k);
                end
            end
        end
        
        [pairsOverlap, parentIndices{j}] = max(overlapMatrix, [], 2);
        
        neighborhoodOverlap(j) = sum(pairsOverlap);
        
        for jj = 1:length(pairsOverlap)
            prevCellOverlap{j}(jj) = prevCellOverlapMatrix(jj, parentIndices{j}(jj));
            currCellOverlap{j}(jj) = currCellOverlapMatrix(jj, parentIndices{j}(jj));
        end
        parentIndices{j} = candidateParentIndicesSet(parentIndices{j});
    end
    
    [~, maxNeighborhoodOverlapInd] = max(neighborhoodOverlap);
    parentIndAtMax = parentIndices{maxNeighborhoodOverlapInd};
    prevCellOverlapAtMax = prevCellOverlap{maxNeighborhoodOverlapInd};
    currCellOverlapAtMax = currCellOverlap{maxNeighborhoodOverlapInd};
    
    
    if(verb)
        [col, row] = find(prevColonyProps.bwColony);
        colonyPixelsPrevFrame = [row col];
        
        
        figure1 = figure;
        axes1 = axes('Parent', figure1,'YDir','reverse');
        box(axes1,'on');
        grid(axes1,'on');
        hold(axes1,'all');
        
        plot(colonyPixelsPrevFrame(:, 1)+prevColonyProps.bBoxULCorner(1, 1) , colonyPixelsPrevFrame(:, 2)+prevColonyProps.bBoxULCorner(1, 2), 'b.');
        
        for i = 1:length(neighborhoodInd)
            if(parentIndAtMax(i)>0 && ~isempty(rotatedCoords{neighborhoodInd(i), parentIndAtMax(i)}))
                plot(rotatedCoords{neighborhoodInd(i), parentIndAtMax(i)}(:, 1) + currColonyProps.bBoxULCorner(1, 1) - neighborhoodCentroid(1, 1) + candidateAlignmentPositions(maxNeighborhoodOverlapInd, 1), ...
                    rotatedCoords{neighborhoodInd(i), parentIndAtMax(i)}(:, 2) + currColonyProps.bBoxULCorner(1, 2) + neighborhoodCentroid(1, 2) - candidateAlignmentPositions(maxNeighborhoodOverlapInd, 2), 'r+');
            end
        end
        
        %     figure1 = figure;
        %     axes1 = axes('Parent', figure1,'YDir','reverse');
        %     box(axes1,'on');
        %     grid(axes1,'on');
        %     hold(axes1,'all');
        %
        %     plot(colonyPixelsPrevFrame(:, 1), colonyPixelsPrevFrame(:, 2), 'b.');
        %     plot(candidateAlignmentPositions(:, 1), candidateAlignmentPositions(:, 2), 'g+');
        %     plot(neighborhoodCentroid(:, 1), neighborhoodCentroid(:, 2), 'black+');
    end
else
    parentIndAtMax = zeros(length(neighborhoodInd), 1);
    prevCellOverlapAtMax = zeros(length(neighborhoodInd), 1);
    currCellOverlapAtMax = zeros(length(neighborhoodInd), 1);
end

end
