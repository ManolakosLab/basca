function [currColonyProps] = findCandidateMothers(currColonyProps, prevColonyProps, matchedCellsCurrColony, matchedCellsPrevColony, tform, enableCellRotation, neighborhoodSize, verb)
% Find candidate mothers of current colony's cell instants. Candidate mothers are cell instants of the previous frame.
%
% [currColonyProps] = findCandidateMothers(currColonyProps, matchedCellsCurrColony, matchedCellsPrevColony, enableCellRotation, neighborhoodSize, verb)
% 
%  INPUT :
%    currColonyProps : a struct array containing the cellsProps table generated
%    by object segmentation step.
%
%    matchedCellsCurrColony : a logical array of size Nx1, where N is the 
%    number of currrent colony's cells. If a value of the array is true, it 
%    shows that the i-th cell instant is matched with a cell instant of the
%    previous colony.
%   
%    matchedCellsPrevColony : a logical array of size Mx1, where M is the 
%    number of previous colony's cells. If a value of the array is true, it 
%    shows that the j-th cell instant is matched with a cell instant of the
%    current colony.
%
%    enableCellRotation : a logical value showing whether to rotate cells 
%    to match with the orientation of the candidate mothers or not. Notice
%    that when the cell instants (either mothers or daughters) have
%    irregular shape the function does not rotate the cells even if 
%    <enableCellRotation> is True. 
%
%    neighborhoodSize : the function searches for the adjacent cells of a 
%    cell in the subset of 2*neighborhoodSize^2 nearest neighbors, based on
%    the cells' centroids. By doing this, the function quickly rejects cell
%    instants lying far away from a the cell instant.
%
%    verb : a logical value indicating whether to display more information
%    and figures about the function's results.
%
%  OUTPUT :
%    colonyProps : colonyProps updated with the fields shown below:
%    -candidateMotherIndices : a Nx1 cell-array containing the indices of
%     candidate mothers at the cellProps table of the previous frame, where
%     N is the number of cell instants of the current frame.
%    -rotatedCoords : a Nx1 cell-array containing the rotated coordinates
%     of cell instants of current frame.
%    Note that when a cell instant has n candidate mothers, then in 
%    <rotateCoords> the function stores the rotated coordinates of the
%    current cell instant corresponding to each candidate mother
%    orientation.
%
%
%  Note that if the autocorrectErrors option is activated then the frame 
%  struct output will be different from the frame struct input, because
%  cell instant surfaces may have been modified by the function, during
%  autocorrection step.
%   
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
% 
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.


currColonyProps.rotatedCoords = cell(height(currColonyProps.cellProps), height(prevColonyProps.cellProps));
adjacencyMatrix = sparse(double(prevColonyProps.adjacencyMatrix));
%adjacencyMatrix(logical(matchedCellsPrevColony{:, 1}), :) = 0;
%adjacencyMatrix(:, logical(matchedCellsPrevColony{:, 1})) = 0;

%prevCellPropsCentralPixels = cell2mat(prevColonyProps.cellProps{:, 'centralPixel'});
prevCellPropsCentralPixels = cell2mat(prevColonyProps.cellProps.boundaryPixelList);
prevCellPropsCentralPixelsLabels = [];
for i = 1:height(prevColonyProps.cellProps)
    prevCellPropsCentralPixelsLabels = [prevCellPropsCentralPixelsLabels;i*ones(size(cell2mat( prevColonyProps.cellProps{i, 'boundaryPixelList'} ), 1), 1)];
end

prevCellPropsHasIrregShape = prevColonyProps.cellProps{:, 'has_irregular_shape'};

prevCellPropsCentralPixels(:, 1) = prevCellPropsCentralPixels(:, 1) + prevColonyProps.bBoxULCorner(1, 1) - 1;
prevCellPropsCentralPixels(:, 2) = prevCellPropsCentralPixels(:, 2) + prevColonyProps.bBoxULCorner(1, 2) - 1;

[prevCellPropsCentralPixels(:, 1), prevCellPropsCentralPixels(:, 2)] = transformPointsForward(tform,double(prevCellPropsCentralPixels(:, 1)),double(prevCellPropsCentralPixels(:, 2)));

bucketSize = neighborhoodSize*neighborhoodSize-1;
Mdl = KDTreeSearcher(double(prevCellPropsCentralPixels), 'bucketSize', bucketSize);

cellPropsCentralPixels = cell2mat(currColonyProps.cellProps{:, 'centralPixel'});
cellPropsHasIrregShape = currColonyProps.cellProps{:, 'has_irregular_shape'};

cellPropsPixelList = currColonyProps.cellProps{:, 'pixelList'};
cellPropsOrientation = currColonyProps.cellProps{:, 'orientation'};
cellPropsPrevOrientation = prevColonyProps.cellProps{:, 'orientation'};

bBoxULCorner = currColonyProps.bBoxULCorner;
candidateMotherIndices = cell(height(currColonyProps.cellProps), 1);
rotatedCoords = cell(height(currColonyProps.cellProps), 1);

for i = 1:height(currColonyProps.cellProps)
    
    %get the nearest cell (of the previous colony) to the cell
    %central pixel coordinates (of the current colony)
    if(matchedCellsCurrColony{i, 1} == 0)
        idx = knnsearch(Mdl, cellPropsCentralPixels(i, :) + bBoxULCorner - 1, 'K', 1);
        
        candidateMotherIndices{i, 1} = graphtraverse(adjacencyMatrix, prevCellPropsCentralPixelsLabels(idx), 'Depth', 1, 'Method', 'BFS');
        
        candidateMotherIndices{i, 1} = setdiff(candidateMotherIndices{i, 1}, find(matchedCellsPrevColony{:, 1}));
        cellCentralPixel = cellPropsCentralPixels(i, :);
        cellCentralPixel(1, 2) = -cellCentralPixel(1, 2);
        
        %The orientation is the angle between the x-axis line and the major
        %so in order to rotate a bacterium, firstly it must be translated
        %to (0,0).
        translatedBacteriumCoords = double(cellPropsPixelList{i});
        translatedBacteriumCoords(:, 2) = -translatedBacteriumCoords(:, 2); 
        
        translatedBacteriumCoords(:, 1) = translatedBacteriumCoords(:, 1) - cellCentralPixel(1, 1);
        translatedBacteriumCoords(:, 2) = translatedBacteriumCoords(:, 2) - cellCentralPixel(1, 2);
        
        theta1 = mod(cellPropsOrientation(i, 1), 180);
        for j = 1:length(candidateMotherIndices{i, 1})
            theta2 = mod(cellPropsPrevOrientation(candidateMotherIndices{i, 1}(j), 1), 180);
            rot_angle = theta1 -theta2;
            
            %filamentous objects must not be rotated!!!
            if(abs(rot_angle) > 1 && enableCellRotation && prevCellPropsHasIrregShape(candidateMotherIndices{i, 1}(j)) == 0 && cellPropsHasIrregShape(i) == 0)

                
                R = [cosd(rot_angle) -sind(rot_angle); sind(rot_angle) cosd(rot_angle)];
                
                rotatedBacteriumCoords = translatedBacteriumCoords*R;
                
                rotatedCoords{i}{candidateMotherIndices{i, 1}(j)}(:, 1) = rotatedBacteriumCoords(:, 1) + cellCentralPixel(1, 1);
                rotatedCoords{i}{candidateMotherIndices{i, 1}(j)}(:, 2) = -(rotatedBacteriumCoords(:, 2) + cellCentralPixel(1, 2));
                
                if(verb)
                    figure('position',[128,128, 1024, 512])
                    
                    subplot(1,2,1)
                    hold on;
                    plot(double(currColonyProps.cellProps{i, 'pixelList'}{1}(:, 1)), double(currColonyProps.cellProps{i, 'pixelList'}{1}(:, 2)), 'og');
                    plot(rotatedCoords{i}{candidateMotherIndices{i}(j)}(:, 1), rotatedCoords{i}{candidateMotherIndices{i}(j)}(:, 2), '.r');
                    set(gca, 'Ydir', 'reverse');
                    hold off
                    title('Rotated cell')
                    
                    subplot(1,2,2)
                    pixelList = double(cell2mat(prevColonyProps.cellProps{candidateMotherIndices{i}(j), 'pixelList'}));
                    plot(pixelList(:, 1), pixelList(:, 2), '.b')
                    set(gca, 'Ydir', 'reverse');
                    title('Candidate mother')
                end
            else
                rotatedCoords{i}{candidateMotherIndices{i, 1}(j)} = double(cellPropsPixelList{i});
            end
        end
    end
end
currColonyProps.candidateMotherIndices = candidateMotherIndices;
for i = 1:size(rotatedCoords, 1);
    for j = 1:length(rotatedCoords{i});
        currColonyProps.rotatedCoords{i, j} = rotatedCoords{i}{j};
    end
end


