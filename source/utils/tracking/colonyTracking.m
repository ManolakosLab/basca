function [prevColonyProps, matchedCellsPrevColony, matchedCellsCurrColony, correspondanceMatrix, colonyTrackingTime] = colonyTracking(frame, t, params)

currColonyProps = cell(length(frame(t).colonyProps), 1);
prevColonyProps = cell(length(frame(t).colonyProps), 1);
matchedCellsPrevColony = cell(length(frame(t).colonyProps), 1);
matchedCellsCurrColony = cell(length(frame(t).colonyProps), 1);
correspondanceMatrix = cell(length(frame(t).colonyProps), 1);
colonyTrackingTime = zeros(length(frame(t).colonyProps), 1);
neighbors = cell(length(frame(t).colonyProps), 1);
prevColIndices = cell(length(frame(t).colonyProps), 1);
for j = 1:length(frame(t).colonyProps)
    %if current colony existed in the previous frame, we shall proceed
    %to cell tracking
    if(~isempty(frame(t).colonyProps(j).correspondingColPrevFrameInd))
        
        
        correspondanceMatrix{j, 1} = frame(t-1).correspondanceMatrix;
        currColonyProps{j} = frame(t).colonyProps(j);
        [prevColIndices{j}] = getPrevColonyIndices(frame, t, j, []);
        if(sum(prevColIndices{j}(:,3) == 0) ~= 0)
            
            if(length(frame(t).colonyProps(j).correspondingColPrevFrameInd) > 1)
                props = regionprops(uint8(createBW(frame(t-1).x, frame(t-1).y, vertcat(frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd).pixelList))), 'PixelList', 'Image', 'BoundingBox', 'Area', 'Centroid');
                
                prevColonyProps{j}.bwColony = props.Image;
                
                neighbors{j}(1,:) = [ceil(props.BoundingBox(1)), ceil(props.BoundingBox(2))];
                neighbors{j}(2,:) = [ceil(props.BoundingBox(1)), floor(props.BoundingBox(2))];
                neighbors{j}(3,:) = [floor(props.BoundingBox(1)), ceil(props.BoundingBox(2))];
                neighbors{j}(4,:) = [floor(props.BoundingBox(1)), floor(props.BoundingBox(2))];
                
                prevColonyProps{j}.bBoxULCorner(1, :) = neighbors{j}(knnsearch(neighbors{j}, props.BoundingBox(1:2), 'K', 1), :)-1;
                prevColonyProps{j}.bBoxOffsets = props.BoundingBox(3:4);
                
                prevColonyProps{j}.pixelList = props.PixelList;
                cellcounter = 1;
                for k = 1:length(frame(t).colonyProps(j).correspondingColPrevFrameInd)
                    numOfCells = height(frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)).cellProps);
                    if(k == 1)
                        prevColonyProps{j}.cellProps = [frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)).cellProps table(num2cell(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)*ones(numOfCells, 1)), 'VariableNames', {'colonyId'})];
                    else
                        prevColonyProps{j}.cellProps = [prevColonyProps{j}.cellProps; [frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)).cellProps table(num2cell(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)*ones(numOfCells, 1)), 'VariableNames', {'colonyId'})]];
                    end
                    for m = 1:numOfCells
                        prevColonyProps{j}.cellProps{cellcounter, 'pixelList'}{1}(1:end, 1) = prevColonyProps{j}.cellProps{cellcounter, 'pixelList'}{1}(1:end, 1) + frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)).bBoxULCorner(1, 1);
                        prevColonyProps{j}.cellProps{cellcounter, 'pixelList'}{1}(1:end, 2) = prevColonyProps{j}.cellProps{cellcounter, 'pixelList'}{1}(1:end, 2) + frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)).bBoxULCorner(1, 2);
                        prevColonyProps{j}.cellProps{cellcounter, 'pixelList'}{1}(1:end, 1) = prevColonyProps{j}.cellProps{cellcounter, 'pixelList'}{1}(1:end, 1) - prevColonyProps{j}.bBoxULCorner(1, 1);
                        prevColonyProps{j}.cellProps{cellcounter, 'pixelList'}{1}(1:end, 2) = prevColonyProps{j}.cellProps{cellcounter, 'pixelList'}{1}(1:end, 2) - prevColonyProps{j}.bBoxULCorner(1, 2);
                        
                        prevColonyProps{j}.cellProps{cellcounter, 'centroid'}{1}(1, :) = mean(double(prevColonyProps{j}.cellProps{cellcounter, 'pixelList'}{1}));
                        
                        prevColonyProps{j}.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 1) = prevColonyProps{j}.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 1) + frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)).bBoxULCorner(1, 1);
                        prevColonyProps{j}.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 2) = prevColonyProps{j}.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 2) + frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)).bBoxULCorner(1, 2);
                        prevColonyProps{j}.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 1) = prevColonyProps{j}.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 1) - prevColonyProps{j}.bBoxULCorner(1, 1);
                        prevColonyProps{j}.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 2) = prevColonyProps{j}.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 2) - prevColonyProps{j}.bBoxULCorner(1, 2);
                        cellcounter = cellcounter + 1;
                    end
                end
            else
                prevColonyProps{j} = frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd(1));
                prevColonyProps{j}.cellProps = [prevColonyProps{j}.cellProps table(num2cell(frame(t).colonyProps(j).correspondingColPrevFrameInd*ones(height(prevColonyProps{j}.cellProps), 1)), 'VariableNames', {'colonyId'})];
            end
            
            
            matchedCellsPrevColony{j} = table(zeros(height(prevColonyProps{j}.cellProps), 1), 'VariableNames', {'isMatched'}, 'RowNames', prevColonyProps{j}.cellProps.Properties.RowNames);
            
            matchedCellsCurrColony{j} = table(zeros(height(frame(t).colonyProps(j).cellProps), 1), 'VariableNames', {'isMatched'}, 'RowNames', frame(t).colonyProps(j).cellProps.Properties.RowNames);
        end
        %automatic correction algorithm can operate iff the colony does
    end
end


parfor j = 1:length(frame(t).colonyProps)
    if(~isempty(matchedCellsCurrColony{j}))
        fprintf('colony id = %d\n', j);
        
        tic;
        [correspondanceMatrix{j}, matchedCellsCurrColony{j}, matchedCellsPrevColony{j}] = trackingNeighborhood(correspondanceMatrix{j}, currColonyProps{j}, prevColonyProps{j}, matchedCellsCurrColony{j}, matchedCellsPrevColony{j}, params);
        colonyTrackingTime(j, 1) = toc;
    end
end