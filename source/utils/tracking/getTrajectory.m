function  [frame_indices, colony_indices, cellInst_indices, cellInst_ids, motherCellInstFound, logs] = getTrajectory(frame, t, currColonyInd, motherCellInstId)

    frame_indices = [];
    colony_indices = [];
    cellInst_indices = {};
    cellInst_ids = {};
    branchLength = 1;
    divisionsCount = 0;
    while(1)
        prevFrameInd = t - branchLength;
        frame_indices = [prevFrameInd; frame_indices];

        prevColonyInd = getColonyIndex(frame(prevFrameInd), motherCellInstId, frame(prevFrameInd+1).colonyProps(currColonyInd).correspondingColPrevFrameInd);
        colony_indices = [prevColonyInd; colony_indices];

        motherCellInstInd = find(strcmp(frame(prevFrameInd).colonyProps(prevColonyInd).cellProps.Properties.RowNames,  motherCellInstId));
        cellInst_indices = [{motherCellInstInd}; cellInst_indices];
        cellInst_ids = [motherCellInstId; cellInst_ids];
        %cell that divided
        if sum(~isnan(frame(prevFrameInd+1).colonyProps(currColonyInd).correspondanceMatrix{ motherCellInstInd, :})) > 1 
            motherCellInstFound = true;
           if length(frame_indices) > 1
                logs = {sprintf('Cell was born at %s colony %d (cell instant %d) and lived until %s colony %d (cell instant %d) (before division)\n', frame(prevFrameInd).id, prevColonyInd, motherCellInstInd, frame(frame_indices(end)).id, colony_indices(end), cellInst_indices{end})};
                break;
           end
        end
        %cell instant of a colony that came in the field of view in current
        %frame, thus there is no correspondance matrix in the previous
        %frame
        if ~isfield(frame(prevFrameInd).colonyProps(prevColonyInd), 'correspondanceMatrix')
            motherCellInstFound = false;
            logs = {sprintf('Cell entered in field of view at %s colony %d (cell instant %d) and lived until %s colony %d (cell instant %d) (before division)\n', frame(prevFrameInd).id, prevColonyInd, motherCellInstInd, frame(frame_indices(end)).id, colony_indices(end), cellInst_indices{end})};
            break;
        end    
        if ~isempty(frame(prevFrameInd).colonyProps(prevColonyInd).correspondanceMatrix)
            motherCellInstId = frame(prevFrameInd).colonyProps(prevColonyInd).correspondanceMatrix.Properties.RowNames(~isnan(frame(prevFrameInd).colonyProps(prevColonyInd).correspondanceMatrix{:, motherCellInstId}));
        else
            motherCellInstId=[];
        end
        if isempty(motherCellInstId)
            %cell instant is not matched to mother cell instant
            logs = {sprintf('Cell instant %d of %s colony %d is not matched to a cell instant at frame %d\n', cellInst_indices{end}, frame(frame_indices(end)).id, colony_indices(end), frame(prevFrameInd).id, prevColonyInd)};           
            motherCellInstFound = false;
            break;
        end
        currColonyInd = prevColonyInd;
        branchLength =  branchLength + 1;
    end
end