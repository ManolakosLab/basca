function [frame] = initCellTrackingStep(frame, t, j, enableCellRotation, neighborhoodSize, verb)
% Initializes variables neccessary for the tracking step of the current colony's cell instants.
%
%[frame, lineageTrees, matchedCellsCurrColony, matchedCellsPrevColony] = initCellTrackingStep(FLTs, frame, t, j, enableCellRotation, neighborhoodSize, verb)
%
%  INPUT :
%    FLTs: a Nx1 cell array containing the lineage tree of each colony,
%    where N is the number of colonies in the first frame plus the number
%    of artifact colonies entering in the field of view (before a merging 
%    event). The function adds a lineage tree for such artifact colonies in
%    order to track their cell instants independently, i.e. without mixing
%    the with cell instants of a colony that is in the field of view.
%
%    frame : a struct array containing the colonyProps struct-array and
%    cellProps table. 
%
%    t : an integer showing the frame index for the tracking step
%    initialization, i.e. cell instants matched between frame t and t-1.
%
%    j : the index of a colony (in current frame) the cells of which are 
%    going to be matched.
%
%    enableCellRotation : a logical value showing whether to rotate cells 
%    to match with the orientation of the candidate mothers or not. Notice
%    that when the cell instants (either mothers or daughters) have
%    irregular shape the function does not rotate the cells even if 
%    <enableCellRotation> is True. 
%
%    neighborhoodSize : the function searches for the adjacent cells of a 
%    cell in the subset of 2*neighborhoodSize^2 nearest neighbors, based on
%    the cells' centroids. By doing this, the function quickly rejects cell
%    instants lying far away from a the cell instant.
%
%    verb : a logical value indicating whether to display more information
%    and figures about the function's results.
%
%  OUTPUT :
%    frame : the frame struct updated with the new fields in j-th 
%    colonyProps. See the new field below:
%    -candidateMotherIndices : a Nx1 cell-array containing the indices of
%     candidate mothers at the cellProps table of the previous frame, where
%     N is the number of cell instants of the current frame.
%    -rotatedCoords : a Nx1 cell-array containing the rotated coordinates
%     of cell instants of current frame.
%    Note that when a cell instant has n candidate mothers, then in 
%    <rotateCoords> the function stores the rotated coordinates of the
%    current cell instant corresponding to each candidate mother
%    orientation.
%    -prevColonyProps : it is a temp struct containg the colonyProps of the
%    corresponding colony in previous frame. When a merging event occured 
%    in current frame then the function creates a new pseudo colony
%    containing the colonies of previous frame that merged in current frame.
%    This is done because tracking algorithm matches the cells in 
%    consecutive frames (frame t with frame t-1), in 1 to 1 colony 
%    correspondance.
%
%    colonyLTs : a cell array containing the lineage trees that correspond
%    to the j-th colony. A colony that merged in the current frame must be
%    connected with more than one lineage trees, e.g. if two colonies were
%    merged in current frame creating a new colony (j-th colony), then two
%    LTs must be connected with that colony.
%    
%    matchedCellsCurrColony : a logical array of size Nx1, where N is the 
%    number of currrent colony's cells. If a value of the array is true, it 
%    shows that the i-th cell instant is matched with a cell instant of the
%    previous colony.
%   
%    matchedCellsPrevColony : a logical array of size Mx1, where M is the 
%    number of previous colony's cells. If a value of the array is true, it 
%    shows that the j-th cell instant is matched with a cell instant of the
%    current colony.
%
%
%  Note that if the autocorrectErrors option is activated then the frame 
%  struct output will be different from the frame struct input, because
%  cell instant surfaces may have been modified by the function, during
%  autocorrection step.
%   
%
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
% 
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.

[prevColIndices] = getPrevColonyIndices(frame, t, j, []);
if(sum(prevColIndices(:, 1) == 1) ~= 0 && sum(prevColIndices(:, 3) ~= 1) ~= 0 && height(frame(t).colonyProps(j).cellProps) > 0)
    
%     frame(t).colonyProps(j).lineageTreeIndices = [];
%     for ii = 1:length(frame(t).colonyProps(j).correspondingColPrevFrameInd)
%         i = frame(t).colonyProps(j).correspondingColPrevFrameInd(ii);
%         if(~isfield(frame(t-1).colonyProps(i), 'lineageTreeIndices') || isempty(frame(t-1).colonyProps(i).lineageTreeIndices))
%             jj = length(FLTs) + 1;
%             FLTs{jj} = tree(sprintf('i%07d_j%07d',t-1, i));
%             frame(t-1).colonyProps(i).lineageTreeIndices = jj;
%         end
%         frame(t).colonyProps(j).lineageTreeIndices = [frame(t).colonyProps(j).lineageTreeIndices; frame(t-1).colonyProps(i).lineageTreeIndices];
%     end
    
    
    if(length(frame(t).colonyProps(j).correspondingColPrevFrameInd) > 1)
        
        props = regionprops(uint8(createBW(frame(t-1).x, frame(t-1).y, vertcat(frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd).pixelList))), 'PixelList', 'BoundingBox', 'Area', 'Centroid');
        
        frame(t).colonyProps(j).prevColonyProps.bwColony = uint8(createBW(frame(t-1).x, frame(t-1).y, vertcat(frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd).pixelList)));
        
        frame(t).colonyProps(j).prevColonyProps.adjacencyMatrix = [];
        frame(t).colonyProps(j).prevColonyProps.bBoxULCorner(1, :) = ceil(props.BoundingBox(1:2));
        frame(t).colonyProps(j).prevColonyProps.bBoxOffsets = [props.BoundingBox(3)-1 props.BoundingBox(4)-1];        
        frame(t).colonyProps(j).prevColonyProps.pixelList = props.PixelList; 

        rows = frame(t).colonyProps(j).prevColonyProps.bBoxULCorner(2):(frame(t).colonyProps(j).prevColonyProps.bBoxULCorner(2)+frame(t).colonyProps(j).prevColonyProps.bBoxOffsets(2));
        columns = frame(t).colonyProps(j).prevColonyProps.bBoxULCorner(1):(frame(t).colonyProps(j).prevColonyProps.bBoxULCorner(1)+frame(t).colonyProps(j).prevColonyProps.bBoxOffsets(1));
        %frame(t).colonyProps(j).prevColonyProps.bwColony = logical(frame(t-1).bwInitSegmentation(rows, columns));
        %frame(t).colonyProps(j).prevColonyProps.bwColonyMask = logical(frame(t-1).bwColoniesMask(rows, columns));
        frame(t).colonyProps(j).prevColonyProps.IColony = frame(t-1).gray_im(rows, columns);
        
        
        cellcounter = 1;
        for k = 1:length(frame(t).colonyProps(j).correspondingColPrevFrameInd)
            
            numOfCells = height(frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)).cellProps);
            if(k == 1)
                frame(t).colonyProps(j).prevColonyProps.adjacencyMatrix = frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)).adjacencyMatrix;
                frame(t).colonyProps(j).prevColonyProps.cellProps = [frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)).cellProps table(num2cell(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)*ones(numOfCells, 1)), 'VariableNames', {'colonyId'})];
            else
                if numOfCells == 0
                    continue
                end                
                frame(t).colonyProps(j).prevColonyProps.adjacencyMatrix = blkdiag(frame(t).colonyProps(j).prevColonyProps.adjacencyMatrix, frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)).adjacencyMatrix);
                frame(t).colonyProps(j).prevColonyProps.cellProps = [frame(t).colonyProps(j).prevColonyProps.cellProps; [frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)).cellProps table(num2cell(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)*ones(numOfCells, 1)), 'VariableNames', {'colonyId'})]];
            end
            for m = 1:numOfCells
                frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'pixelList'}{1}(1:end, 1) = frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'pixelList'}{1}(1:end, 1) + frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)).bBoxULCorner(1, 1) - 1;
                frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'pixelList'}{1}(1:end, 2) = frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'pixelList'}{1}(1:end, 2) + frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)).bBoxULCorner(1, 2) - 1;
                frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'pixelList'}{1}(1:end, 1) = frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'pixelList'}{1}(1:end, 1) - frame(t).colonyProps(j).prevColonyProps.bBoxULCorner(1, 1) + 1;
                frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'pixelList'}{1}(1:end, 2) = frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'pixelList'}{1}(1:end, 2) - frame(t).colonyProps(j).prevColonyProps.bBoxULCorner(1, 2) + 1;

                frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'centroid'}{1}(1, 1) = frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'centroid'}{1}(1, 1) + frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)).bBoxULCorner(1, 1) - 1;
                frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'centroid'}{1}(1, 2) = frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'centroid'}{1}(1, 2) + frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)).bBoxULCorner(1, 2) - 1;
                frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'centroid'}{1}(1, 1) = frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'centroid'}{1}(1, 1) - frame(t).colonyProps(j).prevColonyProps.bBoxULCorner(1, 1) + 1;
                frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'centroid'}{1}(1, 2) = frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'centroid'}{1}(1, 2) - frame(t).colonyProps(j).prevColonyProps.bBoxULCorner(1, 2) + 1;

                frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'centralPixel'}{1}(1, 1) = frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'centralPixel'}{1}(1, 1) + frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)).bBoxULCorner(1, 1) - 1;
                frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'centralPixel'}{1}(1, 2) = frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'centralPixel'}{1}(1, 2) + frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)).bBoxULCorner(1, 2) - 1;
                frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'centralPixel'}{1}(1, 1) = frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'centralPixel'}{1}(1, 1) - frame(t).colonyProps(j).prevColonyProps.bBoxULCorner(1, 1) + 1;
                frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'centralPixel'}{1}(1, 2) = frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'centralPixel'}{1}(1, 2) - frame(t).colonyProps(j).prevColonyProps.bBoxULCorner(1, 2) + 1;
                
                frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 1) = frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 1) + frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)).bBoxULCorner(1, 1) - 1;
                frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 2) = frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 2) + frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd(k)).bBoxULCorner(1, 2) - 1;
                frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 1) = frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 1) - frame(t).colonyProps(j).prevColonyProps.bBoxULCorner(1, 1) + 1;
                frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 2) = frame(t).colonyProps(j).prevColonyProps.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 2) - frame(t).colonyProps(j).prevColonyProps.bBoxULCorner(1, 2) + 1;
                cellcounter = cellcounter + 1;
            end            
        end
    else
        frame(t).colonyProps(j).prevColonyProps = frame(t-1).colonyProps(frame(t).colonyProps(j).correspondingColPrevFrameInd(1));
        frame(t).colonyProps(j).prevColonyProps.cellProps = [frame(t).colonyProps(j).prevColonyProps.cellProps table(num2cell(frame(t).colonyProps(j).correspondingColPrevFrameInd*ones(height(frame(t).colonyProps(j).prevColonyProps.cellProps), 1)), 'VariableNames', {'colonyId'})];
    end
    
    %colonyLTs = FLTs(frame(t).colonyProps(j).lineageTreeIndices);
    frame(t).colonyProps(j).matchedCellsPrevColony = table(zeros(height(frame(t).colonyProps(j).prevColonyProps.cellProps), 1), 'VariableNames', {'isMatched'}, 'RowNames', frame(t).colonyProps(j).prevColonyProps.cellProps.Properties.RowNames);
    frame(t).colonyProps(j).matchedCellsCurrColony = table(zeros(height(frame(t).colonyProps(j).cellProps), 1), 'VariableNames', {'isMatched'}, 'RowNames', frame(t).colonyProps(j).cellProps.Properties.RowNames);
    
    currColonyCellIds =  frame(t).colonyProps(j).cellProps.Properties.RowNames;
    prevColonyCellIds = frame(t).colonyProps(j).prevColonyProps.cellProps.Properties.RowNames;
        
    frame(t).colonyProps(j).correspondanceMatrix = NaN(length(prevColonyCellIds), length(currColonyCellIds));
    frame(t).colonyProps(j).correspondanceMatrix = array2table(frame(t).colonyProps(j).correspondanceMatrix);
    frame(t).colonyProps(j).correspondanceMatrix.Properties.RowNames = prevColonyCellIds;
    frame(t).colonyProps(j).correspondanceMatrix.Properties.VariableNames = currColonyCellIds;
    

    %find candidate mother cell instants for each cell instant of the current colony,
    %rotate the cells instants to match with their candidate mother, if
    %enableCellRotation = True.
    
    frame(t).colonyProps(j).candidateMotherIndices = [];
    frame(t).colonyProps(j).rotatedCoords = [];
    [frame(t).colonyProps(j)] = findCandidateMothers(frame(t).colonyProps(j), frame(t).colonyProps(j).prevColonyProps, frame(t).colonyProps(j).matchedCellsCurrColony, frame(t).colonyProps(j).matchedCellsPrevColony, frame(t).tform, enableCellRotation, neighborhoodSize, verb);
else
    colonyLTs = [];
    frame(t).colonyProps(j).matchedCellsCurrColony = [];
    frame(t).colonyProps(j).matchedCellsPrevColony = [];
end