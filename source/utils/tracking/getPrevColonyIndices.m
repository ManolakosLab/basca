function [prevColIndices] = getPrevColonyIndices(frame, t, prevColInd, prevColIndices)

prevColIndices = [prevColIndices; [t, prevColInd, frame(t).colonyProps(prevColInd).isOnBorder]];

if(isnan(frame(t).colonyProps(prevColInd).correspondingColPrevFrameInd))
    return;
end

% if(frame(t).colonyProps(prevColInd).isOnBorder == 0)
%     return;
% end

% if(length(frame(t).colonyProps(prevColInd).correspondingColPrevFrameInd) > 1)
%     mergingEvents = [mergingEvents; [t, prevColInd, frame(t).colonyProps(prevColInd).correspondingColPrevFrameInd' ]];
% end

for jj = 1:length(frame(t).colonyProps(prevColInd).correspondingColPrevFrameInd)
    [prevColIndices] = getPrevColonyIndices(frame, t-1, frame(t).colonyProps(prevColInd).correspondingColPrevFrameInd(jj), prevColIndices); 
end