function [Y, YI] = getNNMahalanobis(X, mu, s, thresh)

dist = zeros(size(X, 1), 1);
parfor i = 1:size(X, 1);
    dist(i) = (mu-X(i, :))*(squeeze(s(:, :, 1))\(mu-X(i, :))');
end

[~, sortedDistInd] = sort(dist, 'ascend');

if(thresh < length(X))
    Y = X(sortedDistInd(1:thresh), :);
    YI = sortedDistInd(1:thresh);
else
    Y  = X;
    YI = 1:size(X, 1);
end

