function [frame] = propagateCorrection(frame, correctedFrame, t, i, correctionList, trackingTime, correctionTime)

if(~isempty(correctedFrame))
    
    for fn = fieldnames(correctedFrame(t).colonyProps(i))'
        if(~strcmp(fn{1}, 'rotatedCoords') && ~strcmp(fn{1}, 'candidateMotherIndices'))
            frame(t).colonyProps(i).(fn{1}) = correctedFrame(t).colonyProps(i).(fn{1});
        end
    end
    frame(t).colonyProps(i).correctionList = correctionList;
end

frame(t).colonyProps(i).trackingTime = trackingTime;
frame(t).colonyProps(i).correctionTime = correctionTime;
frame(t).trackingTime = frame(t).trackingTime + frame(t).colonyProps(i).trackingTime;
frame(t).correctionTime = frame(t).correctionTime+frame(t).colonyProps(i).correctionTime;

%copying the corrections
for ii = 1:size(correctionList, 1)
    if(correctionList(ii, 1) ~= t)
        for fn = fieldnames(frame(correctionList(ii, 1)).colonyProps(correctionList(ii, 2)))'
            if(~strcmp(fn{1}, 'rotatedCoords') && ~strcmp(fn{1}, 'candidateMotherIndices'))
                frame(correctionList(ii, 1)).colonyProps(correctionList(ii, 2)).(fn{1}) = correctedFrame(correctionList(ii, 1)).colonyProps(correctionList(ii, 2)).(fn{1});
            end
        end
    end
end
