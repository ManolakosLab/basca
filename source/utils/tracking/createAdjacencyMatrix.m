function [colonyProps] = createAdjacencyMatrix(colonyProps, neighborDistanceFactor, neighborhoodSize)
% Creates adjacency matrix of a colony's cell instants
%
%[colonyProps] = createAdjacencyMatrix(colonyProps, neighborDistanceFactor, neighborhoodSize)
%
%  INPUT :
%    colonyProps : a struct array containing the cellsProps table generated
%    by object segmentation step.
%
%    neighborDistanceFactor : the function considers as adjacent cells, the
%    cells that the minimum distance between their boundaries is less than
%    neighborDistanceFactor*cellwidth of the cells. If 
%    <neighborDistanceFactor> = 0, the function considers as adjacent cells,
%    the cells that the minimum distance between their boundaries is less
%    than a pixel.
%   
%    configuration : a struct coantaining the necessary paramaters for the
%    cell trackingtracking
%
%    neighborhoodSize : the function searches for the adjacent cells of a 
%    cell in the subset of 2*neighborhoodSize^2 nearest neighbors, based on
%    the cells' centroids. By doing this, the function quickly rejects cell
%    instants lying far away from a the cell instant.
%
%  OUTPUT :
%    colonyProps : colonyProps updated with the <adjacencyMatrix> of the
%    cell instants. The <adjacencyMatrix> is nxn array where n is number of
%    the current colony's cell instants. Each value in the <adjacencyMatrix> 
%    shows the min distance between the boundaries of the i-th and the j-th
%    cell instants.
%
%
%  Note that if the autocorrectErrors option is activated then the frame 
%  struct output will be different from the frame struct input, because
%  cell instant surfaces may have been modified by the function, during
%  autocorrection step.
%   
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
% 
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.



if(~isempty(colonyProps.cellProps))
    k = 2*neighborhoodSize*neighborhoodSize;

    centroids = cell2mat(colonyProps.cellProps{:, 'centroid'});
    Mdl = KDTreeSearcher(centroids, 'distance', 'chebychev', 'bucketSize', k);
    
    colonyProps.adjacencyMatrix = single(zeros(height(colonyProps.cellProps), height(colonyProps.cellProps)));
    
    if(neighborDistanceFactor == 0)
        neighborDistance = 1.0;
    else
        cellWidth = mean(colonyProps.cellProps{:, 'width_in_pixels'});
        neighborDistance = neighborDistanceFactor*cellWidth;
    end
    
    
    for i = 1:size(centroids, 1)
        IdxNN = knnsearch(Mdl, centroids(i, :), 'K', k);
        currCellPixelList = double(colonyProps.cellProps{i, 'boundaryPixelList'}{1});
        for j = 1:length(IdxNN)
            dist = min(min(pdist2(currCellPixelList, double(colonyProps.cellProps{IdxNN(j), 'boundaryPixelList'}{1}), 'chebychev')));
            if(dist <= neighborDistance)
                colonyProps.adjacencyMatrix(i, IdxNN(j)) = dist;
                colonyProps.adjacencyMatrix(IdxNN(j), i) = dist;
            end
            
        end
    end
else
    colonyProps.adjacencyMatrix = [];
end
