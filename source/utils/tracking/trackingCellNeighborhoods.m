function  [frame, logs] = trackingCellNeighborhoods(frame, t, currColonyInd, configuration)
logs =[];
overlapUpperBound = configuration.overlapThresh;
overlapLowerBound = 0.7*configuration.overlapThresh;
overlapStep = 0.1;

%getting the number of cells in current colony (daughter cells).
numOfCellsCurrColony = height(frame(t).colonyProps(currColonyInd).cellProps);
matchedCellsCurrColony = frame(t).colonyProps(currColonyInd).matchedCellsCurrColony;
matchedCellsPrevColony = frame(t).colonyProps(currColonyInd).matchedCellsPrevColony;

if(sum(matchedCellsCurrColony{:, 1}) == 0)
    colonyCentroid = frame(t).colonyProps(currColonyInd).centroid-frame(t).colonyProps(currColonyInd).bBoxULCorner;
    distance = ones(numOfCellsCurrColony, 1);
    for i = 1:numOfCellsCurrColony
        distance(i) = pdist2(double(frame(t).colonyProps(currColonyInd).cellProps{i, 'centroid'}{1}), colonyCentroid , 'euclidean');
    end
    [~, sortedDistInd] = sort(distance, 'descend');
else
    sortedDistInd = 1:numOfCellsCurrColony;
end

adjacencyMatrixCurrColony = frame(t).colonyProps(currColonyInd).adjacencyMatrix;
correspondanceMatrix = frame(t).colonyProps(currColonyInd).correspondanceMatrix;
rotatedCoords = frame(t).colonyProps(currColonyInd).rotatedCoords;
candidateMotherIndices = frame(t).colonyProps(currColonyInd).candidateMotherIndices;
prevColonyProps = frame(t).colonyProps(currColonyInd).prevColonyProps;

iteration = 0;
overlapThresh = overlapUpperBound + overlapStep;
matchedCellsCounter = 1;
while(overlapThresh > overlapLowerBound)
    if(mod(iteration, 2) == 0)
        cellsPenalty = zeros(numOfCellsCurrColony, 1);
        overlapThresh = overlapThresh - overlapStep;
    end
    %prevValue = sum(matchedBacteriaCurrFrame);
    for pseudo_i = 1:numOfCellsCurrColony
        i = sortedDistInd(pseudo_i);
        %if the cell of the current frame is not matched and not excluded
        %it should be matched
        
        if(~matchedCellsCurrColony{i, 1} && cellsPenalty(i) < 3)
            %create the adjacency matrix for this loop (ommit already matchedBacteriaCurrFrame and excludedBacteria)
            thisAdjacencyMatrixCurrColony = adjacencyMatrixCurrColony;
            thisAdjacencyMatrixCurrColony(logical(matchedCellsCurrColony{:, 1})|logical(cellsPenalty >= 3), :) = 0;
            thisAdjacencyMatrixCurrColony(:, logical(matchedCellsCurrColony{:, 1})|logical(cellsPenalty >= 3)) = 0;
          
            %get the neighbors of current cell
            [neighborhoodIndices] = getNeighbors(frame(t).colonyProps(currColonyInd), i, thisAdjacencyMatrixCurrColony, configuration.neighborhoodSize, configuration.neighborDistanceFactor);
            
            %align the current neighborhood with the a neighborhood of the
            %previous frame
            [matchingIndices, matchingCellOverlap, currCellOverlap] = matchNeighborhood(frame(t).colonyProps(currColonyInd), prevColonyProps, frame(t).tform, candidateMotherIndices, rotatedCoords, neighborhoodIndices, configuration.translationStep, configuration.verb);
            
            %just for clearness shake we nullify the indexes (i.e. current
            %cells with 0 overlap)corresponding to no cell in the previous
            %frame
            matchingIndices(matchingCellOverlap == 0) = 0;
            
            
            prevCellIndices = unique(matchingIndices);
            matchings = cell(length(prevCellIndices), 1);
            prevCellOverlap = zeros(length(prevCellIndices), 1);
            for ii = 1:length(prevCellIndices)
                ind = matchingIndices == prevCellIndices(ii);
                matchings{ii} = neighborhoodIndices(ind);
                prevCellOverlap(ii, 1) = sum(matchingCellOverlap(ind));
            end
            
            %when previous cell has low overlap score,
            %either we have wrong matching (1-to-1) or just a missing cell
            %in the current neighborhood for 1-to-2 matching
            %defenitely the matching cannot be accepted in both cases, but
            %only in the first case the cell of current frame should be
            %penalized
            if(sum(prevCellOverlap < overlapThresh) ~= 0 || sum(currCellOverlap == 0) ~= 0)
            %if(sum(prevCellOverlap < overlapThresh) ~= 0 || sum(currCellOverlap < overlapThresh) ~= 0)    
                for ii = 1:length(prevCellOverlap)
                    if(prevCellOverlap(ii) < overlapThresh)
                        
                        %Case to be tested:
                        %try to find a neighboring cell(i.e. sibling) so as
                        %to maximize the overlap of the corresponding cell
                        %if this procedure fails then penalize this cells
                        
                        for jj = 1:length(matchings{ii})
                            
                            %when current cell has low overlap score,
                            %either we have undersegmentation error or it is
                            %just a wrong matching (most frequent scenario)
                            %if(currCellOverlap(matchings{ii}(jj)) < currOverlapThresh)
                            %if(prevCellOverlap(ii) > overlapLowerBound)
                                cellsPenalty(matchings{ii}(jj)) = cellsPenalty(matchings{ii}(jj))+1;
                            %else
                            %    cellsPenalty(matchings{ii}(jj)) = 3;
                            %end
                            %end
                        end
                        
                    end
                end
                cellsPenalty(neighborhoodIndices(currCellOverlap == 0)) = cellsPenalty(neighborhoodIndices(currCellOverlap == 0))+1;
                %cellsPenalty(neighborhoodIndices(currCellOverlap < overlapThresh)) = cellsPenalty(neighborhoodIndices(currCellOverlap < overlapThresh))+1;
                continue;
            end
            
            if(configuration.verb)
                fprintf('[%d', matchingIndices(1));
                for jj = 2:length(matchingIndices)
                    fprintf(', %d', matchingIndices(jj));
                end
                fprintf(']->');
                fprintf('[%d', neighborhoodIndices(1));
                for jj = 2:length(neighborhoodIndices)
                    fprintf(', %d', neighborhoodIndices(jj));
                end
                fprintf(']\n');
                fprintf('previous neighborhood score: %4.2f\n', sum(prevCellOverlap)/length(prevCellOverlap));
                fprintf('current neighborhood score: %4.2f\n', sum(currCellOverlap)/length(currCellOverlap));
            end
            
            for j = 1:length(neighborhoodIndices)
                if(correspondanceMatrix{prevColonyProps.cellProps(matchingIndices(j), :).Properties.RowNames{1}, frame(t).colonyProps(currColonyInd).cellProps(neighborhoodIndices(j), :).Properties.RowNames{1}} > matchingCellOverlap(j))
                    cellsPenalty(neighborhoodIndices(j)) = cellsPenalty(neighborhoodIndices(j))+1;
                    neighborhoodIndices = [];
                end
            end
            if(~isempty(neighborhoodIndices))
                for j = 1:length(neighborhoodIndices)
                    matchedCellsPrevColony{matchingIndices(j), 1} =  1;
                    matchedCellsCurrColony{neighborhoodIndices(j), 1} = 1;
                    
                    prevColonyInd = getColonyIndex(frame(t-1), prevColonyProps.cellProps(matchingIndices(j), :).Properties.RowNames{1}, frame(t).colonyProps(currColonyInd).correspondingColPrevFrameInd);
                    
                    logs{matchedCellsCounter} = sprintf('cell %d of %s colony %d was matched with cell(s) %d of %s colony %d\n', matchingIndices(j), frame(t-1).id, prevColonyInd, neighborhoodIndices(j), frame(t).id, currColonyInd);
                    correspondanceMatrix{prevColonyProps.cellProps(matchingIndices(j), :).Properties.RowNames{1}, frame(t).colonyProps(currColonyInd).cellProps(neighborhoodIndices(j), :).Properties.RowNames{1}} = matchingCellOverlap(j);
                    matchedCellsCounter = matchedCellsCounter + 1;
                end
                for ii = 1:length(matchingIndices)
                    for j = 1:numOfCellsCurrColony
                        if(~isempty(candidateMotherIndices{j}))
                            candidateMotherIndices{j}(candidateMotherIndices{j} == matchingIndices(ii)) = []; 
                        end
                    end
                end
            end
        end
    end
    iteration = iteration + 1;
end

frame(t).colonyProps(currColonyInd).correspondanceMatrix = correspondanceMatrix;
frame(t).colonyProps(currColonyInd).rotatedCoords = rotatedCoords;
frame(t).colonyProps(currColonyInd).candidateMotherIndices = candidateMotherIndices;
frame(t).colonyProps(currColonyInd).matchedCellsPrevColony = matchedCellsPrevColony;
frame(t).colonyProps(currColonyInd).matchedCellsCurrColony = matchedCellsCurrColony;

end


