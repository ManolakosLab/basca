function [nextColIndices] = getNextColonyIndices(frame, t, nextColInd, nextColIndices)

nextColIndices = [nextColIndices; {t, nextColInd, frame(t).colonyProps(nextColInd).isOnBorder, frame(t).colonyProps(nextColInd).correspondingColPrevFrameInd }];
if(isnan(frame(t).colonyProps(nextColInd).correspondingColNextFrameInd))
    return;
end

[nextColIndices] = getNextColonyIndices(frame, t+1, frame(t).colonyProps(nextColInd).correspondingColNextFrameInd, nextColIndices); 