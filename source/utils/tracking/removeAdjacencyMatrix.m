function [colonyProps] = removeAdjacencyMatrix(colonyProps, indices)

colonyProps.adjacencyMatrix(indices, :) = [];
colonyProps.adjacencyMatrix(:, indices) = [];

end
