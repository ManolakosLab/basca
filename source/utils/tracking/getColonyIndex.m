function colonyInd = getColonyIndex(frame, cellId, candidateColonyIndices)
colonyInd = [];
for i = 1:length(candidateColonyIndices)
    
    cellIds = frame.colonyProps(candidateColonyIndices(i)).cellProps.Properties.RowNames;
    if(sum(strcmp(cellId, cellIds)) == true)
        colonyInd = candidateColonyIndices(i);
        return;
    end
end
if(isempty(colonyInd))

end

end