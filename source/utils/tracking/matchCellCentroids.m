function [centroids, sigma, pp]=matchCellCentroids(prevColonyProps, currColonyProps, matchedCellPrevFrame, matchedCellCurrFrame, verb)

prevColonyCellIds = prevColonyProps.cellProps.Properties.RowNames;
prevFrameObjectCoords = [];
prevCellCentroid = zeros(length(matchedCellPrevFrame),2);
for i = 1:length(matchedCellPrevFrame)
    prevFrameObjectCoords = [prevFrameObjectCoords; double(cell2mat(prevColonyProps.cellProps{prevColonyCellIds{matchedCellPrevFrame(i)}, 'pixelList'}))];
    prevCellCentroid(i, :) = cell2mat(prevColonyProps.cellProps{prevColonyCellIds{matchedCellPrevFrame(i)}, 'centroid'});
end

prevFrameObjectCentroid = mean(prevFrameObjectCoords);

currCellCoords(:, 1) = double(currColonyProps.cellProps{matchedCellCurrFrame, 'pixelList'}{1}(:, 1));
currCellCoords(:, 2) = double(currColonyProps.cellProps{matchedCellCurrFrame, 'pixelList'}{1}(:, 2));

currCellCoords(:, 1) = currCellCoords(:, 1) - currColonyProps.cellProps{matchedCellCurrFrame, 'centroid'}{1}(1, 1) + prevFrameObjectCentroid(:, 1);
currCellCoords(:, 2) = currCellCoords(:, 2) - currColonyProps.cellProps{matchedCellCurrFrame, 'centroid'}{1}(1, 2) + prevFrameObjectCentroid(:, 2);

[x, y] = size(currColonyProps.bwColony);
bwObject = createBW(x, y, cell2mat(currColonyProps.cellProps{matchedCellCurrFrame, 'pixelList'}));

[bwObjectCL, endpoints] = objectCenterline(bwObject);
objectCLlen = sum(sum(bwObjectCL));

objectCLVertices = bwtraceboundary(bwObjectCL, [endpoints(2, 1) endpoints(1, 1)], 'N', 8, objectCLlen);
objectCLVertices = [objectCLVertices(:, 2) objectCLVertices(:, 1)];
objectCLVerticesTranslated(:, 1) = objectCLVertices(:, 1) - currColonyProps.cellProps{matchedCellCurrFrame, 'centroid'}{1}(1, 1) + prevFrameObjectCentroid(:, 1);
objectCLVerticesTranslated(:, 2) = objectCLVertices(:, 2) - currColonyProps.cellProps{matchedCellCurrFrame, 'centroid'}{1}(1, 2) + prevFrameObjectCentroid(:, 2);

IDX = knnsearch([objectCLVerticesTranslated(:, 1) -objectCLVerticesTranslated(:, 2)],  [prevCellCentroid(:, 1) -prevCellCentroid(:, 2)]);
% IDX = sort(IDX);
% if(verb)
%     figure;
%     hold on;
%     plot(prevFrameObjectCoords(:, 1), -prevFrameObjectCoords(:, 2), 'r.')
%     plot(prevFrameObjectCentroid(:, 1), -prevFrameObjectCentroid(:, 2), 'k+')
%     plot(currCellCoords(:, 1), -currCellCoords(:, 2), 'ob')
%     for i = 1:length(matchedCellPrevFrame)
%         plot(prevCellCentroid(i, 1), -prevCellCentroid(i,2), '*k');
%     end
%     plot(objectCLVerticesTranslated(IDX(1):IDX(end), 1), -objectCLVerticesTranslated(IDX(1):IDX(end), 2), 'xk');
%     hold off;
% end
% splitIntervalCoords = objectCLVertices(IDX(1):IDX(end), :);
centroids = objectCLVertices(IDX, :);
centroids(:, 2) = - centroids(:, 2);
sigma = zeros(2, 2, length(matchedCellPrevFrame));
pp = zeros(length(matchedCellPrevFrame), 1);
for ii = 1:length(matchedCellPrevFrame)
    sigma(:, :, ii) = cov([double(prevColonyProps.cellProps{matchedCellPrevFrame(ii), 'pixelList'}{1}(:, 1)) -double(prevColonyProps.cellProps{matchedCellPrevFrame(ii), 'pixelList'}{1}(:, 2))]);
    pp(ii) = length(prevColonyProps.cellProps{matchedCellPrevFrame(ii), 'pixelList'}{1});
end
pp = pp/sum(pp);
end

