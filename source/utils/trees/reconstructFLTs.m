function FLTs = reconstructFLTs(frame, t)

%create the forest of lineage trees
FLTs = tree('root');

for j = 1:length(frame(1).colonyProps)
    %ignore a colony if it lies on the frame's border.
    if(frame(1).colonyProps(j).isOnBorder)
        continue;
    end
    
    %create the label of the LT's root node for each colony.
    [FLTs, newNodeId] = FLTs.addnode(1, sprintf('i0000001_j%07d', j));
    for jj = 1:height(frame(1).colonyProps(j).cellProps)
        currCellId = sprintf('x%07d_y%07d', ...
            round((frame(1).colonyProps(j).cellProps{jj, 'centroid'}{1}(1, 1)+frame(1).colonyProps(j).bBoxULCorner(1, 1))*1000) , ...
            round((frame(1).colonyProps(j).cellProps{jj, 'centroid'}{1}(1, 2)+frame(1).colonyProps(j).bBoxULCorner(1, 2))*1000));
        FLTs = FLTs.addnode(newNodeId, currCellId);
    end
end


for i = 2:t
    for j = 1:length(frame(i).colonyProps)
        
        %if there is no correspondance matrix (i.e. the colony is ignored
        %from the analysis), proceed to the next colony
        if isempty(frame(i).colonyProps(j).correspondanceMatrix)
            continue;
        end
        prevFrameCellIds = frame(i).colonyProps(j).correspondanceMatrix.Properties.RowNames;
        
        %insert new nodes to the tree, for each cell of previous frame add
        %the corresponding cell(s) of current frame
        for k = 1:height(frame(i).colonyProps(j).correspondanceMatrix)
            
            %find the cell of the previous frame in the tree
            motherCellInstPtr = find (strcmp(FLTs, prevFrameCellIds{k}), 1, 'last');            
            if (strcmp(prevFrameCellIds{k}, 'x0877878_y0553299'))
            end
            %if the cell of the previous frame is not found,
            %insert a new node for this cell
            if(isempty(motherCellInstPtr))
                continue;
                %find the subtree for the specific colony in the previous
                %colony
                
                [prevColIndices] = getPrevColonyIndices(frame, i, j, []);
                
                colonyNodeId = sprintf('i%07d_j%07d', prevColIndices(2, 1), prevColIndices(2, 2));
                
                colonyNodePtr = find (strcmp(FLTs, colonyNodeId), 1, 'last');
                if(isempty(colonyNodePtr))
                    %add a colony node, if the colony entered in the field
                    %of view
                    nodePtr = 1;
                    for ii = 1:i-2
                        [FLTs, nodePtr] = FLTs.addnode(nodePtr, sprintf('i%07d_j%07d', 0, prevColIndices(2, 2)));
                    end
                    [FLTs, nodePtr] = FLTs.addnode(nodePtr, colonyNodeId);
%                 else
%                     nodePtr = colonyNodePtr;
%                     %make the tree branch equal to current depth
%                     for ii = 1:i-1
%                         [FLTs, nodePtr] = FLTs.addnode(nodePtr, sprintf('x%07d_y%07d', 0, 0));
%                     end
                end
                
                %add the new node for the cell in previous frame
                [FLTs, motherCellInstPtr] = FLTs.addnode(nodePtr, prevFrameCellIds{k});
            end

            %add node(s) for the cell(s) of current frame corresponding to
            %the node of previous cell
            currFrameCellIds = frame(i).colonyProps(j).correspondanceMatrix.Properties.VariableNames;
            currFrameCellIds = currFrameCellIds';
            currFrameCellLogicalVector = ~isnan(frame(i).colonyProps(j).correspondanceMatrix{prevFrameCellIds{k}, :});
            currFrameCellIndices = find(currFrameCellLogicalVector);
            for kk = 1:length(currFrameCellIndices)
                FLTs = FLTs.addnode(motherCellInstPtr, currFrameCellIds{currFrameCellIndices(kk)});
            end
        end
    end
end


% centroids = cell2mat(frame(i).colonyProps(j).cellProps{:, 'centroid'});
% 
% %insert new nodes to the tree, for the unmatched cells of current
% %frame
% currFrameCellIds = frame(i).colonyProps(j).correspondanceMatrix.Properties.VariableNames;
% for k = 1:length(frame(i).colonyProps(j).correspondanceMatrix.Properties.VariableNames)
%     
%     if(nansum(frame(i).colonyProps(j).correspondanceMatrix{:, currFrameCellIds{k}}) == 0)
%         
%         [neighborhoodIndices] = getNeighbors(frame(i).colonyProps(j), k, frame(i).colonyProps(j).adjacencyMatrix, configuration.neighborhoodSize + 1, configuration.neighborDistanceFactor);
%         neighborhoodIndices(1) = [];
%         if(isempty(neighborhoodIndices))
%             neighborhoodIndices = knnsearch(centroids, cell2mat(frame(i).colonyProps(j).cellProps{currFrameCellIds{k}, 'centroid'}),'K', 9);
%             neighborhoodIndices(1) = [];
%         end
%         lineageTreeInd = zeros(length(neighborhoodIndices), 1);
%         for ii = 1:length(neighborhoodIndices)
%             [~, lineageTreeInd(ii, 1)] = findLineageTree(lineageTrees, currFrameCellIds{neighborhoodIndices(ii)});
%         end
%         ancestralColIndex = frame(i).colonyProps(j).ancestralColIndex();
%         if(isempty(ancestralColIndex))
%         end
%         %nodeId = sprintf('i%07d_j%07d', 1, ancestralColIndex);
%         %[nodePtr, lineageTreeInd] = findLineageTree(lineageTrees, nodeId);
%         nodePtr = 1;
%         lineageTreeInd = mode(lineageTreeInd);
%         if(~isempty(nodePtr))
%             for ii = 1:i-1
%                 [lineageTrees{lineageTreeInd}, nodePtr] = lineageTrees{lineageTreeInd}.addnode(nodePtr, sprintf('x%07d_y%07d', 0, 0));
%             end
%             lineageTrees{lineageTreeInd} = lineageTrees{lineageTreeInd}.addnode(nodePtr, currFrameCellIds{k});
%             
%         end
%     end
% end
% 
% end
