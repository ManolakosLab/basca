function [newLineageTree] = removeSubTreesLT(newLineageTree, newLineageTreePtr, lineageTree, lineageTreePtr, colony_indices)

if(isempty(newLineageTree))
    [newLineageTree] = tree(lineageTree.get(lineageTreePtr));
    newNodePtr = 1;
else
    [newLineageTree, newNodePtr] = newLineageTree.addnode(newLineageTreePtr, lineageTree.get(lineageTreePtr));
end

if(lineageTree.isleaf(lineageTreePtr))
    return;
end

childrenPtr = lineageTree.getchildren(lineageTreePtr);
for i = 1:length(childrenPtr)
    value = lineageTree.get(childrenPtr(i));
    if isstruct(value)
        startIndex = regexp(value.id, 'x\w*_y\w*', 'ONCE');
    else
        startIndex = regexp(value, 'x\w*_y\w*', 'ONCE');
    end
    if(~isempty(startIndex))
        [newLineageTree] = removeSubTreesLT(newLineageTree, newNodePtr, lineageTree, childrenPtr(i),  colony_indices);
    end
    
    value = lineageTree.get(childrenPtr(i));
    if isstruct(value)
        startIndex = regexp(value.id, 'i0000001_j\w*', 'ONCE');
        colonyInd = str2double(value.id(11:end));
    else
        startIndex = regexp(value, 'i0000001_j\w*', 'ONCE');
        colonyInd = str2double(value(11:end));        
    end
    if(~isempty(startIndex))
        if(ismember(colonyInd, colony_indices))            
            [newLineageTree] = removeSubTreesLT(newLineageTree, newNodePtr, lineageTree, childrenPtr(i),  colony_indices);
        end
    end
end