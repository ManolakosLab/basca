function [updatedFrame] = addAttributesToFrameFromLT(frame, indexTable, hyperLT)


updatedFrame = frame;
for i = 1:length(hyperLT)
    if ~strcmp(hyperLT(i, 1).get(1),'pseudo_node')
        
        iterator = hyperLT(i, 1).breadthfirstiterator;
        for j = 1:length(iterator)
            
            cellInstant = hyperLT(i, 1).get(iterator(j));
            [frameInd, colonyInd, cellInd] = searchIndexTable(indexTable, cellInstant.id);
            
            updatedFrame(frameInd).colonyProps(colonyInd).cellProps(cellInd).length =  cellInstant.length;
            updatedFrame(frameInd).colonyProps(colonyInd).cellProps(cellInd).width = cellInstant.width;
            updatedFrame(frameInd).colonyProps(colonyInd).cellProps(cellInd).area =  cellInstant.area;
            updatedFrame(frameInd).colonyProps(colonyInd).cellProps(cellInd).perimeter = cellInstant.perimeter;
            updatedFrame(frameInd).colonyProps(colonyInd).cellProps(cellInd).distanceFromCentroid = cellInstant.distanceFromCentroid;
            updatedFrame(frameInd).colonyProps(colonyInd).cellProps(cellInd).lengthRoC =  cellInstant.lengthRoC;
            updatedFrame(frameInd).colonyProps(colonyInd).cellProps(cellInd).widthRoC = cellInstant.widthRoC;
            updatedFrame(frameInd).colonyProps(colonyInd).cellProps(cellInd).areaRoC =  cellInstant.areaRoC;
            updatedFrame(frameInd).colonyProps(colonyInd).cellProps(cellInd).perimeterRoC = cellInstant.perimeterRoC;
            updatedFrame(frameInd).colonyProps(colonyInd).cellProps(cellInd).distanceFromCentroidRoC = cellInstant.distanceFromCentroidRoC;
            updatedFrame(frameInd).colonyProps(colonyInd).cellProps(cellInd).orientationRoC =  cellInstant.orientationRoC;
            updatedFrame(frameInd).colonyProps(colonyInd).cellProps(cellInd).isInsideColony = cellInstant.isInsideColony;

            if(isfield(frame(frameInd).colonyProps(colonyInd),'gfp'))
                updatedFrame(frameInd).colonyProps(colonyInd).cellProps(cellInd).gfpRoC = cellInstant.gfp;
            end
            updatedFrame(frameInd).colonyProps(colonyInd).cellProps(cellInd).generationInd = cellInstant.generationInd;
        end
    end
end
end