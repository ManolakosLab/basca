function [generation] = createGenerationStruct(cellAttributesDT)

cellAttributesDepthDT = cellAttributesDT.depthtree-2;
cellAttributesDT_depth = depth(cellAttributesDepthDT);
maxGenIndex = cellAttributesDT_depth;
for genIndex = 1:maxGenIndex+1
    
    generation(genIndex).colonyProps(1).cellAttributes = [];
    generation(genIndex).colonyProps(1).counterId = 0;
    
    iterator = find(cellAttributesDepthDT == genIndex-1);
    for i = 1:length(iterator)
        
        currCellAttributes = cellAttributesDT.get(iterator(i));
        
        if(genIndex - 1 > 0)
            prevCellAttributes = cellAttributesDT.get(cellAttributesDT.getparent(iterator(i)));
            currCellAttributes.motherStructId = [genIndex-1, prevCellAttributes.colonyIndInFrame_values(1), prevCellAttributes.cellStructId];
        else
            currCellAttributes.motherStructId = [];
        end
        
        colonyIndInFrame = currCellAttributes.colonyIndInFrame_values(1);
        if(length(generation(genIndex).colonyProps) < colonyIndInFrame)
            generation(genIndex).colonyProps(colonyIndInFrame).cellAttributes = [];
            generation(genIndex).colonyProps(colonyIndInFrame).counterId = 0;
        end
        
        if(isempty(generation(genIndex).colonyProps(colonyIndInFrame).counterId))
            generation(genIndex).colonyProps(colonyIndInFrame).counterId = 0;
        end
        
        generation(genIndex).colonyProps(colonyIndInFrame).counterId = generation(genIndex).colonyProps(colonyIndInFrame).counterId + 1;
        
        currCellAttributes.cellStructId = generation(genIndex).colonyProps(colonyIndInFrame).counterId;
        
        generation(genIndex).colonyProps(colonyIndInFrame).cellAttributes = [generation(genIndex).colonyProps(colonyIndInFrame).cellAttributes; currCellAttributes];
        
        cellAttributesDT = cellAttributesDT.set(iterator(i),  currCellAttributes);
    end
end

for genIndex = 1:maxGenIndex+1
    iterator = find(cellAttributesDepthDT == genIndex-1);
    for i = 1:length(iterator)
        
        currCellAttributes = cellAttributesDT.get(iterator(i));
        colonyIndInFrame = currCellAttributes.colonyIndInFrame_values(1);
        cellInd = currCellAttributes.cellStructId;
        
        generation(genIndex).colonyProps(colonyIndInFrame).cellAttributes(cellInd).daughterStructIds  = [];
        currCellAttributes.daughterStructIds = [];
        daughterPtr = cellAttributesDT.getchildren(iterator(i));
        
        
        %if the cell has daughters
        if(~isempty(daughterPtr))
            for j = 1:length(daughterPtr)
                daughtercellAttributes = cellAttributesDT.get(daughterPtr(j));
                generation(genIndex).colonyProps(colonyIndInFrame).cellAttributes(cellInd).daughterStructIds = [ generation(genIndex).colonyProps(colonyIndInFrame).cellAttributes(cellInd).daughterStructIds;genIndex+1, daughtercellAttributes.colonyIndInFrame_values(1), daughtercellAttributes.cellStructId];
            end
        end
        
        cellAttributesDT = cellAttributesDT.set(iterator(i),  generation(genIndex).colonyProps(colonyIndInFrame).cellAttributes(cellInd));
    end
end