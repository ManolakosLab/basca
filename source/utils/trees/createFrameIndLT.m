function [lineageTree] = createFrameIndLT(lineageTree, lineageTreePtr, frameInd)
lineageTree = lineageTree.set(lineageTreePtr, frameInd);
if(lineageTree.isleaf(lineageTreePtr))
    return;
end

childrenPtr = lineageTree.getchildren(lineageTreePtr);
for i = 1:length(childrenPtr)
   [lineageTree] = createFrameIndLT(lineageTree, childrenPtr(i), frameInd+1);
end