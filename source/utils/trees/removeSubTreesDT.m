function [newDT] = removeSubTreesDT(newDT, newDTPtr, DT, DTPtr, colony_indices)

if(isempty(newDT))
    [newDT] = tree(DT.get(DTPtr));
    newNodePtr = 1;
else
    [newDT, newNodePtr] = newDT.addnode(newDTPtr, DT.get(DTPtr));
end

if(DT.isleaf(DTPtr))
    return;
end

childrenPtr = DT.getchildren(DTPtr);
for i = 1:length(childrenPtr)
    value = DT.get(childrenPtr(i));
    if isfield(value, 'ids')
        startIndex = regexp(value.ids(1), 'x\w*_y\w*', 'ONCE');
        if(~isempty(startIndex))
            [newDT] = removeSubTreesDT(newDT, newNodePtr, DT, childrenPtr(i),  colony_indices);
        end
    else
        
        value = DT.get(childrenPtr(i));
        startIndex = regexp(value.id, 'i0000001_j\w*', 'ONCE');
        colonyInd = str2double(value.id(11:end));
        if(~isempty(startIndex))
            if(ismember(colonyInd, colony_indices))
                [newDT] = removeSubTreesDT(newDT, newNodePtr, DT, childrenPtr(i),  colony_indices);
            end
        end
    end
end