function [lineageTree] = createColonyIndLT(lineageTree, lineageTreePtr, colonyInd)
lineageTree = lineageTree.set(lineageTreePtr, colonyInd);
if(lineageTree.isleaf(lineageTreePtr))
    return;
end

childrenPtr = lineageTree.getchildren(lineageTreePtr);
for i = 1:length(childrenPtr)
   [lineageTree] = createColonyIndLT(lineageTree, childrenPtr(i), colonyInd);
end