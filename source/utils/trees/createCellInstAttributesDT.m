function [cellInstAttributesDT] = createCellInstAttributesDT(cellInstAttributesDT, cellInstAttributesDTPtr, cellInstAttributesLT, cellInstAttributesLTPtr, cellInstAttributesStruct)

cellInstantAttributes = cellInstAttributesLT.get(cellInstAttributesLTPtr);
startIndex = regexp(cellInstantAttributes.id, 'i\w*_j\w*', 'ONCE');
cellInstAttributesStruct = [cellInstAttributesStruct; cellInstantAttributes];
if(cellInstAttributesLT.isleaf(cellInstAttributesLTPtr) || strcmp(cellInstantAttributes.id, 'root') || ~isempty(startIndex) || length(cellInstAttributesLT.getchildren(cellInstAttributesLTPtr)) == 2)
    if(isempty(cellInstAttributesDT))
        [cellInstAttributesDT] = tree(cellInstAttributesStruct);
        cellInstAttributesDTPtr = 1;
    else
        motherCellInstAttributesStruct = cellInstAttributesDT.get(cellInstAttributesDTPtr);
        motherStartIndex = regexp(motherCellInstAttributesStruct(1).id, 'i\w*_j\w*', 'ONCE'); 
        if isempty(startIndex) || isempty(motherStartIndex)
            [cellInstAttributesDT, cellInstAttributesDTPtr] = cellInstAttributesDT.addnode(cellInstAttributesDTPtr, cellInstAttributesStruct);
        else
            cellInstAttributesDT = cellInstAttributesDT.set(cellInstAttributesDTPtr, cellInstAttributesStruct);
        end
    end
    cellInstAttributesStruct = [];
end

if(cellInstAttributesLT.isleaf(cellInstAttributesLTPtr))
    return;
end

childrenPtr = cellInstAttributesLT.getchildren(cellInstAttributesLTPtr);
for i = 1:length(childrenPtr)
    [cellInstAttributesDT] = createCellInstAttributesDT(cellInstAttributesDT, cellInstAttributesDTPtr, cellInstAttributesLT, childrenPtr(i), cellInstAttributesStruct);
end

end