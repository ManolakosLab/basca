function [genIndexLT] = createGenIndexLT(genIndexLT, ptr, genNo)

if( genIndexLT.isleaf(ptr) && length(genIndexLT.getchildren(genIndexLT.getparent(ptr)))>1)
    [genIndexLT] = genIndexLT.set(ptr, genNo);
    return;
end

childrenPtr = genIndexLT.getchildren(ptr);
if(length(childrenPtr) ==  1)
    [genIndexLT] = genIndexLT.set(childrenPtr, genNo);
    [genIndexLT] = createGenIndexLT(genIndexLT, childrenPtr, genNo);
else
    for i = 1:length(childrenPtr)
        [genIndexLT] = genIndexLT.set(childrenPtr(i), genNo+1);
        [genIndexLT] = createGenIndexLT(genIndexLT, childrenPtr(i), genNo+1);
    end
end