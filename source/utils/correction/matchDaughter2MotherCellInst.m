function [frame, logs, correctionList] = matchDaughter2MotherCellInst(frame, t, currColonyInd, motherLessCellInstId, configuration)
%here the user must specify the cell for segmentation and the number of new cells to be created.
prevColonyCellIds = frame(t).colonyProps(currColonyInd).prevColonyProps.cellProps.Properties.RowNames;
currColonyCellIds = frame(t).colonyProps(currColonyInd).cellProps.Properties.RowNames;
motherLessCellInstInd = find(strcmp(motherLessCellInstId, currColonyCellIds));
%daughterCellInstIds = frame(t).colonyProps(currColonyInd).correspondanceMatrix.Properties.VariableNames(~isnan(frame(t).colonyProps(currColonyInd).correspondanceMatrix{motherLessCellInstId, :}));
motherLessCellInstIds = {motherLessCellInstId};
%correct the tracking error manually
if sum(strcmp(configuration.correctErrors, {'specifyCells', 'manual'}))
    while(~isempty(motherLessCellInstIds))
        motherLessCellInstId = motherLessCellInstIds{1};
        while(1)
            dlg_title = 'Error in LT structure: Cell instant with no mother';
            prompt = {sprintf('Select one cell instant of %s (previous frame) to match with the cell instant %d of %s (current frame).\nLeave empty if the cell instant of current frame matches to no cell instant of previous frame: ', frame(t-1).id, motherLessCellInstInd, frame(t).id), ...
                sprintf('Select one cell instant of %s (current frame) that it is daughter of the cell instant %d of %s (current frame).\nLeave empty if the cell instant of current frame has no sibling: ', frame(t).id, motherLessCellInstInd)};
            
            default_answers = {'cell instant 1', ''};
            dims = [1 70];
            
            answers = inputdlg(prompt, dlg_title, dims, default_answers);
            if size(answers) == 0
                %if the user cancel the selection, then they must choose to cancel the correction approach or to have the function automatically specifying the cell instants for the correction
                if strcmp(configuration.correctErrors, 'manual')
                    choice = questdlg(sprintf('Do you want to choose another correction approach?'), 'Error in LT structure', 'yes', 'no, but proceed with automatic cell instant specification', 'cancel', 'yes');
                    if strcmp(choice, 'yes')
                        logs = [];
                        return;
                    elseif strcmp(choice, 'no, but proceed with automatic cell instant specification')
                        configuration.correctErrors = 'auto';
                        break;
                    else
                        %do nothing
                    end
                else
                    fprintf('You canceled the selection, the function will specify the cell instants for correction automatically!\n');
                    configuration.correctErrors = 'auto';
                end
            else
                if isempty(isempty(answers{1}))
                    correctionList = [correctionList; [t currColonyInd NaN]];
                    selectedMotherCellInstInd = [];
                    frame(t).colonyProps(currColonyInd).frame(t).colonyProps(currColonyInd).matchedCellsCurrColony{motherLessCellInstId, 1} = NaN;
                    logs{1} = sprintf('cell instant %d of %s was matched with no cell instant in %s (i.e. cell entered the FOV)\n', motherLessCellInstInd, frame(t).id, frame(t-1).id);
                    break;
                else
                    selectedMotherCellInstInd = str2double(answers{1});
                    if length(selectedMotherCellInstInd) ~= 1
                        fprintf('Error: <input> must contain only one value!\n');
                    elseif sum(isnan(selectedMotherCellInstInd)) ~= 0 || sum(selectedMotherCellInstInd ~= ceil(selectedMotherCellInstInd)) ~= 0 || sum(selectedMotherCellInstInd < 0) ~= 0 ||  sum(selectedMotherCellInstInd > length(prevColonyCellIds)) ~= 0
                        fprintf('Error: <input> must be a positive integer <= %d!\n', length(prevColonyCellIds));
                    else
                        selectedMotherCellInstId = prevColonyCellIds(selectedMotherCellInstInd);
                        if ~isempty(answers{2})
                            selectedSiblingCellInstIndices = str2double(answers{2});
                            if length(selectedSiblingCellInstIndices) ~= 1
                                fprintf('Error: <input> must contain only one values!\n');
                            elseif sum(isnan(selectedSiblingCellInstIndices)) ~= 0 || sum(selectedSiblingCellInstIndices ~= ceil(selectedSiblingCellInstIndices)) ~= 0 || sum(selectedSiblingCellInstIndices < 0) ~= 0 ||  sum(selectedSiblingCellInstIndices > length(frame(t).colonyProps(currColonyInd).cellProps.Properties.RowNames)) ~= 0
                                fprintf('Error: <input> must be a positive integer <= %d!\n', length(frame(t).colonyProps(currColonyInd).cellProps.Properties.RowNames));
                            else
                                selectedSiblingCellInstIds = frame(t).colonyProps(currColonyInd).cellProps.Properties.RowNames(selectedSiblingCellInstIndices);
                                selectedSiblingCellInstIds = [selectedSiblingCellInstIds; motherLessCellInstId];
                                selectedSiblingCellInstIndices = [selectedSiblingCellInstIndices; motherLessCellInstInd];
                                [frame_ind, colony_ind, cellInst_ind, motherCellInstFound, ~] = getTrajectory(frame, t, currColonyInd, selectedMotherCellInstId);
                                if length(frame_ind) > configuration.trajectoryThresh || ~motherCellInstFound
                                    break;
                                else
                                    f = warndlg({'The cell instants you specified are not valid, because there is a division event earlier in the trajectory! Specify other cell instants or leave sibling empty'},'Warning');
                                    
                                end
                            end
                        end
                    end
                end
            end
        end
        
        if ~isempty(selectedMotherCellInstInd)
            
            frame(t).colonyProps(currColonyInd).frame(t).colonyProps(currColonyInd).matchedCellsPrevColony{selectedMotherCellInstId, 1} = 0;
            frame(t).colonyProps(currColonyInd).frame(t).colonyProps(currColonyInd).matchedCellsCurrColony{selectedSiblingCellInstIds, 1} = 0;
            daughtersCellInsIds = frame(t).colonyProps(currColonyInd).correspondanceMatrix.Properties.VariableNames(frame(t).colonyProps(currColonyInd).correspondanceMatrix{selectedMotherCellInstId, :} > 0);
            motherLessCellInstIds = [motherLessCellInstIds; daughtersCellInsIds];
            frame(t).colonyProps(currColonyInd).frame(t).colonyProps(currColonyInd).matchedCellsCurrColony{daughtersCellInsIds, 1} = 0;
            frame(t).colonyProps(currColonyInd).correspondanceMatrix{selectedMotherCellInstId, :} = NaN;
            
            frame(t).colonyProps(currColonyInd).correspondanceMatrix{selectedMotherCellInstId, selectedSiblingCellInstIds} = 1/length(selectedSiblingCellInstIds);
            
            frame(t).colonyProps(currColonyInd).matchedCellsPrevColony{motherLessCellInstId, 1} = 1;
            
            frame(t).colonyProps(currColonyInd).matchedCellsCurrColony{selectedSiblingCellInstIds, 1} = 1;
            
            correctionList = [t currColonyInd -1];
            logs{1} = sprintf('cell %d of %s was matched with cell(s) %d of %s\n', selectedMotherCellInstInd, frame(t-1).id, selectedSiblingCellInstIndices(1), frame(t).id);
            logs{2} = sprintf('cell %d of %s was matched with cell(s) %d of %s\n', selectedMotherCellInstInd, frame(t-1).id, selectedSiblingCellInstIndices(2), frame(t).id);
        end
        motherLessCellInstIds(strcmp(motherLessCellInstIds, selectedSiblingCellInstIds)) = [];
    end
end

if sum(strcmp(configuration.correctErrors, {'auto', 'specifyError'}))
    logs{1} = sprintf('cell instant %d of %s will be matched automatically with a cell instant in %s \n', motherLessCellInstInd, frame(t).id, frame(t-1).id);
    correctionList = [t currColonyInd 0];
end

end
