function [colonyProps, newCellId] = mergeCells(fluo_channels_im, fluo_channels_thresh, colonyProps, cellsToBeMergedIndices, neighborDistanceFactor, neighborhoodSize, verb)
% Merges two or more cell instants (objects) to create a new cell.
%
%  [colonyProps, newCellId] = mergeCells(fluo_channels_im, fluo_channels_thresh, colonyProps, cellsToBeMergedIndices, verb)
%
%  INPUT :
%    fluo_channels_im : a cell-array containing the fluorescence images of
%    the input dataset.
%
%    fluo_channels_thresh : a vector containing the threshold for each
%    fluorescence image.
%
%    colonyProps : a struct containing the colony properties, along with
%    the cellProps table.
%
%    cellsToBeMergedIndices : a cell-array containing the ids (char-arrays)
%    or indices (integers) of the cells instants in the cellProps table
%    that are going to be merged.
%
%    verb : a logical value indicating whether to display more information
%    and figures about the function's results.
%
%  OUTPUT :
%    colonyProps : the colonyProps struct updated with the cellProps table.
%    The function inserts a new cell instant and removes the previous cell
%    instants.
%
%    newCellId : a char array with the cell id of the new cell instant.
%
%  Note than this function fails when the cell instants to be merged do not
%  touch with each other.
%
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
%
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.
warning('off', 'MATLAB:table:RowsAddedExistingVars');

[x, y] = size(colonyProps.bwColony);


if(any(strcmp(colonyProps.cellProps.Properties.VariableNames, 'colony_ind')))
    colony_ind = colonyProps.cellProps{cellsToBeMergedIndices{1}, 'colony_ind'};
end

pixelList = [];
for i = 1:length(cellsToBeMergedIndices)
    pixelList = [pixelList;colonyProps.cellProps{cellsToBeMergedIndices(i), 'pixelList'}{1}];
end
bwCell12= createBW(x, y, pixelList);


newCellProps = regionprops(bwCell12, 'MinorAxisLength', 'MajorAxisLength', 'orientation', 'solidity');
if(length(newCellProps) ~= 1)
    %     [~, maxValInd] = max([length(cell1Props{1, 'pixelList'}{1});length(cell2Props{1, 'pixelList'}{1})]);
    %     if(maxValInd == 1)
    %         bwCell12 = createBW(x, y, [cell1Props{1, 'pixelList'}{1}]);
    %     else
    %         bwCell12 = createBW(x, y, [cell2Props{1, 'pixelList'}{1}]);
    %     end
    %     newCellProps = regionprops(bwCell12, 'MinorAxisLength', 'MajorAxisLength', 'orientation', 'solidity');
    newCellId = [];
    return;
end

%delete the cell properties from the current frame
if(~isnumeric(cellsToBeMergedIndices))
    cellsToBeMergedIndices = ismember(colonyProps.cellProps.Properties.RowNames, cellsToBeMergedIndices); 
    colonyProps.cellProps(cellsToBeMergedIndices, :) = [];
    [colonyProps] = removeAdjacencyMatrix(colonyProps, cellsToBeMergedIndices);
else
    colonyProps.cellProps(cellsToBeMergedIndices, :) = [];
    [colonyProps] = removeAdjacencyMatrix(colonyProps, cellsToBeMergedIndices);
end


[bwCell12] = imfillborder(bwCell12);

[bwCell12CL, endpoints] = objectCenterline(bwCell12);
cell12CLlen = length(find(bwCell12CL));

%Use chessboard distance transform to get the cell12 radius.
bwCell12WithPad = padarray(bwCell12,[1 1]);
chessBoardDT = bwdist(~bwCell12WithPad, 'chessboard');
chessBoardDT = chessBoardDT(2:end-1,2:end-1);

%create a record for the new cell.
newCellCentroid = mean(double(pixelList));
%assuming that the radius of the segment is the maximum distance from the boundary.
cell12Radius = max(max(chessBoardDT));
if(cell12CLlen <= cell12Radius)
    cell12WidthInPixels = 2*double(cell12Radius)+1;
    cell12Width = newCellProps.MinorAxisLength;
    cell12LengthInPixels = cell12CLlen;
    cell12Length = newCellProps.MajorAxisLength;
    cell12CentralPixel = round(newCellCentroid);
    
else
    if(size(endpoints, 2) ~= 2)
    end
    cell12CLVerticesWithoutPoles = bwtraceboundary(bwCell12CL, [endpoints(2, 1) endpoints(1, 1)], 'N', 8, cell12CLlen);
    [verticesFromPoleA, ~, verticesToPoleB, ~] = getObjectPoles(bwCell12, cell12CLVerticesWithoutPoles, 'euclidean', verb);
    vertices = [verticesFromPoleA; verticesToPoleB; cell12CLVerticesWithoutPoles];
    %if the segment is non-complex, get the segment's centerline to
    %check for bowties
    vertices = [vertices(:, 2) vertices(:, 1)];
    bwCell12CL = createBW(x, y, vertices);
    [~, endpoints, ~] = anaskel(bwCell12CL);
    cell12CLVertices = bwtraceboundary(bwCell12CL, [endpoints(2, 1) endpoints(1, 1)], 'N', 8,length(vertices));
    
    [distanceToBoundaryInPixels, ~, ~, ~] = meausureDistToBoundary2(bwCell12, cell12CLVertices, 'chessboard');
    [distanceToBoundary, ~, ~, ~] = meausureDistToBoundary2(bwCell12, cell12CLVertices, 'euclidean');
    cell12LengthInPixels = length(distanceToBoundary);
    [cell12SimplifiedCLVertices, ~] = dpsimplify(cell12CLVertices , 1.4142);
    
    [cell12Length, ~] = arclength(cell12SimplifiedCLVertices(:, 2), cell12SimplifiedCLVertices(:, 1), 'linear');
    
    cell12WidthInPixels = max(medfilt1(distanceToBoundaryInPixels, 2*double(floor(cell12Radius))+1));
    cell12Width = max(medfilt1(distanceToBoundary, ceil(cell12WidthInPixels/2)));
    cell12CentralPixel = cell12CLVertices(ceil(size(cell12CLVertices, 1)/2), :);
    cell12CentralPixel = [cell12CentralPixel(2) cell12CentralPixel(1)];
end

newCellId = sprintf('x%07d_y%07d', ...
    round((newCellCentroid(1, 1)+colonyProps.bBoxULCorner(1, 1))*1000) , ...
    round((newCellCentroid(1, 2)+colonyProps.bBoxULCorner(1, 2))*1000));
colonyProps.cellProps{newCellId, 'pixelList'} = {uint16(pixelList)};
colonyProps.cellProps{newCellId, 'centroid'} = {newCellCentroid};
colonyProps.cellProps{newCellId, 'width_in_pixels'} = cell12Width;
colonyProps.cellProps{newCellId, 'length_in_pixels'} = cell12Length;
colonyProps.cellProps{newCellId, 'width_in_pixels_chessboard'} = cell12WidthInPixels;
colonyProps.cellProps{newCellId, 'length_in_pixels_chessboard'} = cell12LengthInPixels;
colonyProps.cellProps{newCellId, 'centralPixel'} = {cell12CentralPixel};
if bwCell12(ceil(newCellCentroid(2)), ceil(newCellCentroid(1))) == 0
    colonyProps.cellProps{newCellId, 'has_irregular_shape'} = true;
else
    colonyProps.cellProps{newCellId, 'has_irregular_shape'} = false;
end
bwCell12Perim = bwperim(bwCell12);
colonyProps.cellProps{newCellId, 'minorAxis'} = newCellProps.MinorAxisLength;
colonyProps.cellProps{newCellId, 'majorAxis'} = newCellProps.MajorAxisLength;
colonyProps.cellProps{newCellId, 'eccentricity'} = newCellProps.MinorAxisLength/newCellProps.MajorAxisLength;
colonyProps.cellProps{newCellId, 'orientation'} = newCellProps.Orientation;
if(colonyProps.cellProps{newCellId, 'orientation'} < 0) %#ok<*BDSCA>
    colonyProps.cellProps{newCellId, 'orientation'} = 360+colonyProps.cellProps{newCellId, 'orientation'};
end
colonyProps.cellProps{newCellId, 'solidity'} = newCellProps.Solidity;
colonyProps.cellProps{newCellId, 'covariance'} = {cov(double(colonyProps.cellProps{newCellId, 'pixelList'}{1}))};
b = bwboundaries(bwCell12Perim);
colonyProps.cellProps{newCellId, 'boundaryPixelList'} = {uint16([b{1}(:, 2) b{1}(:, 1)])};

for channel_i = 1:length(fluo_channels_thresh)
    [fluo_channel_int_pixelList] = measureFluorescence(fluo_channels_im{channel_i}, pixelList+uint16([ones(size(pixelList, 1), 1)*colonyProps.bBoxULCorner(1) ones(size(pixelList, 1), 1)*colonyProps.bBoxULCorner(2)]));
    colonyProps.cellProps(newCellId, sprintf('fluo_pixelList_channel%d', channel_i )) = {{fluo_channel_int_pixelList}};
    colonyProps.cellProps(newCellId, sprintf('fluo_int_channel%d', channel_i)) = {mean(fluo_channel_int_pixelList(fluo_channel_int_pixelList>fluo_channels_thresh(channel_i)))};
    colonyProps.cellProps(newCellId, sprintf('fluo_cov_channel%d', channel_i)) = {sum(fluo_channel_int_pixelList>fluo_channels_thresh(channel_i))/length(fluo_channel_int_pixelList)};
end
newCellInd = ismember(colonyProps.cellProps.Properties.RowNames, newCellId);
[colonyProps] = insertToAdjacencyMatrix(colonyProps, newCellInd, neighborDistanceFactor, neighborhoodSize);

if(any(strcmp(colonyProps.cellProps.Properties.VariableNames, 'colonyId')))
    colonyProps.cellProps{insertInd, 'colony_ind'} = colony_ind;
end
%colonyProps.cellProps.Properties.RowNames{insertInd} = newCellId;