function [frame, correctionList] = correctMultipleDaughterCellInstsPattern(frame, t, currColonyInd, configuration, correctionList)

currCorrection = [];
prevColonyProps = frame(t).colonyProps(currColonyInd).prevColonyProps;
prevCellIds = prevColonyProps.cellProps.Properties.RowNames;
for i = 1:height(prevColonyProps.cellProps)
    indices = find(frame(t).colonyProps(currColonyInd).correspondanceMatrix{prevCellIds{i}, :} > 0);
    prevCellInd = find(strcmp(prevCellIds{i}, prevColonyProps.cellProps.Properties.RowNames));
    cellToBeMergedIds = frame(t).colonyProps(currColonyInd).correspondanceMatrix.Properties.VariableNames(indices);
    
    %if a mother cell instant has more than 2 daughters cell instants,
    %oversegmentation occured (current frame)
    if(length(indices) > 2)
        fprintf('1toN error detected...\n');
        %plot figure with trajectory
        if sum(strcmp(configuration.correctErrors, {'specifyError', 'manual'}))
            
            jj = getColonyIndex(frame(t-1), prevCellIds{i}, frame(t).colonyProps(currColonyInd).correspondingColPrevFrameInd);
            
            plotTrajectory('Trajectory of Type I error', frame, [t-1 t], [jj currColonyInd], {prevCellInd, indices}, [1 0 0], [0 1 0], [1 1 1]);
            
            while(1)
                %choice = questdlg(sprintf('Please, specify the correction for the Type I error.'), 'Error in LT structure', 'segmentation and tracking', 'only tracking', 'only segmentation', 'only segmentation');
                
                [indx,~] = listdlg('Name','Error in LT structure', 'PromptString', ...
                    {'Please specify the error, leading to problematic tree pattern :',...
                    'Only one error type can be selected at a time.'},...
                    'SelectionMode','single', ...
                    'ListString',...
                    {'over-segmentation in current frame',  ...
                    'over-segmentation and tracking error in current frame', ...
                    'tracking error in current frame'}, 'ListSize',[300, 100]);
                if indx == 2
                    [frame, cellToBeMergedIds, frame(t).colonyProps(currColonyInd).matchedCellsCurrColony, logs, currCorrectionSegmentation] = mergeMultipleCells(frame, t, currColonyInd, frame(t).colonyProps(currColonyInd).matchedCellsCurrColony, cellToBeMergedIds, configuration);
                    if(isempty(logs))
                        f = warndlg({'The function failed to correct the error or you canceled the correction strategy.','Specify again the error type!'},'Warning');
                    else
                        if(configuration.verb)
                            fprintf('Segmentation error in current frame corrected...\n');
                            for ii = 1:length(logs)
                                fprintf('%s', logs{ii});
                            end
                            fprintf('\n');
                        end
                        [tempFrame, logs, currCorrectionTracking] = matchMother2DaughterCellInst(frame, t, currColonyInd, {prevCellIds{i}}, configuration);
                        if(isempty(logs))
                            f = warndlg({'The function failed to correct the error or you canceled the correction strategy.','Specify again the error type!'},'Warning');
                        else
                            frame = tempFrame;
                            if(configuration.verb)
                                fprintf('Tracking error in current frame corrected...\n');
                                for ii = 1:length(logs)
                                    fprintf('%s', logs{ii});
                                end
                                fprintf('\n');
                            end
                            currCorrection =[currCorrectionSegmentation; currCorrectionTracking];
                        end
                    end
                    
                elseif indx == 3
                    [tempFrame, logs, currCorrection] = matchMother2DaughterCellInst(frame, t, currColonyInd, {prevCellIds{i}}, configuration);
                    if(isempty(logs))
                        f = warndlg({'The function failed to correct the error or you canceled the correction strategy.','Specify again the error type!'},'Warning');
                    else
                        frame = tempFrame;
                        
                        if(configuration.verb)
                            fprintf('Tracking error in current frame corrected...\n');
                            for ii = 1:length(logs)
                                fprintf('%s', logs{ii});
                            end
                            fprintf('\n');
                        end
                    end
                    
                elseif indx == 1
                    [frame, cellToBeMergedIds, frame(t).colonyProps(currColonyInd).matchedCellsCurrColony, logs, currCorrection] = mergeMultipleCells(frame, t, currColonyInd, frame(t).colonyProps(currColonyInd).matchedCellsCurrColony, cellToBeMergedIds, configuration);
                    if(isempty(logs))
                        f = warndlg({'The function failed to correct the error or you canceled the correction strategy.','Specify again the error type!'},'Warning');
                    else
                        if(configuration.verb)
                            fprintf('Segmentation error in current frame corrected...\n');
                            for ii = 1:length(logs)
                                fprintf('%s', logs{ii});
                            end
                            fprintf('\n');
                        end
                    end
                else
                    %if the user cancel the selection, then they must choose to cancel the correction approach or to have the function automatically specifying the cell for correction for the correction
                    choice = questdlg(sprintf('Do you want to cancel the correction of this error?'), 'Error in LT structure', 'yes', 'no, but proceed with automatic correction specification', 'cancel', 'yes');
                    if strcmp(choice, 'yes')
                        continue;
                    elseif strcmp(choice, 'no, but proceed with automatic correction specification')
                        configuration.correctErrors = 'auto';
                        break;
                    else
                        %do nothing
                    end
                end
            end
        end
        if sum(strcmp(configuration.correctErrors, {'specifyCells', 'auto'}))
            [frame, cellToBeMergedIds, frame(t).colonyProps(currColonyInd).matchedCellsCurrColony, logs, currCorrection] = mergeMultipleCells(frame, t, currColonyInd, frame(t).colonyProps(currColonyInd).matchedCellsCurrColony, cellToBeMergedIds, configuration);
            if(configuration.verb && ~isempty(logs))
                fprintf('Segmentation error in current frame corrected...\n');
                for ii = 1:length(logs)
                    fprintf('%s', logs{ii});
                end
                fprintf('\n');
            end
            %if the daughter cell instants did not merge to one or two cell instants, i.e. the function failed to
            %merge all the cell instants with each other to form two new cell instants(automatically), probably there is a tracking error too excepr for the oversegmentation, thus the
            %function will have to correct the tracking error.
            if length(cellToBeMergedIds) > 2
                [tempFrame, logs, currCorrectionTracking] = matchMother2DaughterCellInst(frame, t, currColonyInd, {prevCellIds{i}}, configuration);
                if(configuration.verb)
                    frame = tempFrame;
                    
                    fprintf('Tracking error in current frame corrected...\n');
                    for ii = 1:length(logs)
                        fprintf('%s', logs{ii});
                    end
                    fprintf('\n');
                end
                currCorrection =[currCorrection; currCorrectionTracking];
            end
        end
    end
    correctionList = [correctionList; currCorrection];
    
end

end