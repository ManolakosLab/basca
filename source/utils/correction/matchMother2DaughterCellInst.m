function [frame, logs, correctionList] = matchMother2DaughterCellInst(frame, t, currColonyInd, motherCellInstIds, configuration)
%matchMother2DaughterCellInst
correctionList =[];
logs = [];
prevColonyCellIds = frame(t).colonyProps(currColonyInd).prevColonyProps.cellProps.Properties.RowNames;
correctionsCounter = 1;
if sum(strcmp(configuration.correctErrors, {'specifyCells', 'manual'}))
    
    %manual correction of a tracking error may lead to manual correction of
    %other tracking errors, thus the correction approach must continue
    %while all the cell instant of previous frame are matched.
    while(~isempty(motherCellInstIds))
        dlg_title = 'Error in LT structure';
        dims = [1 70];
        motherCellInstId = motherCellInstIds{1};
        motherCellInstInd = find(strcmp(motherCellInstId, prevColonyCellIds));
        daughterCellInstIds = frame(t).colonyProps(currColonyInd).correspondanceMatrix.Properties.VariableNames(~isnan(frame(t).colonyProps(currColonyInd).correspondanceMatrix{motherCellInstId, :}));
        if isempty(daughterCellInstIds)
            daughterCellInstIndices  = [];
        else
            daughterCellInstIndices  = find(ismember(frame(t).colonyProps(currColonyInd).cellProps.Properties.RowNames, daughterCellInstIds ));
        end
        prevColonyInd = getColonyIndex(frame(t-1), motherCellInstId, frame(t).colonyProps(currColonyInd).correspondingColPrevFrameInd);
        
        [frame_ind, colony_ind, cellInst_ind, motherCellInstFound, ~] = getTrajectory(frame, t, currColonyInd, motherCellInstId);
        
        branchLength = length(frame_ind);
        if branchLength < configuration.trajectoryThresh
            while(1)
                prompt = {sprintf('Specify one cell instant(s) of %s (current frame) to match with the cell instant %d of %s (previous frame).\nLeave empty if the cell instant of current frame matches to no cell instant of previous frame: ', frame(t).id, motherCellInstInd, frame(t-1).id)};
                
                default_answers = {'cell instant 1'};
                answers = inputdlg(prompt, dlg_title, dims, default_answers);
                if size(answers) == 0
                    %if the user cancel the selection, then they must choose to cancel the correction approach or to have the function automatically specifying the cell instants for the correction
                    if strcmp(configuration.correctErrors, 'manual')
                        choice = questdlg(sprintf('Do you want to choose another correction approach?'), 'Error in LT structure', 'yes', 'no, but proceed with automatic cell instant specification if possible', 'cancel', 'yes');
                        
                        if strcmp(choice, 'yes')
                            logs = [];
                            return;
                        elseif strcmp(choice, 'no, but proceed with automatic cell instant specification if possible')
                            configuration.correctErrors = 'auto';
                            break;
                        else
                            %do nothing
                        end
                    else
                        fprintf('You canceled the selection, the function will specify the cell instants for correction automatically if possible!\n');
                        configuration.correctErrors = 'auto';
                        break;
                        
                    end
                else
                    if isempty(isempty(answers{1}))
                        selectedDaughterCellInstIds = [];
                        break;
                    else
                        selectedDaughterCellInstIndices = str2double(strsplit(answers{1},','));
                        if length(selectedDaughterCellInstIndices) ~= 1 && length(selectedDaughterCellInstIndices) ~= 2
                            fprintf('Error: <input> must contain only one or two values!\n');
                        elseif sum(isnan(selectedDaughterCellInstIndices)) ~= 0 || sum(selectedDaughterCellInstIndices ~= ceil(selectedDaughterCellInstIndices)) ~= 0 || sum(selectedDaughterCellInstIndices < 0) ~= 0 ||  sum(selectedDaughterCellInstIndices > length(frame(t).colonyProps(currColonyInd).cellProps.Properties.RowNames)) ~= 0
                            fprintf('Error: <input> must be a vector of positive integers <= %d!\n', length(frame(t).colonyProps(currColonyInd).cellProps.Properties.RowNames));
                        else
                            selectedDaughterCellInstIds = frame(t).colonyProps(currColonyInd).cellProps.Properties.RowNames(selectedDaughterCellInstIndices);
                            break;
                        end
                    end
                end
            end
            frame(t).colonyProps(currColonyInd).matchedCellsPrevColony{motherCellInstId, 1} = 0;
            frame(t).colonyProps(currColonyInd).matchedCellsCurrColony{daughterCellInstIds, 1} = 0;
            frame(t).colonyProps(currColonyInd).correspondanceMatrix{motherCellInstId, :} = NaN;
            if isempty(selectedDaughterCellInstIds)
                correctionList = [correctionList; [t currColonyInd Inf]];
                frame(t).colonyProps(currColonyInd).matchedCellsPrevColony{motherCellInstId, 1} = NaN;
                
                logs{end+1} = sprintf('cell instant %d of %s was matched with no cell instant in %s (i.e. cell died or left the FOV)\n', motherCellInstInd, frame(t-1).id, frame(t).id);
            else
                
                if length(selectedDaughterCellInstIds) == 1
                    if sum(frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, selectedDaughterCellInstIds(1)} > 0)
                        newMotherCellInstId = frame(t).colonyProps(currColonyInd).correspondanceMatrix.Properties.RowNames(~isnan(frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, selectedDaughterCellInstIds(1)}));
                        frame(t).colonyProps(currColonyInd).matchedCellsCurrColony{newMotherCellInstId, 1} = 0;
                        motherCellInstIds = [motherCellInstIds ; newMotherCellInstId];
                        frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, daughterCellInstIds(1)} = NaN;
                        
                    end
                    frame(t).colonyProps(currColonyInd).correspondanceMatrix{motherCellInstId, selectedDaughterCellInstIds} = 1/length(selectedDaughterCellInstIds);
                    
                    frame(t).colonyProps(currColonyInd).matchedCellsPrevColony{motherCellInstId, 1} = 1;
                    
                    frame(t).colonyProps(currColonyInd).matchedCellsCurrColony{selectedDaughterCellInstIds, 1} = 1;
                end
                
                logs{end+1} = sprintf('cell %d of %s was matched with cell(s) %d of %s\n', motherCellInstInd, frame(t-1).id, selectedDaughterCellInstIndices(1), frame(t).id);
                correctionList = [correctionList; [t currColonyInd -1]];
            end
        else
            while(1)
                prompt = {sprintf('Specify one or two cell instant(s) of %s (current frame) to match with the cell instant %d of %s (previous frame).\nLeave empty if the cell instant of previous frame matches to no cell instant of current frame (Cell instant left FoF or died): ', frame(t).id, motherCellInstInd, frame(t-1).id)};
                default_answers = {'cell instant 1, cell instant 2'};
                answers = inputdlg(prompt, dlg_title, dims, default_answers);
                if size(answers) == 0
                    %if the user cancel the selection, then they must choose to cancel the correction approach or to have the function automatically specifying the cell instants for the correction
                    if strcmp(configuration.correctErrors, 'manual')
                        choice = questdlg(sprintf('Do you want to choose another correction approach?'), 'Error in LT structure', 'yes', 'no, but proceed with automatic cell instant specification if possible', 'cancel', 'yes');
                        
                        if strcmp(choice, 'yes')
                            logs = [];
                            return;
                        elseif strcmp(choice, 'no, but proceed with automatic cell instant specification if possible')
                            configuration.correctErrors = 'auto';
                            break;
                        else
                            %do nothing
                        end
                    else
                        fprintf('You canceled the selection, the function will specify the cell instants for correction automatically if possible!\n');
                        configuration.correctErrors = 'auto';
                        break;
                        
                    end
                else
                    if isempty(answers{1})
                        selectedDaughterCellInstIndices = [];
                        break;
                    else
                        selectedDaughterCellInstIndices = str2double(strsplit(answers{1},', '));
                        if length(selectedDaughterCellInstIndices) ~= 1 && length(selectedDaughterCellInstIndices) ~= 2
                            fprintf('Error: <input> must contain only one or two values!\n');
                        elseif sum(isnan(selectedDaughterCellInstIndices)) ~= 0 || sum(selectedDaughterCellInstIndices ~= ceil(selectedDaughterCellInstIndices)) ~= 0 || sum(selectedDaughterCellInstIndices < 0) ~= 0 ||  sum(selectedDaughterCellInstIndices > length(frame(t).colonyProps(currColonyInd).cellProps.Properties.RowNames)) ~= 0
                            fprintf('Error: <input> must be a vector of positive integers <= %d!\n', length(frame(t).colonyProps(currColonyInd).cellProps.Properties.RowNames));
                        else
                            selectedDaughterCellInstIds = frame(t).colonyProps(currColonyInd).cellProps.Properties.RowNames(selectedDaughterCellInstIndices);
                            break;
                        end
                    end
                end
            end
            frame(t).colonyProps(currColonyInd).matchedCellsPrevColony{motherCellInstId, 1} = 0;
            frame(t).colonyProps(currColonyInd).matchedCellsCurrColony{daughterCellInstIds, 1} = 0;
            frame(t).colonyProps(currColonyInd).correspondanceMatrix{motherCellInstId, :} = NaN;
            
            if isempty(selectedDaughterCellInstIndices)
                correctionList = [correctionList ;[t currColonyInd Inf]];
                logs{end+1} = sprintf('cell instant %d of %s was matched with no cell instant in %s (i.e. cell died or left the FOV)\n', motherCellInstInd, frame(t-1).id, frame(t).id);
                frame(t).colonyProps(currColonyInd).matchedCellsPrevColony{motherCellInstId, 1} = NaN;
            else
                
                
                if length(selectedDaughterCellInstIds) == 1
                    if sum(frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, selectedDaughterCellInstIds(1)} > 0)
                        newMotherCellInstId = frame(t).colonyProps(currColonyInd).correspondanceMatrix.Properties.RowNames(~isnan(frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, selectedDaughterCellInstIds(1)}));
                        frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, selectedDaughterCellInstIds(1)} = NaN;
                        
                        motherCellInstIds = [motherCellInstIds ; newMotherCellInstId];
                        frame(t).colonyProps(currColonyInd).matchedCellsPrevColony{newMotherCellInstId, 1} = 0;
                    end
                    logs{end+1} = sprintf('cell %d of %s was matched with cell(s) %d of %s \n', motherCellInstInd, frame(t-1).id, selectedDaughterCellInstIndices(1), frame(t).id);
                    correctionList = [correctionList; [t currColonyInd -1]];
                    
                end
                if length(selectedDaughterCellInstIds) == 2
                    if sum(frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, selectedDaughterCellInstIds(1)} > 0)
                        newMotherCellInstId = frame(t).colonyProps(currColonyInd).correspondanceMatrix.Properties.RowNames(~isnan(frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, selectedDaughterCellInstIds(1)}));
                        frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, selectedDaughterCellInstIds(1)} = NaN;
                        motherCellInstIds = [motherCellInstIds ; newMotherCellInstId];
                        
                        frame(t).colonyProps(currColonyInd).matchedCellsPrevColony{newMotherCellInstId, 1} = 0;
                    end
                    if sum(frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, selectedDaughterCellInstIds(2)} > 0)
                        newMotherCellInstId = frame(t).colonyProps(currColonyInd).correspondanceMatrix.Properties.RowNames(~isnan(frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, selectedDaughterCellInstIds(2)}));
                        frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, selectedDaughterCellInstIds(2)} = NaN;
                        motherCellInstIds = [motherCellInstIds ; newMotherCellInstId];
                        
                        frame(t).colonyProps(currColonyInd).matchedCellsPrevColony{newMotherCellInstId, 2} = 0;
                    end
                    logs{end+1} = sprintf('cell %d of %s was matched with cell(s) %d of %s \n', motherCellInstInd, frame(t-1).id, selectedDaughterCellInstIndices(1), frame(t).id);
                    logs{end+1} = sprintf('cell %d of %s was matched with cell(s) %d of %s \n', motherCellInstInd, frame(t-1).id, selectedDaughterCellInstIndices(2), frame(t).id);
                    correctionList = [correctionList ;[t currColonyInd -2];[t currColonyInd -2]];
                    
                end
                frame(t).colonyProps(currColonyInd).correspondanceMatrix{motherCellInstId, selectedDaughterCellInstIds} = 1/length(selectedDaughterCellInstIds);
                frame(t).colonyProps(currColonyInd).matchedCellsPrevColony{motherCellInstId, 1} = 1;
                frame(t).colonyProps(currColonyInd).matchedCellsCurrColony{selectedDaughterCellInstIds, 1} = 1;
            end
        end
        correctionsCounter = correctionsCounter+1;
        motherCellInstIds(strcmp(motherCellInstIds, motherCellInstId)) = [];
        
    end
end

if sum(strcmp(configuration.correctErrors, {'auto', 'specifyError'}))
    for i = 1:length(motherCellInstIds)
        motherCellInstId = motherCellInstIds{i};
        motherCellInstInd = find(strcmp(motherCellInstId, prevColonyCellIds));
        daughterCellInstIds = frame(t).colonyProps(currColonyInd).correspondanceMatrix.Properties.VariableNames(~isnan(frame(t).colonyProps(currColonyInd).correspondanceMatrix{motherCellInstId, :}));
        prevColonyInd = getColonyIndex(frame(t-1), motherCellInstId, frame(t).colonyProps(currColonyInd).correspondingColPrevFrameInd);
        if isempty(daughterCellInstIds)
            daughterCellInstIndices  = [];
        else
            daughterCellInstIndices  = find(ismember(frame(t).colonyProps(currColonyInd).cellProps.Properties.RowNames, daughterCellInstIds ));
        end
       
        if isempty(daughterCellInstIds)
            %the pipeline will proceed with automatic tracking after
            %error corrections
            correctionList = [correctionList;[t, currColonyInd, NaN]];
            logs{end+1} = sprintf('cell instant %d of %s colony %d was matched to no cell instant of %s colony %d\n', motherCellInstInd, frame(t-1).id,  prevColonyInd, frame(t).id, currColonyInd);
        elseif length(daughterCellInstIds) == 1
            %the pipeline will proceed with automatic tracking after
            %error corrections, first remove existing matching
            frame(t).colonyProps(currColonyInd).matchedCellsPrevColony{motherCellInstId, 1} = 0;
            frame(t).colonyProps(currColonyInd).matchedCellsCurrColony{daughterCellInstIds, 1} = 0;
            
            frame(t).colonyProps(currColonyInd).correspondanceMatrix{motherCellInstId, :} = NaN;
            
            correctionList = [correctionList;[t, currColonyInd, 0]];
            logs{end+1} = sprintf('cell instant %d of %s colony %d was disjoined from cell instant %d of %s colony %d\n', motherCellInstInd, frame(t-1).id,  prevColonyInd, daughterCellInstIndices, frame(t).id, currColonyInd);
        elseif length(daughterCellInstIds) == 2
            %automatic mode: correct the tracking error by
            %keeping the cell instant of current frame
            %that maximize mother cell instant (previous frame) overlap.
            [maxOverlapScore, maxOverlapScoreInd] = max(frame(t).colonyProps(currColonyInd).correspondanceMatrix{motherCellInstId, :});
            frame(t).colonyProps(currColonyInd).correspondanceMatrix{motherCellInstId, :} = NaN;
            frame(t).colonyProps(currColonyInd).correspondanceMatrix{motherCellInstId, maxOverlapScoreInd} = maxOverlapScore;
            frame(t).colonyProps(currColonyInd).matchedCellsCurrColony{daughterCellInstIds, 1} = 0;
            
            frame(t).colonyProps(currColonyInd).matchedCellsCurrColony{maxOverlapScoreInd, 1} = 1;
            logs{end+1} = sprintf('cell instant %d of %s colony %d was matched with cell instant %d of %s colony %d\n', motherCellInstInd, frame(t-1).id,  prevColonyInd, maxOverlapScoreInd, frame(t).id, currColonyInd);
            correctionList = [correctionList;[t currColonyInd -1]];
        else
            %automatic mode: correct the tracking error by
            %keeping the 2 cell instants of current frame
            %that maximize mother cell instant (previous frame) overlap.
            [maxOverlapScore1, maxOverlapScoreInd1] = max(frame(t).colonyProps(currColonyInd).correspondanceMatrix{motherCellInstId, :});
            frame(t).colonyProps(currColonyInd).correspondanceMatrix{motherCellInstId, maxOverlapScoreInd1} = NaN;
            [maxOverlapScore2, maxOverlapScoreInd2] = max(frame(t).colonyProps(currColonyInd).correspondanceMatrix{motherCellInstId, :});
            frame(t).colonyProps(currColonyInd).correspondanceMatrix{motherCellInstId, :} = NaN;
            
            frame(t).colonyProps(currColonyInd).correspondanceMatrix{motherCellInstId, maxOverlapScoreInd1} = maxOverlapScore1;
            frame(t).colonyProps(currColonyInd).correspondanceMatrix{motherCellInstId, maxOverlapScoreInd2} = maxOverlapScore2;
            frame(t).colonyProps(currColonyInd).matchedCellsCurrColony{daughterCellInstIds, 1} = 0;
            
            frame(t).colonyProps(currColonyInd).matchedCellsCurrColony{maxOverlapScoreInd1, 1} = 1;
            frame(t).colonyProps(currColonyInd).matchedCellsCurrColony{maxOverlapScoreInd2, 1} = 1;
            logs{end+1} = sprintf('cell instant %d of %s colony %d was matched with cell instant %d of %s colony %d\n', motherCellInstInd, frame(t-1).id,  prevColonyInd, maxOverlapScoreInd1, frame(t).id, currColonyInd);
            logs{end+1} = sprintf('cell instant %d of %s colony %d was matched with cell instant %d of %s colony %d\n', motherCellInstInd, frame(t-1).id,  prevColonyInd, maxOverlapScoreInd2, frame(t).id, currColonyInd);
            
            correctionList = [correctionList;[t currColonyInd -2];[t currColonyInd -2]];
        end
    end
end

end
