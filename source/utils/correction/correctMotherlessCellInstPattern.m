function [frame, correctionList] = correctMotherlessCellInstPattern(frame, t, currColonyInd, configuration, correctionList)

currCorrection = [];
correctedErrors = frame(t).colonyProps(currColonyInd).matchedCellsCurrColony;
correctedErrors{:, 1} = 3*correctedErrors{:, 1};
while(1)
    currFrameCellIds = frame(t).colonyProps(currColonyInd).cellProps.Properties.RowNames;
    nonMatchedCellsIds_currColony = correctedErrors.Properties.RowNames(correctedErrors{:, 1} < 3);
    if(~isempty(nonMatchedCellsIds_currColony))
        for i = 1:length(nonMatchedCellsIds_currColony)
            nonMatchedCellsInd_currColony = find(strcmp(currFrameCellIds, nonMatchedCellsIds_currColony{i}));            
            if sum(strcmp(configuration.correctErrors, {'specifyError', 'manual'}))
                
                plotTrajectory('Trajectory of Type IV error', frame, t, currColonyInd, {nonMatchedCellsInd_currColony}, [1 0 0], [0 1 0], [1 1 1]);
                
                while(1)
                    %choice = questdlg(sprintf('Please, specify the correction for the Type I error.'), 'Error in LT structure', 'segmentation and tracking', 'only tracking', 'only segmentation', 'only segmentation');
                    
                    [indx,~] = listdlg('Name','Error in LT structure', 'PromptString', ...
                        {'Please specify the error, leading to problematic tree pattern :',...
                        'Only one error type can be selected at a time.'},...
                        'SelectionMode','single', ...
                        'ListString',...
                        {'over-segmentation in current frame',  ...
                        'tracking error in current frame', ...
                        'cell entered in the FoV'}, 'ListSize',[300, 100]);
                    if indx == 1
                        [frame, logs, firstNewCellId] = merge2Cells(frame, t, currColonyInd, nonMatchedCellsIds_currColony{i}, configuration);
                        if(isempty(logs))
                            f = warndlg({'The function failed to correct the error or you canceled the correction strategy.','Specify again the error type!'},'Warning');
                        else
                            correctedErrors{nonMatchedCellsIds_currColony{i}, 1} =  3;
                            
                            if(configuration.verb)
                                fprintf('Segmentation error in current frame corrected...\n');
                                for ii = 1:length(logs)
                                    fprintf('%s', logs{ii});
                                end
                                fprintf('\n');
                            end
                        end
                        
                    elseif indx == 2
                        [frame, logs, currCorrection] = matchDaughter2MotherCellInst(frame, t, currColonyInd, nonMatchedCellsIds_currColony{i}, configuration);
                        if(isempty(logs))
                            f = warndlg({'The function failed to correct the error or you canceled the correction strategy.','Specify again the error type!'},'Warning');
                        else
                            correctedErrors{nonMatchedCellsIds_currColony{i}, 1} = 3;
                            if(configuration.verb)
                                fprintf('Tracking error in current frame corrected...\n');
                                for ii = 1:length(logs)
                                    fprintf('%s', logs{ii});
                                end
                                fprintf('\n');
                            end
                        end
                        
                    else
                        %if the user cancel the selection, then they must choose to cancel the correction approach or to have the function automatically specifying the cell for correction for the correction
                        choice = questdlg(sprintf('Do you want to cancel the correction of this error?'), 'Error in LT structure', 'yes', 'no, but proceed with automatic correction specification', 'cancel', 'yes');
                        if strcmp(choice, 'yes')
                            correctedErrors{nonMatchedCellsIds_currColony{i}, 1} = 3;
                            continue;
                        elseif strcmp(choice, 'no, but proceed with automatic correction specification')
                            configuration.correctErrors = 'auto';
                            break;
                        else
                            %do nothing
                        end
                    end
                end
            end
            
            if sum(strcmp(configuration.correctErrors, {'specifyCells', 'auto'}))
                [frame, logs, firstNewCellId] = merge2Cells(frame, t, currColonyInd, nonMatchedCellsIds_currColony{i}, configuration);
                if(isempty(logs))
                    if(configuration.verb)
                        fprintf('The function failed to correct the mothterless cell instant!\n');
                    end
                    correctedErrors{nonMatchedCellsIds_currColony{i}, 1} = correctedErrors{nonMatchedCellsIds_currColony{i}, 1}+1;
                else
                    correctedErrors{nonMatchedCellsIds_currColony{i}, 1} = 3;
                    if(configuration.verb)
                        fprintf('Segmentation error in current frame corrected...\n');
                        for ii = 1:length(logs)
                            fprintf('%s', logs{ii});
                        end
                        fprintf('\n');
                    end
                end
            end
        end
    else
        break;
    end
end
correctionList = [correctionList; currCorrection];

end