function [frame, correctionList] = correctDaughterlessCellInstPattern(frame, t, thisColonyInd, configuration, correctionList)
minLifeSpan = configuration.trajectoryThresh;
correctedErrors = frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony;
while(1)
    nonMatchedCellsInd_prevColony = find(correctedErrors{:, 1} == 0);
    if(isempty(nonMatchedCellsInd_prevColony))
        break;
    end
    nonMatchedCellIds_prevColony = correctedErrors.Properties.RowNames(nonMatchedCellsInd_prevColony);
    %getting current cell's colony index
    prevColonyInd = getColonyIndex(frame(t-1), nonMatchedCellIds_prevColony{1}, frame(t).colonyProps(thisColonyInd).correspondingColPrevFrameInd);
    
    
    [frame_ind, colony_ind, cellInst_ind, cellInst_ids, motherCellInstFound, ~] = getTrajectory(frame, t, thisColonyInd, nonMatchedCellIds_prevColony{1});
    
    branchLength = length(frame_ind)-1; %minus one for the mother-cell
    
    %add current frame for ploting
    frame_ind = [frame_ind; t];
    colony_ind = [colony_ind;thisColonyInd];
    cellInst_ind{length(cellInst_ind)+1} = [];
    cellInst_ids{length(cellInst_ids)+1} = [];
    
    if(configuration.verb)
        fprintf('Cell %d of %s colony %d failed to match with a cell instant of %s colony %d!\n', nonMatchedCellInd_prevColony(1), frame(t-1).id, prevColonyInd, frame(t).id, thisCur);
    end
    
    %parameterize correction according to the users input
    if(sum(strcmp(configuration.correctErrors, {'specifyError', 'manual'})))
        
        %here must plot the cell Trajectory
        plotTrajectory('Trajectory of Type II error', frame, frame_ind, colony_ind, cellInst_ind, [1 0 0], [0 1 0], [1 1 1]);
        
        if ~motherCellInstFound
            f = warndlg('Current trajectory has NOT mother cell instant','Warning');
        end
        while(1)
            
            [indx,~] = listdlg('Name','Error in LT structure', 'PromptString', ...
                {'Please specify the error, leading to problematic tree pattern:',...
                'Only one error type can be selected at a time.'},...
                'SelectionMode','single', ...
                'ListString',...
                {'under-segmentation in current frame',  ...
                'over-segmentation and/or tracking error in previous frame(s)', ...
                'tracking error in current frame', ...
                'tracking error in previous frame (wrong trajectory)'}, 'ListSize',[300, 100]);
            
            if(indx == 1) %false negative case
                [tempFrame, log, currCorrectionList] = correctUnderSegmentationError(frame, t, thisColonyInd, nonMatchedCellIds_prevColony{1}, configuration);
                %the variables must be updated if and only if the log is not empty,
                %i.e. the function have successfully corrected undersegmentation in
                %current frame.
                if(~isempty(log))
                    frame = tempFrame;
                    correctionList = [correctionList; currCorrectionList];
                    
                    ids_1 = frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony.Properties.RowNames(frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony{:, 1} == 1);
                    ids_nan = frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony.Properties.RowNames(isnan(frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony{:, 1}));
                    correctedErrors{[ids_1;ids_nan], 1} = 1;
                    correctedErrors{nonMatchedCellIds_prevColony{1}, 1} = true;
                    
                    [frame_ind, colony_ind, cellInst_ind, ~, ~] = getTrajectory(frame, t, thisColonyInd, nonMatchedCellIds_prevColony{1});
                    frame_ind = [frame_ind; t];
                    colony_ind = [colony_ind; thisColonyInd];
                    temp = frame(t).colonyProps(thisColonyInd).correspondanceMatrix.Properties.VariableNames(frame(t).colonyProps(thisColonyInd).correspondanceMatrix{nonMatchedCellIds_prevColony{1}, :} > 0);
                    cellInst_ind{length(cellInst_ind)+1} = find(strcmp(frame(t).colonyProps(thisColonyInd).cellProps.Properties.RowNames , temp));
                    plotTrajectory('Trajectory of Type II error', frame, frame_ind, colony_ind, cellInst_ind, [1 0 0], [0 1 0], [1 1 1]);
                    
                    if(configuration.verb)
                        fprintf('Under-segmentation error corrected...\n');
                        for i = 1:length(log)
                            fprintf('%s', log{i});
                        end
                        fprintf('\n');
                    end
                    break;
                    
                else
                    
                    f = warndlg('Correction failed!','Warning');
                end
            elseif(indx == 2)
                [tempFrame, log, currCorrectionList, firstNewCellId] = correctRepeatedOverSegmentationErrorsII_withoutTrees(frame, frame_ind(end:-1:1), colony_ind(end:-1:1), cellInst_ids(end:-1:1), branchLength,configuration);
                %the variables must be updated if and only if the log is not empty,
                %i.e. the function have successfully corrected repeated
                %oversegmentation in previous frames.
                if(~isempty(log))
                    
                    ids_1 = frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony.Properties.RowNames(frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony{:, 1} == 1);
                    ids_nan = frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony.Properties.RowNames(isnan(frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony{:, 1}));
                    correctedErrors{[ids_1;ids_nan], 1} = 1;
                    correctedErrors(nonMatchedCellIds_prevColony{1}, :) = [];
                    
                    frame = tempFrame;
                    
                    correctionList = [correctionList; currCorrectionList];
                    
                    
                    [frame_ind, colony_ind, cellInst_ind, ~, ~] = getTrajectory(frame, t, thisColonyInd, firstNewCellId);
                    frame_ind = [frame_ind; t];
                    colony_ind = [colony_ind; thisColonyInd];
                    temp = frame(t).colonyProps(thisColonyInd).correspondanceMatrix.Properties.VariableNames(frame(t).colonyProps(thisColonyInd).correspondanceMatrix{firstNewCellId, :} > 0);
                    cellInst_ind{length(cellInst_ind)+1} = find(strcmp(frame(t).colonyProps(thisColonyInd).cellProps.Properties.RowNames , temp));
                    plotTrajectory('Trajectory of Type II error', frame, frame_ind, colony_ind, cellInst_ind, [1 0 0], [0 1 0], [1 1 1]);
                    
                    if(configuration.verb)
                        
                        fprintf('Over-segmentation error corrected...\n');
                        for i = 1:length(log)
                            fprintf('%s', log{i});
                        end
                        fprintf('\n');
                        
                    end
                    break;
                else
                    f = warndlg('Correction failed!','Warning');
                end
                
            elseif(indx == 3)
                
                [tempFrame, log, currCorrectionList] = matchMother2DaughterCellInst(frame, t, thisColonyInd, {nonMatchedCellIds_prevColony{1}}, configuration);
                if(isempty(log))
                    f = warndlg({'The function failed to correct the error or you canceled the correction strategy.','Specify again the error type!'},'Warning');
                else
                    correctionList = [correctionList; currCorrectionList];
                    frame = tempFrame;
                    
                    
                    ids_1 = frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony.Properties.RowNames(frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony{:, 1} == 1);
                    ids_nan = frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony.Properties.RowNames(isnan(frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony{:, 1}));
                    correctedErrors{[ids_1;ids_nan], 1} = 1;
                    
                    [frame_ind, colony_ind, cellInst_ind,  ~, ~] = getTrajectory(frame, t, thisColonyInd, nonMatchedCellIds_prevColony{1});
                    frame_ind = [frame_ind; t];
                    colony_ind = [colony_ind; thisColonyInd];
                    temp = frame(t).colonyProps(thisColonyInd).correspondanceMatrix.Properties.VariableNames(frame(t).colonyProps(thisColonyInd).correspondanceMatrix{nonMatchedCellIds_prevColony{1}, :} > 0);
                    cellInst_ind{length(cellInst_ind)+1} = find(strcmp(frame(t).colonyProps(thisColonyInd).cellProps.Properties.RowNames , temp));
                    plotTrajectory('Trajectory of Type II error', frame, frame_ind, colony_ind, cellInst_ind, [1 0 0], [0 1 0], [1 1 1]);
                    
                    if(configuration.verb)
                        fprintf('Tracking error in current frame corrected...\n');
                        for ii = 1:length(log)
                            fprintf('%s', log{ii});
                        end
                        fprintf('\n');
                    end
                    break;
                end
            elseif(indx == 4)
                [tempFrame, log, currCorrectionList] = correctTrackingErrorsInPrevFrames(frame, frame_ind(end:-1:1), colony_ind(end:-1:1), {cellInst_ids(end:-1:1)}, branchLength, configuration);
                if(isempty(log))
                    f = warndlg({'The function failed to correct the error or you canceled the correction strategy.','Specify again the error type!'},'Warning');
                else
                    
                    frame = tempFrame;
                    correctedErrors = tempCorrectedErrors;
                    correctionList = [correctionList; currCorrectionList];
                    [frame_ind, colony_ind, cellInst_ind,  ~, ~] = getTrajectory(frame, t, thisColonyInd, nonMatchedCellIds_prevColony{1});
                    frame_ind = [frame_ind; t];
                    colony_ind = [colony_ind; thisColonyInd];
                    temp = frame(t).colonyProps(thisColonyInd).correspondanceMatrix.Properties.VariableNames(frame(t).colonyProps(thisColonyInd).correspondanceMatrix{nonMatchedCellIds_prevColony{1}, :} > 0);
                    cellInst_ind{length(cellInst_ind)+1} = find(strcmp(frame(t).colonyProps(thisColonyInd).cellProps.Properties.RowNames , temp));
                    
                    plotTrajectory('Trajectory of Type II error', frame, frame_ind, colony_ind, cellInst_ind, [1 0 0], [0 1 0], [1 1 1]);
                    
                    if(configuration.verb)
                        fprintf('Tracking errors in previous frames corrected...\n');
                        for ii = 1:length(log)
                            fprintf('%s', log{ii});
                        end
                        fprintf('\n');
                    end
                    break;
                end
            else
                %if the user cancel the selection, then they must choose to cancel the correction approach or to have the function automatically specifying the cell for correction for the correction
                choice = questdlg(sprintf('Do you want to cancel the correction of this error?'), 'Error in LT structure', 'yes', 'no, but proceed with automatic correction specification', 'cancel', 'yes');
                if strcmp(choice, 'yes')
                    continue;
                elseif strcmp(choice, 'no, but proceed with automatic correction specification')
                    configuration.correctErrors = 'auto';
                    break;
                else
                    %do nothing
                end
            end
        end
    end
    
    if(sum(strcmp(configuration.correctErrors, {'specifyCells', 'auto'})))
        %if the  branches' length is greater than or equal to the
        %threshold, it is assumed that the unmatched cell of the previous
        %frame is not a false positive (over-segmentation error) but it corresponds
        %to no cell because a false negative (under-segmentation error) occured in frame t.
        if(branchLength >= minLifeSpan) %false negative case
            try
                [tempFrame, log, currCorrectionList] = correctUnderSegmentationError(frame, t, thisColonyInd, nonMatchedCellIds_prevColony{1}, configuration);
            catch
                'here'
            end
                %the variables must be updated if and only if the log is not empty,
            %i.e. the function have successfully corrected undersegmentation in
            %current frame.
            if(~isempty(log))
                frame = tempFrame;
                correctionList = [correctionList; currCorrectionList];
                
                ids_1 = frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony.Properties.RowNames(frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony{:, 1} == 1);
                ids_nan = frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony.Properties.RowNames(isnan(frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony{:, 1}));
                correctedErrors{[ids_1;ids_nan], 1} = 1;
                correctedErrors{nonMatchedCellIds_prevColony{1}, 1} = true;
                
                
                ids_1 = frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony.Properties.RowNames(frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony{:, 1} == 1);
                ids_nan = frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony.Properties.RowNames(isnan(frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony{:, 1}));
                correctedErrors{[ids_1;ids_nan], 1} = 1;
                
                if(configuration.verb)
                    fprintf('Under-segmentation error corrected...\n');
                    for i = 1:length(log)
                        fprintf('%s', log{i});
                    end
                    fprintf('\n');
                end
            else
                %nothing can be done probably the cell went out from the
                %field of view
                correctedErrors{nonMatchedCellIds_prevColony{1}, 1} = true;                               
            end
        elseif(branchLength < minLifeSpan)
            [tempFrame, log, currCorrectionList, firstNewCellId] = correctRepeatedOverSegmentationErrorsII_withoutTrees(frame, frame_ind(end:-1:1), colony_ind(end:-1:1), cellInst_ids(end:-1:1), branchLength, configuration);
            %the variables must be updated if and only if the log is not empty,
            %i.e. the function have successfully corrected repeated
            %oversegmentation in previous frames.
            if(~isempty(log))
                frame = tempFrame;
                correctionList = [correctionList; currCorrectionList];
                ids_1 = frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony.Properties.RowNames(frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony{:, 1} == 1);
                ids_nan = frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony.Properties.RowNames(isnan(frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony{:, 1}));
                correctedErrors{[ids_1;ids_nan], 1} = 1;
                correctedErrors(nonMatchedCellIds_prevColony{1}, :) = [];
                if(configuration.verb)
                    
                    fprintf('Over-segmentation error corrected...\n');
                    for i = 1:length(log)
                        fprintf('%s', log{i});
                    end
                    fprintf('\n');
                    
                end
            else
                %problematic trajectory, i.e. tracking error
                % build function removing trajectories...
                %problematic trajectory, i.e. tracking error
                % build function removing trajectories...
                [tempFrame, log, currCorrectionList] = correctTrackingErrorsInPrevFrames(frame, frame_ind(end:-1:1), colony_ind(end:-1:1), cellInst_ids(end:-1:1), branchLength, configuration);
                if(isempty(log))
                    fprintf('The function failed to correct the daughterless cell instant!\n');
                else
                    frame = tempFrame;
                    correctionList = [correctionList; currCorrectionList];
                    ids_1 = frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony.Properties.RowNames(frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony{:, 1} == 1);
                    ids_nan = frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony.Properties.RowNames(isnan(frame(t).colonyProps(thisColonyInd).matchedCellsPrevColony{:, 1}));
                    correctedErrors{[ids_1;ids_nan], 1} = 1;
                    correctedErrors{nonMatchedCellIds_prevColony{1}, 1} = 1;
                    if(configuration.verb)
                        fprintf('Tracking error in previous frame corrected...\n');
                        for ii = 1:length(log)
                            fprintf('%s', log{ii});
                        end
                        fprintf('\n');
                    end
                end
            end
        end
    end
end

end