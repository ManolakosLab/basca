function [colonyProps, newCellIds] = splitCell(fluo_channels_im, fluo_channels_thresh, colonyProps, cellToBeSplitInd, centroids, sigma, pp, neighborDistanceFactor, neighborhoodSize, verb)
warning('off', 'MATLAB:table:RowsAddedExistingVars');


[x, y] = size(colonyProps.bwColony);
[watersheds, L] = classifyPixels(x, y, [double(colonyProps.cellProps{cellToBeSplitInd, 'pixelList'}{1}(:, 1)) -double(colonyProps.cellProps{cellToBeSplitInd, 'pixelList'}{1}(:, 2))], centroids,sigma, pp);

cellProps = [];
newCellIds = {};

for i = 1:length(watersheds)
    if(length(watersheds{i}) > 5)
        [bwObject] = createBW(x, y, watersheds{i});
        bwObject = bwmorph(bwObject,'majority');

        objectProps = regionprops(bwObject, 'MinorAxisLength', 'MajorAxisLength', 'orientation', 'solidity');
        if(length(objectProps) ~= 1)
            return
        end
        [bwObjectCL, endpoints] = objectCenterline(bwObject);
        objectCLlen = length(find(bwObjectCL));
        
        %Use chessboard distance transform to get the object radius.
        bwObjectWithPad = padarray(bwObject,[1 1]);
        chessBoardDT = bwdist(~bwObjectWithPad, 'chessboard');
        chessBoardDT = chessBoardDT(2:end-1,2:end-1);
        %assuming that the radius of the segment is the maximum distance from the boundary.
        objectRadius = max(max(chessBoardDT));
        if(objectCLlen <= objectRadius ||objectRadius < 2)
            objectWidthInPixels = 2*double(objectRadius)+1;
            objectWidth = objectProps.MinorAxisLength;
            objectLengthInPixels = round(objectProps.MajorAxisLength);
            objectLength = objectProps.MajorAxisLength;
            objectCentralPixel = round(mean(double(watersheds{i})));
            
        else
            try
                objectCLVerticesWithoutPoles = bwtraceboundary(bwObjectCL, [endpoints(2, 1) endpoints(1, 1)], 'N', 8, objectCLlen);
            catch
                'here'
            end
            [verticesFromPoleA, ~, verticesToPoleB, ~] = getObjectPoles(bwObject, objectCLVerticesWithoutPoles, 'euclidean', verb);
            vertices = [verticesFromPoleA; verticesToPoleB; objectCLVerticesWithoutPoles];
            %if the segment is non-complex, get the segment's centerline to
            %check for bowties
            vertices = [vertices(:, 2) vertices(:, 1)];
            bwObjectCL = createBW(x, y, vertices);
            [~, endpoints, ~] = anaskel(bwObjectCL);
            objectCLVertices = bwtraceboundary(bwObjectCL, [endpoints(2, 1) endpoints(1, 1)], 'N', 8,length(vertices));
            if(size(objectCLVertices, 1) > 2)
                [objectSimplifiedCLVertices, ~] = dpsimplify(objectCLVertices , 1.4142);
            else
                objectSimplifiedCLVertices = objectCLVertices;
            end
            [distanceToBoundaryInPixels, ~, ~, ~] = meausureDistToBoundary2(bwObject, objectCLVertices, 'chessboard');
            [distanceToBoundary, ~, ~, ~] = meausureDistToBoundary2(bwObject, objectCLVertices, 'euclidean');
            objectLengthInPixels = length(distanceToBoundary);
            
            [objectLength, ~] = arclength(objectSimplifiedCLVertices(:, 2), objectSimplifiedCLVertices(:, 1), 'linear');
            
            objectWidthInPixels = max(medfilt1(distanceToBoundaryInPixels(2:end-1), double(objectRadius)));
            objectWidth = max(medfilt1(distanceToBoundary(2:end-1), ceil(objectWidthInPixels/2)));
            objectCentralPixel = objectCLVertices(ceil(size(objectCLVertices, 1)/2), :);
            objectCentralPixel = [objectCentralPixel(2) objectCentralPixel(1)];
        end
        segmentsNum = length(cellProps)+1;
        cellProps(segmentsNum).pixelList = uint16(watersheds{i});
        cellProps(segmentsNum).centroid = mean(watersheds{i});
        cellProps(segmentsNum).width = objectWidth;
        cellProps(segmentsNum).widthInPixels = objectWidthInPixels;
        cellProps(segmentsNum).lengthInPixels = objectLengthInPixels;
        cellProps(segmentsNum).length = objectLength;
        bwObjectPerim = bwperim(bwObject);
        cellProps(segmentsNum).centralPixel = objectCentralPixel;
        
        if bwObject(ceil(cellProps(segmentsNum).centroid(2)), ceil(cellProps(segmentsNum).centroid(1))) == 0
            cellProps(segmentsNum).hasIrregularShape = true;
        else
            cellProps(segmentsNum).hasIrregularShape = false;
        end
        cellProps(segmentsNum).minorAxis = objectProps.MinorAxisLength;
        cellProps(segmentsNum).majorAxis = objectProps.MajorAxisLength;
        cellProps(segmentsNum).eccentricity = objectProps.MinorAxisLength/objectProps.MajorAxisLength;
        cellProps(segmentsNum).orientation = objectProps.Orientation;
        cellProps(segmentsNum).solidity = objectProps.Solidity;
        if(cellProps(segmentsNum).orientation < 0)
            cellProps(segmentsNum).orientation = 360+cellProps(segmentsNum).orientation;
        end
        cellProps(segmentsNum).covariance = cov(watersheds{i});
        b = bwboundaries(bwObjectPerim);
        cellProps(segmentsNum).boundaryPixelList = uint16([b{1}(:, 2) b{1}(:, 1)]);
    end
end

colonyProps.cellProps(cellToBeSplitInd, :) = [];
[colonyProps] = removeAdjacencyMatrix(colonyProps, cellToBeSplitInd);

for k = 1:length(cellProps)
    newCellId = sprintf('x%07d_y%07d', ...
        round((cellProps(k).centroid(1, 1)+colonyProps.bBoxULCorner(1, 1))*1000) , ...
        round((cellProps(k).centroid(1, 2)+colonyProps.bBoxULCorner(1, 2))*1000));
    
    
    newCellIds = [newCellIds ; newCellId];
    
    colonyProps.cellProps(newCellId, 'centroid') = {{cellProps(k).centroid}};
    colonyProps.cellProps(newCellId, 'centralPixel') = {{cellProps(k).centralPixel}};
    colonyProps.cellProps(newCellId, 'has_irregular_shape') = {cellProps(k).hasIrregularShape};
    
    colonyProps.cellProps(newCellId, 'pixelList') = {{cellProps(k).pixelList}};
    colonyProps.cellProps(newCellId, 'width_in_pixels') = {cellProps(k).width};
    colonyProps.cellProps(newCellId, 'width_in_pixels_chessboard') = {cellProps(k).widthInPixels};
    colonyProps.cellProps(newCellId, 'length_in_pixels') = {cellProps(k).length};
    colonyProps.cellProps(newCellId, 'length_in_pixels_chessboard') = {cellProps(k).lengthInPixels};
    colonyProps.cellProps(newCellId, 'minorAxis') = {cellProps(k).minorAxis};
    colonyProps.cellProps(newCellId, 'majorAxis') = {cellProps(k).majorAxis};
    colonyProps.cellProps(newCellId, 'eccentricity') = {cellProps(k).eccentricity};
    colonyProps.cellProps(newCellId, 'orientation') = {cellProps(k).orientation};
    colonyProps.cellProps(newCellId, 'solidity') = {cellProps(k).solidity};
    colonyProps.cellProps(newCellId, 'covariance') = {{cellProps(k).covariance}};
    colonyProps.cellProps(newCellId, 'boundaryPixelList') = {{cellProps(k).boundaryPixelList}};
    
    for channel_i = 1:length(fluo_channels_thresh)
        [fluo_channel_int_pixelList] = measureFluorescence(fluo_channels_im{channel_i}, cellProps(k).pixelList+uint16([ones(size(cellProps(k).pixelList, 1), 1)*colonyProps.bBoxULCorner(1) ones(size(cellProps(k).pixelList, 1), 1)*colonyProps.bBoxULCorner(2)]));
        colonyProps.cellProps(newCellId, sprintf('fluo_pixelList_channel%d', channel_i )) = {{fluo_channel_int_pixelList}};
        colonyProps.cellProps(newCellId, sprintf('fluo_int_channel%d', channel_i)) = {mean(fluo_channel_int_pixelList(fluo_channel_int_pixelList>fluo_channels_thresh(channel_i)))};
        colonyProps.cellProps(newCellId, sprintf('fluo_cov_channel%d', channel_i)) = {sum(fluo_channel_int_pixelList>fluo_channels_thresh(channel_i))/length(fluo_channel_int_pixelList)};
    end
    newCellInd = ismember(colonyProps.cellProps.Properties.RowNames, newCellId);
    [colonyProps] = insertToAdjacencyMatrix(colonyProps, newCellInd, neighborDistanceFactor, neighborhoodSize);    
end


end