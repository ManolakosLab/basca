function [frame, logs, correctionList] = correctTrackingErrorsInPrevFrames(frame, frame_ind, colony_ind, cell_ids, branchLength, configuration)

correctionList = [];
logs = [];
i = 2;

%going backwards, until we reach the divided cell instant (i.e. reach the branch's length)
while(i-1 <= branchLength+1)

    [frame, currLogs, currCorrectionList] = matchMother2DaughterCellInst(frame, frame_ind(i-1), colony_ind(i-1), {cell_ids(i)}, configuration);
    
    correctionList = [correctionList; currCorrectionList];
    logs = [ logs; currLogs];
    
    if size(currCorrectionList, 1) == 1
        if isnan(currCorrectionList(1, 3))
            return;
        end
    end
    i = i + 1;   
end


end
