function [frame, logs, firstNewCellId] = merge2Cells(frame, t, currColonyInd , currCellId, configuration)
firstNewCellId = '';
logs = [];

mode = configuration.correctErrors;
%going backwards, until we reach the divided cell instant (i.e. reach the branch's length)

%getting the cell ids of current colony
currFrameCellIds = frame(t).colonyProps(currColonyInd).cellProps.Properties.RowNames;
prevFrameCellIds = frame(t).colonyProps(currColonyInd).prevColonyProps.cellProps.Properties.RowNames;
currCellInd = find(strcmp(currFrameCellIds, currCellId));

%parameterize correction according to the users input
if sum(strcmp(mode, {'specifyCells', 'manual'}))
    while(1)
        dlg_title = 'Error in LT structure';
        prompt = {sprintf('Specify the cell instant for merging with cell instant %d (%s): ', currCellInd, frame(t).id)};
        
        default_answers = {'cell instant 1'};
        dims = [1 70];
        answers = inputdlg(prompt, dlg_title, dims, default_answers);
        if size(answers) == 0
            
            %if the user cancel the selection, then they must choose to cancel the correction approach or to have the function automatically specifying the cell instants for the correction
            if strcmp(mode, 'manual')
                choice = questdlg(sprintf('Do you want to choose another correction approach?'), 'Error in LT structure', 'yes', 'no, but proceed with automatic cell instant specification', 'cancel', 'yes');
                if strcmp(choice, 'yes')
                    return;
                elseif strcmp(choice, 'no, but proceed with automatic cell instant specification')
                    configuration.correctErrors = 'auto';
                    break;
                else
                    %do nothing
                end
            else
                fprintf('You canceled the selection, the function will specify the cell instants for correction automatically!\n');
                configuration.correctErrors = 'auto';
                break;
            end
        else
            
            input = str2double(answers);
            if input(1) ~= ceil(input(1)) || input(1) < 0 ||  input(1) > length(currFrameCellIds)
                fprintf('Error: <input> must be a positive integer <= %d.\n', length(currFrameCellIds));
            else
                candidateCellsForMergingIndices = input(1);
                break;
            end
        end
    end
end

if sum(strcmp(configuration.correctErrors, {'specifyError', 'auto'}))
    candidateCellsForMergingIndices = find(frame(t).colonyProps(currColonyInd).adjacencyMatrix(currCellInd, :) == 1);
end

%getting only the touching neigbors of the cell

overlapScore = zeros(length(candidateCellsForMergingIndices), 1);
currColonyProps = cell(length(candidateCellsForMergingIndices), 1);
newCellIds = cell(length(candidateCellsForMergingIndices), 1);
for ii = 1:length(candidateCellsForMergingIndices)
    
    if(frame(t).colonyProps(currColonyInd).matchedCellsCurrColony{candidateCellsForMergingIndices(ii), 1})
        
        [currColonyProps{ii}, newCellIds{ii}]= mergeCells(frame(t).fluo_channels_im,  frame(t).fluo_channels_thresh,frame(t).colonyProps(currColonyInd), [candidateCellsForMergingIndices(ii); currCellInd],configuration.neighborDistanceFactor, configuration.neighborhoodSize, configuration.verb);
        
        if(~isempty(newCellIds{ii}))
            
            prevFrameCellInd = frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, candidateCellsForMergingIndices(ii)} > 0;
            
            if sum( prevFrameCellInd ) ~= 0
                %getting the cell ids of current colony
                prevFrameColonyInd = getColonyIndex(frame(t-1), prevFrameCellIds{prevFrameCellInd}, frame(t).colonyProps(currColonyInd).correspondingColPrevFrameInd);
                
                prevFrameCellCoords = double(cell2mat(frame(t-1).colonyProps(prevFrameColonyInd).cellProps{prevFrameCellIds{prevFrameCellInd}, 'pixelList'}));
                prevFrameCellCentroid = round(cell2mat(frame(t-1).colonyProps(prevFrameColonyInd).cellProps{prevFrameCellIds{prevFrameCellInd}, 'centroid'}));
                prevFrameCellCoords(:, 1) = prevFrameCellCoords(:, 1) - prevFrameCellCentroid(1, 1);
                prevFrameCellCoords(:, 2) = prevFrameCellCoords(:, 2) - prevFrameCellCentroid(1, 2);
                
                currFrameCellInd = frame(t).colonyProps(currColonyInd).correspondanceMatrix{prevFrameCellInd, :} > 0;
                currFrameCellInd(candidateCellsForMergingIndices(ii)) = 0;
                
                newCellCoords = double(cell2mat(currColonyProps{ii}.cellProps{newCellIds{ii}, 'pixelList'}));
                if(sum(currFrameCellInd) > 0)
                    newCellCoords = [newCellCoords; double(cell2mat(frame(t).colonyProps(currColonyInd).cellProps{currFrameCellInd, 'pixelList'}))];
                    
                    newCellCoords(:, 1) = newCellCoords(:, 1) - round(mean(newCellCoords(:, 1)));
                    newCellCoords(:, 2) = newCellCoords(:, 2) - round(mean(newCellCoords(:, 2)));
                else
                    currFrameCellCentroid = round(cell2mat(currColonyProps{ii}.cellProps{newCellIds{ii}, 'centroid'}));
                    newCellCoords(:, 1) = newCellCoords(:, 1) - currFrameCellCentroid(:, 1);
                    newCellCoords(:, 2) = newCellCoords(:, 2) - currFrameCellCentroid(:, 2);
                end
                overlapNewCell = ismember(newCellCoords, prevFrameCellCoords, 'rows');
                overlapScoreNewCell = sum(overlapNewCell)/(length(newCellCoords)+length(prevFrameCellCoords)-sum(overlapNewCell));
                
                existingCellCoords = double(cell2mat(frame(t).colonyProps(currColonyInd).cellProps{candidateCellsForMergingIndices(ii), 'pixelList'}));
                if(sum(currFrameCellInd) > 0)
                    existingCellCoords = [existingCellCoords; double(cell2mat(frame(t).colonyProps(currColonyInd).cellProps{currFrameCellInd, 'pixelList'}))];
                    
                    existingCellCoords(:, 1) = existingCellCoords(:, 1) - round(mean(existingCellCoords(:, 1)));
                    existingCellCoords(:, 2) = existingCellCoords(:, 2) - round(mean(existingCellCoords(:, 2)));
                else
                    ccurrFrameCellCentroid = round(cell2mat(frame(t).colonyProps(currColonyInd).cellProps{candidateCellsForMergingIndices(ii), 'centroid'}));
                    existingCellCoords(:, 1) = existingCellCoords(:, 1) - ccurrFrameCellCentroid(:, 1);
                    existingCellCoords(:, 2) = existingCellCoords(:, 2) - ccurrFrameCellCentroid(:, 2);
                end
                overlapExisting = ismember(existingCellCoords, prevFrameCellCoords, 'rows');
                overlapScoreExistingCell = sum(overlapExisting)/(length(existingCellCoords)+length(prevFrameCellCoords)-sum(overlapExisting));
                
                %we must consider this merging, if and only if the overlap
                %score between the cell of previous frame and the cell(s) of
                %current frame is %is imporeved...
                if(overlapScoreNewCell > overlapScoreExistingCell)
                    overlapScore(ii) = overlapScoreNewCell;
                end
                clear ccurrFrameCellCoords currFrameCellCoords prevFrameCellCoords;
            end
        end
    end
end

[maxOverlapScore, ii] = max(overlapScore);
if(maxOverlapScore > 0)
    
    newCellId = newCellIds{ii};
    frame(t).colonyProps(currColonyInd) = currColonyProps{ii};
    mergedCellsIds = {currCellId; currFrameCellIds{candidateCellsForMergingIndices(ii)}};
    
    
    frame(t).colonyProps(currColonyInd).matchedCellsCurrColony{newCellId, 1} = 1;
    
    frame(t).colonyProps(currColonyInd).matchedCellsCurrColony(mergedCellsIds, :) = [];
    
    %add a column to correspondanceMatrix of current frame (i.e. for the new formated cell).
    frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, mergedCellsIds{1}}(isnan(frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, mergedCellsIds{1}})) = 0;
    frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, newCellId} = frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, mergedCellsIds{1}};
    for jj = 2:length(mergedCellsIds)
        frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, mergedCellsIds{jj}}(isnan(frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, mergedCellsIds{jj}})) = 0;
        frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, newCellId} = frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, newCellId} + frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, mergedCellsIds{jj}};
    end
    frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, newCellId}(frame(t).colonyProps(currColonyInd).correspondanceMatrix{:, newCellId} == 0) = NaN;
    
    %remove the columns to correspondanceMatrix of current frame (i.e. for the merged cells).
    frame(t).colonyProps(currColonyInd).correspondanceMatrix(:, mergedCellsIds) = [];
    
    newCellInd = find(ismember(frame(t).colonyProps(currColonyInd).cellProps.Properties.RowNames, newCellId));
    logs{1} = fprintf('frame %d, colony %d: Merged cells (%d, %d) to cell %d\n', t, currColonyInd, candidateCellsForMergingIndices(ii), currCellInd, newCellInd);
    
    firstNewCellId = newCellId;
end

end
