function [frame, correctionList] = correctPrematureDivisionPattern(frame, t, currColonyInd, configuration, correctionList)

currCorrection = [];
minLifeSpan = configuration.trajectoryThresh;

while(1)
    previousHeight = height(frame(t).colonyProps(currColonyInd).correspondanceMatrix);
    prevFrameCellIds = frame(t).colonyProps(currColonyInd).correspondanceMatrix.Properties.RowNames;
    for i = 1:height(frame(t).colonyProps(currColonyInd).correspondanceMatrix)

        if(sum(frame(t).colonyProps(currColonyInd).correspondanceMatrix{i, :} > 0) > 1)
            indices = frame(t).colonyProps(currColonyInd).correspondanceMatrix{prevFrameCellIds{i}, :} > 0;

            cellToBeMergedIds = frame(t).colonyProps(currColonyInd).correspondanceMatrix.Properties.VariableNames(indices);
            fprintf('Cell division: ( %d, %d, %d ) -> ( %d, %d, %s )\n', t-1, getColonyIndex(frame(t-1),  prevFrameCellIds{i}, frame(t).colonyProps(currColonyInd).correspondingColPrevFrameInd), i, t, currColonyInd, strjoin(strsplit(num2str(find(indices))), ', '));

            [frame_ind, colony_ind, cellInst_ind, cellInst_ids, motherCellInstFound, logs] = getTrajectory(frame, t, currColonyInd, prevFrameCellIds{i});

            branchLength = length(frame_ind);

            frame_ind = [frame_ind; t];
            colony_ind = [colony_ind; currColonyInd];
            cellInst_ind = [cellInst_ind; find(indices)];
            cellInst_ids{end+1} = cellToBeMergedIds';


            %if the  branches' length is greater than or equal to the
            %threshold, it is assumed that the unmatched cell of the previous
            %frame is not a false positive (over-segmentation error) but it corresponds
            %to no cell because a false negative (under-segmentation error) occured in frame t.
            if(branchLength < minLifeSpan && motherCellInstFound) %false negative case
                %parameterize correction according to the users input
                if sum(strcmp(configuration.correctErrors, {'specifyError', 'manual'}))
                    %here must plot the cell Trajectory
                    plotTrajectory('Trajectory of Type III error', frame, frame_ind, colony_ind, cellInst_ind, [1 0 0], [0 1 0], [1 1 1]);
                    ind = frame(frame_ind(1)).colonyProps(colony_ind(1)).correspondanceMatrix{cellInst_ind{1}, :} > 0;


                    fprintf('Previous cell division: ( %d, %d, %d ) -> ( %d, %d, %s )\n', frame_ind(1), colony_ind(1), cellInst_ind{1}, frame_ind(2), ...
                        colony_ind(2), strjoin(strsplit(num2str(find(ind))), ', '));

                    %choice = questdlg(sprintf('Do you agree that it is Type III error'), 'Error in LT structure', 'yes, it is over-segmentation in current frame', 'yes, it is over-segmentation and/or tracking error in previous frame(s)', 'no, it is a tracking error in current frame', 'yes, it is over-segmentation in current frame');
                    while(1)
                        [indx,~] = listdlg('Name','Error in LT structure', 'PromptString', ...
                            {'Please specify the error:',...
                            'Only one error type can be selected at a time.'},...
                            'SelectionMode','single', ...
                            'ListString',...
                            {'over-segmentation in current frame',  ...
                            'over-segmentation and/or tracking errors in previous frame(s)', ...
                            'tracking error in current frame', ...
                            'tracking errors in previous frames'}, 'ListSize',[300, 100]);

                        if indx == 1
                            [frame, cellToBeMergedIds, frame(t).colonyProps(currColonyInd).matchedCellsCurrColony, logs, currCorrection] =  mergeMultipleCells(frame, t, currColonyInd, frame(t).colonyProps(currColonyInd).matchedCellsCurrColony, cellToBeMergedIds, configuration);
                            if isempty(logs)
                                f = warndlg({'The function failed to correct the error or you canceled the correction strategy.','Specify again the error type!'},'Warning');
                            else
                                if(configuration.verb)

                                    fprintf('Over-segmentation in current frame corrected...\n');
                                    for ii = 1:length(logs)
                                        fprintf('%s', logs{ii});
                                    end
                                    fprintf('\n');

                                end
                                break;
                            end
                        elseif indx == 2
                            [tempFrame, logs, currCorrection, firstNewCellId] = correctRepeatedOverSegmentationErrorsIII_withoutTrees(frame, frame_ind(end:-1:1), colony_ind(end:-1:1), cellInst_ids(end:-1:1), branchLength, configuration);
                            if(isempty(logs))
                                f = warndlg({'The function failed to correct the error or you canceled the correction strategy.','Specify again the error type!'},'Warning');
                            else
                                frame = tempFrame;
                                [frame_ind, colony_ind, cellInst_ind,  ~, ~] = getTrajectory(frame, t, currColonyInd, firstNewCellId);
                                frame_ind = [frame_ind; t];
                                colony_ind = [colony_ind; currColonyInd];
                                temp = frame(t).colonyProps(currColonyInd).correspondanceMatrix.Properties.VariableNames(frame(t).colonyProps(currColonyInd).correspondanceMatrix{firstNewCellId, :} > 0);
                                cellInst_ind{length(cellInst_ind)+1} = find(strcmp(frame(t).colonyProps(currColonyInd).cellProps.Properties.RowNames , temp));

                                plotTrajectory('Trajectory of Type III error', frame, frame_ind, colony_ind, cellInst_ind, [1 0 0], [0 1 0], [1 1 1]);

                                if(configuration.verb)

                                    fprintf('Over-segmentation or tracking error in previous frame(s) corrected...\n');
                                    for ii = 1:length(logs)
                                        fprintf('%s', logs{ii});
                                    end
                                    fprintf('\n');

                                end
                                break;
                            end
                        elseif indx == 3
                            [tempFrame, logs, currCorrection] = matchMother2DaughterCellInst(frame, t, currColonyInd, {prevFrameCellIds(i)}, configuration);
                            if(isempty(logs))
                                f = warndlg({'The function failed to correct the error or you canceled the correction strategy.','Specify again the error type!'},'Warning');
                            else
                                frame = tempFrame;
                                [frame_ind, colony_ind, cellInst_ind,  ~, ~] = getTrajectory(frame, t, currColonyInd, prevFrameCellIds{i});
                                frame_ind = [frame_ind; t];
                                colony_ind = [colony_ind; currColonyInd];
                                temp = frame(t).colonyProps(currColonyInd).correspondanceMatrix.Properties.VariableNames(frame(t).colonyProps(currColonyInd).correspondanceMatrix{prevFrameCellIds{i}, :} > 0);
                                cellInst_ind{length(cellInst_ind)+1} = find(strcmp(frame(t).colonyProps(currColonyInd).cellProps.Properties.RowNames , temp));

                                plotTrajectory('Trajectory of Type III error', frame, frame_ind, colony_ind, cellInst_ind, [1 0 0], [0 1 0], [1 1 1]);

                                if(configuration.verb)
                                    fprintf('Tracking error in current frame corrected...\n');
                                    for ii = 1:length(logs)
                                        fprintf('%s', logs{ii});
                                    end
                                    fprintf('\n');
                                end
                                break;
                            end
                        elseif indx == 4
                            [tempFrame, log, currCorrection] = correctTrackingErrorsInPrevFrames(frame, frame_ind(end:-1:1), colony_ind(end:-1:1), {cellInst_ids(end:-1:1)}, branchLength, configuration);
                            if(isempty(log))
                                f = warndlg({'The function failed to correct the error or you canceled the correction strategy.','Specify again the error type!'},'Warning');
                            else

                                frame = tempFrame;
                                [frame_ind, colony_ind, cellInst_ind,  ~, ~] = getTrajectory(frame, t, currColonyInd, prevFrameCellIds{i});
                                frame_ind = [frame_ind; t];
                                colony_ind = [colony_ind; currColonyInd];
                                temp = frame(t).colonyProps(currColonyInd).correspondanceMatrix.Properties.VariableNames(frame(t).colonyProps(currColonyInd).correspondanceMatrix{prevFrameCellIds{i}, :} > 0);
                                cellInst_ind{length(cellInst_ind)+1} = find(strcmp(frame(t).colonyProps(currColonyInd).cellProps.Properties.RowNames , temp));

                                plotTrajectory('Trajectory of Type III error', frame, frame_ind, colony_ind, cellInst_ind, [1 0 0], [0 1 0], [1 1 1]);

                                if(configuration.verb)
                                    fprintf('Tracking errors in previous frames corrected...\n');
                                    for ii = 1:length(log)
                                        fprintf('%s', log{ii});
                                    end
                                    fprintf('\n');
                                end
                                break;
                            end
                        else
                            %if the user cancel the selection, then they must choose to cancel the correction approach or to have the function automatically specifying the cell for correction for the correction
                            choice = questdlg(sprintf('Do you want to cancel the correction of this error?'), 'Error in LT structure', 'yes', 'no, but proceed with automatic correction specification', 'cancel', 'yes');
                            if strcmp(choice, 'yes')
                                continue;
                            elseif strcmp(choice, 'no, but proceed with automatic correction specification')
                                configuration.correctErrors = 'auto';
                                break;
                            else
                                %do nothing
                            end
                        end
                    end
                end

                if  sum(strcmp(configuration.correctErrors, {'specifyCells', 'auto'}))
                    [frame, cellToBeMergedIds, frame(t).colonyProps(currColonyInd).matchedCellsCurrColony, logs, currCorrection] =  mergeMultipleCells(frame, t, currColonyInd, frame(t).colonyProps(currColonyInd).matchedCellsCurrColony, cellToBeMergedIds, configuration);
                    if(length(cellToBeMergedIds) > 1)
                        %if the cell instants did not merge to one cell instant, i.e. the function failed to
                        %merge all the cell instants or the user choose not to merge
                        %the cell instants, probably the division is correct, thus the
                        %function will have to correct the oversegmentation and/or tracking errors in previous
                        %frames.
                        %t
                        %if t==26
                        %[tempFrame, logs, currCorrection, firstNewCellId] = correctRepeatedOverSegmentationErrorsIII_withoutTrees(frame, frame_ind(end:-1:1), colony_ind(end:-1:1), cellInst_ids(end:-1:1), branchLength, configuration);
                        %else
                        [tempFrame, logs, currCorrection, firstNewCellId] = correctRepeatedOverSegmentationErrorsIII_withoutTrees(frame, frame_ind(end:-1:1), colony_ind(end:-1:1), cellInst_ids(end:-1:1), branchLength, configuration);
                        %end
                        if(~isempty(logs))
                            frame = tempFrame;

                            if(configuration.verb)

                                fprintf('Over-segmentation error corrected...\n');
                                for ii = 1:length(logs)
                                    fprintf('%s', logs{ii});
                                end
                                fprintf('\n');

                            end
                        else
                            %if the function fails to correct the oversegmentaiton
                            %and/or tracking in previous frames, correct
                            %tracking error in current frame.
                            [tempFrame, logs, currCorrection] = matchMother2DaughterCellInst(frame, t, currColonyInd, {prevFrameCellIds(i)}, configuration);
                            if(isempty(logs))
                               fprintf('The function failed to correct the premature division pattern!\n');

                            else
                                frame = tempFrame;

                                if(configuration.verb)
                                    fprintf('Tracking error in current frame corrected...\n');
                                    for ii = 1:length(logs)
                                        fprintf('%s', logs{ii});
                                    end
                                    fprintf('\n');
                                end
                                break;
                            end
                        end
                    else
                        if(configuration.verb)

                            fprintf('Over-segmentation in current frame corrected...\n');
                            for ii = 1:length(logs)
                                fprintf('%s', logs{ii});
                            end
                            fprintf('\n');

                        end
                    end
                end
                correctionList = [correctionList; currCorrection];
            else

                fprintf('%s', logs{1});
            end
        end
        if height(frame(t).colonyProps(currColonyInd).correspondanceMatrix) ~= previousHeight
            break;
        end
    end
    if height(frame(t).colonyProps(currColonyInd).correspondanceMatrix) == previousHeight
        break;
    end   
end