function [frame, logs, correctionList, firstNewCellId] = correctRepeatedOverSegmentationErrorsIII_withoutTrees(frame, frame_ind, colony_ind, cell_ids, branchLength, configuration)
correctionList = [];
firstNewCellId = '';
logs = {};
i = 2;
mode = configuration.correctErrors;
%going backwards, until we reach the divided cell instant (i.e. reach the branch's length)
while(i <= branchLength+1)
    
    %getting current cell's id (the cell to be merged with another cell)
    currCellId = cell_ids{i};
    
    %getting current cell's colony index
    currColonyInd = colony_ind(i);
    
    %getting the cell ids of current colony
    currColCellIds = frame(frame_ind(i)).colonyProps(currColonyInd).cellProps.Properties.RowNames;
    
    currCellInd = find(strcmp(currColCellIds, currCellId));
    
    %parameterize correction according to the users input
    if sum(strcmp(mode, {'specifyCells', 'manual'}))
        while(1)
            dlg_title = 'Error in LT structure';
            prompt = {sprintf('Specify the cell instant for merging with cell instant %d (%s) or leave it empty if you believe that it is tracking error: ', currCellInd, frame(frame_ind(i)).id)};
            
            default_answers = {''};
            dims = [1 70];
            answers = inputdlg(prompt, dlg_title, dims, default_answers);
            if size(answers) == 0
                
                %if the user cancel the selection, then they must choose to cancel the correction approach or to have the function automatically specifying the cell instants for the correction
                if strcmp(mode, 'manual')
                    choice = questdlg(sprintf('Do you want to choose another correction approach?'), 'Error in LT structure', 'yes', 'no, but proceed with automatic cell instant specification', 'cancel', 'yes');
                    if strcmp(choice, 'yes')
                        return;
                    elseif strcmp(choice, 'no, but proceed with automatic cell instant specification')
                        mode = 'auto';
                        break;
                    else
                        %do nothing
                    end
                else
                    fprintf('You canceled the selection, the function will specify the cell instants for correction automatically!\n');
                    mode = 'auto';
                    break;
                end
            else
                if isempty(answers{1})
                    %it is a tracking error, remove it from tree.
                    candidateCellsForMergingIndices = [];
                    break;
                else
                    input = str2double(answers);
                    if input(1) ~= ceil(input(1)) || input(1) < 0 ||  input(1) > length(currColCellIds)
                        fprintf('Error: <input> must be a positive integer <= %d.\n', length(currColCellIds));
                    else
                        candidateCellsForMergingIndices = input(1);
                        break;
                    end
                end
            end
        end
    end
    
    if sum(strcmp(mode, {'specifyError', 'auto'}))
        candidateCellsForMergingIndices = find(frame(frame_ind(i)).colonyProps(currColonyInd).adjacencyMatrix(currCellInd, :) == 1);
        mode = configuration.correctErrors;
    end
    
    
    overlapScore = zeros(length(candidateCellsForMergingIndices), 1);
    currColonyProps = cell(length(candidateCellsForMergingIndices), 1);
    newCellId = cell(length(candidateCellsForMergingIndices), 1);
    for ii = 1:length(candidateCellsForMergingIndices)
        
        [currColonyProps{ii}, newCellId{ii}]= mergeCells( frame(frame_ind(i)).fluo_channels_im,  frame(frame_ind(i)).fluo_channels_thresh, frame(frame_ind(i)).colonyProps(currColonyInd), [candidateCellsForMergingIndices(ii); currCellInd], configuration.neighborDistanceFactor, configuration.neighborhoodSize, configuration.verb);
        %find the node of the cell that will be merged within the lineage tree
        
        if(~isempty(newCellId{ii}))
            
            existingCellCoords = double(cell2mat(frame(frame_ind(i)).colonyProps(currColonyInd).cellProps{candidateCellsForMergingIndices(ii), 'pixelList'}));
            existingCellCentroid = round(cell2mat(frame(frame_ind(i)).colonyProps(currColonyInd).cellProps{candidateCellsForMergingIndices(ii), 'centroid'}));
            
            existingCellCoords(:, 1) = existingCellCoords(:, 1) - existingCellCentroid(1, 1);
            existingCellCoords(:, 2) = existingCellCoords(:, 2) - existingCellCentroid(1, 2);
            
            newCellCoords = double(cell2mat(currColonyProps{ii}.cellProps{newCellId{ii}, 'pixelList'}));
            newCellCentroid = round(cell2mat(currColonyProps{ii}.cellProps{newCellId{ii}, 'centroid'}));
            
            
            newCellCoords(:, 1) = newCellCoords(:, 1) - newCellCentroid(1, 1);
            newCellCoords(:, 2) = newCellCoords(:, 2) - newCellCentroid(1, 2);
            
            %get the index of the next colony (each colony of current frame can point only to a colony in the next frame)
            nextColonyInd  = currColonyProps{ii}.correspondingColNextFrameInd;
            
            %find the daughter of the cell instant that it is candidate
            %for merging cell so as compute the overlap score between the
            %daughter and the existing cell instant and the daughter and
            %the new cell instant. Only if the overlap score is improved,
            %we keep the merging.
            nextframeCellInd1 = frame(frame_ind(i-1)).colonyProps(nextColonyInd).correspondanceMatrix{currColCellIds{candidateCellsForMergingIndices(ii)}, :} > 0;
            nextframeCellInd2 = frame(frame_ind(i-1)).colonyProps(nextColonyInd).correspondanceMatrix{currColCellIds{currCellInd}, :} > 0;
            
            daughtersInd = find((nextframeCellInd1 | nextframeCellInd2) > 0);

            nextFrameCellCoordsforNew =[];
            for jj=1:length(daughtersInd)
                nextFrameCellCoordsforNew = [nextFrameCellCoordsforNew;double(cell2mat(frame(frame_ind(i-1)).colonyProps(nextColonyInd).cellProps{daughtersInd(jj), 'pixelList'}))];
            end
            nextFrameCellCoordsforNew(:, 1) = nextFrameCellCoordsforNew(:, 1) - mean(nextFrameCellCoordsforNew(:, 1));
            nextFrameCellCoordsforNew(:, 2) = nextFrameCellCoordsforNew(:, 2) - mean(nextFrameCellCoordsforNew(:, 2));            
            [in, ~] = inpolygon(newCellCoords(:, 1), newCellCoords(:, 2),  nextFrameCellCoordsforNew(:, 1),  nextFrameCellCoordsforNew(:, 2));
            overlapBitmapWithNew = sum(in);            

            
            daughtersInd = find(nextframeCellInd1 > 0);
            if isempty(daughtersInd)
                continue
            end
            nextFrameCellCoordsforExisting =[];
            for jj=1:length(daughtersInd)
                nextFrameCellCoordsforExisting = [nextFrameCellCoordsforExisting;double(cell2mat(frame(frame_ind(i-1)).colonyProps(nextColonyInd).cellProps{daughtersInd(jj), 'pixelList'}))];
            end
            nextFrameCellCoordsforExisting(:, 1) = nextFrameCellCoordsforExisting(:, 1) - mean(nextFrameCellCoordsforExisting(:, 1));
            nextFrameCellCoordsforExisting(:, 2) = nextFrameCellCoordsforExisting(:, 2) - mean(nextFrameCellCoordsforExisting(:, 2));            
            [in, ~] = inpolygon(existingCellCoords(:, 1), existingCellCoords(:, 2),  nextFrameCellCoordsforExisting(:, 1),  nextFrameCellCoordsforExisting(:, 2));
            overlapBitmapWithExisting = sum(in);           


            if(sum(overlapBitmapWithExisting)/(length(existingCellCoords)+length(nextFrameCellCoordsforExisting)-sum(overlapBitmapWithExisting)) < sum(overlapBitmapWithNew)/(length(newCellCoords)+length(nextFrameCellCoordsforNew)-sum(overlapBitmapWithNew)))
                overlapScore(ii) = sum(overlapBitmapWithNew)/(length(newCellCoords)+length(nextFrameCellCoordsforNew)-sum(overlapBitmapWithNew));
            end

        end
    end
    
    [maxOverlapScore, ii] = max(overlapScore);
    if(isempty(overlapScore))
        maxOverlapScore = 0;
    end
    if(maxOverlapScore == 0)
        
        %ask user again...
        if sum(strcmp(mode, {'specifyCells', 'manual'})) && ~isempty(answers{1})
            %warning the cell you specified, failed to merge, ask again, if
            %it is a traking error leave it blank
            f = warndlg('The cell you specified failed to merge!','Warning');
            currColonyInd = colony_ind{i};
            continue;
        end
        
        %if the alorithm failed to merge in the first step, we assume that
        %it is wrong correction approach
        if i == 2
            return;
        end
        
        [frame, currLogs, currCorrectionList] = matchMother2DaughterCellInst(frame, frame_ind(i-1), colony_ind(i-1), {cell_ids{i}}, configuration);
        correctionList = [correctionList; currCorrectionList];
        logs = [ logs; currLogs];
        
        if size(currCorrectionList, 1) == 1
            if isnan(currCorrectionList(1, 3))
                return;
            end
        end
        if(i == 2)
            firstNewCellId = currCellId;
        end
    else
        frame(frame_ind(i)).colonyProps(currColonyInd) = currColonyProps{ii};
        logs{i-1, 1} = sprintf('%s, colony %d: cell %d and cell %d were merged to cell %s!\n', frame(frame_ind(i)).id, currColonyInd, currCellInd, candidateCellsForMergingIndices(ii), newCellId{ii});
           
        %add a column to correspondanceMatrix of current frame (i.e. current frame's cells).
        
        insertInd = size(frame(frame_ind(i)).colonyProps(currColonyInd).correspondanceMatrix, 2)+1;
        
        % add a column to correspondanceMatrix of current frame.
        % We must estimate again the overlap score with the mother cell
        % instant. However beacuse it will be useless, we arbritraly assign the
        % optimal score value, i.e. 1.
        frame(frame_ind(i)).colonyProps(currColonyInd).correspondanceMatrix{:, insertInd} = NaN;
        
        motherCellInstantlogicIndex=frame(frame_ind(i)).colonyProps(currColonyInd).correspondanceMatrix{:, currColCellIds{candidateCellsForMergingIndices(ii)}} > 0;
        
        frame(frame_ind(i)).colonyProps(currColonyInd).correspondanceMatrix{motherCellInstantlogicIndex, insertInd} = 1;
        
        frame(frame_ind(i)).colonyProps(currColonyInd).correspondanceMatrix.Properties.VariableNames{insertInd} = newCellId{ii};
        
        %remove the columns from correspondanceMatrix of previous frame (i.e. current frame's cells).
        frame(frame_ind(i)).colonyProps(currColonyInd).correspondanceMatrix(:, currCellId) = [];
        frame(frame_ind(i)).colonyProps(currColonyInd).correspondanceMatrix(:,  currColCellIds{candidateCellsForMergingIndices(ii)}) = [];
        
        % add a row (i.e. previous frame's cell instant) to correspondanceMatrix of next frame.
        % We must estimate again the overlap score with the daughter cell
        % instant. How ever beacuse it will be useless we arbritraly assign the
        % optimal score value, i.e. 1.
        % We must be carefull because if the cell instant have two daughter cell
        % instants, then the optimal score value is 0.5 for each one.
        
        frame(frame_ind(i-1)).colonyProps(nextColonyInd).correspondanceMatrix{newCellId{ii}, :} = NaN;
        
        daughterCellInstantslogicIndex = frame(frame_ind(i-1)).colonyProps(nextColonyInd).correspondanceMatrix{currColCellIds{candidateCellsForMergingIndices(ii)}, :} > 0;
        
        frame(frame_ind(i-1)).colonyProps(nextColonyInd).correspondanceMatrix{newCellId{ii}, daughterCellInstantslogicIndex} = 1/sum(daughterCellInstantslogicIndex);
        
        %remove the rows from correspondanceMatrix of next frame.
        frame(frame_ind(i-1)).colonyProps(nextColonyInd).correspondanceMatrix(currCellId, :) = [];
        frame(frame_ind(i-1)).colonyProps(nextColonyInd).correspondanceMatrix(currColCellIds{candidateCellsForMergingIndices(ii)}, :) = [];
        
        %at the father cell of the newcell so as to change in
        %the frame structure its daughterIds variable.
        %     prevCellId = lineageTree.get(prevPtr);
        %     prevColonyInd = getColonyIndex(frame(frame_ind(i)-1), prevCellId, frame(frame_ind(i)).colonyProps(currColonyInd).correspondingColPrevFrameInd);
        %     prevCellInstantChildrenPtr = lineageTree.getchildren(lineageTree.getparent(cellToBeMergedWithPtr));
        %     frame(frame_ind(i)-1).colonyProps(prevColonyInd).cellProps{prevCellId, 'daughterIds'} = cell(length(prevCellInstantChildrenPtr), 1);
        %     for ii = 1:length(prevCellInstantChildrenPtr);
        %         frame(frame_ind(i)-1).colonyProps(prevColonyInd).cellProps{prevCellId, 'daughterIds'}{ii, 1} = lineageTree.get(prevCellInstantChildrenPtr(ii));
        %     end
        
        
        if(length(frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).correspondingColPrevFrameInd) == 1)
            frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps = frame(frame_ind(i)).colonyProps(currColonyInd).cellProps;
            frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.adjacencyMatrix = frame(frame_ind(i)).colonyProps(currColonyInd).adjacencyMatrix;
        else
            cellcounter = 1;
            for k = 1:length(frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).correspondingColPrevFrameInd)
                
                numOfCells = height(frame(frame_ind(i)).colonyProps(frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).correspondingColPrevFrameInd(k)).cellProps);
                if(k == 1)
                    frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.adjacencyMatrix = frame(frame_ind(i)).colonyProps(frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).correspondingColPrevFrameInd(k)).adjacencyMatrix;
                    frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps = [frame(frame_ind(i)).colonyProps(frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).correspondingColPrevFrameInd(k)).cellProps table(num2cell(frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).correspondingColPrevFrameInd(k)*ones(numOfCells, 1)), 'VariableNames', {'colonyId'})];
                else
                    frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.adjacencyMatrix = blkdiag(frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.adjacencyMatrix, frame(frame_ind(i)).colonyProps(frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).correspondingColPrevFrameInd(k)).adjacencyMatrix);
                    frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps = [frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps; [frame(frame_ind(i)).colonyProps(frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).correspondingColPrevFrameInd(k)).cellProps table(num2cell(frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).correspondingColPrevFrameInd(k)*ones(numOfCells, 1)), 'VariableNames', {'colonyId'})]];
                end
                for m = 1:numOfCells
                    frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'pixelList'}{1}(1:end, 1) = frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'pixelList'}{1}(1:end, 1) + frame(frame_ind(i)).colonyProps(frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).correspondingColPrevFrameInd(k)).bBoxULCorner(1, 1);
                    frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'pixelList'}{1}(1:end, 2) = frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'pixelList'}{1}(1:end, 2) + frame(frame_ind(i)).colonyProps(frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).correspondingColPrevFrameInd(k)).bBoxULCorner(1, 2);
                    frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'pixelList'}{1}(1:end, 1) = frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'pixelList'}{1}(1:end, 1) - frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.bBoxULCorner(1, 1);
                    frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'pixelList'}{1}(1:end, 2) = frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'pixelList'}{1}(1:end, 2) - frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.bBoxULCorner(1, 2);
                    
                    frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'centroid'}(1, 1) = frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'centroid'}(1, 1) + frame(frame_ind(i)).colonyProps(frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).correspondingColPrevFrameInd(k)).bBoxULCorner(1, 1) - 1;
                    frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'centroid'}(1, 2) = frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'centroid'}(1, 2) + frame(frame_ind(i)).colonyProps(frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).correspondingColPrevFrameInd(k)).bBoxULCorner(1, 2) - 1;
                    frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'centroid'}(1, 1) = frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'centroid'}(1, 1) - frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.bBoxULCorner(1, 1) + 1;
                    frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'centroid'}(1, 2) = frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'centroid'}(1, 2) - frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.bBoxULCorner(1, 2) + 1;
                    
                    frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'centralPixel'}(1, 1) = frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'centralPixel'}(1, 1) + frame(frame_ind(i)).colonyProps(frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).correspondingColPrevFrameInd(k)).bBoxULCorner(1, 1) - 1;
                    frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'centralPixel'}(1, 2) = frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'centralPixel'}(1, 2) + frame(frame_ind(i)).colonyProps(frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).correspondingColPrevFrameInd(k)).bBoxULCorner(1, 2) - 1;
                    frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'centralPixel'}(1, 1) = frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'centralPixel'}(1, 1) - frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.bBoxULCorner(1, 1) + 1;
                    frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'centralPixel'}(1, 2) = frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'centralPixel'}(1, 2) - frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.bBoxULCorner(1, 2) + 1;
                    
                    frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 1) = frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 1) + frame(frame_ind(i)).colonyProps(frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).correspondingColPrevFrameInd(k)).bBoxULCorner(1, 1);
                    frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 2) = frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 2) + frame(frame_ind(i)).colonyProps(frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).correspondingColPrevFrameInd(k)).bBoxULCorner(1, 2);
                    frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 1) = frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 1) - frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.bBoxULCorner(1, 1);
                    frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 2) = frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.cellProps{cellcounter, 'boundaryPixelList'}{1}(1:end, 2) - frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).prevColonyProps.bBoxULCorner(1, 2);
                    cellcounter = cellcounter + 1;
                end
            end
        end
        frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).matchedCellsPrevColony(currCellId, :) = [];
        frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).matchedCellsPrevColony{newCellId{ii}, 1} = frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).matchedCellsPrevColony{currColCellIds{candidateCellsForMergingIndices(ii)}, 1};
        frame(frame_ind(i-1)).colonyProps(colony_ind(i-1)).matchedCellsPrevColony(currColCellIds{candidateCellsForMergingIndices(ii)}, :) = [];
        if(i == 2)
            firstNewCellId = newCellId{ii};
        end
        correctionList = [correctionList; [frame_ind(i) currColonyInd 1]];

    end
    
    i = i + 1;
end


end
