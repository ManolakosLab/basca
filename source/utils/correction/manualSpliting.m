function  [colonyProps, newCellIds] = manualSpliting(colonyProps, cellToBeSplitInd, numOfCellsToBeMatched, verb)

[x, y] = size(colonyProps.bwColony);
bwObject = createBW(x, y, colonyProps.cellProps{cellToBeSplitInd, 'pixelList'}{1});
 bwObject = imfillborder(bwObject);     

% bwObject = colonyProps.cellProps{cellToBeSplitInd, 'bwObject'}{1};
% [x, y] = size(colonyProps.cellProps{cellToBeSplitInd, 'bwObject'}{1});
%Use chessboard distance transform to get the object radius.
bwObjectWithPad = padarray(bwObject,[1 1]);
chessBoardDT = bwdist(~bwObjectWithPad, 'chessboard');
chessBoardDT = chessBoardDT(2:end-1,2:end-1);
%assuming that the radius of the object is the maximum distance from the boundary.
objectRadius = max(max(chessBoardDT));
%getting the centerline of the object
[bwObjectCL, endpoints] = objectCenterline(bwObject);
objectCLlen = length(find(bwObjectCL));
%if the object is non-complex, get the object's centerline to
%check for bowties
objectCLVerticesWithoutPoles = bwtraceboundary(bwObjectCL, [endpoints(2, 1) endpoints(1, 1)], 'N', 8, objectCLlen);
[verticesFromPoleA, ~, verticesToPoleB, ~] = getObjectPoles(bwObject, objectCLVerticesWithoutPoles, 'euclidean', 1);
objectCLVertices = [verticesFromPoleA; verticesToPoleB; objectCLVerticesWithoutPoles];
objectCLVertices = [objectCLVertices(:, 2) objectCLVertices(:, 1)];

%a trick to order the vertices of the extended centerline.
bwObjectCL = createBW(x, y, objectCLVertices);
[~, endpoints, ~] = anaskel(bwObjectCL);
orderedObjectCLVertices = bwtraceboundary(bwObjectCL, [endpoints(2, 1) endpoints(1, 1)], 'N', 8, length(objectCLVertices));

[distanceToBoundaryInPixels, ~, ~, ~] = meausureDistToBoundary2(bwObject, orderedObjectCLVertices, 'chessboard');

windowSize = 2*max(medfilt1(distanceToBoundaryInPixels, 2*double(objectRadius)+1))+1;

[~, orientationAngles, ~, ~] = meausureDistToBoundary2(bwObject, orderedObjectCLVertices, 'euclidean');
figure; imagesc(bwObject);
figure; imagesc(bwObjectCL);
warndlg({sprintf('Choose %d pixels!', numOfCellsToBeMatched-1 ); 'Pressing OK to proceed'}, 'Help')

row = [];
column = [];
while(1)
   pixelsNum = 0;
   [row, column, ~] = impixel;
   
   for i = 1:length(row)
        pixelsNum = pixelsNum + bwObjectCL(column(i), row(i));
   end
   if(pixelsNum == numOfCellsToBeMatched-1)
        break;
   else
       warndlg({sprintf('You must choose %d pixels!', numOfCellsToBeMatched-1 ); 'Pressing OK to proceed'}, 'Error!')
   end
end
close;

splitCoordinates = [column, row];
splitMask = cell(size(splitCoordinates, 1), 1);
for j = 1:size(splitCoordinates, 1)
    se = strel('line', windowSize+2, orientationAngles(ismember(orderedObjectCLVertices, splitCoordinates(j, :), 'rows')));
    nhood = getnhood(se);
    splitMask{j} = nhood;
end
[W, newSegments] = divideObject(bwObject, splitCoordinates, splitMask);
figure; imagesc(W); colormap jet;

for ii = 1:length(newSegments);
    [bwSegment] = createBW(x, y, newSegments{ii});
    thisSegmentProps = regionprops(bwSegment, 'MinorAxisLength', 'MajorAxisLength', 'orientation', 'solidity');
    
    [bwSegmentCL, endpoints] = objectCenterline(bwSegment);
    segmentCLlen = length(find(bwSegmentCL));
    
    %Use chessboard distance transform to get the segment radius.
    bwSegmentWithPad = padarray(bwSegment,[1 1]);
    chessBoardDT = bwdist(~bwSegmentWithPad, 'chessboard');
    chessBoardDT = chessBoardDT(2:end-1,2:end-1);
    
    %assuming that the radius of the segment is the maximum distance from the boundary.
    segmentRadius = max(max(chessBoardDT));
    if(segmentCLlen <= segmentRadius)
        segmentWidthInPixels = 2*double(segmentRadius)+1;
        segmentWidth = thisSegmentProps.MinorAxisLength;
        segmentLengthInPixels = segmentCLlen;
        segmentLength = thisSegmentProps.MajorAxisLength;
    else
        
        segmentCLVerticesWithoutPoles = bwtraceboundary(bwSegmentCL, [endpoints(2, 1) endpoints(1, 1)], 'N', 8, segmentCLlen);
        %[~, verticesFromPoleA, ~, verticesToPoleB, ~] = measureBacLength(bwsegment, segmentCLVerticesWithoutPoles, double(segmentRadius), 'euclidean', 1);
        [verticesFromPoleA, ~, verticesToPoleB, ~] = getObjectPoles(bwSegment, segmentCLVerticesWithoutPoles, 'euclidean', verb);
        vertices = [verticesFromPoleA; verticesToPoleB; segmentCLVerticesWithoutPoles];
        %if the segment is non-complex, get the segment's centerline to
        %check for bowties
        vertices = [vertices(:, 2) vertices(:, 1)];
        bwSegmentCL = createBW(x, y, vertices);
        [~, endpoints, ~] = anaskel(bwSegmentCL);
        segmentCLVertices = bwtraceboundary(bwSegmentCL, [endpoints(2, 1) endpoints(1, 1)], 'N', 8,length(vertices));
        
        [distanceToBoundaryInPixels, ~, ~, ~] = meausureDistToBoundary2(bwSegment, segmentCLVertices, 'chessboard');
        [distanceToBoundary, ~, ~, ~] = meausureDistToBoundary2(bwSegment, segmentCLVertices, 'euclidean');
        segmentLengthInPixels = length(distanceToBoundary);
        [segmentSimplifiedCLVertices, ~] = dpsimplify(segmentCLVertices , 1.4142);
        
        [segmentLength, ~] = arclength(segmentSimplifiedCLVertices(:, 2), segmentSimplifiedCLVertices(:, 1), 'linear');
        
        segmentWidthInPixels = max(medfilt1(distanceToBoundaryInPixels, 2*double(floor(segmentRadius))+1));
        segmentWidth = max(medfilt1(distanceToBoundary, ceil(segmentWidthInPixels/2)));
    end
    cellProps(ii).pixelList = uint16(newSegments{ii});
%     cellProps(ii).bwObject = bwSegment;
    cellProps(ii).centroid = mean(newSegments{ii});
    cellProps(ii).width = segmentWidth;
    cellProps(ii).widthInPixels = segmentWidthInPixels;
    cellProps(ii).lengthInPixels = segmentLengthInPixels;
    cellProps(ii).length = segmentLength;
    bwsegmentPerim = bwperim(bwSegment);
    
    cellProps(ii).minorAxis = thisSegmentProps.MinorAxisLength;
    cellProps(ii).majorAxis = thisSegmentProps.MajorAxisLength;
    cellProps(ii).eccentricity = thisSegmentProps.MinorAxisLength/thisSegmentProps.MajorAxisLength;
    cellProps(ii).orientation = thisSegmentProps.Orientation;
    cellProps(ii).solidity = thisSegmentProps.Solidity;
    if(cellProps(ii).orientation < 0)
        cellProps(ii).orientation = 360+cellProps(ii).orientation;
    end
    cellProps(ii).covariance = cov(newSegments{ii});
    b = bwboundaries(bwsegmentPerim);
    cellProps(ii).boundaryPixelList = uint16([b{1}(:, 2) b{1}(:, 1)]); 
end

colonyProps.cellProps(cellToBeSplitInd, :) = [];
newCellIds = {};
for k = 1:length(cellProps)
    newCellId = sprintf('x%07d_y%07d', ...
        round((cellProps(k).centroid(1, 1)+colonyProps.bBoxULCorner(1, 1))*1000) , ...
        round((cellProps(k).centroid(1, 2)+colonyProps.bBoxULCorner(1, 2))*1000));
    
    
    newCellIds = [newCellIds ; newCellId];
    
    colonyProps.cellProps(newCellId, :) = table(...
        {cellProps(k).centroid}, {cellProps(k).pixelList}, {cellProps(k).width}, ...
        {cellProps(k).widthInPixels}, {cellProps(k).length}, {cellProps(k).lengthInPixels}, {cellProps(k).minorAxis}, ...
        {cellProps(k).majorAxis}, {cellProps(k).eccentricity}, {cellProps(k).orientation}, {cellProps(k).solidity}, ...
        {cellProps(k).covariance}, {cellProps(k).boundaryPixelList}, 'VariableNames', {'centroid', 'pixelList', ...
        'width', 'widthInPixels', 'length', 'lengthInPixels', 'minorAxis', 'majorAxis', 'eccentricity', ...
        'orientation', 'solidity', 'covariance', 'boundaryPixelList'});
end
end
