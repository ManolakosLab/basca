function [cellAttributeValues] = getCellAttrValues(generation, groupBy, generation_indices, colony_indices, attribute, attributeStat, separateCells)

if ~strcmp('both',groupBy)
    if strcmp('pop',groupBy) && separateCells == 0
        cellAttributeValues = cell(1, 1, 1);
        for i = 1:length(generation_indices)
            for j = 1:length(generation(generation_indices(i)).colonyProps)
                numOfCells = length(generation(generation_indices(i)).colonyProps(j).cellAttributes);
                for k = 1:numOfCells
                    ind = ismember(colony_indices, generation(generation_indices(i)).colonyProps(j).cellAttributes(k).colony_ind);
                    if sum(ind) > 0
                        cellAttributeValues{1, 1, 1} = [cellAttributeValues{1, 1, 1};generation(generation_indices(i)).colonyProps(j).cellAttributes(k).(createCellAttribute(attributeStat, attribute))];
                    end
                end
            end
        end
    elseif strcmp('pop',groupBy) && separateCells == 1
        cellAttributeValues = cell(1, 1, 2);
        for i = 1:length(generation_indices)
            for j = 1:length(generation(generation_indices(i)).colonyProps)
                numOfCells = length(generation(generation_indices(i)).colonyProps(j).cellAttributes);
                for k = 1:numOfCells
                    ind = ismember(colony_indices, generation(generation_indices(i)).colonyProps(j).cellAttributes(k).colony_ind);
                    if sum(ind) > 0
                        if (generation(generation_indices(i)).colonyProps(j).cellAttributes(k).isInsideColony == 0)
                            cellAttributeValues{1, 1, 1} = [cellAttributeValues{1, 1, 1};generation(generation_indices(i)).colonyProps(j).cellAttributes(k).(createCellAttribute(attributeStat, attribute))];
                        else
                            cellAttributeValues{1, 1, 2} = [cellAttributeValues{1, 1, 2};generation(generation_indices(i)).colonyProps(j).cellAttributes(k).(createCellAttribute(attributeStat, attribute))];
                        end
                    end
                end
            end
        end
    elseif ~strcmp('pop',groupBy) && separateCells == 0
        if strcmp(groupBy, 'col')
            cellAttributeValues = cell(length(colony_indices), 1, 1);
        else
            cellAttributeValues = cell(length(generation_indices), 1, 1);
        end
        for i = 1:length(generation_indices)
            for j = 1:length(generation(generation_indices(i)).colonyProps)
                for k = 1:length(generation(generation_indices(i)).colonyProps(j).cellAttributes)
                    ind = ismember(colony_indices, generation(generation_indices(i)).colonyProps(j).cellAttributes(k).colony_ind);
                    if sum(ind) > 0
                        if strcmp(groupBy, 'col')
                            cellAttributeValues{ind,1, 1} = [cellAttributeValues{ind, 1, 1}; generation(generation_indices(i)).colonyProps(j).cellAttributes(k).(createCellAttribute(attributeStat, attribute))];
                        else
                            cellAttributeValues{i, 1, 1} = [cellAttributeValues{i, 1, 1}; generation(generation_indices(i)).colonyProps(j).cellAttributes(k).(createCellAttribute(attributeStat, attribute))];
                        end
                    end
                end
            end
        end
    elseif(~strcmp('pop',groupBy) && separateCells == 1)
        if strcmp(groupBy, 'col')
            cellAttributeValues = cell(length(colony_indices), 1, 2);
        else
            cellAttributeValues = cell(length(generation_indices), 1, 2);
        end
        for i = 1:length(generation_indices)
            for j = 1:length(generation(generation_indices(i)).colonyProps)
                for k = 1:length(generation(generation_indices(i)).colonyProps(j).cellAttributes)
                    ind = ismember(colony_indices, generation(generation_indices(i)).colonyProps(j).cellAttributes(k).colony_ind);
                    if sum(ind) > 0
                        if (generation(generation_indices(i)).colonyProps(j).cellAttributes(k).isInsideColony == 0)
                            if strcmp(groupBy, 'col')
                                cellAttributeValues{ind, 1, 1} = [cellAttributeValues{ind, 1, 1}; generation(generation_indices(i)).colonyProps(j).cellAttributes(k).(createCellAttribute(attributeStat, attribute))];
                            else
                                cellAttributeValues{i, 1, 1} = [cellAttributeValues{i, 1, 1}; generation(generation_indices(i)).colonyProps(j).cellAttributes(k).(createCellAttribute(attributeStat, attribute))];
                            end
                        else
                            if strcmp(groupBy, 'col')
                                cellAttributeValues{ind, 1, 2} = [cellAttributeValues{ind, 1, 2}; generation(generation_indices(i)).colonyProps(j).cellAttributes(k).(createCellAttribute(attributeStat, attribute))];
                            else
                                cellAttributeValues{i, 1, 2} = [cellAttributeValues{i, 1, 2}; generation(generation_indices(i)).colonyProps(j).cellAttributes(k).(createCellAttribute(attributeStat, attribute))];
                            end
                        end
                    end
                end
            end
        end
    end
else
    if separateCells == 0
        cellAttributeValues = cell(length(generation_indices), length(colony_indices), 1);
        for i = 1:length(generation_indices)
            for j = 1:length(generation(generation_indices(i)).colonyProps)
                numOfCells = length(generation(generation_indices(i)).colonyProps(j).cellAttributes);
                for k = 1:numOfCells
                    ind = ismember(colony_indices, generation(generation_indices(i)).colonyProps(j).cellAttributes(k).colony_ind);
                    if sum(ind) > 0
                        cellAttributeValues{i, ind, 1} = [cellAttributeValues{i, ind, 1};generation(generation_indices(i)).colonyProps(j).cellAttributes(k).(createCellAttribute(attributeStat, attribute))];
                    end
                end
            end
        end
    else
        cellAttributeValues = cell(length(generation_indices), length(colony_indices), 2);
        for i = 1:length(generation_indices)
            for j = 1:length(generation(generation_indices(i)).colonyProps)
                numOfCells = length(generation(generation_indices(i)).colonyProps(j).cellAttributes);
                for k = 1:numOfCells
                    ind = ismember(colony_indices, generation(generation_indices(i)).colonyProps(j).cellAttributes(k).colony_ind);
                    if sum(ind) > 0
                        if (generation(generation_indices(i)).colonyProps(j).cellAttributes(k).isInsideColony == 0)                           
                            cellAttributeValues{i, ind, 1} = [cellAttributeValues{i, ind, 1};generation(generation_indices(i)).colonyProps(j).cellAttributes(k).(createCellAttribute(attributeStat, attribute))];
                        else
                            cellAttributeValues{i, ind, 2} = [cellAttributeValues{i, ind, 2};generation(generation_indices(i)).colonyProps(j).cellAttributes(k).(createCellAttribute(attributeStat, attribute))];                           
                        end
                    end
                end
            end
        end
    end
end

