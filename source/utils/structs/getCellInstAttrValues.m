function [cellInstantAttributeValues] = getCellInstAttrValues(frame,  groupBy, frame_indices, colony_indices, attribute, separateCells)
if ~strcmp('both', groupBy)
    if strcmp('pop',groupBy) && separateCells == 0
        cellInstantAttributeValues = cell(1, 1, 1);
        for i = 1:length(frame_indices)
            for j = 1:length(frame(frame_indices(i)).colonyProps)
                for k = 1:height(frame(frame_indices(i)).colonyProps(j).cellProps)
                    ind = ismember(colony_indices, frame(frame_indices(i)).colonyProps(j).cellProps{k, 'colony_ind'});
                    if sum(ind) > 0
                        cellInstantAttributeValues{1,1, 1} = [cellInstantAttributeValues{1, 1, 1}; frame(frame_indices(i)).colonyProps(j).cellProps{k, sprintf('%s',attribute)}];
                    end
                end
            end
        end
    elseif strcmp('pop',groupBy) && separateCells == 1
        cellInstantAttributeValues = cell(1, 1,  2);
        for i = 1:length(frame_indices)
            for j = 1:length(frame(frame_indices(i)).colonyProps)
                for k = 1:height(frame(frame_indices(i)).colonyProps(j).cellProps)
                    ind = ismember(colony_indices, frame(frame_indices(i)).colonyProps(j).cellProps{k, 'colony_ind'});
                    if sum(ind) > 0
                        if (frame(frame_indices(i)).colonyProps(j).cellProps{k, 'isInsideColony'} == 0)
                            cellInstantAttributeValues{1, 1, 1} = [cellInstantAttributeValues{1, 1}; frame(frame_indices(i)).colonyProps(j).cellProps{k, sprintf('%s',attribute)}];
                        else
                            cellInstantAttributeValues{1, 1, 2} = [cellInstantAttributeValues{1, 2}; frame(frame_indices(i)).colonyProps(j).cellProps{k, sprintf('%s',attribute)}];
                        end
                    end
                end
            end
        end
    elseif ~strcmp('pop',groupBy) && separateCells == 0
        if strcmp(groupBy, 'col')
            cellInstantAttributeValues = cell(length(colony_indices), 1, 1);
        else
            cellInstantAttributeValues = cell(length(frame_indices), 1, 1);
        end
        for i = 1:length(frame_indices)
            for j = 1:length(frame(frame_indices(i)).colonyProps)
                for k = 1:height(frame(frame_indices(i)).colonyProps(j).cellProps)
                    ind = ismember(colony_indices, frame(frame_indices(i)).colonyProps(j).cellProps{k, 'colony_ind'});
                    if sum(ind) > 0
                        if strcmp(groupBy, 'col')
                            cellInstantAttributeValues{ind,1, 1} = [cellInstantAttributeValues{ind, 1, 1}; frame(frame_indices(i)).colonyProps(j).cellProps{k, sprintf('%s',attribute)}];
                        else
                            cellInstantAttributeValues{i, 1, 1} = [cellInstantAttributeValues{i, 1, 1}; frame(frame_indices(i)).colonyProps(j).cellProps{k, sprintf('%s',attribute)}];
                        end
                    end
                end
            end
        end
    elseif~strcmp('pop',groupBy) && separateCells == 1
        if strcmp(groupBy, 'col')
            cellInstantAttributeValues = cell(length(colony_indices), 1, 2);
        else
            cellInstantAttributeValues = cell(length(frame_indices), 1, 2);
        end
        for i = 1:length(frame_indices)
            for j = 1:length(frame(frame_indices(i)).colonyProps)
                for k = 1:height(frame(frame_indices(i)).colonyProps(j).cellProps)
                    ind = ismember(colony_indices, frame(frame_indices(i)).colonyProps(j).cellProps{k, 'colony_ind'});
                    if sum(ind) > 0
                        if (frame(frame_indices(i)).colonyProps(j).cellProps{k, 'isInsideColony'} == 0)   
                            if strcmp(groupBy, 'col')
                                cellInstantAttributeValues{ind,1, 1} = [cellInstantAttributeValues{ind,1, 1}; frame(frame_indices(i)).colonyProps(j).cellProps{k, sprintf('%s',attribute)}];
                            else
                                cellInstantAttributeValues{i,1, 1} = [cellInstantAttributeValues{i,1, 1}; frame(frame_indices(i)).colonyProps(j).cellProps{k, sprintf('%s',attribute)}];
                            end    
                        else
                            if strcmp(groupBy, 'col')
                                cellInstantAttributeValues{ind,1, 2} = [cellInstantAttributeValues{ind,1, 2}; frame(frame_indices(i)).colonyProps(j).cellProps{k, sprintf('%s',attribute)}];
                            else
                                cellInstantAttributeValues{i,1, 2} = [cellInstantAttributeValues{i,1, 2}; frame(frame_indices(i)).colonyProps(j).cellProps{k, sprintf('%s',attribute)}];
                            end 
                        end
                    end
                end
            end
        end
    end
else
    if separateCells == 0
        cellInstantAttributeValues = cell(length(frame_indices), length(colony_indices), 1);
        for i = 1:length(frame_indices)
            for j = 1:length(frame(frame_indices(i)).colonyProps)
                for k = 1:height(frame(frame_indices(i)).colonyProps(j).cellProps)
                    ind = ismember(colony_indices, frame(frame_indices(i)).colonyProps(j).cellProps{k, 'colony_ind'});
                    if sum(ind) > 0
                        cellInstantAttributeValues{i, ind, 1} = [cellInstantAttributeValues{i, ind, 1}; frame(frame_indices(i)).colonyProps(j).cellProps{k, sprintf('%s',attribute)}];
                    end
                end
            end
        end
    elseif separateCells == 1
        cellInstantAttributeValues = cell(length(frame_indices), length(colony_indices), 2);
        for i = 1:length(frame_indices)
            for j = 1:length(frame(frame_indices(i)).colonyProps)
                for k = 1:height(frame(frame_indices(i)).colonyProps(j).cellProps)
                    ind = ismember(colony_indices, frame(frame_indices(i)).colonyProps(j).cellProps{k, 'colony_ind'});
                    if sum(ind) > 0
                        if (frame(frame_indices(i)).colonyProps(j).cellProps{k, 'isInsideColony'} == 0)
                            cellInstantAttributeValues{i, ind, 1} = [cellInstantAttributeValues{i, ind, 1}; frame(frame_indices(i)).colonyProps(j).cellProps{k, sprintf('%s',attribute)}];
                        else
                            cellInstantAttributeValues{i, ind, 2} = [cellInstantAttributeValues{i, ind, 2}; frame(frame_indices(i)).colonyProps(j).cellProps{k, sprintf('%s',attribute)}];
                        end
                    end
                end
            end
        end
    end
end