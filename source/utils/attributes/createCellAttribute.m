function cellAttribute = createCellAttribute(attributeStat, attribute)
    if isempty(attributeStat)
        cellAttribute = attribute;
    else
        cellAttribute = sprintf('%s_%s', attributeStat, attribute);
    end
end
