function cellAttributes = getCellAttributes(whichCellAttributes)

if strcmp(whichCellAttributes, 'lifeAttrs')
    cellAttributes = getLifeAttributes();
elseif strcmp(whichCellAttributes, 'statAttrs')
    attributeStatList = getAttributeStats();
    cellAttributes = {};
    cellInstantAttributes = getCellInstAttributes('all');
    for i = 1:length(attributeStatList)
        for j = 1:length(cellInstantAttributes)
            cellAttributes = [cellAttributes; sprintf('%s_%s', attributeStatList{i}, cellInstantAttributes{j})];
        end
    end
else
    attributeStatList = getAttributeStats();
    
    cellAttributes = getLifeAttributes();
    cellInstantAttributes = getCellInstAttributes('all');
    for i = 1:length(attributeStatList)
        for j = 1:length(cellInstantAttributes)
            cellAttributes = [cellAttributes; sprintf('%s_%s', attributeStatList{i}, cellInstantAttributes{j})];
        end
    end
end
