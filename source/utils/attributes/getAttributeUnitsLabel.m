function [unitsLabel] = getAttributeUnitsLabel(units)
if isempty(units)
    unitsLabel = '';
else
    unitsLabel = sprintf(' (%s)', units);  
end