function [attributeStat, attribute, units, errorMessage] = checkCellAttribute(cellAttribute)
errorMessage =[];
if (length(cellAttribute)==3)
    attributeStat = lower(cellAttribute{1});
    attribute = lower(cellAttribute{2});
    units = cellAttribute{3};
 
    if(~ischar(attributeStat) || ~ischar(attribute) ||~ischar(units))
        errorMessage = sprintf('Error in <cellAttribute>: cellAttribute must contain only strings\n');
        attributeStat = []; 
        attribute = [];
        units =[];
        return;
    end
    attributeStats = getAttributeStats();
    attributeStatsInString = sprintf('''%s''', strjoin(attributeStats,''', '''));
    if(sum(strcmp(attributeStat, attributeStats)) == 0)
        errorMessage = sprintf('Error in <cellAttribute>: <attributeStat> can be {%s}\n', attributeStatsInString);
        attributeStat = []; 
        attribute = [];
        units =[];        
        return;
    end
    
    attributes = getCellInstAttributes('all');
    attributesInString = sprintf('''%s''', strjoin(attributes,''', \n'''));
    if(sum(strcmp(attributes, attribute))==0)
        errorMessage = sprintf('Error in <cellAttribute>: <attribute> can be {%s}\n', attributesInString);
        attributeStat = []; 
        attribute = [];
        units =[];        
        return;
    end
    
    cellAttributes = getCellAttributes('statAttrs');
    cellAttributesInString = sprintf('''%s''', strjoin(cellAttributes,''', \n'''));
    cellAttribute = createCellAttribute(attributeStat, attribute);
    if(sum(strcmp(cellAttributes, cellAttribute))==0)
        errorMessage = sprintf('Error in <cellAttribute>: <attributeStat>_<attribute> can be {%s}\n', cellAttributesInString);
        attributeStat = []; 
        attribute = [];
        units =[];        
        return;
    end
else
    attributeStat = '';
    attribute = lower(cellAttribute{1});
    units = cellAttribute{2};
    if(~ischar(attributeStat) || ~ischar(attribute) ||~ischar(units))
        errorMessage = sprintf('Error in <cellAttribute>: cellAttribute must contain only strings\n');
        attributeStat = []; 
        attribute = [];
        units =[];
        return;
    end    
    attributes = getCellAttributes('lifeAttrs');
    attributesInString = sprintf('''%s''', strjoin(attributes,''', \n'''));
    if(sum(strcmp(attributes, attribute))==0)
        attributeStat = []; 
        attribute = [];
        units =[];        
        errorMessage = sprintf('Error in <cellAttribute>: <attribute> can be {%s}\n', attributesInString);
        return;
    end    
end