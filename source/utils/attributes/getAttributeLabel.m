function attributeLabel = getAttributeLabel(attribute)
    %attributes
    if(strcmpi(attribute, 'length'))
        attributeLabel = 'Length';
    elseif(strcmpi(attribute, 'width'))
        attributeLabel = 'Width';
    elseif(strcmpi(attribute, 'area'))
        attributeLabel = 'Area';
    elseif(strcmpi(attribute, 'perimeter'))
        attributeLabel = 'Perimeter';
    elseif(strcmpi(attribute, 'length_in_pixels'))
        attributeLabel = 'Length';
    elseif(strcmpi(attribute, 'width_in_pixels'))
        attributeLabel = 'Width';
    elseif(strcmpi(attribute, 'area_in_pixels'))
        attributeLabel = 'Area';
    elseif(strcmpi(attribute, 'perimeter_in_pixels'))
        attributeLabel = 'Perimeter';        
    elseif(strcmpi(attribute, 'orientation'))
        attributeLabel = 'Orientation';
    elseif(strcmpi(attribute, 'distance_from_centroid'))
        attributeLabel = 'Distance';
    elseif(strcmpi(attribute, 'LW'))
        attributeLabel = 'Length-to-Width Ratio';        
    elseif(strcmpi(attribute, 'fluo_int_channel1'))
        attributeLabel = 'Av. Fluorescence (Channel 1)';
    elseif(strcmpi(attribute, 'fluo_int_channel2'))
        attributeLabel = 'Av. Fluorescence (Channel 2)';
    elseif(strcmpi(attribute, 'fluo_int_channel3'))
        attributeLabel = 'Av. Fluorescence (Channel 3)';
    elseif(strcmpi(attribute, 'fluo_cov_channel1'))
        attributeLabel = 'Fluorescence Cov (Channel 1)';
    elseif(strcmpi(attribute, 'fluo_cov_channel2'))
        attributeLabel = 'Fluorescence Cov (Channel 2)';
    elseif(strcmpi(attribute, 'fluo_cov_channel3'))
        attributeLabel = 'Fluorescence Cov (Channel 3)';        
    %RoC attributes
    elseif(strcmpi(attribute, 'length_roc'))
        attributeLabel = 'Length RoC';
    elseif(strcmpi(attribute, 'width_roc'))
        attributeLabel = 'Width RoC';
    elseif(strcmpi(attribute, 'area_roc'))
        attributeLabel = 'Area RoC';
    elseif(strcmpi(attribute, 'perimeter_roc'))
        attributeLabel = 'Perimeter RoC';
    elseif(strcmpi(attribute, 'orientation_roc'))
        attributeLabel = 'Orientation RoC';  
    elseif(strcmpi(attribute, 'distance_from_centroid_roc'))
        attributeLabel = 'Distance RoC';
    elseif(strcmpi(attribute, 'LW_roc'))
        attributeLabel = 'Length-to-Width Ratio RoC';         
    elseif(strcmpi(attribute, 'fluo_int_channel1_roc'))
        attributeLabel = 'Av. Fluorescence RoC (Channel 1)';
    elseif(strcmpi(attribute, 'fluo_int_channel2_roc'))
        attributeLabel = 'Av. Fluorescence RoC (Channel 2)';
    elseif(strcmpi(attribute, 'fluo_int_channel3_roc'))
        attributeLabel = 'Av. Fluorescence RoC (Channel 3)';
    elseif(strcmpi(attribute, 'fluo_cov_channel1_roc'))
        attributeLabel = 'Fluorescence Cov RoC (Channel 1)';
    elseif(strcmpi(attribute, 'fluo_cov_channel2_roc'))
        attributeLabel = 'Fluorescence Cov RoC (Channel 2)';
    elseif(strcmpi(attribute, 'fluo_cov_channel3_roc'))
        attributeLabel = 'Fluorescence Cov RoC (Channel 3)';         
    %life attributes
    elseif(strcmpi(attribute, 'division_time'))
        attributeLabel = 'Div. Time';
    elseif(strcmpi(attribute, 'elongation'))
        attributeLabel = 'Elongation';
    elseif(strcmpi(attribute, 'elongation_rate'))
        attributeLabel = 'Elongation Rate';
    elseif(strcmpi(attribute, 'velocity'))
        attributeLabel = 'Velocity';    
    elseif(strcmpi(attribute, 'colony_ind'))
        attributeLabel = 'Colony';
    elseif(strcmpi(attribute, 'generation_ind'))
        attributeLabel = 'Gen. Index';
    end
end