function attributeStatLabel = getAttributeStatLabel(attributeStat)
if isempty(attributeStat)
    attributeStatLabel = '';
elseif(strcmpi(attributeStat, 'min'))
    attributeStatLabel = 'Min ';
elseif(strcmpi(attributeStat, 'max'))
    attributeStatLabel = 'Max ';
elseif(strcmpi(attributeStat, 'mean'))
    attributeStatLabel = 'Mean ';
elseif(strcmpi(attributeStat, 'std'))
    attributeStatLabel = 'Std ';   
end