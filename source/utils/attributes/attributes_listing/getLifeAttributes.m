function cellLifeAttributes = getLifeAttributes()
    cellLifeAttributes = {'division_time';'elongation';'elongation_rate';'velocity';'colony_ind';'generation_ind'};
end