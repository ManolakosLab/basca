function cellInstantAttributes = getCellInstAttributes(whichCellInstAttributes)
if strcmp('all', whichCellInstAttributes)
cellInstantAttributes  = {
    'length'
    'width'
    'area'
    'perimeter'
    'length_in_pixels'
    'width_in_pixels'
    'area_in_pixels'
    'perimeter_in_pixels'    
    'orientation'
    'distance_from_centroid'
    'LW'
    'fluo_int_channel1'
    'fluo_int_channel2'
    'fluo_int_channel3'
    'fluo_cov_channel1'
    'fluo_cov_channel2'
    'fluo_int_channel3'
    'LW_roc'
    'length_roc'
    'width_roc'
    'area_roc'
    'perimeter_roc'
    'orientation_roc'
    'distance_from_centroid_roc'
    'fluo_int_channel1_roc'
    'fluo_int_channel2_roc'
    'fluo_int_channel3_roc'
    'fluo_cov_channel1_roc'
    'fluo_cov_channel2_roc'
    'fluo_cov_channel3_roc'     
    };
elseif strcmp('trajectories', whichCellInstAttributes)
    cellInstantAttributes  = {
    'length'
    'area'
    'distance_from_centroid'    
    };
end