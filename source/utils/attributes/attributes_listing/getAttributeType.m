function attributeType = getAttributeType(attribute)
    %attributes
    if(strcmpi(attribute, 'length'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'width'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'area'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'perimeter'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'length_in_pixels'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'width_in_pixels'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'area_in_pixels'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'perimeter_in_pixels'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'orientation'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'distance_from_centroid'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'LW'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'fluo_int_channel1'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'fluo_int_channel2'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'fluo_int_channel3'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'fluo_cov_channel1'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'fluo_cov_channel2'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'fluo_cov_channel3'))
        attributeType = 'numerical';
    %RoC attributes
    elseif(strcmpi(attribute, 'length_roc'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'width_roc'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'area_roc'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'perimeter_roc'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'orientation_roc'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'distance_from_centroid_roc'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'LW_roc'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'fluo_int_channel1_roc'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'fluo_int_channel2_roc'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'fluo_int_channel3_roc'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'fluo_cov_channel1_roc'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'fluo_cov_channel2_roc'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'fluo_cov_channel3_roc'))
        attributeType = 'numerical';
    %life attributes
    elseif(strcmpi(attribute, 'division_time'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'elongation'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'elongation_rate'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'velocity'))
        attributeType = 'numerical';
    elseif(strcmpi(attribute, 'colony_ind'))
        attributeType = 'categorical';
    elseif(strcmpi(attribute, 'generation_ind'))
        attributeType = 'categorical';
    end
end