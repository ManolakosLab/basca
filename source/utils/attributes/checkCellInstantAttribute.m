function [attribute, units, errorMessage] = checkCellInstantAttribute(cellInstantAttribute, whichCellInstAttributes)
errorMessage =[];
attribute = lower(cellInstantAttribute{1});
units = cellInstantAttribute{2};
if(~ischar(attribute) ||~ischar(units))
    errorMessage = sprintf('Error in <cellInstantAttribute>: cellInstantAttribute must contain only strings\n');
    attribute = [];
    units =[];
    return;
end
attributes = getCellInstAttributes(whichCellInstAttributes);
attributesInString = sprintf('''%s''', strjoin(attributes,''', \n'''));
if(sum(strcmp(attributes, attribute))==0)
    attribute = [];
    units =[];
    errorMessage = sprintf('Error in <cellInstantAttribute>: <attribute> can be {%s}\n', attributesInString);
    return;
end
end