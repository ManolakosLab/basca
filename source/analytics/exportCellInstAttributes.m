function exportCellInstAttributes(frame, cellInstAttributes, configuration, suffix)
% Export cell instant attributes per colony in xls files. 
% 
% exportCellInstAttributes(frame, cellInstAttributes, configuration, suffix)
%
% INPUT :
%  frame : a struct containing the segmentation output. 
%
%  cellInstAttributes : a cell array containing the cell instant attributes
%  to be exported in the xls, e.g.
% 
%  cellInstAttributes ={
%     'width'
%     'width_in_pixels'
%     'length'
%     'length_in_pixels'
%     'LW'
%     'orientation'
%     'area'
%     'area_in_pixels'
%     'perimeter'
%     'perimeter_in_pixels'
%     'distance_from_centroid'
%     'distance_from_centroid_in_pixels'
%     'isInsideColony'
%     'has_irregular_shape'
%     };
%
%  configuration : a struct containing the executation setup.
%
%  suffix : a string containing a suffix for the filename of each xls file
%
%  Copyright (C) 2017 Manolakos Lab
%  Written by Elias Manolakos.
%  University of Athens, 2017
%  This file is part of BaSCA.
% 
%  BaSCA is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  BaSCA is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with BaSCA.  If not, see <http://www.gnu.org/licenses/>.

for colony_ind = 1:length(frame(1).colonyProps)
    [nextColIndices] = getNextColonyIndices(frame, 1, frame(1).colonyProps(colony_ind).correspondingColNextFrameInd, []);
    output_path = strrep(configuration.output_mat_path, '.mat', sprintf('_Colony_%d%s.xls', colony_ind, suffix));
    frameids={};
    cellCounts = zeros(length(nextColIndices), 1);
    for i=1:length(nextColIndices)
        cellCounts(i, 1) = height(frame(nextColIndices{i, 1}).colonyProps(nextColIndices{i, 2}).cellProps);
        frameids = [frameids; strrep(frame(nextColIndices{i, 1}).id, ' ', '_')];
    end
    maxCellInstantNum = max(cellCounts);
    T_rownames = array2table(frameids, 'VariableNames', {'frame'}); 
    T = [T_rownames, array2table(cellCounts,'VariableNames', {'Cell_Instance_Count'})];
    %write the cell count
    writetable(T, output_path, 'Sheet', 1);  

    T_rownames = array2table(strsplit(num2str(1:maxCellInstantNum))', 'VariableNames', {'Cell_Instance'}); 
    for j = 1:length(cellInstAttributes)
        cellInstAttributeArray = zeros(length(nextColIndices), maxCellInstantNum);
        for i=1:length(nextColIndices)
            upto=length(frame(nextColIndices{i, 1}).colonyProps(nextColIndices{i, 2}).cellProps{:, cellInstAttributes{j}});
            cellInstAttributeArray(i, 1:upto) = frame(nextColIndices{i, 1}).colonyProps(nextColIndices{i, 2}).cellProps{:, cellInstAttributes{j}};
        end
        T = [T_rownames, array2table(cellInstAttributeArray','VariableNames', strcat( 'frame_', frameids))];
        if(length(cellInstAttributes{j}) > 31) 
           writetable(T, output_path, 'Sheet', cellInstAttributes{j}(1:31));
        else
           writetable(T, output_path, 'Sheet', cellInstAttributes{j});        
        end
    end
end